<?php

namespace App\AdminModule\NastaveniModule\ReklamaModule\Presenters;

use App\AdminModule\Presenters\BaseAdminPresenter;

final class EditacePresenter extends BaseAdminPresenter
{

    /*================================== nastavení aplikace ==================================*/
    public function startup()
    {
        parent::startup();

        $this->allowRole = ['admin'];
    }

    /*================================== render metody ==================================*/

    public function renderDefault()
    {

    }
}