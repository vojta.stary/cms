<?php

namespace App\AdminModule\NastaveniModule\ZakladniInformaceModule\Models;

use App\Managers\BaseInfoManager;

final class BaseInfoModel
{
    private $baseInfoManager;

    public function __construct(BaseInfoManager $baseInfoManager)
    {
        $this->baseInfoManager = $baseInfoManager;
    }

    /**
     * ukládá základní informace o stránkách
     * @param $values
     * @return void
     */
    public function saveBaseInfo($values): void
    {
        foreach ($values as $value => $data){
            $this->baseInfoManager->updateInfo($value, $data);
        }
    }
}