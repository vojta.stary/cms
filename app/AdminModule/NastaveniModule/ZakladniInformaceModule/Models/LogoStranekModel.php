<?php

namespace App\AdminModule\NastaveniModule\ZakladniInformaceModule\Models;

use App\Managers\BaseInfoManager;
use Nette\Utils\Strings;

final class LogoStranekModel
{
    const dir = __DIR__.'/../../../../../www/src/images/logo/';

    private $baseInfoManager;

    public function __construct(BaseInfoManager $baseInfoManager)
    {
        $this->baseInfoManager = $baseInfoManager;
    }

    /**
     * updatuje logo stránek
     * @param $values
     * @return bool
     */
    public function update($values): bool
    {
        $logo = $values->logo;

        if ($logo->hasFile()){
            $logoName = $logo->getName();

            $pripona = pathinfo($logoName);

            $logoName = Strings::webalize($pripona['filename']).'.'.$pripona['extension'];

            $logoFile = self::dir.$logoName;
            $logo->move($logoFile);

            $oldLogo = $this->baseInfoManager->getOneBaseInfo('logo');
            $oldLogoFile = self::dir.$oldLogo->value;
            if(is_file($oldLogoFile)){
                unlink($oldLogoFile);
            }

            $this->baseInfoManager->updateInfo('logo', $logoName);
            return true;
        }
        return false;
    }

    /**
     * maže logo stránek
     * @return void
     */
    public function delete(): void
    {
        $logo = $this->baseInfoManager->getOneBaseInfo('logo');
        $logoFile = self::dir.$logo->value;
        if(is_file($logoFile)){
            unlink($logoFile);
        }

        $this->baseInfoManager->updateInfo('logo',null);
    }
}