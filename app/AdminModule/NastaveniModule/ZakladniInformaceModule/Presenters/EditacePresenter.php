<?php

namespace App\AdminModule\NastaveniModule\ZakladniInformaceModule\Presenters;

use App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\EditAdresa\EditAdresaFactory;
use App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\EditLogoStranek\EditLogoStranekFactory;
use App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\EditNazevStranek\EditNazevStranekFactory;
use App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\EditSeo\EditSeoFactory;
use App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\VymazLogo\VymazLogoFactory;
use App\AdminModule\Presenters\BaseAdminPresenter;

final class EditacePresenter extends BaseAdminPresenter
{
    private $editNazevStrankeFactory;
    private $editLogoStranekFactory;
    private $editAdresaFactory;
    private $editSeoFactory;
    private $vymazLogoFactory;


    public function __construct(EditNazevStranekFactory $editNazevStranekFactory,
                                EditLogoStranekFactory  $editLogoStranekFactory,
                                EditAdresaFactory       $editAdresaFactory,
                                EditSeoFactory          $editSeoFactory,
                                VymazLogoFactory        $vymazLogoFactory)
    {
        $this->editNazevStrankeFactory = $editNazevStranekFactory;
        $this->editLogoStranekFactory = $editLogoStranekFactory;
        $this->editAdresaFactory = $editAdresaFactory;
        $this->editSeoFactory = $editSeoFactory;
        $this->vymazLogoFactory = $vymazLogoFactory;
    }

    /*================================== nastavení aplikace ==================================*/
    public function startup()
    {
        parent::startup();

        $this->allowRole = ['admin'];
    }

    /*================================== komponenty ==================================*/

    /**
     * editace názvu stránek
     * @return \App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\EditNazevStranek\EditNazevStranekControl
     */
    protected function createComponentNazevStranek()
    {
        return $this->editNazevStrankeFactory->create();
    }

    /**
     * editace loga stránek
     * @return \App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\EditLogoStranek\EditLogoStranekControl
     */
    protected function createComponentLogoStranek()
    {
        return $this->editLogoStranekFactory->create();
    }

    /**
     * vymazání loga stránek
     * @return \App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\VymazLogo\VymazLogoControl
     */
    protected function createComponentVymazLogo()
    {
        return $this->vymazLogoFactory->create();
    }

    /**
     * editace adresy
     * @return \App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\EditAdresa\EditAdresaControl
     */
    protected function createComponentAdresa()
    {
        return $this->editAdresaFactory->create();
    }

    /**
     * editace seo popisu
     * @return \App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\EditSeo\EditSeoControl
     */
    protected function createComponentEditSeo()
    {
        return $this->editSeoFactory->create();
    }
}