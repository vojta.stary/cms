<?php

namespace App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\EditLogoStranek;

use App\AdminModule\NastaveniModule\ZakladniInformaceModule\Models\LogoStranekModel;
use App\Managers\BaseInfoManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class EditLogoStranekControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $logoStranekModel;
    private $baseInfoManager;

    public function __construct(LogoStranekModel    $logoStranekModel,
                                BaseInfoManager     $baseInfoManager)
    {
        $this->logoStranekModel = $logoStranekModel;
        $this->baseInfoManager = $baseInfoManager;
    }

    public function render()
    {
        $logo = $this->baseInfoManager->getOneBaseInfo('logo');

        $this->template->logo = $logo->value;
        $this->template->render(self::template);
    }

    /*=================================== formulář ===================================*/
    /**
     * formulář editace loga
     * @return Form
     */
    protected function createComponentEditLogoForm(): Form
    {
        $form = new Form;
        $form->addUpload('logo')
            ->setRequired('Musíte vybrat obrázek loga.')
            ->addRule($form::IMAGE, 'Obrázek musí být JPEG, PNG, GIF nebo WebP.');
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendEditLogoForm'];
        return $form;
    }

    public function sendEditLogoForm(Form $form, $values)
    {
        try {
            $check = $this->logoStranekModel->update($values);

            if($check == true){
                $this->presenter->flashMessage('Logo bylo nahráno.','success');
                $this->redirect('this');
            }else{
                $this->presenter->flashMessage('Chyba při nahrávaní loga.','danger');
                $this->redirect('this');
            }
        }catch (\PDOException $e)
        {
            $this->presenter->flashMessage('Chyba zápisu do databáze.','danger');
            $this->redirect('this');
        }
    }
}