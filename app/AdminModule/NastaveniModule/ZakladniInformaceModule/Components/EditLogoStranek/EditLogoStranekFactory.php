<?php

namespace App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\EditLogoStranek;

use App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\VymazLogo\VymazLogoFactory;
use App\AdminModule\NastaveniModule\ZakladniInformaceModule\Models\LogoStranekModel;
use App\Managers\BaseInfoManager;

class EditLogoStranekFactory
{
    private $logoStranekModel;
    private $baseInfoManager;

    public function __construct(LogoStranekModel    $logoStranekModel,
                                BaseInfoManager     $baseInfoManager)
    {
        $this->logoStranekModel = $logoStranekModel;
        $this->baseInfoManager = $baseInfoManager;
    }

    public function create()
    {
        return new EditLogoStranekControl(  $this->logoStranekModel,
                                            $this->baseInfoManager);
    }
}