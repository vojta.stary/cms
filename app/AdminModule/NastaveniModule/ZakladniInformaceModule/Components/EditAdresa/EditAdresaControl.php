<?php

namespace App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\EditAdresa;

use App\AdminModule\NastaveniModule\ZakladniInformaceModule\Models\BaseInfoModel;
use App\Managers\BaseInfoManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class EditAdresaControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $baseInfoModel;
    private $baseInfoManager;

    public function __construct(BaseInfoModel   $baseInfoModel,
                                BaseInfoManager $baseInfoManager)
    {
        $this->baseInfoModel = $baseInfoModel;
        $this->baseInfoManager = $baseInfoManager;
    }

    public function render()
    {
        $baseInfo = $this->baseInfoManager->getBaseInfo();

        $this['editAdresaForm']->setDefaults($baseInfo);
        $this->template->render(self::template);
    }

    /*================================= formulář =================================*/
    protected function createComponentEditAdresaForm(): Form
    {
        $form = new Form;
        $form->addText('company')
            ->setRequired('Musíte vyplnit název!');
        $form->addText('compAddr')
            ->setRequired('Musíte vyplnit adresu!');
        $form->addText('compPost')
            ->addRule($form::PATTERN, 'Špatný formát Psč', '[0-9]{3} [0-9]{2}')
            ->setRequired('Musíte vyplnit psč!');
        $form->addText('compCity')
            ->setRequired('Musíte vyplnit město!');
        $form->addText('compIc');
        $form->addText('compDic');
        $form->addText('phone')
            ->addRule($form::PATTERN, 'Špatný formát Tel. čísla', '[0-9]{3} [0-9]{3} [0-9]{3}')
            ->setHtmlType('tel');
        $form->addText('email')
            ->setRequired('Musíte vyplnit emailovou adresu!');
        $form->addText('mapa');
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendEditAdresaForm'];
        return $form;
    }

    public function sendEditAdresaForm(Form $form, $values)
    {
        try {
            $this->baseInfoModel->saveBaseInfo($values);
            $this->presenter->flashMessage('Data byla uložena','success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Chyba zápisu do databáze','danger');
            $this->redirect('this');
        }
    }
}