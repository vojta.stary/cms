<?php

namespace App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\EditAdresa;

use App\AdminModule\NastaveniModule\ZakladniInformaceModule\Models\BaseInfoModel;
use App\Managers\BaseInfoManager;

class EditAdresaFactory
{
    private $baseInfoModel;
    private $baseInfoManager;

    public function __construct(BaseInfoModel   $baseInfoModel,
                                BaseInfoManager $baseInfoManager)
    {
        $this->baseInfoModel = $baseInfoModel;
        $this->baseInfoManager = $baseInfoManager;
    }

    public function create()
    {
        return new EditAdresaControl(   $this->baseInfoModel,
                                        $this->baseInfoManager);
    }
}