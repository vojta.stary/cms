<?php

namespace App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\EditSeo;

use App\AdminModule\NastaveniModule\ZakladniInformaceModule\Models\BaseInfoModel;
use App\Managers\BaseInfoManager;

class EditSeoFactory
{
    const template = __DIR__.'/templates/template.latte';

    private $baseInfoManager;
    private $baseInfoModel;

    public function __construct(BaseInfoManager $baseInfoManager,
                                BaseInfoModel   $baseInfoModel)
    {
        $this->baseInfoManager = $baseInfoManager;
        $this->baseInfoModel = $baseInfoModel;
    }

    public function create()
    {
        return new EditSeoControl(  $this->baseInfoManager,
                                    $this->baseInfoModel);
    }
}