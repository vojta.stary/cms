<?php

namespace App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\EditSeo;

use App\AdminModule\NastaveniModule\ZakladniInformaceModule\Models\BaseInfoModel;
use App\Managers\BaseInfoManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class EditSeoControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $baseInfoManager;
    private $baseInfoModel;

    public function __construct(BaseInfoManager $baseInfoManager,
                                BaseInfoModel   $baseInfoModel)
    {
        $this->baseInfoManager = $baseInfoManager;
        $this->baseInfoModel = $baseInfoModel;
    }

    public function render()
    {
        $data = $this->baseInfoManager->getOneBaseInfo('meta_desc');

        $formData = ['meta_desc' => $data->value];

        $this->template->baseInfo = $this->baseInfoManager->getBaseInfo();
        $this['editSeoForm']->setDefaults($formData);
        $this->template->render(self::template);
    }

    /*================================= formulář =================================*/
    protected function createComponentEditSeoForm(): Form
    {
        $form = new Form;
        $form->addTextArea('meta_desc', 'Seo popis');
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendEditSeoForm'];
        return $form;
    }

    public function sendEditSeoForm(Form $form, $values)
    {
        try{
            $this->baseInfoModel->saveBaseInfo($values);
            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Chyba zápisu dat do databáze', 'danger');
            $this->redirect('this');
        }
    }
}