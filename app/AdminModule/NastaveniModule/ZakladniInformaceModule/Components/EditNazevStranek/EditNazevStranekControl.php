<?php

namespace App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\EditNazevStranek;

use App\AdminModule\NastaveniModule\ZakladniInformaceModule\Models\BaseInfoModel;
use App\Managers\BaseInfoManager;
use Nette\Application\UI\Form;

class EditNazevStranekControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/templates/template.latte';

    private $baseInfoManager;
    private $baseInfoModel;

    public function __construct(BaseInfoManager $baseInfoManager,
                                BaseInfoModel   $baseInfoModel)
    {
        $this->baseInfoManager = $baseInfoManager;
        $this->baseInfoModel = $baseInfoModel;
    }

    public function render()
    {
        $siteName = $this->baseInfoManager->getOneBaseInfo('siteName');
        $address = $this->baseInfoManager->getOneBaseInfo('address');

        $formData = ['siteName' => $siteName->value, 'address' => $address->value];

        $this['editNazevStranekForm']->setDefaults($formData);
        $this->template->render(self::template);
    }

    /*================================= formulář =================================*/
    protected function createComponentEditNazevStranekForm(): Form
    {
        $form = new Form;
        $form->addText('siteName', 'Název stránek')
            ->setRequired('Musíte vyplnit název stránek!');
        $form->addText('address', 'URL adresa stránek')
            ->setRequired('Musíte vyplnit URL adresu stránek!');
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendEditNazevStranekForm'];
        return $form;
    }

    public function sendEditNazevStranekForm(Form $form, $values)
    {
        try{
            $this->baseInfoModel->saveBaseInfo($values);
            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Chyba zápisu dat do databáze', 'danger');
            $this->redirect('this');
        }
    }
}