<?php

namespace App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\EditNazevStranek;

use App\AdminModule\NastaveniModule\ZakladniInformaceModule\Models\BaseInfoModel;
use App\Managers\BaseInfoManager;

class EditNazevStranekFactory
{
    private $baseInfoManager;
    private $baseInfoModel;

    public function __construct(BaseInfoManager $baseInfoManager,
                                BaseInfoModel   $baseInfoModel)
    {
        $this->baseInfoManager = $baseInfoManager;
        $this->baseInfoModel = $baseInfoModel;
    }

    public function create()
    {
        return new EditNazevStranekControl( $this->baseInfoManager,
                                            $this->baseInfoModel);
    }
}