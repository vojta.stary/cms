<?php

namespace App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\VymazLogo;

use App\AdminModule\NastaveniModule\ZakladniInformaceModule\Models\LogoStranekModel;

class VymazLogoFactory
{
    private $logoStranekModel;

    public function __construct(LogoStranekModel $logoStranekModel)
    {
        $this->logoStranekModel = $logoStranekModel;
    }

    public function create()
    {
        return new VymazLogoControl($this->logoStranekModel);
    }
}