<?php

namespace App\AdminModule\NastaveniModule\ZakladniInformaceModule\Components\VymazLogo;

use App\AdminModule\NastaveniModule\ZakladniInformaceModule\Models\LogoStranekModel;
use Nette\Application\UI\Control;

class VymazLogoControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $logoStranekModel;

    public function __construct(LogoStranekModel $logoStranekModel)
    {
        $this->logoStranekModel = $logoStranekModel;
    }

    public function render()
    {
        $this->template->render(self::template);
    }

    public function handleVymazLogo()
    {
        $this->logoStranekModel->delete();
        $this->presenter->flashMessage('Logo bylo vymazáno', 'success');
        $this->redirect('this');
    }
}