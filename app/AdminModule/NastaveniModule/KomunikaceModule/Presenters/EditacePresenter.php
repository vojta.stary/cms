<?php

namespace App\AdminModule\NastaveniModule\KomunikaceModule\Presenters;

use App\AdminModule\NastaveniModule\KomunikaceModule\Components\EditEmail\EditEmailFactory;
use App\AdminModule\Presenters\BaseAdminPresenter;
use App\Managers\BaseInfoManager;

final class EditacePresenter extends BaseAdminPresenter
{
    private $baseInfoManager;
    private $editEmailFactory;

    public function __construct(BaseInfoManager     $baseInfoManager,
                                EditEmailFactory    $editEmailFactory)
    {
        $this->baseInfoManager = $baseInfoManager;
        $this->editEmailFactory = $editEmailFactory;
    }

    /*================================== nastavení aplikace ==================================*/
    public function startup()
    {
        parent::startup();

        $this->allowRole = ['admin'];
    }

    /*================================== render metody ==================================*/

    public function renderDefault()
    {
        $mailInfo = ['mailAdresa','mailSMTP','mailZabez','mailPort','mailUser','mailPass'];
        $this->template->mail = $this->baseInfoManager->getBaseInfo($mailInfo);
    }

    /*====================================== komponenty ======================================*/
    /**
     * editace nastavení emailu
     * @return \App\AdminModule\NastaveniModule\KomunikaceModule\Components\EditEmail\EditEmailControl
     */
    protected function createComponentEditEmail()
    {
        return $this->editEmailFactory->create();
    }
}