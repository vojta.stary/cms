<?php

namespace App\AdminModule\NastaveniModule\KomunikaceModule\Components\EditEmail;

use App\AdminModule\NastaveniModule\ZakladniInformaceModule\Models\BaseInfoModel;
use App\Managers\BaseInfoManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class EditEmailControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $baseInfoManager;
    private $baseInfoModel;

    public function __construct(BaseInfoManager $baseInfoManager,
                                BaseInfoModel   $baseInfoModel)
    {
        $this->baseInfoManager = $baseInfoManager;
        $this->baseInfoModel = $baseInfoModel;
    }

    public function render()
    {
        $mailInfo = ['mailAdresa','mailSmtp','mailZabez','mailPort','mailUser','mailPass'];

        $data = $this->baseInfoManager->getBaseInfo($mailInfo);

        $this['editEmailForm']->setDefaults($data);
        $this->template->render(self::template);
    }

    /*================================= formulář =================================*/
    protected function createComponentEditEmailForm(): Form
    {
        $zabezpec = [null => 'Bez zapezpečení','tls' => 'TLS', 'ssl' => 'SSL'];

        $form = new Form;
        $form->addEmail('mailAdresa', 'Adresa odesílatele')
            ->setRequired('Musíte vyplnit adresu odesílatele!');
        $form->addText('mailSmtp', 'SMTP server')
            ->setRequired('Musíte vyplnit SMTP server!');
        $form->addSelect('mailZabez', 'Typ zabezpečení',$zabezpec)
            ->setPrompt('Vyberte možnost');
        $form->addText('mailPort', 'Číslo portu')
            ->setRequired('Musíte vyplnit číslo portu!');
        $form->addText('mailUser', 'Přihlašovací jméno')
            ->setRequired('Musíte vyplnit přihlašovací jméno!');
        $form->addText('mailPass', 'Heslo')
            ->setRequired('Musíte vyplnit heslo!');
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendEditEmailForm'];
        return $form;
    }

    public function sendEditEmailForm(Form $form, $values)
    {
        try{
            $this->baseInfoModel->saveBaseInfo($values);
            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Chyba zápisu dat do databáze', 'danger');
            $this->redirect('this');
        }
    }
}