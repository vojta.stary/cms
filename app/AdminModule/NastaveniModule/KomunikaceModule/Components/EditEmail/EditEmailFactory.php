<?php

namespace App\AdminModule\NastaveniModule\KomunikaceModule\Components\EditEmail;

use App\AdminModule\NastaveniModule\ZakladniInformaceModule\Models\BaseInfoModel;
use App\Managers\BaseInfoManager;

class EditEmailFactory
{
    private $baseInfoManager;
    private $baseInfoModel;

    public function __construct(BaseInfoManager $baseInfoManager,
                                BaseInfoModel   $baseInfoModel)
    {
        $this->baseInfoManager = $baseInfoManager;
        $this->baseInfoModel = $baseInfoModel;
    }

    public function create()
    {
        return new EditEmailControl($this->baseInfoManager,
                                    $this->baseInfoModel);
    }
}