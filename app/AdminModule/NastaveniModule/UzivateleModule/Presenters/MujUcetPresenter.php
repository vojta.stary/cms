<?php

namespace App\AdminModule\NastaveniModule\UzivateleModule\Presenters;

use App\AdminModule\NastaveniModule\UzivateleModule\Components\EditUzivatel\EditUzivatelFactory;
use App\AdminModule\NastaveniModule\UzivateleModule\Components\ZmenaHesla\ZmenaHeslaFactory;
use App\AdminModule\Presenters\BaseAdminPresenter;
use App\Managers\UsersManager;

final class MujUcetPresenter extends BaseAdminPresenter
{
    private $usersManager;
    private $zmenaHeslaFactory;
    private $editUzivatel;

    public function __construct(UsersManager        $usersManager,
                                ZmenaHeslaFactory   $zmenaHeslaFactory,
                                EditUzivatelFactory $editUzivatelFactory)
    {
        $this->usersManager = $usersManager;
        $this->zmenaHeslaFactory = $zmenaHeslaFactory;
        $this->editUzivatel = $editUzivatelFactory;
    }


    public function actionDefault()
    {
        $uzivatel = $this->usersManager->getOne($this->userData->id);

        if($uzivatel){
            $this->template->uzivatel = $uzivatel;
        }else{
            $this->flashMessage('Uživatel nebyl nalezen','danger');
            $this->redirect(':Admin:Dashboard:default');
        }
    }

    /*================================= komponenty =================================*/

    /**
     * změna hesla uživatele
     * @return \App\AdminModule\NastaveniModule\UzivateleModule\Components\ZmenaHesla\ZmenaHeslaControl
     */
    protected function createComponentZmenaHesla()
    {
        $this->zmenaHeslaFactory->setUzivatel($this->userData->id);
        return $this->zmenaHeslaFactory->create();
    }

    /**
     * editace uživatele
     * @return \App\AdminModule\NastaveniModule\UzivateleModule\Components\EditUzivatel\EditUzivatelControl
     */
    protected function createComponentEditUzivatel()
    {
        $this->editUzivatel->setUzivatel($this->userData->id);
        $this->editUzivatel->hideRole('hide');
        return $this->editUzivatel->create();
    }
}