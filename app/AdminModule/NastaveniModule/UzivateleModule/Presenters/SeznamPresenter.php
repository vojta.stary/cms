<?php

namespace App\AdminModule\NastaveniModule\UzivateleModule\Presenters;

use App\AdminModule\Components\Paginator\PaginatorFactory;
use App\AdminModule\NastaveniModule\UzivateleModule\Components\EditUzivatel\EditUzivatelFactory;
use App\AdminModule\NastaveniModule\UzivateleModule\Components\NovyUzivatel\NovyUzivatelFactory;
use App\AdminModule\NastaveniModule\UzivateleModule\Components\VymazUzivatel\VymazUzivatelFactory;
use App\AdminModule\Presenters\BaseAdminPresenter;
use App\Managers\UsersManager;
use Nette\Application\UI\Multiplier;

final class SeznamPresenter extends BaseAdminPresenter
{
    const pageItems = 12;

    private $userManager;
    private $novyUzivatelFactory;
    private $editUzivatelFactory;
    private $vymazUzivatelFactory;
    private $paginator;
    private $itemsCount;
    private $page = 1;
    private $filter = [];

    public function __construct(UsersManager            $usersManager,
                                PaginatorFactory        $paginatorFactory,
                                NovyUzivatelFactory     $novyUzivatelFactory,
                                EditUzivatelFactory     $editUzivatelFactory,
                                VymazUzivatelFactory    $vymazUzivatelFactory)
    {
        $this->userManager = $usersManager;
        $this->paginator = $paginatorFactory;
        $this->novyUzivatelFactory = $novyUzivatelFactory;
        $this->editUzivatelFactory = $editUzivatelFactory;
        $this->vymazUzivatelFactory = $vymazUzivatelFactory;
    }

    public function startup()
    {
        parent::startup();

        $this->allowRole = ['admin'];
    }

    public function renderDefault()
    {
        $offset = ($this->page - 1)* self::pageItems;

        $data = $this->getUzivatele();
        $this->itemsCount = $data->count();

        $this->template->data = $data->limit(self::pageItems, $offset)->fetchAll();
    }

    /*================================= komponenty ========================================*/
    /**
     * paginator component
     * @return \App\AdminModule\Components\Paginator\PaginatorControl
     */
    protected function createComponentPaginator()
    {
        return $this->paginator->create($this->itemsCount,$this->page,'page',$this->filter,self::pageItems);
    }

    /**
     * nový uživatel
     * @return \App\AdminModule\NastaveniModule\UzivateleModule\Components\NovyUzivatel\NovyUzivatelControl
     */
    protected function createComponentNovyUzivatel()
    {
        return $this->novyUzivatelFactory->create();
    }

    /**
     * editace uživatele
     * @return Multiplier
     */
    protected function createComponentEditUzivatel()
    {
        return new Multiplier(function ($id){
            $this->editUzivatelFactory->setUzivatel($id);
            return $this->editUzivatelFactory->create();
        });
    }

    /**
     * vymazání uživatele
     * @return Multiplier
     */
    protected function createComponentVymazUzivatel()
    {
        return new Multiplier(function ($id){
            $this->vymazUzivatelFactory->setUzivatel($id);
            return $this->vymazUzivatelFactory->create();
        });
    }

    /*================================= handle metody =================================*/
    /**
     * nastavení ajax stránkování pro paginator
     * @param int $page
     * @param array $filter
     * @return void
     */
    public function handlePage(int $page, array $filter): void
    {
        if ($this->isAjax()){
            $this->filter = $filter;
            $this->page = $page;
            $this->redrawControl('list');
        }
    }

    /**
     * nastavení filtrace seznamu
     * @param array $filter
     * @return void
     */
    public function handleFilter(array $filter): void
    {
        if ($this->isAjax())
        {
            $this->filter = $filter;
            $this->redrawControl('list');
        }
    }

    /**
     * handle for user activation
     * @param int $userId
     */
    public function handleUserActive(int $userId){
        if ($this->isAjax()){
            $this->userManager->changeActive($userId);
            $this->redrawControl('list');
        }
    }

    /*============================ Pomocné metody ======================================*/
    /**
     * @return \Nette\Database\Table\Selection
     */
    private function getUzivatele()
    {
        $data = $this->userManager->select();
        $data->where('NOT (id ?)', $this->userData->id)->where('NOT (email ?)', null);
        $data->where(UsersManager::columnRole, ['admin', 'editor' ]);


        if (isset($this->filter['name']))
        {
            $data->where('name LIKE ?','%'.$this->filter['name'].'%');
        }
        if (isset($this->filter['email']))
        {
            $data->where('email LIKE ?','%'.$this->filter['email'].'%');
        }
        if (isset($this->filter['role']))
        {
            $data->where('role LIKE ?','%'.$this->filter['role'].'%');
        }

        return $data;
    }
}