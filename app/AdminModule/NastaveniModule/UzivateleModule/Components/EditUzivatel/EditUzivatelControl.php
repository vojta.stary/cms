<?php

namespace App\AdminModule\NastaveniModule\UzivateleModule\Components\EditUzivatel;

use App\Managers\UsersManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class EditUzivatelControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $uzivatelManager;
    private $uzivatelId;
    private $role = 'true';

    public function __construct(UsersManager    $usersManager,
                                                $uzivatelId,
                                                $role)
    {
        $this->uzivatelManager = $usersManager;
        $this->uzivatelId = $uzivatelId;
        $this->role = $role;
    }

    public function render()
    {
        $uzivatel = $this->uzivatelManager->getOne($this->uzivatelId);

        $this['editUzivatelForm']->setDefaults($uzivatel);
        $this->template->role = $this->role;
        $this->template->uzivatel = $uzivatel;
        $this->template->render(self::template);
    }

    /*============================= handle metody =============================*/
    public function handleCheckEmail($email = null)
    {
        if($this->presenter->isAjax())
        {
            $this->presenter->payload->email = $this->uzivatelManager->checkEmail($email);
            $this->presenter->sendPayload();
        }
    }

    /*============================= formulář =============================*/
    protected function createComponentEditUzivatelForm(): Form
    {
        $roles = ['admin' => 'Administrátor', 'editor' => 'Editor'];

        $form = new Form;
        $form->addProtection();
        $form->addHidden('id');
        $form->addText('name')
            ->setRequired('Musíte vyplnit celé jméno uživatele!');
        $form->addEmail('email')
            ->setRequired('Musíte vyplnit email uživatele!');
        if ($this->role == 'true'){
            $form->addSelect('role', 'Uživatelská role', $roles)
                ->setPrompt('Vyberte uživatelskou roli')
                ->setRequired('Musíte vybrat uživatelskou roli!');
        }
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendEditUzivatelForm'];
        return $form;
    }

    public function sendEditUzivatelForm(Form $form, $values)
    {
        try {
            $this->uzivatelManager->update($values->id,$values);
            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->presenter->flashMessage('Změny se projevý při přístím přihlášení','info');
            $this->redirect('this');
        }catch (\PDOException $e){
            if ((int)$e->errorInfo[1] === 1062) { // SQL kód pro narušení unikátního klíče
                $this->presenter->flashMessage('Emailová adresa je již registrována u jiného uživatele!', 'warning');
                $form->setDefaults($values);
                $this->redirect('this');
            }else{
                $this->presenter->flashMessage('Chyba zápisu dat do databáze!','danger');
                $form->setDefaults($values);
                $this->redirect('this');
            }
        }
    }
}