<?php

namespace App\AdminModule\NastaveniModule\UzivateleModule\Components\EditUzivatel;

use App\Managers\UsersManager;

class EditUzivatelFactory
{
    private $uzivatelManager;
    private $uzivatelId;
    private $role = 'true';

    public function __construct(UsersManager $usersManager)
    {
        $this->uzivatelManager = $usersManager;
    }

    public function create()
    {
        return new EditUzivatelControl( $this->uzivatelManager,
                                        $this->uzivatelId,
                                        $this->role);
    }

    public function setUzivatel(int $id)
    {
        $this->uzivatelId = $id;
    }

    public function hideRole(string $viditelnost)
    {
        $this->role = $viditelnost;
    }
}