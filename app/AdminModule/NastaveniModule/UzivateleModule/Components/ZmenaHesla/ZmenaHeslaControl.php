<?php

namespace App\AdminModule\NastaveniModule\UzivateleModule\Components\ZmenaHesla;

use App\Managers\UsersManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class ZmenaHeslaControl extends Control
{
    const template = __DIR__ . '/templates/template.latte';

    private $usersManager;
    private $uzivatelId;

    public function __construct(UsersManager    $usersManager,
                                                $uzivatelId)
    {
        $this->usersManager = $usersManager;
        $this->uzivatelId = $uzivatelId;
    }

    public function render()
    {
        $uzivatel = $this->usersManager->getOne($this->uzivatelId);

        $this['zmenaHeslaForm']->setDefaults($uzivatel);
        $this->template->uzivatel = $uzivatel;
        $this->template->render(self::template);
    }

    /*================================= formulář =================================*/
    protected function createComponentZmenaHeslaForm(): Form
    {
        $form = new Form;
        $form->addProtection();
        $form->addHidden('id');
        $form->addPassword("password")
            ->setRequired('Musíte vyplnit heslo!')
            ->addRule($form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaků.', 8)
            ->addCondition($form::MIN_LENGTH, 8)
            ->addRule($form::PATTERN, 'Heslo musí obsahovat číslici', '.*[0-9].*');
        $form->addPassword("passVerify")
            ->setRequired('Musíte vyplnit heslo znovu pro kontrolu!')
            ->addRule($form::EQUAL, 'Hesla se neshodují!', $form['password'])
            ->setOmitted();
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendZmenaHeslaForm'];
        return $form;
    }

    public function sendZmenaHeslaForm(Form $form, $values): void
    {
        try {
            $this->usersManager->changePassword($values->id, $values->password);
            $this->presenter->flashMessage('Data byla uložena','success');
            $this->presenter->flashMessage('Změny se projevý při přístím přihlášení','info');
            $this->redirect('this');
        }catch (\PDOException $e){
            $form->setDefaults($values);
            $this->presenter->flashMessage('Chyba zápisu do databáze!','danger');
            $this->redirect('this');
        }
    }
}