<?php

namespace App\AdminModule\NastaveniModule\UzivateleModule\Components\ZmenaHesla;

use App\Managers\UsersManager;

class ZmenaHeslaFactory
{
    private $usersManager;
    private $uzivatelId;

    public function __construct(UsersManager    $usersManager)
    {
        $this->usersManager = $usersManager;
    }

    public function create()
    {
        return new ZmenaHeslaControl(   $this->usersManager,
                                        $this->uzivatelId);
    }

    public function setUzivatel(int $id)
    {
        $this->uzivatelId = $id;
    }
}