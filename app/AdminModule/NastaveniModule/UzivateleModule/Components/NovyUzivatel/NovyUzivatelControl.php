<?php

namespace App\AdminModule\NastaveniModule\UzivateleModule\Components\NovyUzivatel;

use App\Managers\UsersManager;
use App\Models\MailerModel;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Mail\FallbackMailerException;

class NovyUzivatelControl extends Control
{
    const template = __DIR__.'/templates/template.latte';
    const newUserTemplate = 'novyUzivatel.latte';

    private $mailerModel;
    private $uzivatelManager;

    public function __construct(MailerModel     $mailerModel,
                                UsersManager    $usersManager)
    {
        $this->mailerModel = $mailerModel;
        $this->uzivatelManager = $usersManager;
    }

    public function render()
    {
        $this->template->render(self::template);
    }

    /*============================= handle metody =============================*/
    public function handleCheckEmail($email = null)
    {
        if($this->presenter->isAjax())
        {
            $this->presenter->payload->email = $this->uzivatelManager->checkEmail($email);
            $this->presenter->sendPayload();
        }
    }

    /*============================= formulář =============================*/
    protected function createComponentNovyUzivatelForm(): Form
    {
        $roles = ['admin' => 'Administrátor', 'editor' => 'Editor'];

        $form = new Form;
        $form->addProtection();
        $form->addText('name')
            ->setRequired('Musíte vyplnit celé jméno uživatele!');
        $form->addEmail('email')
            ->setRequired('Musíte vyplnit email uživatele!');
        $form->addPassword("password")
            ->setRequired('Musíte vyplnit heslo!')
            ->addRule($form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaků.', 8)
            ->addCondition($form::MIN_LENGTH, 8)
            ->addRule($form::PATTERN, 'Heslo musí obsahovat číslici', '.*[0-9].*');
        $form->addPassword("passVerify")
            ->setRequired('Musíte vyplnit heslo znovu pro kontrolu!')
            ->addRule($form::EQUAL, 'Hesla se neshodují!', $form['password'])
            ->setOmitted();
        $form->addSelect('role', 'Uživatelská role', $roles)
            ->setPrompt('Vyberte uživatelskou roli')
            ->setRequired('Musíte vybrat uživatelskou roli!');
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendNovyUzivatelForm'];
        return $form;
    }

    public function sendNovyUzivatelForm(Form $form, $values)
    {
        try {
            try{
                $this->mailerModel->sendMail($values->email,self::newUserTemplate,$values);
            }catch (FallbackMailerException $e){
                $this->presenter->flashMessage('Email se nepodařilo odeslat', 'warning');
            }
            $this->uzivatelManager->create($values);
            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->redirect('this');
        }catch (\PDOException $e){
            if ((int)$e->errorInfo[1] === 1062) { // SQL kód pro narušení unikátního klíče
                $this->presenter->flashMessage('Emailová adresa je již registrována u jiného uživatele!', 'warning');
                $form->setDefaults($values);
                $this->redirect('this');
            }else{
                $this->presenter->flashMessage('Chyba zápisu dat do databáze!','danger');
                $form->setDefaults($values);
                $this->redirect('this');
            }
        }
    }
}