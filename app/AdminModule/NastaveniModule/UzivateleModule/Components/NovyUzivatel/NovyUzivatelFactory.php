<?php

namespace App\AdminModule\NastaveniModule\UzivateleModule\Components\NovyUzivatel;

use App\Managers\UsersManager;
use App\Models\MailerModel;

class NovyUzivatelFactory
{
    private $mailerModel;
    private $uzivatelManager;

    public function __construct(MailerModel     $mailerModel,
                                UsersManager    $usersManager)
    {
        $this->mailerModel = $mailerModel;
        $this->uzivatelManager = $usersManager;
    }

    public function create()
    {
        return new NovyUzivatelControl( $this->mailerModel,
                                        $this->uzivatelManager);
    }
}