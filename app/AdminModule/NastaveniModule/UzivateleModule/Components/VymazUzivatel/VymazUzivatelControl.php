<?php

namespace App\AdminModule\NastaveniModule\UzivateleModule\Components\VymazUzivatel;

use App\Managers\UsersManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class VymazUzivatelControl extends Control
{
    const template = __DIR__ . '/templates/template.latte';

    private $usersManager;
    private $userId;

    public function __construct(UsersManager    $usersManager,
                                                $userId)
    {
        $this->usersManager = $usersManager;
        $this->userId = $userId;
    }

    public function render(){
        $uzivatel = $this->usersManager->getOne($this->userId);

        $this['vymazUzivatelForm']->setDefaults($uzivatel);
        $this->template->uzivatel = $uzivatel;
        $this->template->render(self::template);
    }

    /*================================== formulář ==================================*/
    protected function createComponentVymazUzivatelForm(): Form
    {
        $form = new Form;
        $form->addProtection();
        $form->addHidden('id');
        $form->addSubmit('send','Vymazat');
        $form->onSuccess[] = [$this, 'sendVymazUzivatelForm'];
        return $form;
    }

    public function sendVymazUzivatelForm(Form $form, $values): void
    {
        try {
            $this->usersManager->delete($values->id);

            $this->presenter->flashMessage('Uživatel byl vymazán.','success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this['delUserForm']->setDefaults($values);
            $this->presenter->flashMessage('Chyba zápisu do databáze!','danger');
            $this->redirect('this');
        }
    }
}