<?php

namespace App\AdminModule\NastaveniModule\UzivateleModule\Components\VymazUzivatel;

use App\Managers\UsersManager;

class VymazUzivatelFactory
{
    private $usersManager;
    private $userId;

    public function __construct(UsersManager $usersManager)
    {
        $this->usersManager = $usersManager;
    }

    public function create()
    {
        return new VymazUzivatelControl($this->usersManager,
                                        $this->userId);
    }

    public function setUzivatel(int $id)
    {
        $this->userId = $id;
    }
}