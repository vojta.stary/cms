<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Models;

use App\Managers\SetExpMenuDokuManager;
use App\Managers\SetExpMenuHlavManager;
use Nette\Utils\Strings;

final class NastExportMenuModel
{
    const dir = __DIR__.'/../../../../../www/src/restaurace/menu/';

    private $setExpMenuDokuManager;
    private $setExpMenuHlavManager;

    public function __construct(SetExpMenuDokuManager $setExpMenuDokuManager,
                                SetExpMenuHlavManager $expMenuHlavManager)
    {
        $this->setExpMenuDokuManager = $setExpMenuDokuManager;
        $this->setExpMenuHlavManager = $expMenuHlavManager;
    }

    /**
     * ukládá obrázek pozadí menu
     * @param $values
     * @return void
     */
    public function saveBgImageMenu($values)
    {
        $setting = $this->setExpMenuDokuManager->select()
            ->where(SetExpMenuDokuManager::columnRestMenuId, $values->rest_menu_id)
            ->fetch();

        if ($setting->bg_image) {
            $this->vymazPozadi($setting->bg_image, $setting->rest_menu_id);
        }

        $this->savePozadi($values->pozadi, $setting->id);
    }

    /**
     * ukládá logo menu
     * @param $values
     * @return void
     */
    public function saveLogoMenu($values)
    {
        $setting = $this->setExpMenuHlavManager->select()
            ->where(SetExpMenuHlavManager::columnRestMenuId, $values->rest_menu_id)
            ->fetch();

        if ($setting->logo) {
            $this->vymazLogo($setting->logo, $setting->rest_menu_id);
        }

        $this->saveLogo($values->logo, $setting->id);
    }

    /**
     * maže pozadí menu
     * @param $setId
     * @return void
     */
    public function deleteImage($setId): void
    {
        $setting = $this->setExpMenuDokuManager->getOne($setId);

        $this->vymazPozadi($setting->bg_image, $setting->rest_menu_id);

        $setting->update([SetExpMenuDokuManager::columnBgImage => null]);
    }

    /**
     * vymazává logo menu
     * @param $setId
     * @return void
     */
    public function deleteLogo($setId): void
    {
        $setting = $this->setExpMenuHlavManager->getOne($setId);

        $this->vymazLogo($setting->logo, $setting->rest_menu_id);

        $setting->update([SetExpMenuHlavManager::columnLogo => null]);
    }

    /*==================================== práce se soubory ====================================*/
    /**
     * ukládá pozadí
     * @param $pozadi
     * @param $setId
     * @return void
     */
    private function savePozadi($pozadi, $setId): void
    {
        if ($pozadi->hasFile()) {
            $setting = $this->setExpMenuDokuManager->getOne($setId);

            $newName = $pozadi->getName();

            $pripona = pathinfo($newName);

            //upravý názen na web friendly
            $newName = Strings::webalize($pripona['filename']) . '.' . $pripona['extension'];

            //uloží soubor
            $newFile = self::dir .'/'.$setting->rest_menu_id.'/'. $newName;
            $pozadi->move($newFile);

            //uloží data do databáze
            $setting->update([SetExpMenuDokuManager::columnBgImage => $newName]);
        }
    }

    /**
     * ukládá logo
     * @param $logo
     * @param $setId
     * @return void
     */
    private function saveLogo($logo, $setId): void
    {
        if ($logo->hasFile()) {
            $setting = $this->setExpMenuHlavManager->getOne($setId);

            $newName = $logo->getName();

            $pripona = pathinfo($newName);

            //upravý názen na web friendly
            $newName = Strings::webalize($pripona['filename']) . '.' . $pripona['extension'];

            //uloží soubor
            $newFile = self::dir .'/'.$setting->rest_menu_id.'/'. $newName;
            $logo->move($newFile);

            //uloží data do databáze
            $setting->update([SetExpMenuHlavManager::columnLogo => $newName]);
        }
    }

    /**
     * maže pozadí
     * @param $pozadi
     * @param $menuId
     * @return void
     */
    private function vymazPozadi($pozadi, $menuId)
    {
        $file = self::dir.'/'.$menuId.'/'.$pozadi;

        if (is_file($file))
        {
            unlink($file);
        }
    }

    /**
     * maže logo
     * @param $logo
     * @param $menuId
     * @return void
     */
    private function vymazLogo($logo, $menuId)
    {
        $file = self::dir.'/'.$menuId.'/'.$logo;

        if (is_file($file))
        {
            unlink($file);
        }
    }
}