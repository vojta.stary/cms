<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Models;

use App\Managers\BaseInfoManager;
use App\Managers\RestMenuManager;
use App\Managers\SetExpMenuBodyManager;
use App\Managers\SetExpMenuDokuManager;
use App\Managers\SetExpMenuHlavManager;
use App\Managers\SetExpMenuPatManager;
use Mpdf\Mpdf;

final class ExportMenuModel
{
    const imageDir = __DIR__.'/../../../../../www/src/restaurace/menu/';

    private $setExpMenuDokuManager;
    private $setExpMenuHlavManager;
    private $setExpMenuPatManager;
    private $setExpMenuBodyManager;
    private $restMenuManager;
    private $menuId;
    private $menu;
    private $setPage;
    private $setHeader;
    private $setFooter;
    private $setBody;

    public function __construct(SetExpMenuDokuManager       $setExpMenuDokuManager,
                                SetExpMenuHlavManager       $setExpMenuHlavManager,
                                SetExpMenuPatManager        $setExpMenuPatManager,
                                SetExpMenuBodyManager       $setExpMenuBodyManager,
                                RestMenuManager             $restMenuManager)
    {
        $this->setExpMenuDokuManager = $setExpMenuDokuManager;
        $this->setExpMenuHlavManager = $setExpMenuHlavManager;
        $this->setExpMenuPatManager = $setExpMenuPatManager;
        $this->setExpMenuBodyManager = $setExpMenuBodyManager;
        $this->restMenuManager = $restMenuManager;
    }

    public function export(int $menuId)
    {
        //nastavit data
        $this->menuId = $menuId;

        //ziska data
        $this->setPage = $this->setExpMenuDokuManager->select()
            ->where(SetExpMenuDokuManager::columnRestMenuId, $this->menuId)
            ->fetch();
        $this->setHeader = $this->setExpMenuHlavManager->select()
            ->where(SetExpMenuHlavManager::columnRestMenuId, $this->menuId)
            ->fetch();
        $this->setFooter = $this->setExpMenuPatManager->select()
            ->where(SetExpMenuPatManager::columnRestMenuId, $this->menuId)
            ->fetch();
        $this->setBody = $this->setExpMenuBodyManager->select()
            ->where(SetExpMenuBodyManager::columnRestMenuId, $this->menuId)
            ->fetch();

        $this->menu = $this->getMenuData();

        //vytvořit PDF
        $mpdfConfig = $this->setConfPdf();


        $mpdf = new Mpdf($mpdfConfig);

        //barva textu
        $mpdf->SetDefaultBodyCSS('color', $this->setPage['color']);

        //pozadí stránek
        $mpdf->SetDefaultBodyCSS('background','background: '.$this->setPage->bg_color);
        if ($this->setPage['bg_image']) {
            $background = self::imageDir.$this->menuId.'/'.$this->setPage->bg_image;
            $mpdf->SetDefaultBodyCSS('background','background: url('.$background.')');
            $mpdf->SetDefaultBodyCSS('background-image-resize', $this->setPage['bg_image_size']);
        }

        $mpdf->SetHTMLHeader($this->setHeader());
        $mpdf->SetHTMLFooter($this->setFooter());

        $content = $this->createContent();
        $mpdf->WriteHTML($content);

        return $mpdf;
    }

    /*================================ getters ================================*/

    /**
     * ziskava data pro menu
     * @return \Nette\Database\Table\ActiveRow|null
     */
    private function getMenuData()
    {
        $data = $this->restMenuManager->getOne($this->menuId);
        return $data;
    }

    /*================================ setters ================================*/

    /**
     * nastavuje konfiguraci PDF
     * @return array
     */
    private function setConfPdf(): array
    {
        $mpdfConfig = array(
            'margin_left' => $this->setPage->margin_left,
            'margin_right' => $this->setPage->margin_right,
            'margin_top' => $this->setPage->margin_top,
            'margin_bottom' => $this->setPage->margin_bottom,
            'margin_header' => $this->setHeader->margin_header,
            'margin_footer' => $this->setFooter->margin_footer,
            'orientation' => $this->setPage->orientation
        );

        return $mpdfConfig;
    }

    /**
     * nastavuje hlavičku menu
     * @return string
     */
    private function setHeader(): string
    {
        $headFirst = '<header style="position:relative; margin:0;color: '.$this->setHeader->color.'; background-color: '.$this->setHeader->bg_color.';">';
        $headLast = '</header>';
        $headLogo = '';

        //logo
        if($this->setHeader->logo){
            $headLogo = '<img src="'.self::imageDir.$this->menuId.'/'.$this->setHeader->logo.'" style="max-height: '.$this->setHeader->max_logo_size.'mm; margin-left: '.$this->setHeader->margin_logo_left.'mm; margin-right: '.$this->setHeader->margin_logo_right.'mm; margin-top: '.$this->setHeader->margin_logo_top.'mm; margin-bottom:'.$this->setHeader->margin_logo_bottom.'mm; float: '.$this->setHeader->logo_align.'">';
        }

        $headTitle = '<h1 style="position: relative; font-size: '.$this->setHeader->title_size.'mm; padding-left: '.$this->setHeader->margin_title_left.'mm; padding-right: '.$this->setHeader->margin_title_right.'mm; padding-top: '.$this->setHeader->margin_title_top.'mm; padding-bottom: '.$this->setHeader->margin_title_bottom.'mm; margin-bottom: 0;">'.$this->menu->nazev.'</h1>';
        $header = $headFirst.$headLogo.$headTitle.$headLast;

        return $header;
    }

    /**
     * nastavuje patičku menu
     * @return string
     */
    private function setFooter(): string
    {
        $footerFirst = '<footer style="padding: '.$this->setFooter->margin_top.'mm '.$this->setFooter->margin_right.'mm '.$this->setFooter->margin_bottom.'mm '.$this->setFooter->margin_left.'mm; background: '.$this->setFooter->bg_color.';">';
        $footerText = '<div style="color: '.$this->setFooter->color.'; font-size: '.$this->setFooter->font_size.'mm">'.$this->setFooter->text.'</div>';
        $footerLast = '</footer>';

        $footer = $footerFirst.$footerText.$footerLast;

        return $footer;
    }

    /**
     * vytváří obsah menu
     * @return string
     */
    private function createContent(): string
    {
        $popis_font_size = $this->setBody->font_size - 0.5;

        $first = '<columns column-count="'.$this->setPage->columns.'">';
        $last = '</columns>';
        $table = '<table style="width:100%; margin-left: '.$this->setBody->margin_skup_left.'mm; margin-right: '.$this->setBody->margin_skup_right.'mm; margin-top: '.$this->setBody->margin_skup_top.'mm; margin-bottom: '.$this->setBody->margin_skup_bottom.'mm;">';
        $tableEnd = '</table>';
        $thead = '<thead>';
        $theadEnd = '<thead>';
        $tbody = '<tbody>';
        $tbodyEnd = '</tbody>';
        $tr = '<tr style="width: 100%;">';
        $trEnd = '</tr>';
        $tdNazev = '<td style="padding-top: 2mm; font-size: '.$this->setBody->font_size.'mm;">';
        $tdAler = '<td style="width: 10%; padding-top: 2mm; font-size: '.$this->setBody->font_size.'mm;">';
        $tdMnoz = '<td style="width: 15%; padding-top: 2mm; text-align: right; font-size: '.$this->setBody->font_size.'mm;">';
        $tdCena = '<td style="width: 20%; padding-top: 2mm; text-align: right; font-size: '.$this->setBody->font_size.'mm;">';
        $tdPopis = '<td colspan="4" style="padding-bottom: '.$this->setBody->margin_row_bottom.'mm; font-size: '.$popis_font_size.'mm;">';
        $tdEnd = '</td>';
        $th = '<th colspan="4" style="padding-bottom: 2mm; font-size: '.$this->setBody->header_font_size.'mm">';
        $thEnd = '</th>';

        $polozky = $first;

        foreach ($this->menu->related('rest_menu_skupiny.rest_menu_id')->order('sort ASC') as $skupina) {
            if ($skupina->active) {
                $polozky = $polozky.'<div style="width: 100%; margin: auto">'.$table.$thead.$tr.$th.$skupina->nazev.$thEnd.$trEnd.$theadEnd.$tbody;

                foreach ($skupina->related('rest_menu_skupiny_polozky.rest_menu_skupiny_id')->order('sort ASC') as $polozka) {
                    if ($polozka->active) {
                        if ($this->menu->typ == 'napoj') {
                            $polozky = $polozky . $tr . $tdNazev . $polozka->nazev . $tdEnd . $tdAler . $polozka->alergen . $tdEnd
                                . $tdMnoz . $polozka->mnozstvi . $tdEnd . $tdCena . $polozka->cena . ',- Kč' . $tdEnd . $trEnd
                                . $tr . $tdPopis . '<i style="font-size: .9em">' . $polozka->popis . '</i>' . $tdEnd . $trEnd;
                        }
                        if ($this->menu->typ == 'jidlo') {
                            $tdNazev = '<td style="width: 60%; padding-top: 2mm; font-size: '.$this->setBody->font_size.'mm;">';
                            $tdAler = '<td style="width: 20%; padding-top: 2mm; text-align: right; font-size: '.$this->setBody->font_size.'mm;">';
                            $tdPopis = '<td colspan="3" style="padding-bottom: '.$this->setBody->margin_row_bottom.'mm; font-size: '.$popis_font_size.'mm;">';

                            $polozky = $polozky . $tr . $tdNazev . $polozka->nazev . $tdEnd . $tdAler . $polozka->alergen . $tdEnd
                                . $tdCena . $polozka->cena . ',- Kč' . $tdEnd . $trEnd
                                . $tr . $tdPopis . '<i style="font-size: .9em">' . $polozka->popis . '</i>' . $tdEnd . $trEnd;
                        }
                    }
                }
                $polozky = $polozky.$tbodyEnd.$tableEnd.'</div>';
            }
        }

        $polozky = $polozky.$last;

        return $polozky;
    }
}