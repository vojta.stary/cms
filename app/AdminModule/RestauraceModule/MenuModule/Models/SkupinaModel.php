<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Models;

use App\Managers\RestMenuSkupinyManager;

final class SkupinaModel
{
    private $restMenuSkupinyManager;

    public function __construct(RestMenuSkupinyManager $restMenuSkupinyManager)
    {
        $this->restMenuSkupinyManager = $restMenuSkupinyManager;
    }

    /**
     * ukládá novou skupinu
     * @param $values
     * @return void
     */
    public function create($values): void
    {
        $count = $this->restMenuSkupinyManager->select()->where(RestMenuSkupinyManager::columnRestMenuId, $values->rest_menu_id)->count();
        $sort = $count + 1;

        $values->sort = $sort;

        $this->restMenuSkupinyManager->create($values);
    }

    /**
     * maže skupinu a přepočítá řazení
     * @param int $id
     * @return void
     */
    public function delete(int $id): void
    {
        $skupina = $this->restMenuSkupinyManager->getOne($id);

        $restMenuId = $skupina->rest_menu_id;

        $skupina->delete();
        $this->resort($restMenuId);
    }

    /**
     * upravuje pořadí směrem nahoru
     * @param int $id
     * @param int $restMenuId
     * @return void
     */
    public function sortUp(int $id, int $restMenuId): void
    {
        $database = $this->restMenuSkupinyManager->select()->where(RestMenuSkupinyManager::columnRestMenuId, $restMenuId);

        $zaznam = $this->restMenuSkupinyManager->getOne($id);

        if ($zaznam->sort != 1){
            $aktualSort = $zaznam->sort;
            $newSort = $aktualSort - 1;

            $zaznamPod = $database->where(RestMenuSkupinyManager::columnSort,$newSort)->fetch();

            if ($zaznamPod){
                $zaznamPod->update([RestMenuSkupinyManager::columnSort => $aktualSort]);
            }
            $zaznam->update([RestMenuSkupinyManager::columnSort => $newSort]);
        }
    }

    /**
     * upravuje pořadí směrem dolu
     * @param int $id
     * @param int $restMenuId
     * @return void
     */
    public function sortDown(int $id, int $restMenuId): void
    {
        $database = $this->restMenuSkupinyManager->select()->where(RestMenuSkupinyManager::columnRestMenuId, $restMenuId);

        $zaznam = $this->restMenuSkupinyManager->getOne($id);

        $pocetZaznamu = $database->count();

        if ($zaznam->sort != $pocetZaznamu){
            $aktualSort = $zaznam->sort;
            $newSort = $aktualSort + 1;

            $zaznamPod = $database->where(RestMenuSkupinyManager::columnSort,$newSort)->fetch();

            if ($zaznamPod){
                $zaznamPod->update([RestMenuSkupinyManager::columnSort => $aktualSort]);
            }
            $zaznam->update([RestMenuSkupinyManager::columnSort => $newSort]);
        }
    }

    /**
     * srovná pořadí
     * @param $restMenuId
     */
    public function resort($restMenuId): void
    {
        $zaznamy = $this->restMenuSkupinyManager->select()
            ->where(RestMenuSkupinyManager::columnRestMenuId, $restMenuId)
            ->order(RestMenuSkupinyManager::columnSort.' ASC')
            ->fetchAll();

        $i = 1;

        foreach ($zaznamy as $one){
            $one->update([RestMenuSkupinyManager::columnSort => $i]);
            $i++;
        }
    }
}