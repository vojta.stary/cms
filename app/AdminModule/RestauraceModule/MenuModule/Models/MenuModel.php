<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Models;

use App\Managers\RestMenuManager;
use App\Managers\SetExpMenuBodyManager;
use App\Managers\SetExpMenuDokuManager;
use App\Managers\SetExpMenuHlavManager;
use App\Managers\SetExpMenuPatManager;
use Nette\Utils\FileSystem;

final class MenuModel
{
    const imageDir = __DIR__.'/../../../../../www/src/restaurace/menu/';

    private $restMenuManager;
    private $setExpMenuBodyManager;
    private $setExpMenuDokuManager;
    private $setExpMenuHlavManager;
    private $setExpMenuPatManager;

    public function __construct(RestMenuManager         $restMenuManager,
                                SetExpMenuBodyManager   $setExpMenuBodyManager,
                                SetExpMenuDokuManager   $setExpMenuDokuManager,
                                SetExpMenuHlavManager   $setExpMenuHlavManager,
                                SetExpMenuPatManager    $setExpMenuPatManager)
    {
        $this->restMenuManager = $restMenuManager;
        $this->setExpMenuBodyManager = $setExpMenuBodyManager;
        $this->setExpMenuDokuManager = $setExpMenuDokuManager;
        $this->setExpMenuHlavManager = $setExpMenuHlavManager;
        $this->setExpMenuPatManager = $setExpMenuPatManager;
    }

    public function create($values): void
    {
        $newId = $this->restMenuManager->create($values);
        $this->setExpMenuBodyManager->create([SetExpMenuBodyManager::columnRestMenuId => $newId]);
        $this->setExpMenuDokuManager->create([SetExpMenuDokuManager::columnRestMenuId => $newId]);
        $this->setExpMenuHlavManager->create([SetExpMenuHlavManager::columnRestMenuId => $newId]);
        $this->setExpMenuPatManager->create([SetExpMenuPatManager::columnRestMenuId => $newId]);
    }

    public function delete(int $menuId): void
    {
        $menu = $this->restMenuManager->getOne($menuId);

        if ($menu) {
            $dir = self::imageDir.$menuId;

            if (is_dir($dir)) {
                FileSystem::delete($dir);
            }
            $menu->delete();
        }
    }
}