<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Models;

use App\Managers\RestMenuSkupinyPolozkyManager;

final class PolozkyModel
{
    private $restMenuSkupinyPolozkyManager;

    public function __construct(RestMenuSkupinyPolozkyManager $restMenuSkupinyPolozkyManager)
    {
        $this->restMenuSkupinyPolozkyManager = $restMenuSkupinyPolozkyManager;
    }

    /**
     * ukládá novou skupinu
     * @param $values
     * @return void
     */
    public function create($values): void
    {
        $count = $this->restMenuSkupinyPolozkyManager->select()->where(RestMenuSkupinyPolozkyManager::columnRestMenuSkupinyId, $values->rest_menu_skupiny_id)->count();
        $sort = $count + 1;

        $values->sort = $sort;

        $this->restMenuSkupinyPolozkyManager->create($values);
    }

    /**
     * maže skupinu a přepočítá řazení
     * @param int $id
     * @return void
     */
    public function delete(int $id): void
    {
        $skupina = $this->restMenuSkupinyPolozkyManager->getOne($id);

        $restMenuSkupinyId = $skupina->rest_menu_skupiny_id;

        $skupina->delete();
        $this->resort($restMenuSkupinyId);
    }

    /**
     * upravuje pořadí směrem nahoru
     * @param int $id
     * @param int $restMenuSkupinyId
     * @return void
     */
    public function sortUp(int $id, int $restMenuSkupinyId): void
    {
        $database = $this->restMenuSkupinyPolozkyManager->select()->where(RestMenuSkupinyPolozkyManager::columnRestMenuSkupinyId, $restMenuSkupinyId);

        $zaznam = $this->restMenuSkupinyPolozkyManager->getOne($id);

        if ($zaznam->sort != 1){
            $aktualSort = $zaznam->sort;
            $newSort = $aktualSort - 1;

            $zaznamPod = $database->where(RestMenuSkupinyPolozkyManager::columnSort,$newSort)->fetch();

            if ($zaznamPod){
                $zaznamPod->update([RestMenuSkupinyPolozkyManager::columnSort => $aktualSort]);
            }
            $zaznam->update([RestMenuSkupinyPolozkyManager::columnSort => $newSort]);
        }
    }

    /**
     * upravuje pořadí směrem dolu
     * @param int $id
     * @param int $restMenuId
     * @return void
     */
    public function sortDown(int $id, int $restMenuSkupinyId): void
    {
        $database = $this->restMenuSkupinyPolozkyManager->select()->where(RestMenuSkupinyPolozkyManager::columnRestMenuSkupinyId, $restMenuSkupinyId);

        $zaznam = $this->restMenuSkupinyPolozkyManager->getOne($id);

        $pocetZaznamu = $database->count();

        if ($zaznam->sort != $pocetZaznamu){
            $aktualSort = $zaznam->sort;
            $newSort = $aktualSort + 1;

            $zaznamPod = $database->where(RestMenuSkupinyPolozkyManager::columnSort,$newSort)->fetch();

            if ($zaznamPod){
                $zaznamPod->update([RestMenuSkupinyPolozkyManager::columnSort => $aktualSort]);
            }
            $zaznam->update([RestMenuSkupinyPolozkyManager::columnSort => $newSort]);
        }
    }

    /**
     * srovná pořadí
     * @param $restMenuSkupinyId
     */
    public function resort($restMenuSkupinyId): void
    {
        $zaznamy = $this->restMenuSkupinyPolozkyManager->select()
            ->where(RestMenuSkupinyPolozkyManager::columnRestMenuSkupinyId, $restMenuSkupinyId)
            ->order(RestMenuSkupinyPolozkyManager::columnSort.' ASC')
            ->fetchAll();

        $i = 1;

        foreach ($zaznamy as $one){
            $one->update([RestMenuSkupinyPolozkyManager::columnSort => $i]);
            $i++;
        }
    }
}