<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\VymazMenu;

use App\AdminModule\RestauraceModule\MenuModule\Models\MenuModel;
use App\Managers\RestMenuManager;
use Nette\Application\UI\Form;

class VymazMenuControl extends \Nette\Application\UI\Control
{
    const template = __DIR__ . '/templates/template.latte';

    private $restMenuManager;
    private $menuModel;
    private $menuId;

    public function __construct(RestMenuManager $restMenuManager,
                                MenuModel       $menuModel,
                                                $menuId)
    {
        $this->restMenuManager = $restMenuManager;
        $this->menuModel = $menuModel;
        $this->menuId = $menuId;
    }

    public function render()
    {
        $data = $this->restMenuManager->getOne($this->menuId);

        $this['vymazMenuForm']->setDefaults($data);
        $this->template->data = $data;
        $this->template->render(self::template);
    }

    /*=================== formulář ===================*/

    protected function createComponentVymazMenuForm(): Form
    {
        $form = new Form;
        $form->addHidden('id');
        $form->addSubmit('send', 'Vymazat');
        $form->onSuccess[] = [$this, 'sendVymazMenuForm'];
        return $form;
    }

    public function sendVymazMenuForm(Form $form, $values)
    {
        try {
        $this->menuModel->delete($values->id);
        $this->presenter->flashMessage('Data byla vymazána','success');
        $this->presenter->redirect(':Admin:Restaurace:Menu:Seznam:default');
        }catch (\PDOException $e){
            $form->setDefaults($values);
            $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
            $this->redirect('this');
        }
    }
}