<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\VymazMenu;

use App\AdminModule\RestauraceModule\MenuModule\Models\MenuModel;
use App\Managers\RestMenuManager;

class VymazMenuFactory
{
    private $restMenuManager;
    private $menuModel;
    private $menuId;

    public function __construct(RestMenuManager $restMenuManager,
                                MenuModel       $menuModel)
    {
        $this->restMenuManager = $restMenuManager;
        $this->menuModel = $menuModel;
    }

    public function create()
    {
        return new VymazMenuControl($this->restMenuManager,
                                    $this->menuModel,
                                    $this->menuId);
    }

    public function setMenu(int $id)
    {
        $this->menuId = $id;
    }
}