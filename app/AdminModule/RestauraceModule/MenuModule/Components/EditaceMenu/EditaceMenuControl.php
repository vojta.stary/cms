<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\EditaceMenu;

use App\Managers\RestMenuManager;
use Nette\Application\UI\Form;

class EditaceMenuControl extends \Nette\Application\UI\Control
{
    const template = __DIR__ . '/templates/template.latte';

    private $restMenuManager;
    private $menuId;

    public function __construct(RestMenuManager $restMenuManager,
                                                $menuId)
    {
        $this->restMenuManager = $restMenuManager;
        $this->menuId = $menuId;
    }

    public function render()
    {
        $data = $this->restMenuManager->getOne($this->menuId);

        $this['editMenuForm']->setDefaults($data);
        $this->template->render(self::template);
    }

    /*=================== formulář ===================*/

    protected function createComponentEditMenuForm(): Form
    {
        $form = new Form;
        $form->addText('nazev', 'Název')
            ->setRequired('Musíte vyplnit název!');
        $form->addHidden('id');
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = [$this, 'sendEditMenuForm'];
        return $form;
    }

    public function sendEditMenuForm(Form $form, $values)
    {
        try {
            $this->restMenuManager->update($values->id,$values);
            $this->presenter->flashMessage('Data byla uložena','success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $form->setDefaults($values);
            $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
            $this->redirect('this');
        }
    }
}