<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\EditaceMenu;

use App\Managers\RestMenuManager;

class EditaceMenuFactory
{
    private $restMenuManager;
    private $menuId;

    public function __construct(RestMenuManager $restMenuManager)
    {
        $this->restMenuManager = $restMenuManager;
    }

    public function create()
    {
        return new EditaceMenuControl(  $this->restMenuManager,
                                        $this->menuId);
    }

    public function setMenu(int $id)
    {
        $this->menuId = $id;
    }
}