<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\EditaceSkupina;

use App\Managers\RestMenuSkupinyManager;
use Nette\Application\UI\Form;

class EditaceSkupinaControl extends \Nette\Application\UI\Control
{
    const template = __DIR__ . '/templates/template.latte';

    private $restMenuSkupinyManager;
    private $skupinaId;

    public function __construct(RestMenuSkupinyManager      $restMenuSkupinyManager,
                                                            $skupinaId)
    {
        $this->restMenuSkupinyManager = $restMenuSkupinyManager;
        $this->skupinaId = $skupinaId;
    }

    public function render()
    {
        $skupina = $this->restMenuSkupinyManager->getOne($this->skupinaId);

        $this['editSkupinaForm']->setDefaults($skupina);
        $this->template->skupina = $skupina;
        $this->template->render(self::template);
    }

    /*=================== formulář ===================*/

    protected function createComponentEditSkupinaForm(): Form
    {
        $form = new Form;
        $form->addText('nazev', 'Název')
            ->setRequired('Musíte vyplnit název!');
        $form->addHidden('id');
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = [$this, 'sendEditSkupinaForm'];
        return $form;
    }

    public function sendEditSkupinaForm(Form $form, $values)
    {
        try {
        $this->restMenuSkupinyManager->update($values->id,$values);
        $this->presenter->flashMessage('Data byla uložena','success');
        $this->redirect('this');
        }catch (\PDOException $e){
            $form->setDefaults($values);
            $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
            $this->redirect('this');
        }
    }
}