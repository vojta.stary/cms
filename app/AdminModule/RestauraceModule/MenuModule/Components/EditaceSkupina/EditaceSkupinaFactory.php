<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\EditaceSkupina;

use App\AdminModule\RestauraceModule\MenuModule\Components\EditaceSkupina\EditaceSkupinaControl;
use App\Managers\RestMenuSkupinyManager;

class EditaceSkupinaFactory
{
    private $restMenuSkupinyManager;
    private $skupinaId;

    public function __construct(RestMenuSkupinyManager      $restMenuSkupinyManager)
    {
        $this->restMenuSkupinyManager = $restMenuSkupinyManager;
    }

    public function create()
    {
        return new EditaceSkupinaControl(   $this->restMenuSkupinyManager,
                                            $this->skupinaId);
    }

    public function setSkupina(int $id)
    {
        $this->skupinaId = $id;
    }
}