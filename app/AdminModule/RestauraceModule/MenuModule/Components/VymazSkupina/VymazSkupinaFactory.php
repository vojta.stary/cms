<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\VymazSkupina;

use App\AdminModule\RestauraceModule\MenuModule\Models\SkupinaModel;
use App\AdminModule\RestauraceModule\MenuModule\Components\VymazSkupina\VymazSkupinaControl;
use App\Managers\RestMenuSkupinyManager;

class VymazSkupinaFactory
{
    private $skupinaModel;
    private $restMenuSkupinyManager;
    private $skupinaId;

    public function __construct(RestMenuSkupinyManager      $restMenuSkupinyManager,
                                SkupinaModel                $skupinaModel)
    {
        $this->restMenuSkupinyManager = $restMenuSkupinyManager;
        $this->skupinaModel = $skupinaModel;
    }

    public function create(){
        return new VymazSkupinaControl( $this->restMenuSkupinyManager,
                                        $this->skupinaModel,
                                        $this->skupinaId);
    }

    public function setSkupina(int $id)
    {
        $this->skupinaId = $id;
    }
}