<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\VymazSkupina;

use App\AdminModule\RestauraceModule\MenuModule\Models\SkupinaModel;
use App\Managers\RestMenuSkupinyManager;
use Nette\Application\UI\Form;

class VymazSkupinaControl extends \Nette\Application\UI\Control
{
    const template = __DIR__ . '/templates/template.latte';

    private $skupinaModel;
    private $restMenuSkupinyManager;
    private $skupinaId;

    public function __construct(RestMenuSkupinyManager      $restMenuSkupinyManager,
                                SkupinaModel                $skupinaModel,
                                                            $skupinaId)
    {
        $this->restMenuSkupinyManager = $restMenuSkupinyManager;
        $this->skupinaModel = $skupinaModel;
        $this->skupinaId = $skupinaId;
    }

    public function render()
    {
        $skupina = $this->restMenuSkupinyManager->getOne($this->skupinaId);

        $this['vymazSkupinaForm']->setDefaults($skupina);
        $this->template->data = $skupina;
        $this->template->render(self::template);
    }

    /*=================== formulář ===================*/

    protected function createComponentVymazSkupinaForm(): Form
    {
        $form = new Form;
        $form->addHidden('id');
        $form->addSubmit('send', 'Vymazat');
        $form->onSuccess[] = [$this, 'sendVymazSkupinaForm'];
        return $form;
    }

    public function sendVymazSkupinaForm(Form $form, $values)
    {
        try {
        $this->skupinaModel->delete($values->id);
        $this->presenter->flashMessage('Data byla vymazána','success');
        $this->redirect('this');
        }catch (\PDOException $e){
            $form->setDefaults($values);
            $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
            $this->redirect('this');
        }
    }
}