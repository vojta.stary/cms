<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\NovaSkupina;

use App\AdminModule\RestauraceModule\MenuModule\Components\NovaSkupina\NovaSkupinaControl;
use App\AdminModule\RestauraceModule\MenuModule\Models\SkupinaModel;

class NovaSkupinaFactory
{
    private $skupinaModel;
    private $restMenuId;

    public function __construct(SkupinaModel    $skupinaModel)
    {
        $this->skupinaModel = $skupinaModel;
    }

    public function create()
    {
        return new NovaSkupinaControl(  $this->skupinaModel,
                                        $this->restMenuId);
    }

    public function setMenu(int $id)
    {
        $this->restMenuId = $id;
    }
}