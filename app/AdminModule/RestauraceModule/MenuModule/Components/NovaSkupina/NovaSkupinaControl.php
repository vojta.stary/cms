<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\NovaSkupina;

use App\AdminModule\RestauraceModule\MenuModule\Models\SkupinaModel;
use Nette\Application\UI\Form;

class NovaSkupinaControl extends \Nette\Application\UI\Control
{
    const template = __DIR__ . '/templates/template.latte';

    private $skupinaModel;
    private $restMenuId;

    public function __construct(SkupinaModel    $skupinaModel,
                                                $restMenuId)
    {
        $this->skupinaModel = $skupinaModel;
        $this->restMenuId = $restMenuId;
    }

    public function render()
    {
        $this->template->render(self::template);
    }

    /*=================== formulář ===================*/

    protected function createComponentNovaSkupinaForm(): Form
    {
        $form = new Form;
        $form->addText('nazev', 'Název')
            ->setRequired('Musíte vyplnit název!');
        $form->addHidden('rest_menu_id')
            ->setDefaultValue($this->restMenuId);
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = [$this, 'sendNovaSkupinaForm'];
        return $form;
    }

    public function sendNovaSkupinaForm(Form $form, $values)
    {
        try {
            $this->skupinaModel->create($values);
            $this->presenter->flashMessage('Data byla uložena','success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $form->setDefaults($values);
            $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
            $this->redirect('this');
        }
    }
}