<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\ExportMenu;

use App\AdminModule\RestauraceModule\MenuModule\Models\ExportMenuModel;
use App\Managers\RestMenuManager;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;

class ExportMenuControl extends Control
{
    const template = __DIR__ . '/templates/template.latte';

    private $exportMenuModel;
    private $restMenuManager;
    private $menuId;

    public function __construct(ExportMenuModel $exportMenuModel,
                                RestMenuManager $restMenuManager,
                                                $menuId)
    {
        $this->exportMenuModel = $exportMenuModel;
        $this->restMenuManager = $restMenuManager;
        $this->menuId = $menuId;
    }

    public function render()
    {
        $this->template->render(self::template);
    }

    /*================================== handle metody ==================================*/
    public function handleExportMenu()
    {
        $menu = $this->restMenuManager->getOne($this->menuId);

        $nazev = Strings::webalize($menu->nazev);

        $pdf = $this->exportMenuModel->export($this->menuId);
        $pdf->Output($nazev.'.pdf','D');
    }
}