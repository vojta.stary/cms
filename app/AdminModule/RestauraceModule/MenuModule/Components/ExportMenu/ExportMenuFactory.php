<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\ExportMenu;

use App\AdminModule\RestauraceModule\MenuModule\Models\ExportMenuModel;
use App\Managers\RestMenuManager;

class ExportMenuFactory
{
    private $exportMenuModel;
    private $restMenuManager;
    private $menuId;

    public function __construct(ExportMenuModel $exportMenuModel,
                                RestMenuManager $restMenuManager)
    {
        $this->exportMenuModel = $exportMenuModel;
        $this->restMenuManager = $restMenuManager;
    }

    public function create()
    {
        return new ExportMenuControl(   $this->exportMenuModel,
                                        $this->restMenuManager,
                                        $this->menuId);
    }

    public function setMenu(int $id)
    {
        $this->menuId = $id;
    }
}
