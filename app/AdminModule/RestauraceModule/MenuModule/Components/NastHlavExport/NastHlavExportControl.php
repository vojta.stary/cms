<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\NastHlavExport;

use App\AdminModule\RestauraceModule\MenuModule\Models\NastExportMenuModel;
use App\Managers\SetExpMenuHlavManager;
use Nette\Application\UI\Form;

class NastHlavExportControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/templates/template.latte';

    private $setExpMenuHlavManager;
    private $nastExportMenuModel;
    private $menuId;

    public function __construct(SetExpMenuHlavManager   $setExpMenuHlavManager,
                                NastExportMenuModel     $nastExportMenuModel,
                                                        $menuId)
    {
        $this->setExpMenuHlavManager = $setExpMenuHlavManager;
        $this->nastExportMenuModel = $nastExportMenuModel;
        $this->menuId = $menuId;
    }

    public function render()
    {
        $data = $this->setExpMenuHlavManager->select()
            ->where(SetExpMenuHlavManager::columnRestMenuId, $this->menuId)
            ->fetch();

        $this->template->menuId = $this->menuId;
        $this->template->data = $data;
        $this->template->render(self::template);
    }

    /*============================= formuláře =============================*/
    /**
     * nahrání loga
     * @return Form
     */
    protected function createComponentPridatLogoForm(): Form
    {
        $form = new Form;
        $form->addHidden('rest_menu_id')
            ->setDefaultValue($this->menuId);
        $form->addUpload('logo')
            ->addRule($form::IMAGE, 'Pozadí musí být JPEG, PNG, GIF nebo WebP.')
            ->setRequired('Musíte vypbrat obrázek!');
        $form->addSubmit('send','Nahrát');
        $form->onSuccess[] = [$this, 'sendPridatLogoForm'];
        return $form;
    }

    public function sendPridatLogoForm(Form $form, $values)
    {
        if ($this->presenter->isAjax()) {
            $this->nastExportMenuModel->saveLogoMenu($values);
            $this->presenter->flashMessage('Logo bylo nahráno', 'success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('list');
        }
    }

    /**
     * změna barvy textu
     * @return Form
     */
    protected function createComponentBarvaTextForm(): Form
    {
        $form = new Form;
        $form->addHidden('barva');
        $form->addHidden('id');
        $form->addSubmit('send','Změnit');
        $form->onSuccess[] = [$this, 'sendBarvaTextForm'];
        return $form;
    }

    public function sendBarvaTextForm(Form $form, $values)
    {
        if ($this->presenter->isAjax()) {
            $data = [SetExpMenuHlavManager::columnColor => $values->barva];
            $this->setExpMenuHlavManager->update($values->id, $data);
            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('list');
        }
    }

    /**
     * změna barvy textu
     * @return Form
     */
    protected function createComponentBarvaPozadiForm(): Form
    {
        $form = new Form;
        $form->addHidden('barva');
        $form->addHidden('id');
        $form->addSubmit('send','Změnit');
        $form->onSuccess[] = [$this, 'sendBarvaPozadiForm'];
        return $form;
    }

    public function sendBarvaPozadiForm(Form $form, $values)
    {
        if ($this->presenter->isAjax()) {
            $data = [SetExpMenuHlavManager::columnBgColor => $values->barva];
            $this->setExpMenuHlavManager->update($values->id, $data);
            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('list');
        }
    }

    /*============================= handle metody =============================*/
    /**
     * ukládá data z inputu
     * @param array $data
     * @return void
     */
    public function handleSaveData(array $data): void
    {
        if ($this->presenter->isAjax()) {

            $values = [$data['column'] => $data['value']];
            $this->setExpMenuHlavManager->update($data['id'],$values);

            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('list');
        }
    }

    /**
     * vymazání loga
     * @param $setId
     * @return void
     */
    public function handleDeleteLogo($setId): void
    {
        if ($this->presenter->isAjax()) {

            $this->nastExportMenuModel->deleteLogo($setId);

            $this->presenter->flashMessage('Logo bylo vymazáno', 'success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('list');
        }
    }
}