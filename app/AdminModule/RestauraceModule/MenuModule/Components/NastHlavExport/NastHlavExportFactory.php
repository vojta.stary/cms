<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\NastHlavExport;

use App\AdminModule\RestauraceModule\MenuModule\Models\NastExportMenuModel;
use App\Managers\SetExpMenuHlavManager;

class NastHlavExportFactory
{
    private $setExpMenuHlavManager;
    private $nastExportMenuModel;
    private $menuId;

    public function __construct(SetExpMenuHlavManager   $setExpMenuHlavManager,
                                NastExportMenuModel     $nastExportMenuModel)
    {
        $this->setExpMenuHlavManager = $setExpMenuHlavManager;
        $this->nastExportMenuModel = $nastExportMenuModel;
    }

    public function create()
    {
        return new NastHlavExportControl(   $this->setExpMenuHlavManager,
                                            $this->nastExportMenuModel,
                                            $this->menuId);
    }

    public function setMenu(int $id)
    {
        $this->menuId = $id;
    }
}