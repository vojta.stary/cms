<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\PolozkySkupina;

use App\AdminModule\RestauraceModule\MenuModule\Models\PolozkyModel;
use App\Managers\RestMenuSkupinyPolozkyManager;

class PolozkySkupinaFactory
{
    private $restMenuSkupinyPolozkyManager;
    private $polozkyModel;
    private $restMenuSkupinaId;

    public function __construct(RestMenuSkupinyPolozkyManager   $restMenuSkupinyPolozkyManager,
                                PolozkyModel                    $polozkyModel)
    {
        $this->restMenuSkupinyPolozkyManager = $restMenuSkupinyPolozkyManager;
        $this->polozkyModel = $polozkyModel;
    }

    public function create()
    {
        return new PolozkySkupinaControl($this->restMenuSkupinyPolozkyManager,
                                        $this->polozkyModel,
                                        $this->restMenuSkupinaId);
    }

    public function setSkupina(int $id)
    {
        $this->restMenuSkupinaId = $id;
    }
}