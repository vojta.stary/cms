<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\PolozkySkupina;

use App\AdminModule\RestauraceModule\MenuModule\Models\PolozkyModel;
use App\Managers\RestMenuSkupinyPolozkyManager;
use Nette\Application\UI\Form;

class PolozkySkupinaControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/templates/template.latte';

    private $restMenuSkupinyPolozkyManager;
    private $polozkyModel;
    private $restMenuSkupinaId;

    public function __construct(RestMenuSkupinyPolozkyManager   $restMenuSkupinyPolozkyManager,
                                PolozkyModel                    $polozkyModel,
                                                                $restMenuSkupinaId)
    {
        $this->restMenuSkupinyPolozkyManager = $restMenuSkupinyPolozkyManager;
        $this->polozkyModel = $polozkyModel;
        $this->restMenuSkupinaId = $restMenuSkupinaId;
    }

    public function render()
    {
        $polozky = $this->restMenuSkupinyPolozkyManager->select()
            ->where(RestMenuSkupinyPolozkyManager::columnRestMenuSkupinyId,$this->restMenuSkupinaId)
            ->order(RestMenuSkupinyPolozkyManager::columnSort.' ASC');

        $this->template->count = $polozky->count();
        $this->template->polozky = $polozky->fetchAll();
        $this->template->skupinaId = $this->restMenuSkupinaId;
        $this->template->render(self::template);
    }

    /*=================== hanle metody ===================*/
    /**
     * ukládá změněná data
     * @param array $data
     * @return void
     */
    public function handleSaveData(array $data)
    {
        if ($this->presenter->isAjax()){
            try {
                $this->restMenuSkupinyPolozkyManager->update($data['id'],[$data['column'] => $data['value']]);
                $this->presenter->flashMessage('Data byla uložena','success');
                $this->presenter->redrawControl('flash');
                $this->redrawControl('polozkyList');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
                $this->presenter->redrawControl('flash');
            }
        }
    }

    /**
     * změna aktivace
     * @param $id
     * @return void
     */
    public function handleChangeActive($id)
    {
        if ($this->presenter->isAjax()) {
            $this->restMenuSkupinyPolozkyManager->changeActive($id);
            $this->redrawControl('polozkyList');
        }
    }

    /**
     * změna pořádí nahoru
     * @param $id
     * @return void
     */
    public function handleSortUp($id)
    {
        if ($this->presenter->isAjax()) {
            $this->polozkyModel->sortUp($id,$this->restMenuSkupinaId);
            $this->redrawControl('polozkyList');
        }
    }

    /**
     * změna pořadí dolů
     * @param $id
     * @return void
     */
    public function handleSortDown($id)
    {
        if ($this->presenter->isAjax()) {
            $this->polozkyModel->sortDown($id,$this->restMenuSkupinaId);
            $this->redrawControl('polozkyList');
        }
    }

    /**
     * vymazání položky
     * @param $id
     * @return void
     */
    public function handleDelete($id)
    {
        if ($this->presenter->isAjax()) {
            $this->polozkyModel->delete($id);
            $this->presenter->flashMessage('Položka byla vymazána','success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('polozkyList');
        }
    }

    /*=================== formulář nové položky ===================*/

    protected function createComponentNovaPolozkaForm(): Form
    {
        $form = new Form;
        $form->addText('nazev', 'Název')
            ->setRequired('Musíte vyplnit název!');
        $form->addText('alergen', 'Alergeny');
        $form->addText('mnozstvi', 'Množství');
        $form->addText('cena', 'Cena')
            ->setRequired('Musíte vyplnit cenu!');
        $form->addTextArea('popis', 'Popis');
        $form->addHidden('rest_menu_skupiny_id')
            ->setDefaultValue($this->restMenuSkupinaId);
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = [$this, 'sendNovaPolozkaForm'];
        return $form;
    }

    public function sendNovaPolozkaForm(Form $form, $values)
    {
        if ($this->presenter->isAjax()) {
            try {
                $this->polozkyModel->create($values);
                $form->reset();
                $form['rest_menu_skupiny_id']->setDefaultValue($this->restMenuSkupinaId);
                $this->presenter->flashMessage('Data byla uložena','success');
                $this->presenter->redrawControl('flash');
                $this->redrawControl('formular-wrapper');
                $this->redrawControl('formular');
                $this->redrawControl('polozkyList');
                $this->redrawControl('scripts');
            }catch (\PDOException $e){
                $form->setDefaults($values);
                $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
                $this->presenter->redrawControl('flash');
                $this->redrawControl('polozkyList');
            }
        }
    }
}