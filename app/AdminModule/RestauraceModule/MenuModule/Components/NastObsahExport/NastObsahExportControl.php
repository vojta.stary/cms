<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\NastObsahExport;

use App\Managers\SetExpMenuBodyManager;

class NastObsahExportControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/templates/template.latte';

    private $setExpMenuBodyManager;
    private $menuId;

    public function __construct(SetExpMenuBodyManager   $setExpMenuBodyManager,
                                                        $menuId)
    {
        $this->setExpMenuBodyManager = $setExpMenuBodyManager;
        $this->menuId = $menuId;
    }

    public function render()
    {
        $data = $this->setExpMenuBodyManager->select()
            ->where(SetExpMenuBodyManager::columnRestMenuId, $this->menuId)
            ->fetch();

        $this->template->menuId = $this->menuId;
        $this->template->data = $data;
        $this->template->render(self::template);
    }

    /*============================= handle metody =============================*/
    /**
     * ukládá data z inputu
     * @param array $data
     * @return void
     */
    public function handleSaveData(array $data): void
    {
        if ($this->presenter->isAjax()) {

            $values = [$data['column'] => $data['value']];
            $this->setExpMenuBodyManager->update($data['id'],$values);

            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('list');
        }
    }
}