<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\NastObsahExport;

use App\Managers\SetExpMenuBodyManager;

class NastObsahExportFactory
{
    private $setExpMenuBodyManager;
    private $menuId;

    public function __construct(SetExpMenuBodyManager   $setExpMenuBodyManager)
    {
        $this->setExpMenuBodyManager = $setExpMenuBodyManager;
    }

    public function create()
    {
        return new NastObsahExportControl(  $this->setExpMenuBodyManager,
                                            $this->menuId);
    }

    public function setMenu(int $id)
    {
        $this->menuId = $id;
    }
}