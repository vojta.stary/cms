<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\NoveMenu;

use App\AdminModule\RestauraceModule\MenuModule\Models\MenuModel;

class NoveMenuFactory
{
    private $menuModel;

    public function __construct(MenuModel $menuModel)
    {
        $this->menuModel = $menuModel;
    }

    public function create()
    {
        return new NoveMenuControl($this->menuModel);
    }
}