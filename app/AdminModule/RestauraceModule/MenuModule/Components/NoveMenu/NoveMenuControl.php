<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\NoveMenu;

use App\AdminModule\RestauraceModule\MenuModule\Models\MenuModel;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class NoveMenuControl extends Control
{
    const template = __DIR__ . '/templates/template.latte';

    private $menuModel;

    public function __construct(MenuModel $menuModel)
    {
        $this->menuModel = $menuModel;
    }

    public function render()
    {
        $this->template->render(self::template);
    }

    /*=================== formulář ===================*/

    protected function createComponentNoveMenuForm(): Form
    {
        $typ = ['napoj' => 'Nápojové menu', 'jidlo' => 'Jídelní menu'];

        $form = new Form;
        $form->addText('nazev', 'Název')
            ->setRequired('Musíte vyplnit název!');
        $form->addSelect('typ','Typ menu', $typ)
            ->setPrompt('Vyberte možnost')
            ->setRequired('Musíte vybrat typ menu!');
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = [$this, 'sendNoveMenuForm'];
        return $form;
    }

    public function sendNoveMenuForm(Form $form, $values)
    {
        try {
            $this->menuModel->create($values);
            $this->presenter->flashMessage('Data byla uložena','success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $form->setDefaults($values);
            $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
            $this->redirect('this');
        }
    }
}