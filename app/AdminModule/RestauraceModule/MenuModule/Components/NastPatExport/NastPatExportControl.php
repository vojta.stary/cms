<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\NastPatExport;

use App\Managers\SetExpMenuPatManager;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

class NastPatExportControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/templates/template.latte';

    private $setExpMenuPatManager;
    private $menuId;

    public function __construct(SetExpMenuPatManager    $setExpMenuPatManager,
                                                        $menuId)
    {
        $this->setExpMenuPatManager = $setExpMenuPatManager;
        $this->menuId = $menuId;
    }

    public function render()
    {
        $data = $this->setExpMenuPatManager->select()
            ->where(SetExpMenuPatManager::columnRestMenuId, $this->menuId)
            ->fetch();

        $this['patickaTextForm']->setDefaults($data);
        $this->template->menuId = $this->menuId;
        $this->template->data = $data;
        $this->template->render(self::template);
    }

    /*============================= formuláře =============================*/
    /**
     * změna barvy textu
     * @return Form
     */
    protected function createComponentBarvaTextForm(): Form
    {
        $form = new Form;
        $form->addHidden('barva');
        $form->addHidden('id');
        $form->addSubmit('send','Změnit');
        $form->onSuccess[] = [$this, 'sendBarvaTextForm'];
        return $form;
    }

    public function sendBarvaTextForm(Form $form, $values)
    {
        if ($this->presenter->isAjax()) {
            $data = [SetExpMenuPatManager::columnColor => $values->barva];
            $this->setExpMenuPatManager->update($values->id, $data);
            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('list');
        }
    }

    /**
     * změna barvy textu
     * @return Form
     */
    protected function createComponentBarvaPozadiForm(): Form
    {
        $form = new Form;
        $form->addHidden('barva');
        $form->addHidden('id');
        $form->addSubmit('send','Změnit');
        $form->onSuccess[] = [$this, 'sendBarvaPozadiForm'];
        return $form;
    }

    public function sendBarvaPozadiForm(Form $form, $values)
    {
        if ($this->presenter->isAjax()) {
            $data = [SetExpMenuPatManager::columnBgColor => $values->barva];
            $this->setExpMenuPatManager->update($values->id, $data);
            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('list');
        }
    }

    /**
     * změna textu
     * @return Form
     */
    protected function createComponentPatickaTextForm(): Form
    {
        $form = new Form;
        $form->addTextArea('text');
        $form->addHidden('id');
        $form->addSubmit('send','Změnit');
        $form->onSuccess[] = [$this, 'sendPatickaTextForm'];
        return $form;
    }

    public function sendPatickaTextForm(Form $form, $values)
    {
        if ($this->presenter->isAjax()) {
            $data = [SetExpMenuPatManager::columnText => $values->text];
            $this->setExpMenuPatManager->update($values->id, $data);
            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('list');
        }
    }

    /*============================= handle metody =============================*/
    /**
     * ukládá data z inputu
     * @param array $data
     * @return void
     */
    public function handleSaveData(array $data): void
    {
        if ($this->presenter->isAjax()) {

            $values = [$data['column'] => $data['value']];
            $this->setExpMenuPatManager->update($data['id'],$values);

            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('list');
        }
    }
}