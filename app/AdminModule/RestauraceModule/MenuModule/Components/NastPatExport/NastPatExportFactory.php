<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\NastPatExport;

use App\Managers\SetExpMenuPatManager;

class NastPatExportFactory
{
    private $setExpMenuPatManager;
    private $menuId;

    public function __construct(SetExpMenuPatManager    $setExpMenuPatManager)
    {
        $this->setExpMenuPatManager = $setExpMenuPatManager;
    }

    public function create()
    {
        return new NastPatExportControl($this->setExpMenuPatManager,
                                        $this->menuId);
    }

    public function setMenu(int $id)
    {
        $this->menuId = $id;
    }
}