<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\NastDokuExport;

use App\AdminModule\RestauraceModule\MenuModule\Models\NastExportMenuModel;
use App\Managers\SetExpMenuDokuManager;

class NastDokuExportFactory
{
    private $setExpMenuDokuManager;
    private $nastExportMenuModel;
    private $menuId;

    public function __construct(SetExpMenuDokuManager   $setExpMenuDokuManager,
                                NastExportMenuModel     $nastExportMenuModel)
    {
        $this->setExpMenuDokuManager = $setExpMenuDokuManager;
        $this->nastExportMenuModel = $nastExportMenuModel;
    }

    public function create()
    {
        return new NastDokuExportControl(   $this->setExpMenuDokuManager,
                                            $this->nastExportMenuModel,
                                            $this->menuId);
    }

    public function setMenu(int $id)
    {
        $this->menuId = $id;
    }
}