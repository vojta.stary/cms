<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\NastDokuExport;

use App\AdminModule\RestauraceModule\MenuModule\Models\NastExportMenuModel;
use App\Managers\SetExpMenuDokuManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class NastDokuExportControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $setExpMenuDokuManager;
    private $nastExportMenuModel;
    private $menuId;

    public function __construct(SetExpMenuDokuManager   $setExpMenuDokuManager,
                                NastExportMenuModel     $nastExportMenuModel,
                                                        $menuId)
    {
        $this->setExpMenuDokuManager = $setExpMenuDokuManager;
        $this->nastExportMenuModel = $nastExportMenuModel;
        $this->menuId = $menuId;
    }

    public function render()
    {
        $data = $this->setExpMenuDokuManager->select()
            ->where(SetExpMenuDokuManager::columnRestMenuId, $this->menuId)
            ->fetch();

        $this->template->menuId = $this->menuId;
        $this->template->data = $data;
        $this->template->render(self::template);
    }

    /*============================= formuláře =============================*/

    /**
     * nahrání pozadí
     * @return Form
     */
    protected function createComponentPridatPozadiForm(): Form
    {
        $form = new Form;
        $form->addHidden('rest_menu_id')
            ->setDefaultValue($this->menuId);
        $form->addUpload('pozadi')
            ->addRule($form::IMAGE, 'Pozadí musí být JPEG, PNG, GIF nebo WebP.')
            ->setRequired('Musíte vypbrat obrázek!');
        $form->addSubmit('send','Nahrát');
        $form->onSuccess[] = [$this, 'sendPridatPozadiForm'];
        return $form;
    }

    public function sendPridatPozadiForm(Form $form, $values)
    {
        if ($this->presenter->isAjax()) {
            $this->nastExportMenuModel->saveBgImageMenu($values);
            $this->presenter->flashMessage('Pozadí bylo nahráno', 'success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('list');
        }
    }

    /**
     * změna barvy pozadí
     * @return Form
     */
    protected function createComponentBarvaPozadiForm(): Form
    {
        $form = new Form;
        $form->addHidden('barva');
        $form->addHidden('id');
        $form->addSubmit('send','Změnit');
        $form->onSuccess[] = [$this, 'sendBarvaPozadiForm'];
        return $form;
    }

    public function sendBarvaPozadiForm(Form $form, $values)
    {
        if ($this->presenter->isAjax()) {
            $data = [SetExpMenuDokuManager::columnBgColor => $values->barva];
            $this->setExpMenuDokuManager->update($values->id, $data);
            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('list');
        }
    }

    /**
     * změna barvy pozadí
     * @return Form
     */
    protected function createComponentBarvaTextForm(): Form
    {
        $form = new Form;
        $form->addHidden('barva');
        $form->addHidden('id');
        $form->addSubmit('send','Změnit');
        $form->onSuccess[] = [$this, 'sendBarvaTextForm'];
        return $form;
    }

    public function sendBarvaTextForm(Form $form, $values)
    {
        if ($this->presenter->isAjax()) {
            $data = [SetExpMenuDokuManager::columnColor => $values->barva];
            $this->setExpMenuDokuManager->update($values->id, $data);
            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('list');
        }
    }

    /*============================= handle metody =============================*/
    /**
     * ukládá data z inputu
     * @param array $data
     * @return void
     */
    public function handleSaveData(array $data): void
    {
        if ($this->presenter->isAjax()) {

            $values = [$data['column'] => $data['value']];
            $this->setExpMenuDokuManager->update($data['id'],$values);

            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('list');
        }
    }

    /**
     * vymazání pozadí
     * @param $setId
     * @return void
     */
    public function handleDeleteBack($setId): void
    {
        if ($this->presenter->isAjax()) {

            $this->nastExportMenuModel->deleteImage($setId);

            $this->presenter->flashMessage('Pozadí bylo vymazáno', 'success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('list');
        }
    }
}