<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\NastaveniExport;

use App\AdminModule\RestauraceModule\MenuModule\Components\NastDokuExport\NastDokuExportFactory;
use App\AdminModule\RestauraceModule\MenuModule\Components\NastHlavExport\NastHlavExportFactory;
use App\AdminModule\RestauraceModule\MenuModule\Components\NastObsahExport\NastObsahExportFactory;
use App\AdminModule\RestauraceModule\MenuModule\Components\NastPatExport\NastPatExportFactory;

class NastaveniExportControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/templates/template.latte';

    private $nastDokuExportFactory;
    private $nastHlavExportFactory;
    private $nastPatExportFactory;
    private $nastObsahExportFactory;
    private $menuId;

    public function __construct(NastDokuExportFactory   $nastDokuExportFactory,
                                NastHlavExportFactory   $nastHlavExportFactory,
                                NastPatExportFactory    $nastPatExportFactory,
                                NastObsahExportFactory  $nastObsahExportFactory,
                                                        $menuId)
    {
        $this->nastDokuExportFactory = $nastDokuExportFactory;
        $this->nastHlavExportFactory = $nastHlavExportFactory;
        $this->nastPatExportFactory = $nastPatExportFactory;
        $this->nastObsahExportFactory = $nastObsahExportFactory;
        $this->menuId = $menuId;
    }

    public function render()
    {
        $this->template->render(self::template);
    }

    /*=================================== komponenty ===================================*/

    /**
     * nastavení dokumentu
     * @return \App\AdminModule\RestauraceModule\MenuModule\Components\NastDokuExport\NastDokuExportControl
     */
    protected function createComponentNastaveniDokument()
    {
        $this->nastDokuExportFactory->setMenu($this->menuId);
        return $this->nastDokuExportFactory->create();
    }

    /**
     * nastavení hlavičky
     * @return \App\AdminModule\RestauraceModule\MenuModule\Components\NastHlavExport\NastHlavExportControl
     */
    protected function createComponentNastaveniHlavicka()
    {
        $this->nastHlavExportFactory->setMenu($this->menuId);
        return $this->nastHlavExportFactory->create();
    }

    /**
     * nastavení patičky
     * @return \App\AdminModule\RestauraceModule\MenuModule\Components\NastPatExport\NastPatExportControl
     */
    protected function createComponentNastaveniPaticka()
    {
        $this->nastPatExportFactory->setMenu($this->menuId);
        return $this->nastPatExportFactory->create();
    }

    /**
     * nastavení obsahu
     * @return \App\AdminModule\RestauraceModule\MenuModule\Components\NastObsahExport\NastObsahExportControl
     */
    protected function createComponentNastaveniObsah()
    {
        $this->nastObsahExportFactory->setMenu($this->menuId);
        return $this->nastObsahExportFactory->create();
    }
}