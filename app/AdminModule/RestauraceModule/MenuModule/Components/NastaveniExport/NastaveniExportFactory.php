<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Components\NastaveniExport;

use App\AdminModule\RestauraceModule\MenuModule\Components\NastDokuExport\NastDokuExportFactory;
use App\AdminModule\RestauraceModule\MenuModule\Components\NastHlavExport\NastHlavExportFactory;
use App\AdminModule\RestauraceModule\MenuModule\Components\NastObsahExport\NastObsahExportFactory;
use App\AdminModule\RestauraceModule\MenuModule\Components\NastPatExport\NastPatExportFactory;

class NastaveniExportFactory
{
    private $nastDokuExportFactory;
    private $nastHlavExportFactory;
    private $nastPatExportFactory;
    private $nastObsahExportFactory;
    private $menuId;

    public function __construct(NastDokuExportFactory   $nastDokuExportFactory,
                                NastHlavExportFactory   $nastHlavExportFactory,
                                NastPatExportFactory    $nastPatExportFactory,
                                NastObsahExportFactory  $nastObsahExportFactory)
    {
        $this->nastDokuExportFactory = $nastDokuExportFactory;
        $this->nastHlavExportFactory = $nastHlavExportFactory;
        $this->nastPatExportFactory = $nastPatExportFactory;
        $this->nastObsahExportFactory = $nastObsahExportFactory;
    }

    public function create()
    {
        return new NastaveniExportControl(  $this->nastDokuExportFactory,
                                            $this->nastHlavExportFactory,
                                            $this->nastPatExportFactory,
                                            $this->nastObsahExportFactory,
                                            $this->menuId);
    }

    public function setMenu(int $id)
    {
        $this->menuId = $id;
    }
}