<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Presenters;

use App\AdminModule\RestauraceModule\MenuModule\Components\EditaceMenu\EditaceMenuFactory;
use App\AdminModule\RestauraceModule\MenuModule\Components\EditaceSkupina\EditaceSkupinaFactory;
use App\AdminModule\RestauraceModule\MenuModule\Components\ExportMenu\ExportMenuFactory;
use App\AdminModule\RestauraceModule\MenuModule\Components\NastaveniExport\NastaveniExportFactory;
use App\AdminModule\RestauraceModule\MenuModule\Components\NovaSkupina\NovaSkupinaFactory;
use App\AdminModule\RestauraceModule\MenuModule\Components\PolozkySkupina\PolozkySkupinaFactory;
use App\AdminModule\RestauraceModule\MenuModule\Components\VymazMenu\VymazMenuFactory;
use App\AdminModule\RestauraceModule\MenuModule\Components\VymazSkupina\VymazSkupinaFactory;
use App\AdminModule\RestauraceModule\MenuModule\Models\SkupinaModel;
use App\AdminModule\Presenters\BaseAdminPresenter;
use App\Managers\RestMenuManager;
use App\Managers\RestMenuSkupinyManager;
use Nette\Application\UI\Multiplier;

final class EditacePresenter extends BaseAdminPresenter
{
    private $restMenuManager;
    private $restMenuSkupinyManager;
    private $skupinaModel;
    private $editaceMenuFactory;
    private $vymazMenuFactory;
    private $exportMenuFactory;
    private $nastaveniExportFactory;
    private $novaSkupinaFactory;
    private $editaceSkupinaFactory;
    private $vymazSkupinaFactory;
    private $polozkySkupinaFactory;
    private $menuId;

    public function __construct(RestMenuManager         $restMenuManager,
                                RestMenuSkupinyManager  $restMenuSkupinyManager,
                                SkupinaModel            $skupinaModel,
                                EditaceMenuFactory      $editaceMenuFactory,
                                VymazMenuFactory        $vymazMenuFactory,
                                ExportMenuFactory       $exportMenuFactory,
                                NastaveniExportFactory  $nastaveniExportFactory,
                                NovaSkupinaFactory      $novaSkupinaFactory,
                                EditaceSkupinaFactory   $editaceSkupinaFactory,
                                VymazSkupinaFactory     $vymazSkupinaFactory,
                                PolozkySkupinaFactory   $polozkySkupinaFactory)
    {
        $this->restMenuManager = $restMenuManager;
        $this->restMenuSkupinyManager = $restMenuSkupinyManager;
        $this->skupinaModel = $skupinaModel;
        $this->editaceMenuFactory = $editaceMenuFactory;
        $this->vymazMenuFactory = $vymazMenuFactory;
        $this->exportMenuFactory = $exportMenuFactory;
        $this->nastaveniExportFactory = $nastaveniExportFactory;
        $this->novaSkupinaFactory = $novaSkupinaFactory;
        $this->editaceSkupinaFactory = $editaceSkupinaFactory;
        $this->vymazSkupinaFactory = $vymazSkupinaFactory;
        $this->polozkySkupinaFactory = $polozkySkupinaFactory;
    }

    public function actionDefault(int $id)
    {
        $menu = $this->restMenuManager->getOne($id);

        if(!$menu){
            $this->flashMessage('Menu nebylo nalezeno','warning');
            $this->redirect(':Admin:Restaurace:Menu:Seznam:default');
        }
        $this->menuId = $id;
    }

    public function renderDefault()
    {
        $this->template->menu = $this->restMenuManager->getOne($this->menuId);
    }

    /*================================ handle metody ================================*/

    public function handleChangeMenuActive()
    {
        if ($this->isAjax()) {
            $this->restMenuManager->changeActive($this->menuId);
            $this->redrawControl('active');
        }
    }

    /**
     * změna aktivity skupiny
     * @param int $skupinaId
     * @return void
     */
    public function handleChangeGroupActive(int $skupinaId)
    {
        if ($this->isAjax()) {
            try{
                $this->restMenuSkupinyManager->changeActive($skupinaId);
                $this->redrawControl('list');
                $this->redrawControl('scripts');
            }catch(\PDOException $e){
                $this->flashMessage('Chyba zápisu dat do databáze','danger');
            }
        }
    }

    /**
     * změna pořádí skupiny nahoru
     * @param int $skupinaId
     * @return void
     */
    public function handleSortGroupUp(int $skupinaId)
    {
        if ($this->isAjax()) {
            try{
                $this->skupinaModel->sortUp($skupinaId, $this->menuId);
                $this->redrawControl('list');
                $this->redrawControl('scripts');
            }catch(\PDOException $e){
                $this->flashMessage('Chyba zápisu dat do databáze','danger');
            }
        }
    }

    /**
     * změna pořádí skupiny dolu
     * @param int $skupinaId
     * @return void
     */
    public function handleSortGroupDown(int $skupinaId)
    {
        if ($this->isAjax()) {
            try{
                $this->skupinaModel->sortDown($skupinaId, $this->menuId);
                $this->redrawControl('list');
                $this->redrawControl('scripts');
            }catch(\PDOException $e){
                $this->flashMessage('Chyba zápisu dat do databáze','danger');
            }
        }
    }



    /*================================ komponenty ================================*/
    /**
     * editace menu
     * @return \App\AdminModule\RestauraceModule\MenuModule\Components\EditaceMenu\EditaceMenuControl
     */
    protected function createComponentEditMenu()
    {
        $this->editaceMenuFactory->setMenu($this->menuId);
        return $this->editaceMenuFactory->create();
    }

    /**
     * vymazání menu
     * @return \App\AdminModule\RestauraceModule\MenuModule\Components\VymazMenu\VymazMenuControl
     */
    protected function createComponentVymazMenu()
    {
        $this->vymazMenuFactory->setMenu($this->menuId);
        return $this->vymazMenuFactory->create();
    }

    /**
     * export menu do PDF
     * @return \App\AdminModule\RestauraceModule\MenuModule\Components\ExportMenu\ExportMenuControl
     */
    protected function createComponentExportMenu()
    {
        $this->exportMenuFactory->setMenu($this->menuId);
        return $this->exportMenuFactory->create();
    }

    protected function createComponentNastaveniExport()
    {
        $this->nastaveniExportFactory->setMenu($this->menuId);
        return $this->nastaveniExportFactory->create();
    }

    /**
     * nová skupina položek
     * @return \App\AdminModule\RestauraceModule\MenuModule\Components\NovaSkupina\NovaSkupinaControl
     */
    protected function createComponentNovaSkupina()
    {
        $this->novaSkupinaFactory->setMenu($this->menuId);
        return $this->novaSkupinaFactory->create();
    }

    /**
     * editace skupiny
     * @return Multiplier
     */
    protected function createComponentEditSkupina()
    {
        return new Multiplier(function ($skupinaId){
            $this->editaceSkupinaFactory->setSkupina($skupinaId);
            return $this->editaceSkupinaFactory->create();
        });
    }

    /**
     * vymazání skupiny
     * @return Multiplier
     */
    protected function createComponentVymazSkupina()
    {
        return new Multiplier(function ($skupinaId){
            $this->vymazSkupinaFactory->setSkupina($skupinaId);
            return $this->vymazSkupinaFactory->create();
        });
    }

    /**
     * položky skupiny
     * @return Multiplier
     */
    protected function createComponentPolozkySkupina()
    {
        return new Multiplier(function ($skupinaId){
            $this->polozkySkupinaFactory->setSkupina($skupinaId);
            return $this->polozkySkupinaFactory->create();
        });
    }
}