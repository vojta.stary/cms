<?php

namespace App\AdminModule\RestauraceModule\MenuModule\Presenters;

use App\AdminModule\Models\Activation;
use App\AdminModule\RestauraceModule\MenuModule\Components\NoveMenu\NoveMenuFactory;
use App\AdminModule\Presenters\BaseAdminPresenter;
use App\Managers\RestMenuManager;

final class SeznamPresenter extends BaseAdminPresenter
{
    private $restMenuManager;
    private $noveMenuFactory;
    private $activation;

    public function __construct(RestMenuManager $restMenuManager,
                                NoveMenuFactory $noveMenuFactory,
                                Activation      $activation)
    {
        $this->restMenuManager = $restMenuManager;
        $this->noveMenuFactory = $noveMenuFactory;
        $this->activation = $activation;
    }

    /*=================== render metody ===================*/

    /**
     * metoda pro vykreslení a přípravu dat pro šablonu
     * @return void
     */
    public function renderDefault()
    {
        $this->template->aktivace = $this->activation->loadActivation();
        $this->template->data = $this->restMenuManager->select()->fetchAll();
    }

    /*=================== handle metody ===================*/
    /**
     * změna aktivity
     * @param int $id
     * @return void
     */
    public function handleChangeActive(int $id)
    {
        if( $this->isAjax()){
            $this->restMenuManager->changeActive($id);
            $this->redrawControl('list');
        }
    }

    /*=================== komponenty ===================*/

    /**
     * nové menu
     * @return \App\AdminModule\RestauraceModule\MenuModule\Components\NoveMenu\NoveMenuControl
     */
    protected function createComponentNoveMenu()
    {
        return $this->noveMenuFactory->create();
    }
}