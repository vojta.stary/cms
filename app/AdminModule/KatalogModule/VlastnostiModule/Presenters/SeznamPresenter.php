<?php

namespace App\AdminModule\KatalogModule\VlastnostiModule\Presenters;

use App\AdminModule\Components\Paginator\PaginatorFactory;
use App\AdminModule\KatalogModule\VlastnostiModule\Components\EditVastnost\EditVlastnostFactory;
use App\AdminModule\KatalogModule\VlastnostiModule\Components\NovaVlastnost\NovaVlastnostFactory;
use App\AdminModule\KatalogModule\VlastnostiModule\Components\VymazVlastnost\VymazVlastnostFactory;
use App\AdminModule\Presenters\BaseAdminPresenter;
use App\Managers\VlastnostiManager;
use Nette\Application\UI\Multiplier;

final class SeznamPresenter extends BaseAdminPresenter
{
    const pageLimit = 12;

    private $vlastnostiManager;
    private $novaVlastnostFactory;
    private $editVlastnostFactory;
    private $vymazVlastnostFactory;
    private $paginator;
    private $page = 1;
    private $itemsCount;
    private $filter = [];

    public function __construct(VlastnostiManager       $vlastnostiManager,
                                NovaVlastnostFactory    $novaVlastnostFactory,
                                EditVlastnostFactory    $editVlastnostFactory,
                                VymazVlastnostFactory   $vymazVlastnostFactory,
                                PaginatorFactory        $paginatorFactory)
    {
        $this->vlastnostiManager = $vlastnostiManager;
        $this->novaVlastnostFactory = $novaVlastnostFactory;
        $this->editVlastnostFactory = $editVlastnostFactory;
        $this->vymazVlastnostFactory = $vymazVlastnostFactory;
        $this->paginator = $paginatorFactory;
    }

    public function renderDefault()
    {
        $offset = ($this->page - 1 )* self::pageLimit;

        $data = $this->getData();

        $this->itemsCount = $data->count();

        $this->template->page = $this->page;
        $this->template->filter = $this->filter;
        $this->template->data = $data->limit(self::pageLimit,$offset)->fetchAll();
    }

    /*=================== handle metody ===================*/
    /**
     * nastavení ajax stránkování pro paginator
     * @param int $page
     * @param array $filter
     * @return void
     */
    public function handlePage(int $page, array $filter): void
    {
        if ($this->presenter->isAjax()){
            $this->filter = $filter;
            $this->page = $page;
            $this->redrawControl('list');
        }
    }

    /**
     * nastavení filtrace seznamu
     * @param array $filter
     * @return void
     */
    public function handleFilter(array $filter): void
    {
        if ($this->presenter->isAjax())
        {
            $this->filter = $filter;
            $this->redrawControl('list');
        }
    }

    /**
     * změna aktivity
     * @param int $id
     * @param array $filter
     * @param int $page
     * @return void
     */
    public function handleChangeFiltrovat(int $id,array $filter, int $page): void
    {
        if( $this->presenter->isAjax()){
            $this->page = $page;
            $this->filter = $filter;
            $this->vlastnostiManager->changeFiltrovat($id);
            $this->redrawControl('list');
        }
    }

    /*=================== komponenty ===================*/

    /**
     * komponenta paginatoru seznamu
     * @return \App\AdminModule\Components\Paginator\PaginatorControl
     */
    protected function createComponentPaginator()
    {
        return $this->paginator->create($this->itemsCount,$this->page,'page!',$this->filter, self::pageLimit);
    }

    /**
     * nová vlastnost
     * @return \App\AdminModule\KatalogModule\VlastnostiModule\Components\NovaVlastnost\NovaVlastnostControl
     */
    protected function createComponentNovaVlastnost()
    {
        return $this->novaVlastnostFactory->create();
    }

    /**
     * editace vlastnosti
     * @return Multiplier
     */
    protected function createComponentEditVlastnost()
    {
        return new Multiplier(function($id){
            $this->editVlastnostFactory->setVlastnost($id);
            return $this->editVlastnostFactory->create();
        });
    }

    /**
     * vymazání vlastnosti
     * @return Multiplier
     */
    protected function createComponentVymazVlastnost()
    {
        return new Multiplier(function($id){
            $this->vymazVlastnostFactory->setVlastnost($id);
            return $this->vymazVlastnostFactory->create();
        });
    }

    /*=================== pomocné funkce ===================*/

    /**
     * načítání a filtrování dat pro seznam
     * @return \Nette\Database\Table\Selection
     */
    public function getData()
    {
        $database = $this->vlastnostiManager->select();

        if (isset($this->filter['nazev']))
        {
            $database->where('nazev LIKE ?','%'.$this->filter['nazev'].'%');
        }

        return $database->order(VlastnostiManager::columnNazev.' ASC');
    }
}