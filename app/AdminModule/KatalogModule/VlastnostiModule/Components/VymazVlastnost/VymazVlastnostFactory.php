<?php

namespace App\AdminModule\KatalogModule\VlastnostiModule\Components\VymazVlastnost;

use App\Managers\VlastnostiManager;

class VymazVlastnostFactory
{
    private $vlastnostiManager;
    private $vlastnostId;

    public function __construct(VlastnostiManager   $vlastnostiManager)
    {
        $this->vlastnostiManager = $vlastnostiManager;
    }

    public function create()
    {
        return new VymazVlastnostControl(   $this->vlastnostiManager,
                                            $this->vlastnostId);
    }

    public function setVlastnost(int $id)
    {
        $this->vlastnostId = $id;
    }
}