<?php

namespace App\AdminModule\KatalogModule\VlastnostiModule\Components\VymazVlastnost;

use App\Managers\VlastnostiManager;
use Nette\Application\UI\Form;

class VymazVlastnostControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/templates/template.latte';

    private $vlastnostiManager;
    private $vlastnostId;

    public function __construct(VlastnostiManager   $vlastnostiManager,
                                                    $vlastnostId)
    {
        $this->vlastnostiManager = $vlastnostiManager;
        $this->vlastnostId = $vlastnostId;
    }

    public function render()
    {
        $data = $this->vlastnostiManager->getOne($this->vlastnostId);

        $this['vymazVlastnostForm']->setDefaults($data);
        $this->template->data = $data;
        $this->template->render(self::template);
    }

    /*================================= formulář =================================*/

    /**
     * formulář vymazání
     * @return Form
     */
    protected function createComponentVymazVlastnostForm(): Form
    {
        $form = new Form;
        $form->addHidden('id');
        $form->addSubmit('send', 'Vymazat');
        $form->onSuccess[] = [$this, 'sendVymazVlastnostForm'];
        return $form;
    }

    /**
     * zpracování formuláře vymazání
     * @param Form $form
     * @param $values
     * @return void
     * @throws \Nette\Application\AbortException
     */
    public function sendVymazVlastnostForm(Form $form, $values)
    {
        try{
            $this->vlastnostiManager->delete($values->id);
            $this->presenter->flashMessage('Data byla vymazána', 'success');
            $this->presenter->redirect('this');
        }catch (\PDOException $e){
            $form->setDefaults($values);
            $this->presenter->flashMessage('Interní chyba aplikace', 'danger');
            $this->redirect('this');
        }
    }
}