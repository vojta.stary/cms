<?php

namespace App\AdminModule\KatalogModule\VlastnostiModule\Components\EditVastnost;

use App\Managers\VlastnostiManager;

class EditVlastnostFactory
{
    private $vlastnostiManager;
    private $vlastnostId;

    public function __construct(VlastnostiManager   $vlastnostiManager)
    {
        $this->vlastnostiManager = $vlastnostiManager;
    }

    public function create()
    {
        return new EditVlastnostControl($this->vlastnostiManager,
                                        $this->vlastnostId);
    }

    public function setVlastnost(int $id)
    {
        $this->vlastnostId = $id;
    }
}