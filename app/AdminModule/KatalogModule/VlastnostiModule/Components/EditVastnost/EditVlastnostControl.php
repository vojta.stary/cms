<?php

namespace App\AdminModule\KatalogModule\VlastnostiModule\Components\EditVastnost;

use App\Managers\VlastnostiManager;
use Nette\Application\UI\Form;

class EditVlastnostControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/templates/template.latte';

    private $vlastnostiManager;
    private $vlastnostId;

    public function __construct(VlastnostiManager   $vlastnostiManager,
                                                    $vlastnostId)
    {
        $this->vlastnostiManager = $vlastnostiManager;
        $this->vlastnostId = $vlastnostId;
    }

    public function render()
    {
        $data = $this->vlastnostiManager->getOne($this->vlastnostId);

        $this['editVlastnostForm']->setDefaults($data);
        $this->template->data = $data;
        $this->template->render(self::template);
    }

    /*=============================== handle metody ===============================*/

    public function handleCheckNazev($nazev = null)
    {
        if($this->presenter->isAjax())
        {
            $count = $this->vlastnostiManager->select()->where(VlastnostiManager::columnNazev,$nazev)->count();

            $this->presenter->payload->check = $count;
            $this->presenter->sendPayload();
        }
    }

    /*=============================== formulář ===============================*/

    protected function createComponentEditVlastnostForm(): Form
    {
        $form = new Form;
        $form->addHidden('id');
        $form->addText('nazev')
            ->setRequired('Musíte vyplnit název.');
        $form->addCheckbox('filtr');
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendEditVlastnostForm'];
        return $form;
    }

    public function sendEditVlastnostForm(Form $form, $values)
    {
        try {
            $this->vlastnostiManager->update($values->id,$values);
            $this->presenter->flashMessage('Data byla uložena.','success');
            $this->presenter->redirect('this');
        }catch (\PDOException $e){
            if ((int)$e->errorInfo[1] === 1062) { // SQL kód pro narušení unikátního klíče
                $this->presenter->flashMessage('Název vlastnosti je již použit!', 'warning');
                $form->setDefaults($values);
                $this->redirect('this');
            }else{
                $form->setDefaults($values);
                $this->presenter->flashMessage('Chyba zápisu dat do databáze!','danger');
                $this->redirect('this');
            }
        }
    }
}