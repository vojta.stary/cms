<?php

namespace App\AdminModule\KatalogModule\VlastnostiModule\Components\NovaVlastnost;

use App\Managers\VlastnostiManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class NovaVlastnostControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $vlastnostiManager;

    public function __construct(VlastnostiManager $vlastnostiManager)
    {
        $this->vlastnostiManager = $vlastnostiManager;
    }

    public function render()
    {
        $this->template->render(self::template);
    }

    /*=============================== handle metody ===============================*/

    public function handleCheckNazev($nazev = null)
    {
        if ($this->presenter->isAjax()){

            $count = $this->vlastnostiManager->select()->where(VlastnostiManager::columnNazev,$nazev)->count();

            $this->presenter->payload->check = $count;
            $this->presenter->sendPayload();
        }
    }

    /*=============================== formulář ===============================*/

    protected function createComponentNovaVlastnostForm(): Form
    {
        $form = new Form;
        $form->addText('nazev')
            ->setRequired('Musíte vyplnit název.');
        $form->addCheckbox('filtr')
            ->setDefaultValue(true);
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendNovaVlastnostForm'];
        return $form;
    }

    public function sendNovaVlastnostForm(Form $form, $values)
    {
        try {
            $this->vlastnostiManager->create($values);
            $this->presenter->flashMessage('Data byla uložena.','success');
            $this->redirect('this');
        }catch (\PDOException $e){
            if ((int)$e->errorInfo[1] === 1062) { // SQL kód pro narušení unikátního klíče
                $this->presenter->flashMessage('Název vlastnosti je již použit!', 'warning');
                $form->setDefaults($values);
                $this->redirect('this');
            }else{
                $form['editVlastForm']->setDefaults($values);
                $this->presenter->flashMessage('Chyba zápisu dat do databáze!','danger');
                $this->redirect('this');
            }
        }
    }
}