<?php

namespace App\AdminModule\KatalogModule\VlastnostiModule\Components\NovaVlastnost;

use App\Managers\VlastnostiManager;

class NovaVlastnostFactory
{
    private $vlastnostiManager;

    public function __construct(VlastnostiManager $vlastnostiManager)
    {
        $this->vlastnostiManager = $vlastnostiManager;
    }

    public function create()
    {
        return new NovaVlastnostControl($this->vlastnostiManager);
    }
}