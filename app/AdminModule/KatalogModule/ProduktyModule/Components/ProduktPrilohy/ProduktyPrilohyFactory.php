<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Components\ProduktPrilohy;

use App\AdminModule\KatalogModule\ProduktyModule\Models\ProduktyPrilohyModel;
use App\Managers\ProduktyPrilohyManager;

class ProduktyPrilohyFactory
{
    private $produktyPrilohyManager;
    private $produktyPrilohyModel;
    private $produktId;

    public function __construct(ProduktyPrilohyManager  $produktyPrilohyManager,
                                ProduktyPrilohyModel    $produktyPrilohyModel)
    {
        $this->produktyPrilohyManager = $produktyPrilohyManager;
        $this->produktyPrilohyModel = $produktyPrilohyModel;
    }

    public function create()
    {
        return new ProduktyPrilohyControl(  $this->produktyPrilohyManager,
                                            $this->produktyPrilohyModel,
                                            $this->produktId);
    }

    public function setProdukt(int $id)
    {
        $this->produktId = $id;
    }
}