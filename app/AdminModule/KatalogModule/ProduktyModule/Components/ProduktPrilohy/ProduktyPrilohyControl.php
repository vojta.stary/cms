<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Components\ProduktPrilohy;

use App\AdminModule\KatalogModule\ProduktyModule\Models\ProduktyPrilohyModel;
use App\Managers\ProduktyPrilohyManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class ProduktyPrilohyControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $produktyPrilohyManager;
    private $produktyPrilohyModel;
    private $produktId;

    public function __construct(ProduktyPrilohyManager  $produktyPrilohyManager,
                                ProduktyPrilohyModel    $produktyPrilohyModel,
                                                        $produktId)
    {
        $this->produktyPrilohyManager = $produktyPrilohyManager;
        $this->produktyPrilohyModel = $produktyPrilohyModel;
        $this->produktId = $produktId;
    }

    public function render()
    {
        $prilohy = $this->produktyPrilohyManager->select()->where(ProduktyPrilohyManager::columnProduktyId, $this->produktId)->fetchAll();

        $this->template->prilohy = $prilohy;
        $this->template->render(self::template);
    }

    /*======================== formulář ========================*/

    protected function createComponentAddPrilohyForm(): Form
    {
        $form = new Form;
        $form->addText('text','Název přílohy')
            ->setRequired('Musíte vyplnit název přílohy!');
        $form->addUpload('priloha', 'Soubor k nahrání')
            ->setRequired('Musíte vybrat soubor k nahrání!');
        $form->addSubmit('send','Nahrát');
        $form->onSuccess[] = [$this, 'sendAddPrilohyForm'];
        return $form;
    }

    public function sendAddPrilohyForm(Form $form, $values)
    {
        if ($this->presenter->isAjax()){
            try {
                $this->produktyPrilohyModel->nahrat($this->produktId, $values);
                $form->reset();
                $this->presenter->flashMessage('Data byla uložena','success');
                $this->presenter->redrawControl('flash');
                $this->redrawControl('prilohy');
                $this->redrawControl('formular');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('Chyba při nahrávání','danger');
                $this->presenter->redrawControl('flash');
            }
        }
    }

    /*======================== handle metody ========================*/

    public function handleVymazPriloha(int $id)
    {
        if ($this->presenter->isAjax()){
            $this->produktyPrilohyModel->delete($id);
            $this->presenter->flashMessage('Příloha byla vymazána','success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('prilohy');
        }
    }
}