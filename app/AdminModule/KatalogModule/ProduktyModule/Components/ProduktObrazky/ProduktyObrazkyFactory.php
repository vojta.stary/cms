<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Components\ProduktObrazky;

use App\AdminModule\KatalogModule\ProduktyModule\Models\ProduktImageModel;
use App\Managers\ProduktyManager;

class ProduktyObrazkyFactory
{
    private $produktyManager;
    private $produktImageModel;
    private $produktId;

    public function __construct(ProduktyManager     $produktyManager,
                                ProduktImageModel   $produktImageModel)
    {
        $this->produktyManager = $produktyManager;
        $this->produktImageModel = $produktImageModel;
    }

    public function create()
    {
        return new ProduktObrazkyControl(   $this->produktyManager,
                                            $this->produktImageModel,
                                            $this->produktId);
    }

    public function setProdukt(int $id)
    {
        $this->produktId = $id;
    }
}