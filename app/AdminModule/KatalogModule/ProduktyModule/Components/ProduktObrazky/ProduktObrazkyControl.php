<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Components\ProduktObrazky;

use App\AdminModule\KatalogModule\ProduktyModule\Models\ProduktImageModel;
use App\Managers\ProduktyManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class ProduktObrazkyControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $produktyManager;
    private $produktImageModel;
    private $produktId;

    public function __construct(ProduktyManager     $produktyManager,
                                ProduktImageModel   $produktImageModel,
                                                    $produktId)
    {
        $this->produktyManager = $produktyManager;
        $this->produktImageModel = $produktImageModel;
        $this->produktId = $produktId;
    }

    public function render()
    {
        $this->template->produkt = $this->produktyManager->getOne($this->produktId);
        $this->template->render(self::template);
    }

    /*======================== formulář ========================*/

    protected function createComponentAddObrazkyForm(): Form
    {
        $form = new Form;
        $form->addMultiUpload('obrazky')
            ->setRequired('Musíte vybrat soubory k nahrání!')
            ->addRule($form::IMAGE, 'Obrázek musí být JPEG, PNG, GIF or WebP.')
            ->addRule($form::MAX_LENGTH, 'Maximálně lze nahrát %d souborů najednou.', 10);
        $form->addSubmit('send','Nahrát');
        $form->onSuccess[] = [$this, 'sendAddObrazkyForm'];
        return $form;
    }

    public function sendAddObrazkyForm(Form $form, $values)
    {
        if ($this->presenter->isAjax()){
            try {
                $this->produktImageModel->nahrat($this->produktId, $values->obrazky);
                $this->presenter->flashMessage('Data byla uložena','success');
                $this->presenter->redrawControl('flash');
                $this->redrawControl('obrazky');
                $this->redrawControl('formular-wrapper');
                $this->redrawControl('formular');
                $this->redrawControl('scripty');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('Chyba při nahrávání souborů','danger');
                $this->presenter->redrawControl('flash');
                $this->redrawControl('obrazky');
                $this->redrawControl('formular-wrapper');
                $this->redrawControl('formular');
                $this->redrawControl('scripty');
            }
        }
    }

    /*=================================== handle metody ===================================*/

    /**
     * vymazání obrázku produktu
     * @param $imageId
     * @return void
     */
    public function handleDelImage(int $imageId): void
    {
        if ($this->presenter->isAjax()){
            $this->produktImageModel->deleteImage($imageId);
            $this->presenter->flashMessage('Obrázek byl vymazán.','success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('obrazky');
        }
    }

    public function handleDelBaseImage()
    {
        if ($this->presenter->isAjax()){
            $this->produktImageModel->deleteBaseImage($this->produktId);
            $this->presenter->flashMessage('Obrázek byl vymazán.','success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('obrazky');
        }
    }

    public function handleSetHlavni(int $imageId)
    {
        if ($this->presenter->isAjax()){
            $this->produktImageModel->setHlavni($imageId, $this->produktId);
            $this->redrawControl('obrazky');
        }
    }
}