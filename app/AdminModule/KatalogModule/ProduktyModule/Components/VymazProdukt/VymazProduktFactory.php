<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Components\VymazProdukt;

use App\AdminModule\KatalogModule\ProduktyModule\Models\ProduktyModel;
use App\Managers\ProduktyManager;

class VymazProduktFactory
{
    private $produktyModel;
    private $produktyManager;
    private $produktId;

    public function __construct(ProduktyModel   $produktyModel,
                                ProduktyManager $produktyManager)
    {
        $this->produktyModel = $produktyModel;
        $this->produktyManager = $produktyManager;
    }

    public function create()
    {
        return new VymazProduktControl( $this->produktyModel,
                                        $this->produktyManager,
                                        $this->produktId);
    }

    public function setProdukt(int $id)
    {
        $this->produktId = $id;
    }
}