<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Components\VymazProdukt;

use App\AdminModule\KatalogModule\ProduktyModule\Models\ProduktyModel;
use App\Managers\ProduktyManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class VymazProduktControl extends Control
{
    const template = __DIR__.'/template/template.latte';

    private $produktyModel;
    private $produktyManager;
    private $produktId;

    public function __construct(ProduktyModel   $produktyModel,
                                ProduktyManager $produktyManager,
                                                $produktId)
    {
        $this->produktyModel = $produktyModel;
        $this->produktyManager = $produktyManager;
        $this->produktId = $produktId;
    }

    /**
     * render metoda komponenty
     * @return void
     */
    public function render()
    {
        $data = $this->produktyManager->getOne($this->produktId);

        $this['vymazProduktForm']->setDefaults($data);
        $this->template->data = $data;
        $this->template->render(self::template);
    }

    /*================================= formulář =================================*/

    /**
     * formulář vymazání
     * @return Form
     */
    protected function createComponentVymazProduktForm(): Form
    {
        $form = new Form;
        $form->addHidden('id');
        $form->addSubmit('send', 'Vymazat');
        $form->onSuccess[] = [$this, 'sendVymazProduktForm'];
        return $form;
    }

    /**
     * zpracování formuláře vymazání
     * @param Form $form
     * @param $values
     * @return void
     * @throws \Nette\Application\AbortException
     */
    public function sendVymazProduktForm(Form $form, $values)
    {
        //try{
            $this->produktyModel->delete($values->id);
            $this->presenter->flashMessage('Data byla vymazána', 'success');
            $this->presenter->redirect(':Admin:Katalog:Produkty:Seznam:default');
        /*}catch (\PDOException $e){
            $this->presenter->flashMessage('Interní chyba aplikace', 'danger');
            $this->redirect('this');
        }*/
    }
}