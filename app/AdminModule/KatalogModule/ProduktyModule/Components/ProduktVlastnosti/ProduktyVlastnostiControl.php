<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Components\ProduktVlastnosti;

use App\AdminModule\KatalogModule\ProduktyModule\Models\ProduktyVlastnostiModel;
use App\Managers\ProduktyVlastnostiManager;
use App\Managers\VlastnostiHodnotyManager;
use App\Managers\VlastnostiManager;
use Nette\Application\UI\Control;

class ProduktyVlastnostiControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $vlastnostiManager;
    private $vlastnostiHodnotyManager;
    private $produktyVlastnostiManager;
    private $produktyVlastnostiModel;
    private $produktId;
    private $hodnoty = [];

    public function __construct(VlastnostiManager           $vlastnostiManager,
                                VlastnostiHodnotyManager    $vlastnostiHodnotyManager,
                                ProduktyVlastnostiManager   $produktyVlastnostiManager,
                                ProduktyVlastnostiModel     $produktyVlastnostiModel,
                                                            $produktId)
    {
        $this->vlastnostiManager = $vlastnostiManager;
        $this->vlastnostiHodnotyManager = $vlastnostiHodnotyManager;
        $this->produktyVlastnostiManager = $produktyVlastnostiManager;
        $this->produktyVlastnostiModel = $produktyVlastnostiModel;
        $this->produktId = $produktId;
    }

    public function render()
    {
        $vlastnosti = $this->produktyVlastnostiManager->select()->where(ProduktyVlastnostiManager::columnProduktyId, $this->produktId)->order(ProduktyVlastnostiManager::columnVlastnostiId,'ASC')->fetchAll();

        $this->template->hodnotyVlast = $this->hodnoty;
        $this->template->selectVlast = $this->vlastnostiManager->select()->fetchAll();
        $this->template->vlastnosti = $vlastnosti;
        $this->template->produktId = $this->produktId;
        $this->template->render(self::template);
    }

    /*=================================== handle metody ===================================*/

    /**
     * maže vlastnost produktu
     * @param $id
     * @return void
     */
    public function handleVymazVlastnost($id)
    {
        if ($this->presenter->isAjax()){
            try {
                $this->produktyVlastnostiManager->delete($id);
                $this->presenter->flashMessage('Vlastnost byla vymazána','success');
                $this->presenter->redrawControl('flash');
                $this->redrawControl('list');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
                $this->presenter->redrawControl('flash');
            }
        }
    }

    /**
     * načítá seznam hodnot vlastnosti
     * @param array $data
     * @return void
     */
    public function handleChangeVlastnost(array $data)
    {
        if ($this->presenter->isAjax()) {
            if ($data['vlastnosti_id'] != '') {
                $this->hodnoty = $this->vlastnostiHodnotyManager->select()
                                    ->where(VlastnostiHodnotyManager::columnVlastnostiId, $data['vlastnosti_id'])
                                    ->fetchAll();
                $this->redrawControl('hodnoty');
            }
        }
    }

    /**
     * kontroluje název nové hodnoty
     * @param array $data
     * @return void
     * @throws \Nette\Application\AbortException
     */
    public function handleCheckVlastnost(array $data)
    {
        if ($this->presenter->isAjax()) {
            $hodnota = $this->vlastnostiHodnotyManager->select()->where(VlastnostiHodnotyManager::columnHodnota, $data['hodnota'])->where(VlastnostiHodnotyManager::columnVlastnostiId, $data['vlastnosti_id'])->fetch();

            if ($hodnota) {
                $this->presenter->payload->hodnota = $hodnota->id;
            }else{
                $this->presenter->payload->hodnota = 0;
            }

            $this->presenter->sendPayload();
        }
    }

    /**
     * ukládá vlastnost produktu
     * @param array $data
     * @return void
     */
    public function handleSaveVlastnost(array $data){
        if ($this->presenter->isAjax()){
            try {
                $this->produktyVlastnostiModel->save($data);
                $this->presenter->flashMessage('Vlastnost byla přidána','success');
                $this->presenter->redrawControl('flash');
                $this->redrawControl('list');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
                $this->presenter->redrawControl('flash');
            }
        }
    }
}