<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Components\ProduktVlastnosti;

use App\AdminModule\KatalogModule\ProduktyModule\Models\ProduktyVlastnostiModel;
use App\Managers\ProduktyVlastnostiManager;
use App\Managers\VlastnostiHodnotyManager;
use App\Managers\VlastnostiManager;

class ProduktyVastnostiFactory
{
    private $vlastnostiManager;
    private $vlastnostiHodnotyManager;
    private $produktyVlastnostiManager;
    private $produktyVlastnostiModel;
    private $produktId;

    public function __construct(VlastnostiManager           $vlastnostiManager,
                                VlastnostiHodnotyManager    $vlastnostiHodnotyManager,
                                ProduktyVlastnostiManager   $produktyVlastnostiManager,
                                ProduktyVlastnostiModel     $produktyVlastnostiModel)
    {
        $this->vlastnostiManager = $vlastnostiManager;
        $this->vlastnostiHodnotyManager = $vlastnostiHodnotyManager;
        $this->produktyVlastnostiManager = $produktyVlastnostiManager;
        $this->produktyVlastnostiModel = $produktyVlastnostiModel;
    }

    public function create()
    {
        return new ProduktyVlastnostiControl(   $this->vlastnostiManager,
                                                $this->vlastnostiHodnotyManager,
                                                $this->produktyVlastnostiManager,
                                                $this->produktyVlastnostiModel,
                                                $this->produktId);
    }

    public function setProdukt(int $id)
    {
        $this->produktId = $id;
    }
}