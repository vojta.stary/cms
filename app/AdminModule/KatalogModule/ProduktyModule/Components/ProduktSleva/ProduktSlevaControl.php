<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Components\ProduktSleva;

use App\AdminModule\KatalogModule\ProduktyModule\Models\ProduktySlevaModel;
use App\Managers\ProduktySlevaManager;
use Nette\Application\UI\Control;
use Nette\Utils\DateTime;

class ProduktSlevaControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $produktySlevaManager;
    private $produktySlevaModel;
    private $produktId;

    public function __construct(ProduktySlevaManager    $produktySlevaManager,
                                ProduktySlevaModel      $produktySlevaModel,
                                                        $produktId)
    {
        $this->produktySlevaManager = $produktySlevaManager;
        $this->produktySlevaModel = $produktySlevaModel;
        $this->produktId = $produktId;
    }

    public function render()
    {
        $slevy = $this->produktySlevaManager->select()
            ->where(ProduktySlevaManager::columnProduktyId,$this->produktId)
            ->order(ProduktySlevaManager::columnPlatOd.' DESC')
            ->fetchAll();

        $this->template->today = DateTime::from('now');
        $this->template->produktId = $this->produktId;
        $this->template->slevy = $slevy;
        $this->template->render(self::template);
    }

    /*============================= handle metody =============================*/

    /**
     * ukládá novou slevu
     * @param array $data
     * @return void
     */
    public function handleSaveSleva(array $data)
    {
        if ($this->presenter->isAjax()){
            try {
                $this->produktySlevaModel->createSleva($data);

                $this->presenter->flashMessage('Sleva byla uložena.','success');
                $this->presenter->redrawControl('flash');
                $this->redrawControl('sleva');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
                $this->presenter->redrawControl('flash');
            }
        }
    }

    /**
     * maže slevu
     * @param $id
     * @return void
     */
    public function handleDelSleva($id)
    {
        if ($this->presenter->isAjax()){
            try {
                $this->produktySlevaModel->delSleva($id);

                $this->presenter->flashMessage('Sleva byla vymazána.','success');
                $this->presenter->redrawControl('flash');
                $this->redrawControl('sleva');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
                $this->presenter->redrawControl('flash');
            }
        }
    }
}