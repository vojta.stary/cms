<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Components\ProduktSleva;

use App\AdminModule\KatalogModule\ProduktyModule\Models\ProduktySlevaModel;
use App\Managers\ProduktySlevaManager;

class ProduktSlevaFactory
{
    private $produktySlevaManager;
    private $produktySlevaModel;
    private $produktId;

    public function __construct(ProduktySlevaManager    $produktySlevaManager,
                                ProduktySlevaModel      $produktySlevaModel)
    {
        $this->produktySlevaManager = $produktySlevaManager;
        $this->produktySlevaModel = $produktySlevaModel;
    }

    public function create()
    {
        return new ProduktSlevaControl( $this->produktySlevaManager,
                                        $this->produktySlevaModel,
                                        $this->produktId);
    }

    public function setProdukt(int $produktId)
    {
        $this->produktId = $produktId;
    }
}