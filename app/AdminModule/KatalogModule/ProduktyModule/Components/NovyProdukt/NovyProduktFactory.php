<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Components\NovyProdukt;

use App\AdminModule\KatalogModule\ProduktyModule\Models\ProduktyModel;

class NovyProduktFactory
{
    private $produktyModel;

    public function __construct(ProduktyModel $produktyModel)
    {
        $this->produktyModel = $produktyModel;
    }

    public function create()
    {
        return new NovyProduktControl($this->produktyModel);
    }
}