<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Components\NovyProdukt;

use App\AdminModule\KatalogModule\ProduktyModule\Models\ProduktyModel;
use Nette\Application\UI\Form;

class NovyProduktControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/templates/template.latte';

    private $produktyModel;

    public function __construct(ProduktyModel $produktyModel)
    {
        $this->produktyModel = $produktyModel;
    }

    public function render()
    {
        $this->template->render(self::template);
    }

    /*================================= formulář =================================*/
    protected function createComponentNovyProduktForm(): Form
    {
        $typ = ['standart' => 'Standartní produkt','sada' => 'Sada produktů'];

        $form = new Form;
        $form->addSelect('typ','Typ',$typ)
            ->setRequired('Musíte vybrat typ!')
            ->setPrompt('Vyberte typ');
        $form->addText('nazev','Název')
            ->setRequired('Musíte vyplnit název!');
        $form->addHidden('active')
            ->setDefaultValue(0);
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendNovyProduktForm'];

        return $form;
    }

    public function sendNovyProduktForm(Form $form, $values)
    {
        try{

            $id = $this->produktyModel->create($values);
            $this->presenter->flashMessage('Data byla uložena','success');
            $this->presenter->redirect(':Admin:Katalog:Produkty:Editace:default',$id);

        }catch (\PDOException $e){

            $this->presenter->flashMessage('Interní chyba aplikace', 'danger');
            $this->redirect('this');

        }
    }
}