<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Models;

use App\Managers\ProduktyKategorieManager;

final class ProduktKategorieModel
{
    private $produktyKategorieManager;

    public function __construct(ProduktyKategorieManager $produktyKategorieManager)
    {
        $this->produktyKategorieManager = $produktyKategorieManager;
    }

    /**
     * maže přiřazení kategorií k produktu
     * @param $produktId
     * @return void
     */
    public function deletePrirazeni($produktId)
    {
        $this->produktyKategorieManager->select()
            ->where(ProduktyKategorieManager::columnProduktyId, $produktId)
            ->delete();
    }

    /**
     * přiřazuje kategorie produktu
     * @param $produktId
     * @param $values
     * @return void
     */
    public function priradKategorie($produktId, $values)
    {
        $this->deletePrirazeni($produktId);

        if (isset($values->kategorie)) {
            foreach ($values->kategorie as $one) {
                if ($one == $values->baseKategorie){
                    $prodKat = [ProduktyKategorieManager::columnProduktyId => $produktId,
                        ProduktyKategorieManager::columnKategorieId => $one,
                        ProduktyKategorieManager::columnBaseKategorie => true];
                }else{
                    $prodKat = [ProduktyKategorieManager::columnProduktyId => $produktId,
                        ProduktyKategorieManager::columnKategorieId => $one,
                        ProduktyKategorieManager::columnBaseKategorie => false];
                }
                $this->produktyKategorieManager->create($prodKat);
            }
        }
    }
}