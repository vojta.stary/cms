<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Models;

use App\Managers\ProduktyPrilohyManager;
use Nette\Utils\Strings;
use Nette\Utils\FileSystem;

final class ProduktyPrilohyModel
{
    const dir = __DIR__.'/../../../../../www/src/katalog/';

    private $produktPrilohyManager;

    public function __construct(ProduktyPrilohyManager $produktyPrilohyManager)
    {
        $this->produktPrilohyManager = $produktyPrilohyManager;
    }

    /**
     * nahrání přílohy produktu
     * @param int $produktId
     * @param $values
     * @return void
     */
    public function nahrat(int $produktId, $values): void
    {
        $priloha = $values->priloha;

        if ($priloha->hasFile()){
            $prilohaNazev = $priloha->getName();

            $slozka = self::dir . $produktId . '/prilohy/';

            if (!is_dir($slozka)) {
                mkdir($slozka, 0777, true);
            }

            $pripona = pathinfo($prilohaNazev);

            $prilohaNazev = Strings::webalize($pripona['filename']) . '.' . $pripona['extension'];

            $novySoubor = $slozka . $prilohaNazev;

            if (!is_file($novySoubor)) {
                $priloha->move($novySoubor);
            }

            $this->produktPrilohyManager
                ->create([ProduktyPrilohyManager::columnProduktyId => $produktId,
                    ProduktyPrilohyManager::columnText => $values->text,
                    ProduktyPrilohyManager::columnUrl => $prilohaNazev]);
        }
    }

    public function delete(int $prilohaId)
    {
        $priloha = $this->produktPrilohyManager->getOne($prilohaId);

        $souborNazev = $priloha->url;

        if($souborNazev){
            $slozka = self::dir.$priloha->produkty_id.'/prilohy/';

            $soubor = $slozka.$souborNazev;

            FileSystem::delete($soubor);

            $priloha->delete();
        }
    }
}