<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Models;

use App\Managers\ProduktyManager;
use Nette\Utils\DateTime;
use Nette\Utils\FileSystem;
use Nette\Utils\Strings;

final class ProduktyModel
{
    const dir = __DIR__.'/../../../../../www/src/katalog/';

    private $produktyManager;
    private $produktKategorieModel;
    private $produkImageModel;
    private $produktPrilohyModel;

    public function __construct(ProduktyManager             $produktyManager,
                                ProduktKategorieModel       $produktKategorieModel,
                                ProduktImageModel           $produkImageModel)
    {
        $this->produktyManager = $produktyManager;
        $this->produktKategorieModel = $produktKategorieModel;
        $this->produkImageModel = $produkImageModel;
    }

    /**
     * vytvoření nového produktu
     * @param $values
     * @return int
     */
    public function create($values): int
    {
        // datum vytvoření
        $datum = new DateTime('now');
        $values->created_at = $datum;

        $newId = $this->produktyManager->create($values);

        $url = $newId.'-'.Strings::webalize($values->nazev);

        $this->produktyManager->update($newId,[ProduktyManager::columnUrl => $url]);

        return $newId;
    }

    /**
     * upravuje data produktu
     * @param int $id
     * @param $values
     * @return void
     */
    public function update(int $id, $values)
    {
        if ($values->ean == ''){
            $values->ean = null;
        }
        if ($values->vyrobci_id == ''){
            $values->vyrobci_id = null;
        }

        $produkt = $this->produktyManager->getOne($id);

        //==================== uloží data produktu =================
        if ($values->set_cena){
            $data = [ProduktyManager::columnNazev => $values->nazev,
                ProduktyManager::columnKod => $values->kod,
                ProduktyManager::columnEan => $values->ean,
                ProduktyManager::columnVyrobciId => $values->vyrobci_id,
                ProduktyManager::columnPopisShort => $values->popis_short,
                ProduktyManager::columnPopisLong => $values->popis_long,
                ProduktyManager::columnCena => $values->cena,
                ProduktyManager::columnCenaDph => $values->cena_dph,
                ProduktyManager::columnMetaDesc => $values->meta_desc];
        }else{
            $data = [ProduktyManager::columnNazev => $values->nazev,
                ProduktyManager::columnKod => $values->kod,
                ProduktyManager::columnEan => $values->ean,
                ProduktyManager::columnVyrobciId => $values->vyrobci_id,
                ProduktyManager::columnPopisShort => $values->popis_short,
                ProduktyManager::columnPopisLong => $values->popis_long,
                ProduktyManager::columnMetaDesc => $values->meta_desc];
        }

        $produkt->update($data);

        // uloží url produktu
        $url = $produkt->id.'-'.Strings::webalize($values->nazev);
        $produkt->update([ProduktyManager::columnUrl => $url]);

        //====================== uloží kategorie ====================
        $this->produktKategorieModel->priradKategorie($id, $values);
    }


    public function delete(int $produktId)
    {
        $produkt = $this->produktyManager->getOne($produktId);

        $slozka = self::dir.$produktId;

        FileSystem::delete($slozka);

        $produkt->delete();
    }

    /**
     * maže obrázek produktu
     * @param $imageId
     * @return void
     */
    public function deleteImage($imageId)
    {
        $this->produkImageModel->deleteImage($imageId);
    }
}