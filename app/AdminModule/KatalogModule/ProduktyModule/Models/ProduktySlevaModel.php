<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Models;

use App\Managers\ProduktySlevaManager;
use Nette\Utils\DateTime;

final class ProduktySlevaModel
{
    private $produktySlevaManager;

    public function __construct(ProduktySlevaManager    $produktySlevaManager)
    {
        $this->produktySlevaManager = $produktySlevaManager;
    }

    /**
     * vytváří slevu u produktu
     * @param $values
     * @return void
     * @throws \Exception
     */
    public function createSleva($values): void
    {
        $data = [];

        $plat_od = DateTime::from($values['od']);
        $data['plat_od'] = $plat_od->format('Y-m-d');
        if ($values['do'] != ''){
            $plat_do = DateTime::from($values['do']);
            $data['plat_do'] = $plat_do->format('Y-m-d');
        }
        $data['cena'] = $values['cena'];
        $data['cena_dph'] = $values['cenaDph'];
        $data['produkty_id'] = $values['id'];

        $this->produktySlevaManager->create($data);
    }

    /**
     * maže slevu u produktu
     * @param int $slevaId
     * @return void
     */
    public function delSleva(int $slevaId): void
    {
        $this->produktySlevaManager->delete($slevaId);
    }
}