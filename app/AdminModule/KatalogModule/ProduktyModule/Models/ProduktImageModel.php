<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Models;

use App\Managers\ProduktyManager;
use App\Managers\ProduktyObrazkyManager;
use Nette\Utils\FileSystem;
use Nette\Utils\Image;
use Nette\Utils\Strings;

final class ProduktImageModel
{
    const obrazkyDir = __DIR__.'/../../../../../www/src/katalog/';

    private $produktyObrazkyManager;
    private $produktyManager;

    public function __construct(ProduktyObrazkyManager  $produktyObrazkyManager,
                                ProduktyManager         $produktyManager)
    {
        $this->produktyObrazkyManager = $produktyObrazkyManager;
        $this->produktyManager = $produktyManager;
    }


    /**
     * nahraje nové obrázky
     * @param int $produktId
     * @param $obrazky
     * @return void
     * @throws \Nette\Utils\ImageException
     * @throws \Nette\Utils\UnknownImageFileException
     */
    public function nahrat(int $produktId, $obrazky): void
    {
        //uloží obrázky
        if (!empty($obrazky)) {
            $x = 0;
            foreach ($obrazky as $obrazek) {

                if ($obrazek != null) {
                    $obrazekNazev = $obrazek->getName();

                    $slozka = self::obrazkyDir . $produktId . '/images/';

                    if (!is_dir($slozka)) {
                        mkdir($slozka, 0777, true);
                    }

                    $pripona = pathinfo($obrazekNazev);

                    $obrazekNazev = Strings::webalize($pripona['filename']) . '.' . $pripona['extension'];

                    $novySoubor = $slozka . $obrazekNazev;

                    if (!is_file($novySoubor) && $obrazek->hasFile()) {

                        $obrazek->move($novySoubor);

                        $nazev = $pripona['filename'];
                        $this->produktyObrazkyManager->create([ProduktyObrazkyManager::columnProduktyId => $produktId,
                            ProduktyObrazkyManager::columnUrl => $obrazekNazev,
                            ProduktyObrazkyManager::columnNazev => $nazev]);

                        //vytvori thumb obrázek
                        $thumbSlozka = $slozka . '/thumbs/' . $obrazekNazev;
                        FileSystem::createDir($slozka . '/thumbs/');
                        $thumb = Image::fromFile($novySoubor);
                        $thumb->resize(null, 450);
                        $thumb->save($thumbSlozka, 80);
                    }
                }
            }
        }
    }

    /**
     * maže obrázek
     * @param $imageId
     * @return void
     */
    public function deleteImage($imageId): void
    {
        $obrazek = $this->produktyObrazkyManager->getOne($imageId);

        if($obrazek){
            $slozka = self::obrazkyDir.$obrazek->produkty_id.'/images/';

            $thumb = $slozka.'thumbs/'.$obrazek->url;
            $soubor = $slozka.$obrazek->url;

            unlink($thumb);
            unlink($soubor);

            $this->produktyObrazkyManager->delete($imageId);
        }
    }

    /**
     * vymaže hlavní obrázek
     * @param int $produktId
     * @return void
     */
    public function deleteBaseImage(int $produktId): void
    {
        $produkt = $this->produktyManager->getOne($produktId);

        $obrazek = $produkt->base_image;

        if($obrazek){
            $slozka = self::obrazkyDir.$produktId.'/images/';

            $thumb = $slozka.'thumbs/'.$obrazek;
            $soubor = $slozka.$obrazek;

            FileSystem::delete($thumb);
            FileSystem::delete($soubor);

            $produkt->update([ProduktyManager::columnBaseImage => null]);
        }
    }

    /**
     * nastaví nový hlavní obrázek
     * @param int $obrazekId
     * @param int $produktId
     * @return void
     */
    public function setHlavni(int $obrazekId, int $produktId): void
    {
        //získa data
        $produkt = $this->produktyManager->getOne($produktId);
        $obrazek = $this->produktyObrazkyManager->getOne($obrazekId);

        //zjistí původní hl. obrázek + jméno bez přípony
        $oldHlavni = $produkt->base_image;
        if($oldHlavni){

            $extOldHlav = pathinfo($oldHlavni);

            //přesune starý hl. obrázek mezi obrázky
            $this->produktyObrazkyManager
                ->create([ProduktyObrazkyManager::columnProduktyId => $produktId,
                    ProduktyObrazkyManager::columnNazev => $extOldHlav['filename'],
                    ProduktyObrazkyManager::columnUrl => $oldHlavni]);
        }
        //nastavý nový hl. obrázek
        $produkt->update([ProduktyManager::columnBaseImage => $obrazek->url]);

        //vymaže starý obrázek ze seznamu
        $obrazek->delete();
    }
}