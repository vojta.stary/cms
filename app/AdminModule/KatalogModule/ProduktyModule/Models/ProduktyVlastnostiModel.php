<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Models;

use App\Managers\ProduktyVlastnostiManager;
use App\Managers\VlastnostiHodnotyManager;


final class ProduktyVlastnostiModel
{
    private $vlastnostiHodnotyManager;
    private $produktyVlastnostiManager;

    public function __construct(VlastnostiHodnotyManager    $vlastnostiHodnotyManager,
                                ProduktyVlastnostiManager   $produktyVlastnostiManager)
    {
        $this->vlastnostiHodnotyManager = $vlastnostiHodnotyManager;
        $this->produktyVlastnostiManager = $produktyVlastnostiManager;
    }

    /**
     * ukládá hodnoty vlastností produktu
     * @param $data
     * @return void
     */
    public function save($data)
    {
        if ($data['hodnota'] !== ''){
            $newHodnotaId = $this->vlastnostiHodnotyManager->create([VlastnostiHodnotyManager::columnHodnota => $data['hodnota'], VlastnostiHodnotyManager::columnVlastnostiId => $data['vlastnosti_id']]);
            $dataTable = [ProduktyVlastnostiManager::columnVlastnostiId => $data['vlastnosti_id'],
                ProduktyVlastnostiManager::columnProduktyId => $data['produkty_id'],
                ProduktyVlastnostiManager::columnVlastnostiHodnotyId => $newHodnotaId];
        }else {
            $dataTable = [ProduktyVlastnostiManager::columnVlastnostiId => $data['vlastnosti_id'],
                ProduktyVlastnostiManager::columnProduktyId => $data['produkty_id'],
                ProduktyVlastnostiManager::columnVlastnostiHodnotyId => $data['hodnota_id']];
        }

        $this->produktyVlastnostiManager->create($dataTable);
    }
}