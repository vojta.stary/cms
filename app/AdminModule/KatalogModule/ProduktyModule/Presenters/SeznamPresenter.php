<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Presenters;

use App\AdminModule\Components\Paginator\PaginatorFactory;
use App\AdminModule\KatalogModule\ProduktyModule\Components\NovyProdukt\NovyProduktFactory;
use App\AdminModule\Presenters\BaseAdminPresenter;
use App\Managers\ProduktyManager;

final class SeznamPresenter extends BaseAdminPresenter
{
    const pageLimit = 12;

    private $produktyManager;
    private $novyProduktFactory;
    private $paginator;
    private $page = 1;
    private $itemsCount;
    private $filter = [];

    public function __construct(ProduktyManager     $produktyManager,
                                NovyProduktFactory  $novyProduktFactory,
                                PaginatorFactory    $paginatorFactory)
    {
        $this->produktyManager = $produktyManager;
        $this->novyProduktFactory = $novyProduktFactory;
        $this->paginator = $paginatorFactory;
    }

    public function renderDefault()
    {
        $offset = ($this->page - 1 )* self::pageLimit;

        $data = $this->getData();

        $this->itemsCount = $data->count();

        $this->template->page = $this->page;
        $this->template->filter = $this->filter;
        $this->template->data = $data->limit(self::pageLimit,$offset)->fetchAll();
    }

    /*=================== handle metody ===================*/
    /**
     * nastavení ajax stránkování pro paginator
     * @param int $page
     * @param array $filter
     * @return void
     */
    public function handlePage(int $page, array $filter): void
    {
        if ($this->presenter->isAjax()){
            $this->filter = $filter;
            $this->page = $page;
            $this->redrawControl('list');
        }
    }

    /**
     * nastavení filtrace seznamu
     * @param array $filter
     * @return void
     */
    public function handleFilter(array $filter): void
    {
        if ($this->presenter->isAjax())
        {
            $this->filter = $filter;
            $this->redrawControl('list');
        }
    }

    /**
     * změna aktivity
     * @param int $id
     * @param array $filter
     * @param int $page
     * @return void
     */
    public function handleChangeActive(int $id,array $filter, int $page): void
    {
        if( $this->presenter->isAjax()){
            $this->page = $page;
            $this->filter = $filter;
            $this->produktyManager->changeActive($id);
            $this->redrawControl('list');
        }
    }

    /*=================== komponenty ===================*/

    /**
     * komponenta paginatoru seznamu
     * @return \App\AdminModule\Components\Paginator\PaginatorControl
     */
    protected function createComponentPaginator()
    {
        return $this->paginator->create($this->itemsCount,$this->page,'page!',$this->filter, self::pageLimit);
    }

    /**
     * nový produkt
     * @return \App\AdminModule\KatalogModule\ProduktyModule\Components\NovyProdukt\NovyProduktControl
     */
    protected function createComponentNovyProdukt()
    {
        return $this->novyProduktFactory->create();
    }

    /*=================== pomocné funkce ===================*/

    /**
     * načítání a filtrování dat pro seznam
     * @return \Nette\Database\Table\Selection
     */
    public function getData()
    {
        $database = $this->produktyManager->select();

        if (isset($this->filter['nazev']))
        {
            $database->where('nazev LIKE ?','%'.$this->filter['nazev'].'%');
        }

        if (isset($this->filter['aktivni']))
        {
            $database->where(ProduktyManager::columnActive,$this->filter['aktivni']);
        }

        if (isset($this->filter['typ']))
        {
            $database->where(ProduktyManager::columnTyp,$this->filter['typ']);
        }

        return $database->order(ProduktyManager::columnNazev.' ASC');
    }
}