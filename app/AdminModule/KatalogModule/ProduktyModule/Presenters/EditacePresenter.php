<?php

namespace App\AdminModule\KatalogModule\ProduktyModule\Presenters;

use App\AdminModule\KatalogModule\KategorieModule\Models\KategorieModel;
use App\AdminModule\KatalogModule\ProduktyModule\Components\ProduktObrazky\ProduktyObrazkyFactory;
use App\AdminModule\KatalogModule\ProduktyModule\Components\ProduktPrilohy\ProduktyPrilohyFactory;
use App\AdminModule\KatalogModule\ProduktyModule\Components\ProduktSleva\ProduktSlevaFactory;
use App\AdminModule\KatalogModule\ProduktyModule\Components\ProduktVlastnosti\ProduktyVastnostiFactory;
use App\AdminModule\KatalogModule\ProduktyModule\Components\VymazProdukt\VymazProduktFactory;
use App\AdminModule\KatalogModule\ProduktyModule\Models\ProduktyModel;
use App\AdminModule\Presenters\BaseAdminPresenter;
use App\Managers\KategorieManager;
use App\Managers\ProduktyManager;
use App\Managers\SetKatalogManager;
use App\Managers\VyrobciManager;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

final class EditacePresenter extends BaseAdminPresenter
{
    private $produktyManager;
    private $kategorieManager;
    private $produktyModel;
    private $kategorieModel;
    private $setKatalogManager;
    private $vyrobciManager;
    private $produktySlevaFactory;
    private $produktyVlastnostiFactory;
    private $produktyObrazkyFactory;
    private $produktyPrilohyFactory;
    private $vymazProduktFactory;
    private $produktId;
    private $setCena;

    public function __construct(ProduktyManager             $produktyManager,
                                KategorieManager            $kategorieManager,
                                SetKatalogManager           $setKatalogManager,
                                VyrobciManager              $vyrobciManager,
                                ProduktSlevaFactory         $produktSlevaFactory,
                                ProduktyVastnostiFactory    $produktyVastnostiFactory,
                                ProduktyObrazkyFactory      $produktyObrazkyFactory,
                                ProduktyPrilohyFactory      $produktyPrilohyFactory,
                                VymazProduktFactory         $vymazProduktFactory,
                                ProduktyModel               $produktyModel,
                                KategorieModel              $kategorieModel)
    {
        $this->produktyManager = $produktyManager;
        $this->kategorieManager = $kategorieManager;
        $this->setKatalogManager = $setKatalogManager;
        $this->vyrobciManager = $vyrobciManager;
        $this->produktySlevaFactory = $produktSlevaFactory;
        $this->produktyVlastnostiFactory = $produktyVastnostiFactory;
        $this->produktyObrazkyFactory = $produktyObrazkyFactory;
        $this->produktyPrilohyFactory = $produktyPrilohyFactory;
        $this->vymazProduktFactory = $vymazProduktFactory;
        $this->produktyModel = $produktyModel;
        $this->kategorieModel = $kategorieModel;
    }

    /*=================================== action metody ===================================*/

    public function actionDefault(int $id)
    {
        $produkt = $this->produktyManager->getOne($id);

        if(!$produkt){
            $this->flashMessage('Produkt nebyl nalezen','warning');
            $this->redirect(':Admin:Katalog:Produkty:Seznam:default');
        }

        $this->produktId = $id;
    }

    /*=================================== render metody ===================================*/

    public function renderDefault()
    {
        //nastavení
        $this->setCena = $this->setKatalogManager->getOneSet('zobrazCena');

        //data pro šablonu
        $produkt = $this->produktyManager->getOne($this->produktId);

        $this['editProduktForm']->setDefaults($produkt);
        $this->template->prirazKat = $this->getPrirazKategorie();
        $this->template->kategorieModel = $this->kategorieModel;
        $this->template->produkt = $produkt;
        $this->template->setCena = $this->setCena;
    }

    /*============================= formuláře =============================*/

    /**
     * formulář editace produktu
     * @return Form
     */
    protected function createComponentEditProduktForm(): Form
    {
        $vyrobciData  = $this->vyrobciManager->select()->fetchAll();

        $vyrobci = [];
        foreach ($vyrobciData as $one){
            $vyrobci[$one->id] = $one->nazev;
        }

        $form = new Form;
        $form->addHidden('id');
        $form->addHidden('set_cena')
            ->setDefaultValue($this->setCena);
        $form->addText('nazev')
            ->setRequired('Musíte vyplnit název produktu!');
        $form->addText('kod')
            ->setRequired('Musíte vyplnit kód produktu!');
        $form->addText('ean')
            ->setNullable();
        $form->addSelect('vyrobci_id','',$vyrobci)
            ->setPrompt('Vyberte výrobce');
        $form->addTextArea('popis_short');
        $form->addCheckboxList('kategorie');
        $form->addTextArea('popis_long');
        //pokud je nastaveno používání cen
        if ($this->setCena){
            $form->addText('cena')
                ->setRequired('Musíte vyplnit cenu bez Dph produktu!');
            $form->addText('cena_dph')
                ->setRequired('Musíte vyplnit cenu s Dph produktu!');
        }

        $form->addTextArea('meta_desc');
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendEditProduktForm'];
        return $form;
    }

    public function sendEditProduktForm(Form $form, $values)
    {
        try {
            $data = $form->getHttpData();
            unset($data['_do']);
            unset($data['send']);
            $data = ArrayHash::from($data);

            $this->produktyModel->update($data->id, $data);
            $this->flashMessage('Data byla uložena.', 'success');
            $this->redirect('this');
        } catch (\PDOException $e) {
            $this->flashMessage('Chyba zápisu do databáze.', 'danger');
            $this->redirect('this');
        }
    }

    /*=================================== komponenty ===================================*/

    /**
     * sleva ceny produktu
     * @return \App\AdminModule\KatalogModule\ProduktyModule\Components\ProduktSleva\ProduktSlevaControl
     */
    protected function createComponentProduktSleva()
    {
        $this->produktySlevaFactory->setProdukt($this->produktId);
        return $this->produktySlevaFactory->create();
    }

    /**
     * vlastnosti produktu
     * @return \App\AdminModule\KatalogModule\ProduktyModule\Components\ProduktVlastnosti\ProduktyVlastnostiControl
     */
    protected function createComponentProduktVlastnosti()
    {
        $this->produktyVlastnostiFactory->setProdukt($this->produktId);
        return $this->produktyVlastnostiFactory->create();
    }

    /**
     * obrázky produktu
     * @return \App\AdminModule\KatalogModule\ProduktyModule\Components\ProduktObrazky\ProduktObrazkyControl
     */
    protected function createComponentProduktObrazky()
    {
        $this->produktyObrazkyFactory->setProdukt($this->produktId);
        return $this->produktyObrazkyFactory->create();
    }

    /**
     * přílohy produktu
     * @return \App\AdminModule\KatalogModule\ProduktyModule\Components\ProduktPrilohy\ProduktyPrilohyControl
     */
    protected function createComponentProduktPrilohy()
    {
        $this->produktyPrilohyFactory->setProdukt($this->produktId);
        return $this->produktyPrilohyFactory->create();
    }

    /**
     * vymazání produktu
     * @return \App\AdminModule\KatalogModule\ProduktyModule\Components\VymazProdukt\VymazProduktControl
     */
    protected function createComponentVymazProdukt()
    {
        $this->vymazProduktFactory->setProdukt($this->produktId);
        return $this->vymazProduktFactory->create();
    }

    /*=================================== handle metody ===================================*/

    /**
     * změna aktivity
     * @return void
     */
    public function handleChangeActive(): void
    {
        if ($this->isAjax()){
            $this->produktyManager->changeActive($this->produktId);
            $this->redrawControl('active');
        }
    }

    public function handleCheckKod($kod)
    {
        if ($this->isAjax()){

            $this->payload->kod = $this->produktyManager->select()->where(ProduktyManager::columnKod, $kod)->count();
            $this->sendPayload();
        }
    }

    /**
     * přidá produkt k sadě
     * @return void
     */
    public function handleAddProdukt()
    {
        if ($this->isAjax()){

            $this->redrawControl('formular');
        }
    }

    /*============================= pomocné funkce =============================*/

    /**
     * získání přiřazených kategorií
     * @return array
     */
    private function getPrirazKategorie(): array
    {
        $kategorie = $this->kategorieManager->select();
        $kategorie->where(':produkty_kategorie.produkty_id', $this->produktId)->fetchAll();

        $out = [];
        foreach ($kategorie as $one) {
            array_push($out, $one->id);
        }

        return $out;
    }
}