<?php

namespace App\AdminModule\KatalogModule\VyrobciModule\Components\NovyVyrobce;

use App\AdminModule\KatalogModule\VyrobciModule\Models\VyrobciModel;

class NovyVvyrobceFactory
{
    private $vyrobciModel;

    public function __construct(VyrobciModel $vyrobciModel)
    {
        $this->vyrobciModel = $vyrobciModel;
    }

    public function create()
    {
        return new NovyVyrobceControl($this->vyrobciModel);
    }
}