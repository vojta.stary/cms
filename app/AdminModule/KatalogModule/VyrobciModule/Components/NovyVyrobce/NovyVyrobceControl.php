<?php

namespace App\AdminModule\KatalogModule\VyrobciModule\Components\NovyVyrobce;

use App\AdminModule\KatalogModule\VyrobciModule\Models\VyrobciModel;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\ImageException;

class NovyVyrobceControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $vyrobciModel;

    public function __construct(VyrobciModel $vyrobciModel)
    {
        $this->vyrobciModel = $vyrobciModel;
    }

    public function render()
    {
        $this->template->render(self::template);
    }

    /*============================ formulář ============================*/

    protected function createComponentNovyVyrobceForm(): Form
    {
        $form= new Form;
        $form->addText('nazev','Název')
            ->setRequired('Musíte vyplnit název!');
        $form->addUpload('logo')
            ->addRule($form::IMAGE, 'Obrázek musí být JPEG, PNG, GIF nebo WebP.');
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendNovyVyrobceForm'];
        return $form;
    }

    public function sendNovyVyrobceForm(Form $form, $values)
    {
        try{
            $this->vyrobciModel->create($values);
            $this->presenter->flashMessage('Data byla uložena','success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
            $this->redirect('this');
        }catch (ImageException $e){
            $this->presenter->flashMessage('Logo se nepodařilo nahrát','warning');
            $this->redirect('this');
        }
    }
}