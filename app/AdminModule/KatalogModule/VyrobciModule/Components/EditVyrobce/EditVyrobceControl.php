<?php

namespace App\AdminModule\KatalogModule\VyrobciModule\Components\EditVyrobce;

use App\AdminModule\KatalogModule\VyrobciModule\Models\VyrobciModel;
use App\Managers\VyrobciManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\ImageException;

class EditVyrobceControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $vyrobciManager;
    private $vyrobciModel;
    private $vyrobceId;

    public function __construct(VyrobciManager  $vyrobciManager,
                                VyrobciModel    $vyrobciModel,
                                                $vyrobceId)
    {
        $this->vyrobciManager =  $vyrobciManager;
        $this->vyrobciModel = $vyrobciModel;
        $this->vyrobceId = $vyrobceId;
    }

    public function render()
    {
        $data = $this->vyrobciManager->getOne($this->vyrobceId);

        $this['editVyrobceForm']->setDefaults($data);
        $this->template->data = $data;
        $this->template->render(self::template);
    }

    /*============================ formulář ============================*/

    protected function createComponentEditVyrobceForm(): Form
    {
        $form= new Form;
        $form->addHidden('id');
        $form->addText('nazev','Název')
            ->setRequired('Musíte vyplnit název!');
        $form->addUpload('logo')
            ->addRule($form::IMAGE, 'Obrázek musí být JPEG, PNG, GIF nebo WebP.');
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendEditVyrobceForm'];
        return $form;
    }

    public function sendEditVyrobceForm(Form $form, $values)
    {
        try{
            $this->vyrobciModel->update($values->id,$values);
            $this->presenter->flashMessage('Data byla uložena','success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
            $this->redirect('this');
        }catch (ImageException $e){
            $this->presenter->flashMessage('Logo se nepodařilo nahrát','warning');
            $this->redirect('this');
        }
    }
}