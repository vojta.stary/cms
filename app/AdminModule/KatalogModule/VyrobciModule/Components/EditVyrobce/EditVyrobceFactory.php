<?php

namespace App\AdminModule\KatalogModule\VyrobciModule\Components\EditVyrobce;

use App\AdminModule\KatalogModule\VyrobciModule\Models\VyrobciModel;
use App\Managers\VyrobciManager;

class EditVyrobceFactory
{
    private $vyrobciManager;
    private $vyrobciModel;
    private $vyrobceId;

    public function __construct(VyrobciManager  $vyrobciManager,
                                VyrobciModel    $vyrobciModel)
    {
        $this->vyrobciManager =  $vyrobciManager;
        $this->vyrobciModel = $vyrobciModel;
    }

    public function create()
    {
        return new EditVyrobceControl(  $this->vyrobciManager,
                                        $this->vyrobciModel,
                                        $this->vyrobceId);
    }

    public function setVyrobce(int $id)
    {
        $this->vyrobceId = $id;
    }
}