<?php

namespace App\AdminModule\KatalogModule\VyrobciModule\Components\VymazVyrobce;

use App\AdminModule\KatalogModule\VyrobciModule\Models\VyrobciModel;
use App\AdminModule\ObsahModule\SponzoriModule\Components\VymazVyrobce\VymazVyrobceControl;
use App\Managers\VyrobciManager;

class VymazVyrobceFactory
{
    private $vyrobciModel;
    private $vyrobciManager;
    private $vyrobceId;

    public function __construct(VyrobciModel    $vyrobciModel,
                                VyrobciManager  $vyrobciManager)
    {
        $this->vyrobciModel = $vyrobciModel;
        $this->vyrobciManager = $vyrobciManager;
    }

    public function create()
    {
        return new VymazVyrobceControl( $this->vyrobciModel,
                                        $this->vyrobciManager,
                                        $this->vyrobceId);
    }

    public function setVyrobce(int $id)
    {
        $this->vyrobceId = $id;
    }
}