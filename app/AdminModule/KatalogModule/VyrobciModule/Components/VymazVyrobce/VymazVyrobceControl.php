<?php

namespace App\AdminModule\ObsahModule\SponzoriModule\Components\VymazVyrobce;

use App\AdminModule\KatalogModule\VyrobciModule\Models\VyrobciModel;
use App\Managers\VyrobciManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class VymazVyrobceControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $vyrobciModel;
    private $vyrobciManager;
    private $vyrobceId;

    public function __construct(VyrobciModel    $vyrobciModel,
                                VyrobciManager  $vyrobciManager,
                                                $vyrobceId)
    {
        $this->vyrobciModel = $vyrobciModel;
        $this->vyrobciManager = $vyrobciManager;
        $this->vyrobceId = $vyrobceId;
    }

    public function render()
    {
        $data = $this->vyrobciManager->getOne($this->vyrobceId);

        $this['delVyrobceForm']->setDefaults($data);
        $this->template->data = $data;
        $this->template->render(self::template);
    }

    /*============================ formulář ============================*/

    protected function createComponentDelVyrobceForm(): Form
    {
        $form= new Form;
        $form->addHidden('id');
        $form->addSubmit('send','Vymazat');
        $form->onSuccess[] = [$this, 'sendDelVyrobceForm'];
        return $form;
    }

    public function sendDelVyrobceForm(Form $form, $values)
    {
        try{
            $this->vyrobciModel->delete($values->id);
            $this->presenter->flashMessage('Data byla vymazána','success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
            $this->redirect('this');
        }
    }
}