<?php

namespace App\AdminModule\KatalogModule\VyrobciModule\Presenters;

use App\AdminModule\Components\Paginator\PaginatorFactory;
use App\AdminModule\KatalogModule\VyrobciModule\Components\EditVyrobce\EditVyrobceFactory;
use App\AdminModule\KatalogModule\VyrobciModule\Components\NovyVyrobce\NovyVvyrobceFactory;
use App\AdminModule\KatalogModule\VyrobciModule\Components\VymazVyrobce\VymazVyrobceFactory;
use App\AdminModule\Presenters\BaseAdminPresenter;
use App\Managers\VyrobciManager;
use Nette\Application\UI\Multiplier;

final class SeznamPresenter extends BaseAdminPresenter
{
    const pageLimit = 12;

    private $vyrobciManager;
    private $novyVyrobceFactory;
    private $editVyrobceFactory;
    private $vymazVyrobceFactory;
    private $paginator;
    private $page = 1;
    private $itemsCount;
    private $filter = [];

    public function __construct(VyrobciManager      $vyrobciManager,
                                NovyVvyrobceFactory $novyVvyrobceFactory,
                                EditVyrobceFactory  $editVyrobceFactory,
                                VymazVyrobceFactory $vymazVyrobceFactory,
                                PaginatorFactory    $paginatorFactory)
    {

        $this->vyrobciManager = $vyrobciManager;
        $this->novyVyrobceFactory = $novyVvyrobceFactory;
        $this->editVyrobceFactory = $editVyrobceFactory;
        $this->vymazVyrobceFactory = $vymazVyrobceFactory;
        $this->paginator = $paginatorFactory;
    }

    public function renderDefault()
    {
        $offset = ($this->page - 1 )* self::pageLimit;

        $data = $this->getData();

        $this->itemsCount = $data->count();

        $this->template->page = $this->page;
        $this->template->filter = $this->filter;
        $this->template->data = $data->limit(self::pageLimit,$offset)->fetchAll();
    }

    /*=================== handle metody ===================*/
    /**
     * nastavení ajax stránkování pro paginator
     * @param int $page
     * @param array $filter
     * @return void
     */
    public function handlePage(int $page, array $filter): void
    {
        if ($this->presenter->isAjax()){
            $this->filter = $filter;
            $this->page = $page;
            $this->redrawControl('list');
        }
    }

    /**
     * nastavení filtrace seznamu
     * @param array $filter
     * @return void
     */
    public function handleFilter(array $filter): void
    {
        if ($this->presenter->isAjax())
        {
            $this->filter = $filter;
            $this->redrawControl('list');
        }
    }

    /*=================== komponenty ===================*/

    /**
     * komponenta paginatoru seznamu
     * @return \App\AdminModule\Components\Paginator\PaginatorControl
     */
    protected function createComponentPaginator()
    {
        return $this->paginator->create($this->itemsCount,$this->page,'page!',$this->filter, self::pageLimit);
    }

    /**
     * nový výrobce
     * @return \App\AdminModule\KatalogModule\VyrobciModule\Components\NovyVyrobce\NovyVyrobceControl
     */
    protected function createComponentNovyVyrobce()
    {
        return $this->novyVyrobceFactory->create();
    }

    /**
     * editace výrobce
     * @return Multiplier
     */
    protected function createComponentEditVyrobce()
    {
        return new Multiplier(function ($id){
            $this->editVyrobceFactory->setVyrobce($id);
            return $this->editVyrobceFactory->create();
        });
    }

    /**
     * vymazání výrobce
     * @return Multiplier
     */
    protected function createComponentVymazVyrobce()
    {
        return new Multiplier(function ($id){
            $this->vymazVyrobceFactory->setVyrobce($id);
            return $this->vymazVyrobceFactory->create();
        });
    }

    /*=================== pomocné funkce ===================*/

    /**
     * načítání a filtrování dat pro seznam
     * @return \Nette\Database\Table\Selection
     */
    public function getData()
    {
        $database = $this->vyrobciManager->select();

        if (isset($this->filter['nazev']))
        {
            $database->where('nazev LIKE ?','%'.$this->filter['nazev'].'%');
        }

        return $database->order(VyrobciManager::columnNazev.' ASC');
    }
}