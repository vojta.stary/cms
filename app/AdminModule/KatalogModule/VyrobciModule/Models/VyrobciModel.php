<?php

namespace App\AdminModule\KatalogModule\VyrobciModule\Models;

use App\Managers\VyrobciManager;
use Nette\Utils\Image;
use Nette\Utils\Strings;

final class VyrobciModel
{
    const logoDir = __DIR__.'/../../../../../www/src/images/vyrobci';
    private $vyrobciManager;

    public function __construct(VyrobciManager $vyrobciManager)
    {
        $this->vyrobciManager = $vyrobciManager;
    }

    public function create($values): void
    {
        $databaseData = [VyrobciManager::columnNazev => $values->nazev];

        $newId = $this->vyrobciManager->create($databaseData);

        $this->saveLogo($newId,$values->logo);
    }

    public function update(int $id, $values): void
    {
        $database = $this->vyrobciManager->getOne($id);

        $logo = $values->logo;

        if ($logo->hasFile()){
            $this->deleteLogo($id, $database->logo);
            $this->saveLogo($id, $logo);
        }

        $database->update([VyrobciManager::columnNazev => $values->nazev]);
    }

    /**
     * vymazání výrobce
     * @param int $id
     * @return void
     */
    public function delete(int $id): void
    {
        $database = $this->vyrobciManager->getOne($id);

        if ($database->logo){
            $this->deleteLogo($id, $database->logo);
        }

        $dir = self::logoDir.'/'.$id.'/';

        if (is_dir($dir)){
            rmdir($dir);
        }

        $database->delete();
    }

    /*======================= funkce loga =======================*/
    public function saveLogo(int $vyrobceId, $logo): void
    {
        if ($logo->hasFile()) {
            $newName = $logo->getName();

            $pripona = pathinfo($newName);

            //upravený název na web friendly
            $newName = Strings::webalize($pripona['filename']) . '.' . $pripona['extension'];

            //uloží soubor
            $newFile = self::logoDir .'/'.$vyrobceId.'/'. $newName;
            $logo->move($newFile);

            //uloží data do databáze
            $database = $this->vyrobciManager->getOne($vyrobceId);
            $database->update([VyrobciManager::columnLogo => $newName]);

            //změní velikost
            $obrazek = Image::fromFile($newFile);
            if($obrazek->getHeight() > 150){
                $obrazek->resize(null,150);
                $obrazek->save($newFile,80);
            }
        }
    }

    public function deleteLogo(int $vyrobceId, $logo): void
    {
        $file = self::logoDir.'/'.$vyrobceId.'/'.$logo;

        if (is_file($file)){
            unlink($file);
        }
    }
}