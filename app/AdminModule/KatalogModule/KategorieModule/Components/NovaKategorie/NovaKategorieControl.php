<?php

namespace App\AdminModule\KatalogModule\KategorieModule\Components\NovaKategorie;

use App\AdminModule\KatalogModule\KategorieModule\Models\KategorieModel;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class NovaKategorieControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $kategorieModel;
    private $parentId;

    public function __construct(KategorieModel  $kategorieModel,
                                                $parentId)
    {
        $this->kategorieModel = $kategorieModel;
        $this->parentId = $parentId;
    }

    public function render()
    {
        $this->template->render(self::template);
    }

    /*================================= formulář =================================*/
    protected function createComponentNovaKategorieForm(): Form
    {
        $form = new Form;
        $form->addText('nazev','Název')
            ->setRequired('Musíte vyplnit název!');
        $form->addHidden('active')
            ->setDefaultValue(1);
        if ($this->parentId != null){
            $form->addHidden('parent_id')
                ->setDefaultValue($this->parentId);
        }
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendNovaKategorieForm'];

        return $form;
    }

    public function sendNovaKategorieForm(Form $form, $values)
    {
        try{

            $id = $this->kategorieModel->create($values);
            $this->presenter->flashMessage('Data byla uložena','success');
            $this->presenter->redirect(':Admin:Katalog:Kategorie:Editace:default',$id);

        }catch (\PDOException $e){

            $this->presenter->flashMessage('Chyba zápisu dat do databáze', 'danger');
            $this->redirect('this');

        }
    }
}