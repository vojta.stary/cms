<?php

namespace App\AdminModule\KatalogModule\KategorieModule\Components\NovaKategorie;

use App\AdminModule\KatalogModule\KategorieModule\Models\KategorieModel;

class NovaKategorieFactory
{
    private $kategorieModel;
    private $parentId = null;

    public function __construct(KategorieModel  $kategorieModel)
    {
        $this->kategorieModel = $kategorieModel;
    }

    public function create()
    {
        return new NovaKategorieControl($this->kategorieModel,
                                        $this->parentId);
    }

    public function setParent($id)
    {
        $this->parentId = $id;
    }
}