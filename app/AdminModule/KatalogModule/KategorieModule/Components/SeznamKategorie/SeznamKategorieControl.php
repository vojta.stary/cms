<?php

namespace App\AdminModule\KatalogModule\KategorieModule\Components\SeznamKategorie;

use App\AdminModule\Components\Paginator\PaginatorFactory;
use App\AdminModule\KatalogModule\KategorieModule\Components\NovaKategorie\NovaKategorieFactory;
use App\AdminModule\KatalogModule\KategorieModule\Models\KategorieModel;
use App\Managers\KategorieManager;
use Nette\Application\UI\Control;

class SeznamKategorieControl extends Control
{
    const template = __DIR__.'/templates/template.latte';
    const pageLimit = 12;

    private $kategorieManager;
    private $kategorieModel;
    private $novaKategorieFactory;
    private $parentId;
    private $paginator;
    private $page = 1;
    private $itemsCount;
    private $filter = [];

    public function __construct(KategorieManager        $kategorieManager,
                                KategorieModel          $kategorieModel,
                                NovaKategorieFactory    $novaKategorieFactory,
                                PaginatorFactory        $paginatorFactory,
                                                        $parentId)
    {
        $this->kategorieManager = $kategorieManager;
        $this->kategorieModel = $kategorieModel;
        $this->novaKategorieFactory = $novaKategorieFactory;
        $this->paginator = $paginatorFactory;
        $this->parentId = $parentId;
    }

    public function render()
    {
        $offset = ($this->page - 1 )* self::pageLimit;

        $data = $this->getData();

        $this->itemsCount = $data->count();

        $this->template->count = $this->getCountAll();
        $this->template->page = $this->page;
        $this->template->filter = $this->filter;
        $this->template->data = $data->limit(self::pageLimit,$offset)->fetchAll();

        $this->template->render(self::template);
    }

    /*=================== handle metody ===================*/
    /**
     * nastavení ajax stránkování pro paginator
     * @param int $page
     * @param array $filter
     * @return void
     */
    public function handlePage(int $page, array $filter): void
    {
        if ($this->presenter->isAjax()){
            $this->filter = $filter;
            $this->page = $page;
            $this->redrawControl('list');
        }
    }

    /**
     * nastavení filtrace seznamu
     * @param array $filter
     * @return void
     */
    public function handleFilter(array $filter): void
    {
        if ($this->presenter->isAjax())
        {
            $this->filter = $filter;
            $this->redrawControl('list');
        }
    }

    /**
     * změna aktivity
     * @param int $id
     * @param array $filter
     * @param int $page
     * @return void
     */
    public function handleChangeActive(int $id,array $filter, int $page): void
    {
        if( $this->presenter->isAjax()){
            $this->page = $page;
            $this->filter = $filter;
            $this->kategorieManager->changeActive($id);
            $this->redrawControl('list');
        }
    }

    /**
     * změna pořadí směrem nahoru
     * @param int $id
     * @return void
     */
    public function handleSortUp(int $id): void
    {
        if ($this->presenter->isAjax()){
            try {
                $this->kategorieModel->sortUp($id);
                $this->redrawControl('list');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('Chyba zápisu do databáze','danger');
                $this->presenter->redrawControl('flash');
            }
        }
    }

    /**
     * změna pořadí směrem dolů
     * @param int $id
     * @return void
     */
    public function handleSortDown(int $id): void
    {
        if ($this->presenter->isAjax()){
            try {
                $this->kategorieModel->sortDown($id);
                $this->redrawControl('list');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('Chyba zápisu do databáze','danger');
                $this->presenter->redrawControl('flash');
            }
        }
    }

    /*=================== komponenty ===================*/

    /**
     * komponenta paginatoru seznamu
     * @return \App\AdminModule\Components\Paginator\PaginatorControl
     */
    protected function createComponentPaginator()
    {
        return $this->paginator->create($this->itemsCount,$this->page,'page!',$this->filter, self::pageLimit);
    }

    /**
     * nová kategorie
     * @return \App\AdminModule\KatalogModule\KategorieModule\Components\NovaKategorie\NovaKategorieControl
     */
    protected function createComponentNovaKategorie()
    {
        $this->novaKategorieFactory->setParent($this->parentId);
        return $this->novaKategorieFactory->create();
    }

    /*=================== pomocné funkce ===================*/

    /**
     * načítání a filtrování dat pro seznam
     * @return \Nette\Database\Table\Selection
     */
    public function getData()
    {
        $database = $this->kategorieManager->select();
        $database->where(KategorieManager::columnParentId, $this->parentId);

        if (isset($this->filter['nazev']))
        {
            $database->where('nazev LIKE ?','%'.$this->filter['nazev'].'%');
        }

        if (isset($this->filter['aktivni']))
        {
            $database->where(KategorieManager::columnActive,$this->filter['aktivni']);
        }

        return $database->order(KategorieManager::columnSort.' ASC');
    }

    /**
     * vrací počet kategorií
     * @return int
     */
    private function getCountAll(){
        $database = $this->kategorieManager->select();
        $database->where(KategorieManager::columnParentId, $this->parentId);

        return $database->count();
    }
}