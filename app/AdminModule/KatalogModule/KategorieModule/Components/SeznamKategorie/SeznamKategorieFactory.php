<?php

namespace App\AdminModule\KatalogModule\KategorieModule\Components\SeznamKategorie;

use App\AdminModule\Components\Paginator\PaginatorFactory;
use App\AdminModule\KatalogModule\KategorieModule\Components\NovaKategorie\NovaKategorieFactory;
use App\AdminModule\KatalogModule\KategorieModule\Models\KategorieModel;
use App\Managers\KategorieManager;

class SeznamKategorieFactory
{
    private $kategorieManager;
    private $kategorieModel;
    private $novaKategorieFactory;
    private $parentId = null;
    private $paginator;

    public function __construct(KategorieManager        $kategorieManager,
                                KategorieModel          $kategorieModel,
                                NovaKategorieFactory    $novaKategorieFactory,
                                PaginatorFactory        $paginatorFactory)
    {
        $this->kategorieManager = $kategorieManager;
        $this->kategorieModel = $kategorieModel;
        $this->novaKategorieFactory = $novaKategorieFactory;
        $this->paginator = $paginatorFactory;
    }

    public function create()
    {
        return new SeznamKategorieControl(  $this->kategorieManager,
                                            $this->kategorieModel,
                                            $this->novaKategorieFactory,
                                            $this->paginator,
                                            $this->parentId);
    }

    public function setParent(int $id)
    {
        $this->parentId = $id;
    }
}