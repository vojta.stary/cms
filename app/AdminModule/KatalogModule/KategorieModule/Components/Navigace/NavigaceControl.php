<?php

namespace App\AdminModule\KatalogModule\KategorieModule\Components\Navigace;

use App\AdminModule\KatalogModule\KategorieModule\Models\KatSeznamModel;
use Nette\Application\UI\Control;

class NavigaceControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $katSeznamModel;

    public function __construct(KatSeznamModel      $katSeznamModel)
    {
        $this->katSeznamModel = $katSeznamModel;
    }

    public function render()
    {
        $this->template->katSeznamModel = $this->katSeznamModel;
        $this->template->render(self::template);
    }
}