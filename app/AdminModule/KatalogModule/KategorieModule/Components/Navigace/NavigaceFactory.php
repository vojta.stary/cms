<?php

namespace App\AdminModule\KatalogModule\KategorieModule\Components\Navigace;

use App\AdminModule\KatalogModule\KategorieModule\Models\KatSeznamModel;

class NavigaceFactory
{
    private $katSeznamModel;

    public function __construct(KatSeznamModel  $katSeznamModel)
    {
        $this->katSeznamModel = $katSeznamModel;
    }

    public function create()
    {
        return new NavigaceControl($this->katSeznamModel);
    }
}