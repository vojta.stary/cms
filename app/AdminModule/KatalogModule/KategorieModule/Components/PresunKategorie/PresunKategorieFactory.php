<?php

namespace App\AdminModule\KatalogModule\KategorieModule\Components\PresunKategorie;

use App\AdminModule\KatalogModule\KategorieModule\Models\KategorieModel;
use App\AdminModule\KatalogModule\KategorieModule\Models\KatSeznamModel;
use App\Managers\KategorieManager;

class PresunKategorieFactory
{
    private $kategorieManager;
    private $katSeznamModel;
    private $kategorieModel;
    private $kategorieId;

    public function __construct(KategorieManager    $kategorieManager,
                                KatSeznamModel      $katSeznamModel,
                                KategorieModel      $kategorieModel)
    {
        $this->kategorieManager = $kategorieManager;
        $this->katSeznamModel = $katSeznamModel;
        $this->kategorieModel = $kategorieModel;
    }

    public function create()
    {
        return new PresunKategorieControl(  $this->kategorieManager,
                                            $this->katSeznamModel,
                                            $this->kategorieModel,
                                            $this->kategorieId);
    }

    public function setKategorie($kategorieId)
    {
        $this->kategorieId = $kategorieId;
    }
}