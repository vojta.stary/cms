<?php

namespace App\AdminModule\KatalogModule\KategorieModule\Components\PresunKategorie;

use App\AdminModule\KatalogModule\KategorieModule\Models\KategorieModel;
use App\AdminModule\KatalogModule\KategorieModule\Models\KatSeznamModel;
use App\Managers\KategorieManager;
use Nette\Application\UI\Control;

class PresunKategorieControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $kategorieManager;
    private $katSeznamModel;
    private $kategorieModel;
    private $kategorieId;

    public function __construct(KategorieManager    $kategorieManager,
                                KatSeznamModel      $katSeznamModel,
                                KategorieModel      $kategorieModel,
                                                    $kategorieId)
    {
        $this->kategorieManager = $kategorieManager;
        $this->katSeznamModel = $katSeznamModel;
        $this->kategorieModel = $kategorieModel;
        $this->kategorieId = $kategorieId;
    }

    public function render()
    {
        $kategorie = $this->kategorieManager->getOne($this->kategorieId);

        $this->template->kategorie = $kategorie;
        $this->template->katSeznamModel = $this->katSeznamModel;
        $this->template->render(self::template);
    }

    /*======================= handle metody =======================*/
    public function handlePresun(int $parentId = null, int $kategorieId = null)
    {
        if($this->presenter->isAjax()){
            $this->kategorieModel->changeParent($kategorieId,$parentId);
            $this->presenter->flashMessage('Kategorie byla přesunuta.','success');
            $this->presenter->redrawControl('kategorie');
            $this->presenter->redrawControl('flash');
        }
    }
}