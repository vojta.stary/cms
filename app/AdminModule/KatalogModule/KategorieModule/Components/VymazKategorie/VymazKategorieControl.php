<?php

namespace App\AdminModule\KatalogModule\KategorieModule\Components\VymazKategorie;

use App\AdminModule\KatalogModule\KategorieModule\Models\KategorieModel;
use App\Managers\KategorieManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class VymazKategorieControl extends Control
{
    const tempate = __DIR__.'/templates/template.latte';

    private $kategorieManager;
    private $kategorieModel;
    private $kategorieId;

    public function __construct(KategorieManager    $kategorieManager,
                                KategorieModel      $kategorieModel,
                                                    $kategorieId)
    {
        $this->kategorieManager = $kategorieManager;
        $this->kategorieModel = $kategorieModel;
        $this->kategorieId = $kategorieId;
    }

    public function render()
    {
        $data = $this->kategorieManager->getOne($this->kategorieId);

        $this['vymazKategorieForm']->setDefaults($data);
        $this->template->data = $data;
        $this->template->render(self::tempate);
    }

    /*================================= formulář =================================*/

    /**
     * formulář vymazání
     * @return Form
     */
    protected function createComponentVymazKategorieForm(): Form
    {
        $form = new Form;
        $form->addHidden('id');
        $form->addSubmit('send', 'Vymazat');
        $form->onSuccess[] = [$this, 'sendVymazKategorieForm'];
        return $form;
    }

    public function sendVymazKategorieForm(Form $form, $values)
    {
        try{
            $this->kategorieModel->delete($values->id);
            $this->presenter->flashMessage('Data byla vymazána', 'success');
            $this->presenter->redirect(':Admin:Katalog:Kategorie:Seznam:default');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Interní chyba aplikace', 'danger');
            $this->redirect('this');
        }
    }
}