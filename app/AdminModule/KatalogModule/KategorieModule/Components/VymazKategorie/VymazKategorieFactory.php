<?php

namespace App\AdminModule\KatalogModule\KategorieModule\Components\VymazKategorie;

use App\AdminModule\KatalogModule\KategorieModule\Models\KategorieModel;
use App\Managers\KategorieManager;

class VymazKategorieFactory
{
    private $kategorieManager;
    private $kategorieModel;
    private $kategorieId;

    public function __construct(KategorieManager    $kategorieManager,
                                KategorieModel      $kategorieModel)
    {
        $this->kategorieManager = $kategorieManager;
        $this->kategorieModel = $kategorieModel;
    }

    public function create()
    {
        return new VymazKategorieControl(   $this->kategorieManager,
                                            $this->kategorieModel,
                                            $this->kategorieId);
    }

    public function setKategorie(int $id)
    {
        $this->kategorieId = $id;
    }
}