<?php

namespace App\AdminModule\KatalogModule\KategorieModule\Presenters;

use App\AdminModule\KatalogModule\KategorieModule\Components\PresunKategorie\PresunKategorieFactory;
use App\AdminModule\KatalogModule\KategorieModule\Components\SeznamKategorie\SeznamKategorieFactory;
use App\AdminModule\KatalogModule\KategorieModule\Components\VymazKategorie\VymazKategorieFactory;
use App\AdminModule\KatalogModule\KategorieModule\Models\KatBreadcrumbsModel;
use App\AdminModule\KatalogModule\KategorieModule\Models\KategorieModel;
use App\AdminModule\Presenters\BaseAdminPresenter;
use App\Managers\KategorieManager;
use Nette\Application\UI\Form;

final class EditacePresenter extends BaseAdminPresenter
{
    private $kategorieManager;
    private $kategorieModel;
    private $katBreadCrumbModel;
    private $seznamKategorieFactory;
    private $presunKategorieFactory;
    private $vymazKategorieFactory;
    private $kategorieId;

    public function __construct(KategorieManager        $kategorieManager,
                                KategorieModel          $kategorieModel,
                                KatBreadcrumbsModel     $katBreadCrumbModel,
                                SeznamKategorieFactory  $seznamKategorieFactory,
                                PresunKategorieFactory  $presunKategorieFactory,
                                VymazKategorieFactory   $vymazKategorieFactory)
    {
        $this->kategorieManager = $kategorieManager;
        $this->kategorieModel = $kategorieModel;
        $this->katBreadCrumbModel = $katBreadCrumbModel;
        $this->seznamKategorieFactory = $seznamKategorieFactory;
        $this->presunKategorieFactory = $presunKategorieFactory;
        $this->vymazKategorieFactory = $vymazKategorieFactory;
    }

    /*========================= action metody =========================*/

    public function actionDefault(int $id)
    {
        $kategorie = $this->kategorieManager->getOne($id);

        if (!$kategorie){
            $this->flashMessage('Kategorie nebyla nalezena','warning');
            $this->redirect(':Admin:Katalog:Kategorie:Seznam:default');
        }

        $this->kategorieId = $id;
    }

    /*========================= render metody =========================*/
    public function renderDefault()
    {
        $kategorie = $this->kategorieManager->getOne($this->kategorieId);
        $breadCrumbs = $this->katBreadCrumbModel->generateBreadcrumbs($this->kategorieId);


        $this['editKategorieForm']->setDefaults($kategorie);
        $this->template->breadCrumbs = $breadCrumbs;
        $this->template->kategorie = $kategorie;
    }

    /*========================= formulář =========================*/
    protected function createComponentEditKategorieForm(): Form
    {
        $form= new Form;
        $form->addText('nazev','Název kategorie')
            ->setRequired('Musíte vyplnit název!');
        $form->addText('url','URL adresa')
            ->setRequired('Musíte vyplnit adresu!');
        $form->addTextArea('meta_desc','SEO popis');
        $form->addTextArea('content','Obsah stránky');
        $form->addHidden('id')
            ->setDefaultValue($this->kategorieId);
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendEditKategorieForm'];
        return $form;
    }

    public function sendEditKategorieForm(Form $form, $values)
    {
        try {
            $this->kategorieModel->update($values->id, $values);
            $this->flashMessage('Data byla uložena', 'success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this->flashMessage('Chyba zápisu dat do databáze', 'danger');
            $form->setDefaults($values);
            $this->redirect('this');
        }
    }

    /*========================= handle metody =========================*/

    /**
     * změna aktivity
     * @param int $id
     * @return void
     */
    public function handleChangeActive(int $id): void
    {
        if( $this->isAjax()){
            $this->kategorieManager->changeActive($id);
            $this->redrawControl('active');
        }
    }

    /*========================= komponenty =========================*/
    /**
     * seznam kategorií
     * @return \App\AdminModule\KatalogModule\KategorieModule\Components\SeznamKategorie\SeznamKategorieControl
     */
    protected function createComponentSeznamKategorie()
    {
        $this->seznamKategorieFactory->setParent($this->kategorieId);
        return $this->seznamKategorieFactory->create();
    }

    /**
     * přesun kategorie
     * @return \App\AdminModule\KatalogModule\KategorieModule\Components\PresunKategorie\PresunKategorieControl
     */
    protected function createComponentPresunKategorie()
    {
        $this->presunKategorieFactory->setKategorie($this->kategorieId);
        return $this->presunKategorieFactory->create();
    }

    /**
     * vymazání kategorie
     * @return \App\AdminModule\KatalogModule\KategorieModule\Components\VymazKategorie\VymazKategorieControl
     */
    protected function createComponentVymazKategorie()
    {
        $this->vymazKategorieFactory->setKategorie($this->kategorieId);
        return $this->vymazKategorieFactory->create();
    }
}