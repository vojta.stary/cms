<?php

namespace App\AdminModule\KatalogModule\KategorieModule\Presenters;

use App\AdminModule\KatalogModule\KategorieModule\Components\Navigace\NavigaceFactory;
use App\AdminModule\KatalogModule\KategorieModule\Components\SeznamKategorie\SeznamKategorieFactory;
use App\AdminModule\Presenters\BaseAdminPresenter;

final class SeznamPresenter extends BaseAdminPresenter
{

    private $seznamKategorieFactory;
    private $navigaceFactory;

    public function __construct(SeznamKategorieFactory  $seznamKategorieFactory,
                                NavigaceFactory         $navigaceFactory)
    {
        $this->seznamKategorieFactory = $seznamKategorieFactory;
        $this->navigaceFactory = $navigaceFactory;
    }


    /*=================== komponenty ===================*/

    /**
     * seznam kategorií
     * @return \App\AdminModule\KatalogModule\KategorieModule\Components\SeznamKategorie\SeznamKategorieControl
     */
    protected function createComponentSeznamKategorie()
    {
        return $this->seznamKategorieFactory->create();
    }

    /**
     * rychlá navigace
     * @return \App\AdminModule\KatalogModule\KategorieModule\Components\Navigace\NavigaceControl
     */
    protected function createComponentRychlaNavigace()
    {
        return $this->navigaceFactory->create();
    }

}