<?php

namespace App\AdminModule\KatalogModule\KategorieModule\Models;

use App\Managers\KategorieManager;
use App\Managers\ProduktyKategorieManager;
use Nette\Database\Table\Selection;
use Nette\Utils\Strings;

final class KategorieModel
{
    private $kategorieManager;
    private $produktyKategorieManager;

    public function __construct(KategorieManager            $kategorieManager,
                                ProduktyKategorieManager    $produktyKategorieManager)
    {
        $this->kategorieManager = $kategorieManager;
        $this->produktyKategorieManager = $produktyKategorieManager;
    }

    /**
     * vytváří nový záznam vč. pořadí a url
     * @param $values
     * @return int
     */
    public function create($values): int
    {
        $database = $this->kategorieManager->select();

        if(isset($values->parent_id)){
            $database->where(KategorieManager::columnParentId, $values->parent_id);
        }else{
            $database->where(KategorieManager::columnParentId, null);
        }
        $pocetMenu = $database->count();

        $sort = $pocetMenu + 1;

        $values->sort = $sort;

        $newId = $this->kategorieManager->create($values);

        $url = Strings::webalize($newId.'-'.$values->nazev);

        $this->kategorieManager->update($newId, [KategorieManager::columnUrl => $url]);

        return $newId;
    }

    /**
     * edituje data
     * @param int $id
     * @param $values
     * @return void
     */
    public function update(int $id, $values): void
    {
        $database = $this->kategorieManager->getOne($id);

        $values->url = Strings::webalize($id.'-'.$values->nazev);

        $database->update($values);
    }

    /**
     * maže data
     * @param int $id
     * @return void
     */
    public function delete(int $id): void
    {
        $row = $this->kategorieManager->getOne($id);
        $parentId = $row->parent_id;

        $row->delete();
        $this->resort($parentId);
    }

    /**
     * upravuje pořadí směrem nahoru
     * @param int $id
     * @return void
     */
    public function sortUp(int $id): void
    {
        $database = $this->kategorieManager->select();

        $zaznam = $this->kategorieManager->getOne($id);

        if ($zaznam->sort != 1){
            $aktualSort = $zaznam->sort;
            $newSort = $aktualSort - 1;

            $zaznamPod = $database->where(KategorieManager::columnParentId,$zaznam->parent_id)->where(KategorieManager::columnSort,$newSort)->fetch();

            if ($zaznamPod){
                $zaznamPod->update([KategorieManager::columnSort => $aktualSort]);
            }
            $zaznam->update([KategorieManager::columnSort => $newSort]);
        }
    }

    /**
     * upravuje pořadí směrem dolu
     * @param int $id
     * @return void
     */
    public function sortDown(int $id): void
    {
        $database = $this->kategorieManager->select();

        $zaznam = $this->kategorieManager->getOne($id);

        $pocetZaznamu = $database->where(KategorieManager::columnParentId,$zaznam->parent_id)->count();

        if ($zaznam->sort != $pocetZaznamu){
            $aktualSort = $zaznam->sort;
            $newSort = $aktualSort + 1;

            $zaznamPod = $database->where(KategorieManager::columnParentId,$zaznam->parent_id)->where(KategorieManager::columnSort,$newSort)->fetch();

            if ($zaznamPod){
                $zaznamPod->update([KategorieManager::columnSort => $aktualSort]);
            }
            $zaznam->update([KategorieManager::columnSort => $newSort]);
        }
    }

    /**
     * změna nadřazené kategorie
     * @param $kategorieId
     * @param $parentId
     * @return void
     */
    public function changeParent($kategorieId, $parentId)
    {
        if ($parentId == 0){
            $parentId = null;
        }
        $kat = $this->kategorieManager->getOne($kategorieId);

        $oldParentId = $kat->parent_id;

        $newSort = $this->kategorieManager->select()
            ->where(KategorieManager::columnParentId, $parentId)
            ->count();

        $data = [KategorieManager::columnParentId => $parentId, KategorieManager::columnSort => $newSort+1];

        $kat->update($data);

        $this->resort($oldParentId);
    }

    /**
     * srovná pořadí kategorií
     * @param $parentId
     */
    public function resort($parentId): void
    {
        $zaznamy = $this->kategorieManager->select()
            ->where(KategorieManager::columnParentId,$parentId)
            ->order(KategorieManager::columnSort.' ASC')
            ->fetchAll();

        $i = 1;

        foreach ($zaznamy as $one){
            $one->update([KategorieManager::columnSort => $i]);
            $i++;
        }
    }

    /**
     * získá všechny kategorie
     * @param array|null $parram
     * @return \Nette\Database\Table\Selection
     */
    public function getBaseCategories(array $parram = null): Selection
    {
        $categories = $this->kategorieManager->select();
        $categories->where(KategorieManager::columnParentId, null);
        $categories->order(KategorieManager::columnSort. ' ASC');

        return $categories;
    }

    /**
     * získá všechny podkategorie
     * @param int $parentId
     * @return Selection
     */
    public function getSubCategories(int $parentId): Selection
    {
        $subCategories = $this->kategorieManager->select();
        $subCategories->where(KategorieManager::columnParentId, $parentId);
        $subCategories->order(KategorieManager::columnSort. ' ASC');
        return $subCategories;
    }

    /**
     * vypíše podkategorie do seznamu
     * @param $category
     */
    public function showSubcategories($category):void
    {
        echo '<ul>';
        foreach ($category->related('kategorie.parent_id')->order('sort ASC') as $subcategory){
            echo '<div class="d-flex justify-content-between">';
            echo '<div>';
            echo '<li style="list-style-type: none">';
            echo '<input class="form-check-input" type="checkbox" name="kategorie[]" value="'.$subcategory->id.'">
                    <label class="form-check-label">
                        '.$subcategory->nazev.'
                    </label>';
            echo $this->showSubcategories($subcategory);
            echo '</li>';
            echo '</div>';
            echo '<div class="position-absolute" style="right: 1rem">';
            echo '<input class="form-check-input" type="radio" name="baseKategorie" value="'.$subcategory->id.'">';
            echo '</div>';
            echo '</div>';
        }
        echo '</ul>';
    }

    /**
     * vypíše podkategorie do seznamu
     * @param $category
     */
    public function showEditSubcategories($category,$checkArray,$produktId):void
    {
        echo '<ul>';
        foreach ($category->related('kategorie.parent_id')->order('sort ASC') as $subcategory){
            if (in_array($subcategory->id, $checkArray)){
                $checked = 'checked';
            }else{
                $checked = null;
            }

            $prirazeni = $this->produktyKategorieManager->select()
                ->where(ProduktyKategorieManager::columnKategorieId, $subcategory->id)
                ->where(ProduktyKategorieManager::columnProduktyId, $produktId)
                ->fetch();

            if ($prirazeni != null){
                if ($prirazeni->base_kategorie == true){
                    $base = 'checked';
                }else{
                    $base = null;
                }
            }else{
                $base = null;
            }

            echo '<div class="d-flex justify-content-between">';
            echo '<div>';
            echo '<li style="list-style-type: none">';
            echo '<input class="form-check-input" type="checkbox" name="kategorie[]" value="'.$subcategory->id.'" '.$checked.'>
                    <label class="form-check-label">
                        '.$subcategory->nazev.'
                    </label>';
            echo $this->showEditSubcategories($subcategory, $checkArray, $produktId);
            echo '</li>';
            echo '</div>';
            echo '<div class="position-absolute" style="right: 1rem">';
            echo '<input class="form-check-input" type="radio" name="baseKategorie" value="'.$subcategory->id.'" '.$base.'>';
            echo '</div>';
            echo '</div>';
        }
        echo '</ul>';
    }
}