<?php

namespace App\AdminModule\KatalogModule\KategorieModule\Models;

use App\Managers\KategorieManager;
use Nette\Application\LinkGenerator;
use Nette\Database\Table\Selection;

final class KatSeznamModel
{
    private $kategorieManager;
    private $linkGenerator;

    public function __construct(KategorieManager    $kategorieManager,
                                LinkGenerator       $linkGenerator)
    {
        $this->kategorieManager = $kategorieManager;
        $this->linkGenerator = $linkGenerator;
    }

    /**
     * get all categories
     * @param array|null $parram
     * @return \Nette\Database\Table\Selection
     */
    public function getBaseCategories(array $parram = null): Selection
    {
        $categories = $this->kategorieManager->select();
        $categories->where(KategorieManager::columnParentId, null);
        $categories->order(KategorieManager::columnSort. ' ASC');

        return $categories;
    }

    /**
     * get all subcategories
     * @param int $parentId
     * @return Selection
     */
    public function getSubCategories(int $parentId): Selection
    {
        $subCategories = $this->kategorieManager->select();
        $subCategories->where(KategorieManager::columnParentId, $parentId);
        $subCategories->order(KategorieManager::columnSort. ' ASC');
        return $subCategories;
    }

    /**
     * draw subcategories to list
     * @param $category
     */
    public function showSubcategories($category, $parentId,$baseKatId):void
    {
        echo '<ul>';
        foreach ($category->related('kategorie.parent_id')->order('sort ASC') as $subcategory){
            echo '<li>';
                if($subcategory->id == $parentId){ $check = 'checked'; }else{$check = '';}
                echo '<input class="form-check-input" type="radio" name="parent_cat" value="'.$subcategory->id.'" '.$check.' onclick="changeParent('.$subcategory->id.')">
                    <label class="form-check-label">
                        '.$subcategory->nazev.'
                    </label>';
                if ($baseKatId != $subcategory->id){
                    echo $this->showSubcategories($subcategory, $parentId,$baseKatId);
                }
            echo '</li>';
        }
        echo '</ul>';
    }

    /**
     * draw subcategories to list
     * @param $category
     */
    public function showNavSubcategories($category):void
    {
        echo '<ul>';
        foreach ($category->related('kategorie.parent_id')->order('sort ASC') as $subcategory){
            echo '<li>';
                echo '<div class="my-1 py-1"><a href="'.$this->linkGenerator->link("Admin:Katalog:Kategorie:Editace:default", [$subcategory->id]).'">'.$subcategory->nazev.'</a></div>';
                echo $this->showNavSubcategories($subcategory);
            echo '</li>';
        }
        echo '</ul>';
    }
}