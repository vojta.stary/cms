<?php

namespace App\AdminModule\KatalogModule\KategorieModule\Models;



use App\Managers\KategorieManager;

final class KatBreadcrumbsModel
{
    private $kategorieManager;

    public function __construct(KategorieManager $kategorieManager)
    {
        $this->kategorieManager = $kategorieManager;
    }


    /**
     * generuje breadCrumbs menu
     * @param $kategorieId
     * @return array
     */
    public function generateBreadcrumbs($kategorieId): array
    {
        $breadCrumbs = [];

        $aktKategorie = $this->kategorieManager->getOne($kategorieId);

        if($aktKategorie->parent_id != null){
            $parentKat = $this->kategorieManager->getOne($aktKategorie->parent_id);

            if( $parentKat != null){
                $breadCrumbs[] = ['nazev' => $parentKat->nazev,'id' => $parentKat->id];

                while ($parentKat->parent_id != null){
                    $parentKat = $this->kategorieManager->getOne($parentKat->parent_id);
                    $breadCrumbs[] = ['nazev' => $parentKat->nazev,'id' => $parentKat->id];
                }
            }
        }

        return array_reverse($breadCrumbs, true);
    }
}