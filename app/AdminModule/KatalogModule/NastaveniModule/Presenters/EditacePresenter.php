<?php

namespace App\AdminModule\KatalogModule\NastaveniModule\Presenters;

use App\AdminModule\KatalogModule\NastaveniModule\Components\AktivaceKatalog\AktivaceKatalogFactory;
use App\AdminModule\KatalogModule\NastaveniModule\Components\PrazdnyObrazek\PrazdnyObrazekFactory;
use App\AdminModule\KatalogModule\NastaveniModule\Components\ZobrazCena\ZobrazCenaFactory;
use App\AdminModule\Presenters\BaseAdminPresenter;

final class EditacePresenter extends BaseAdminPresenter
{
    private $aktivaceKatalogFactory;
    private $zobrazCenaFactory;
    private $prazdnyObrazekFactory;

    public function __construct(AktivaceKatalogFactory  $aktivaceKatalogFactory,
                                PrazdnyObrazekFactory   $prazdnyObrazekFactory,
                                ZobrazCenaFactory       $zobrazCenaFactory)
    {
        $this->aktivaceKatalogFactory = $aktivaceKatalogFactory;
        $this->prazdnyObrazekFactory = $prazdnyObrazekFactory;
        $this->zobrazCenaFactory = $zobrazCenaFactory;
    }

    /*================================== nastavení aplikace ==================================*/
    public function startup()
    {
        parent::startup();

        $this->allowRole = ['admin'];
    }

    /*================================== komponenty ==================================*/

    /**
     * aktivace katalogu
     * @return \App\AdminModule\KatalogModule\NastaveniModule\Components\AktivaceKatalog\AktivaceKatalogControl
     */
    protected function createComponentAktivaceKatalog()
    {
        return $this->aktivaceKatalogFactory->create();
    }

    /**
     * používání cen u produktů
     * @return \App\AdminModule\KatalogModule\NastaveniModule\Components\ZobrazCena\ZobrazCenaControl
     */
    protected function createComponentZobrazCena()
    {
        return $this->zobrazCenaFactory->create();
    }

    /**
     * obrázek pro produkty bez foto
     * @return \App\AdminModule\KatalogModule\NastaveniModule\Components\PrazdnyObrazek\PrazdnyObrazekControl
     */
    protected function createComponentPrazdnyObrazek()
    {
        return $this->prazdnyObrazekFactory->create();
    }
}