<?php

namespace App\AdminModule\KatalogModule\NastaveniModule\Models;

use App\Managers\SetKatalogManager;
use Nette\Utils\FileSystem;
use Nette\Utils\Strings;

final class PrazdnyObrazekModel
{
    const dir = __DIR__.'/../../../../../www/src/images/katalog/';

    private $setKatalogManager;

    public function __construct(SetKatalogManager $setKatalogManager)
    {
        $this->setKatalogManager = $setKatalogManager;
    }

    public function save($obrazek)
    {
        if ($obrazek->hasFile()){
            $this->delete();

            $obrazekNazev = $obrazek->getName();

            $pripona = pathinfo($obrazekNazev);

            $obrazekNazev = Strings::webalize($pripona['filename']) . '.' . $pripona['extension'];

            $novySoubor = self::dir.$obrazekNazev;

            $obrazek->move($novySoubor);

            $this->setKatalogManager->updateInfo('emptyImage',$obrazekNazev);
        }
    }

    public function delete()
    {
        $obrazekNazev = $this->setKatalogManager->getOneSet('emptyImage');

        if ($obrazekNazev != ''){
            $soubor = self::dir.$obrazekNazev;

            FileSystem::delete($soubor);

            $this->setKatalogManager->updateInfo('emptyImage','');
        }
    }
}