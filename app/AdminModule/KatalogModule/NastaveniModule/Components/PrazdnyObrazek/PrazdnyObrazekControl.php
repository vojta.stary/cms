<?php

namespace App\AdminModule\KatalogModule\NastaveniModule\Components\PrazdnyObrazek;

use App\AdminModule\KatalogModule\NastaveniModule\Models\PrazdnyObrazekModel;
use App\Managers\SetKatalogManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class PrazdnyObrazekControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $prazdnyObazekModel;
    private $setKatalogManager;

    public function __construct(PrazdnyObrazekModel $prazdnyObrazekModel,
                                SetKatalogManager   $setKatalogManager)
    {
        $this->prazdnyObazekModel = $prazdnyObrazekModel;
        $this->setKatalogManager = $setKatalogManager;
    }

    public function render()
    {
        $this->template->obrazek = $this->setKatalogManager->getOneSet('emptyImage');
        $this->template->render(self::template);
    }

    /*======================== formulář ========================*/

    protected function createComponentPrazdnyObrazekForm(): Form
    {
        $form = new Form;
        $form->addUpload('obrazek')
            ->setRequired('Musíte vybrat soubor k nahrání!')
            ->addRule($form::IMAGE, 'Obrázek musí být JPEG, PNG, GIF, nebo WebP.');
        $form->addSubmit('send','Nahrát');
        $form->onSuccess[] = [$this, 'sendPrazdnyObrazekForm'];
        return $form;
    }

    public function sendPrazdnyObrazekForm(Form $form, $values)
    {
        if ($this->presenter->isAjax()){
            try {
                $this->prazdnyObazekModel->save($values->obrazek);
                $this->presenter->flashMessage('Obrázek byl nahrán','success');
                $this->presenter->redrawControl('flash');
                $this->redrawControl('obrazek');
                $this->redrawControl('formular-wrapper');
                $this->redrawControl('formular');
                $this->redrawControl('scripty');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('Chyba při nahrávání','danger');
                $this->presenter->redrawControl('flash');
                $this->redrawControl('obrazek');
                $this->redrawControl('formular-wrapper');
                $this->redrawControl('formular');
                $this->redrawControl('scripty');
            }
        }
    }

    /*======================== handle metody ========================*/

    public function handleDelete()
    {
        if ($this->presenter->isAjax()){
            try {
                $this->prazdnyObazekModel->delete();
                $this->presenter->flashMessage('Obrázek byl vymazán','success');
                $this->presenter->redrawControl('flash');
                $this->redrawControl('obrazek');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('Interní chyba aplikace','danger');
                $this->presenter->redrawControl('flash');
                $this->redrawControl('obrazek');
            }
        }
    }
}