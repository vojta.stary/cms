<?php

namespace App\AdminModule\KatalogModule\NastaveniModule\Components\PrazdnyObrazek;

use App\AdminModule\KatalogModule\NastaveniModule\Models\PrazdnyObrazekModel;
use App\Managers\SetKatalogManager;

class PrazdnyObrazekFactory
{
    private $prazdnyObazekModel;
    private $setKatalogManager;

    public function __construct(PrazdnyObrazekModel $prazdnyObrazekModel,
                                SetKatalogManager   $setKatalogManager)
    {
        $this->prazdnyObazekModel = $prazdnyObrazekModel;
        $this->setKatalogManager = $setKatalogManager;
    }

    public function create()
    {
        return new PrazdnyObrazekControl(   $this->prazdnyObazekModel,
                                            $this->setKatalogManager);
    }
}