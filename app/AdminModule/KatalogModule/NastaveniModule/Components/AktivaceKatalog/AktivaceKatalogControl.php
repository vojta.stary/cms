<?php

namespace App\AdminModule\KatalogModule\NastaveniModule\Components\AktivaceKatalog;

use App\Managers\SetKatalogManager;
use Nette\Application\UI\Control;

class AktivaceKatalogControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $setKatalogManager;

    public function __construct(SetKatalogManager $setKatalogManager)
    {
        $this->setKatalogManager = $setKatalogManager;
    }

    public function render()
    {
        $data = $this->setKatalogManager->getOneSet('activation');

        $this->template->data = $data;
        $this->template->render(self::template);
    }

    /*============================= handle metody =============================*/

    public function handleAktivace()
    {
        if ($this->presenter->isAjax()){
            $data = $this->setKatalogManager->getOneSet('activation');

            if ($data == true)
            {
                $this->setKatalogManager->updateInfo('activation',0);
            }else{
                $this->setKatalogManager->updateInfo('activation',1);
            }

            $this->redrawControl('aktivace');
        }
    }
}