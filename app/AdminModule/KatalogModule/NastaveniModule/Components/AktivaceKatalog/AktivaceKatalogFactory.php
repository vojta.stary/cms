<?php

namespace App\AdminModule\KatalogModule\NastaveniModule\Components\AktivaceKatalog;

use App\Managers\SetKatalogManager;

class AktivaceKatalogFactory
{
    private $setKatalogManager;

    public function __construct(SetKatalogManager $setKatalogManager)
    {
        $this->setKatalogManager = $setKatalogManager;
    }

    public function create()
    {
        return new AktivaceKatalogControl($this->setKatalogManager);
    }
}