<?php

namespace App\AdminModule\KatalogModule\NastaveniModule\Components\ZobrazCena;

use App\Managers\SetKatalogManager;

class ZobrazCenaFactory
{
    private $setKatalogManager;

    public function __construct(SetKatalogManager $setKatalogManager)
    {
        $this->setKatalogManager = $setKatalogManager;
    }

    public function create()
    {
        return new ZobrazCenaControl($this->setKatalogManager);
    }
}