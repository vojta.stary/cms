<?php

namespace App\AdminModule\KatalogModule\NastaveniModule\Components\ZobrazCena;

use App\Managers\SetKatalogManager;
use Nette\Application\UI\Control;

class ZobrazCenaControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $setKatalogManager;

    public function __construct(SetKatalogManager $setKatalogManager)
    {
        $this->setKatalogManager = $setKatalogManager;
    }

    public function render()
    {
        $data = $this->setKatalogManager->getOneSet('zobrazCena');

        $this->template->data = $data;
        $this->template->render(self::template);
    }

    /*============================= handle metody =============================*/

    public function handleAktivace()
    {
        if ($this->presenter->isAjax()){
            $data = $this->setKatalogManager->getOneSet('zobrazCena');

            if ($data == true)
            {
                $this->setKatalogManager->updateInfo('zobrazCena',0);
            }else{
                $this->setKatalogManager->updateInfo('zobrazCena',1);
            }

            $this->redrawControl('zobrazCena');
        }
    }
}