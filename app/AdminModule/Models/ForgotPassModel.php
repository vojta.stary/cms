<?php

namespace App\AdminModule\Models;

use App\Managers\DatabaseManager;
use App\Managers\UsersManager;
use App\Models\MailerModel;
use Nette\Database\Explorer;
use Nette\Security\Passwords;

class ForgotPassModel extends DatabaseManager
{

    const mailTemplate = 'forgotPass.latte';

    private $usersManager;
    private $password;
    private $mailerModel;

    public function __construct(Explorer $database,
                                UsersManager $usersManager,
                                Passwords $password,
                                MailerModel $mailerModel)
    {
        parent::__construct($database);

        $this->usersManager = $usersManager;
        $this->password = $password;
        $this->mailerModel = $mailerModel;
    }


    /**
     *
     * @param int $email
     * @return bool
     */
    public function generateForgotPass(string $email):bool
    {
        $length = 86;
        $generateToken = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);

        $user = $this->usersManager->getLoginUser($email);

        if ($user){
            $user->update(['passToken' => $generateToken]);
            $mailVal['token'] = $generateToken;
            $this->mailerModel->sendMail($user->email,self::mailTemplate, $mailVal);
            return true;
        }else{
            return false;
        }
    }

    /**
     * verify forgot pass token
     * @param string $token
     * @return false|\Nette\Database\Table\ActiveRow
     */
    public function verifyToken(string $token = null)
    {
        if ($token == null){
            $verify = false;
        }else {
            $verify = $this->database
                ->table(UsersManager::table)
                ->where(UsersManager::columnPassToken, $token)
                ->fetch();
        }
        return $verify;
    }

    /**
     * save new password
     * @param $userId
     * @param $password
     */
    public function saveNewPassword($userId, $password)
    {
        $newPass = $this->password->hash($password);
        $this->database
            ->table(UsersManager::table)
            ->where(UsersManager::columnId,$userId)
            ->update([UsersManager::columnPassword => $newPass, UsersManager::columnPassToken => null]);
    }
}