<?php

namespace App\AdminModule\Models;

use Nette;
use Nette\Security\SimpleIdentity;
use Nette\Security\Authenticator;
use App\Managers\UsersManager;

/**
 * Description of AdminAuthenticator
 *
 * @author Vojtěch Starý
 */
final class AdminAuthenticator implements Authenticator {

    const table = 'users';

    private $userManager;
    private $passwords;

    public function __construct(Nette\Security\Passwords    $passwords,
                                UsersManager                $userManager){

        $this->passwords = $passwords;
        $this->userManager = $userManager;
    }

    public function authenticate(string $username, string $password): Nette\Security\IIdentity
    {
        $row = $this->userManager->getLoginUser($username);

        if (!$row) {
            throw new Nette\Security\AuthenticationException('User not found.');
        }

        if (!$this->passwords->verify($password, $row->password)) {
            throw new Nette\Security\AuthenticationException('Invalid password.');
        }

        return new SimpleIdentity($row->id,$row->role,[
                'id' => $row->id,
                'name' => $row->name,
                'email' => $row->email,
                'role' => $row->role
            ]
        );
    }
}