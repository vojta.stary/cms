<?php

namespace App\AdminModule\Models;

use Nette\Neon\Neon;
use Nette\Utils\ArrayHash;

final class Activation
{
    const config = __DIR__.'/../../../config/modules.neon';

    public function loadActivation()
    {
        $data  = Neon::decodeFile(self::config);

        return ArrayHash::from($data);
    }
}