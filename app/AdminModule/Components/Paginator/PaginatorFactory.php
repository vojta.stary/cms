<?php

namespace App\AdminModule\Components\Paginator;


/**
 * Description of paginatorControlFactory
 *
 * @author vojta
 */
class PaginatorFactory {

    /**
     * @param int $itemsCount
     * @param int $currentPage
     * @param int $itemsPerPage
     * @param string $handle
     * @param  $filter
     * @param int $radius
     * @return \App\AdminModule\Components\Paginator\PaginatorControl
     */
    public function create(int $itemsCount,int $currentPage, string $handle = '', $filter = null, int $itemsPerPage = 12,int $radius = 5): PaginatorControl
    {

        $paginator = new PaginatorControl($itemsCount, $currentPage,$handle, $filter);
        $paginator->setItemsPerPage($itemsPerPage);
        $paginator->setRadius($radius);
        return $paginator;
    }    
}
