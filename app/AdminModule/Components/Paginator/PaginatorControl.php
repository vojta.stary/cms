<?php

namespace App\AdminModule\Components\Paginator;


use Nette\Application\UI\Control;
use Nette\Utils\Paginator;

/**
 * Komponenta pro vykresleni paginatoru
 *
 * 
 */
class PaginatorControl extends Control {
    
    const Template = __DIR__.'/templates/paginator.latte';
    
    private $radius;
    private $itemsCount;
    private $page;
    private $paginator;
    private $filter = null;
    private $handle;
    
    public function __construct($itemsCount,$page,$handle, $filter) {
        $this->paginator = new Paginator();
        $this->itemsCount = $itemsCount;
        $this->page = $page;
        $this->filter = $filter;
        $this->handle = $handle;
    }
    
    /**
     * nastavuje radius okolo aktivní stránky
     * @param int $radius
     */
    public function setRadius($radius){
        if (is_numeric($radius)) $this->radius = (int)$radius;
    }
    
    /**
     * nastavuje počet prvků na stránku
     * @param int $itemsPerPage
     */
    public function setItemsPerPage($itemsPerPage){
        $this->paginator->setItemsPerPage($itemsPerPage);
    }
    
    /**
     * funkce vykresleni
     */
    public function render(){
        
        //pomocné proměné
        $this->paginator->setItemCount($this->itemsCount);
        $this->paginator->setPage($this->page);
        
        $left = $this->page - $this->radius >= 1 ? $this->page - $this->radius : 1;
        $right = $this->page + $this->radius <= $this->paginator->pageCount ? $this->page + $this->radius : $this->paginator->pageCount;
        
        //nastavení šablony
        $this->template->handle = $this->handle;
        $this->template->filter = $this->filter;
        $this->template->page = $this->page;
        $this->template->paginator = $this->paginator;
        $this->template->left = $left;
        $this->template->right = $right;
        $this->template->render(self::Template);
    }
    
    
}
