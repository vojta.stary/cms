<?php

namespace App\AdminModule\ObsahModule\MenuModule\Presenters;

use App\AdminModule\ObsahModule\MenuModule\Components\SeznamMenu\SeznamMenuFactory;
use App\AdminModule\Presenters\BaseAdminPresenter;

class SeznamPresenter extends BaseAdminPresenter
{
    private $seznamMenuFactory;

    public function __construct(SeznamMenuFactory $seznamMenuFactory)
    {
        $this->seznamMenuFactory = $seznamMenuFactory;
    }

    public function renderDefault()
    {

    }

    protected function createComponentSeznamMenu()
    {
        return $this->seznamMenuFactory->create();
    }
}