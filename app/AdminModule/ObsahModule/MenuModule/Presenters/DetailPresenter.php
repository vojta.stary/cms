<?php

namespace App\AdminModule\ObsahModule\MenuModule\Presenters;

use App\AdminModule\ObsahModule\MenuModule\Components\SeznamMenu\SeznamMenuFactory;
use App\AdminModule\ObsahModule\MenuModule\Models\MenuBreadCrumbsModel;
use App\AdminModule\Presenters\BaseAdminPresenter;
use App\Managers\MenuManager;

class DetailPresenter extends BaseAdminPresenter
{
    private $menuManager;
    private $menuBreadCrumbsModel;
    private $seznamMenuFactory;
    private $menuId;

    public function __construct(MenuManager             $menuManager,
                                MenuBreadCrumbsModel    $menuBreadCrumbsModel,
                                SeznamMenuFactory       $seznamMenuFactory)
    {
        $this->menuManager = $menuManager;
        $this->menuBreadCrumbsModel = $menuBreadCrumbsModel;
        $this->seznamMenuFactory = $seznamMenuFactory;
    }

    /*===================================== action metody =====================================*/
    public function actionDefault(int $id)
    {
        $this->menuId = $id;
    }

    /*===================================== render metody =====================================*/
    public function renderDefault()
    {
        $menu = $this->menuManager->getOne($this->menuId);

        $this->template->breadCrumbs = $this->menuBreadCrumbsModel->generateBreadcrumbs($this->menuId);
        $this->template->menu = $menu;
    }

    /*===================================== komponenty =====================================*/
    /**
     * seznam menu
     * @return \App\AdminModule\ObsahModule\MenuModule\Components\SeznamMenu\SeznamMenuControl
     */
    protected function createComponentSeznamMenu()
    {
        $this->seznamMenuFactory->setMenu($this->menuId);
        return $this->seznamMenuFactory->create();
    }
}