<?php

namespace App\AdminModule\ObsahModule\MenuModule\Models;

use App\Managers\MenuManager;

class MenuModel
{
    private $menuManager;

    public function __construct(MenuManager $menuManager)
    {
        $this->menuManager = $menuManager;
    }

    /**
     * přidává nový záznam
     * @param $values
     * @return void
     */
    public function pridatPolozka($values)
    {
        $databaseMenu = $this->menuManager->select();

        if(isset($values->parent_id)){
            $databaseMenu->where(MenuManager::columnParentId, $values->parent_id);
        }else{
            $databaseMenu->where(MenuManager::columnParentId, null);
        }
        $pocetMenu = $databaseMenu->count();

        $sort = $pocetMenu + 1;

        $values->sort = $sort;

        $this->menuManager->create($values);
    }

    /**
     * upravuje pořadí směrem nahoru
     * @param int $id
     * @return void
     */
    public function sortUp(int $id)
    {
        $database = $this->menuManager->select();

        $zaznam = $this->menuManager->getOne($id);

        if ($zaznam->sort != 1){
            $aktualSort = $zaznam->sort;
            $newSort = $aktualSort - 1;

            $zaznamPod = $database->where(MenuManager::columnParentId,$zaznam->parent_id)->where(MenuManager::columnSort,$newSort)->fetch();

            if ($zaznamPod){
                $zaznamPod->update([MenuManager::columnSort => $aktualSort]);
            }
            $zaznam->update([MenuManager::columnSort => $newSort]);
        }
    }

    /**
     * upravuje pořadí směrem dolu
     * @param int $id
     * @return void
     */
    public function sortDown(int $id)
    {
        $database = $this->menuManager->select();

        $zaznam = $this->menuManager->getOne($id);

        $pocetZaznamu = $database->where(MenuManager::columnParentId,$zaznam->parent_id)->count();

        if ($zaznam->sort != $pocetZaznamu){
            $aktualSort = $zaznam->sort;
            $newSort = $aktualSort + 1;

            $zaznamPod = $database->where(MenuManager::columnParentId,$zaznam->parent_id)->where(MenuManager::columnSort,$newSort)->fetch();

            if ($zaznamPod){
                $zaznamPod->update([MenuManager::columnSort => $aktualSort]);
            }
            $zaznam->update([MenuManager::columnSort => $newSort]);
        }
    }

    /**
     * srovná pořadí
     * @param $parentId
     */
    public function resort($parentId)
    {
        $zaznamy = $this->menuManager->select()
            ->where(MenuManager::columnParentId,$parentId)
            ->order(MenuManager::columnSort.' ASC')
            ->fetchAll();

        $i = 1;

        foreach ($zaznamy as $one){
            $one->update([MenuManager::columnSort => $i]);
            $i++;
        }
    }
}