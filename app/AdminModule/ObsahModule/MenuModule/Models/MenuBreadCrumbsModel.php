<?php

namespace App\AdminModule\ObsahModule\MenuModule\Models;

use App\Managers\MenuManager;

class MenuBreadCrumbsModel
{
    private $menuManager;

    public function __construct(MenuManager $menuManager)
    {
        $this->menuManager = $menuManager;
    }

    /**
     * generuje breadCrumbs menu
     * @param $kategorieId
     * @return array
     */
    public function generateBreadcrumbs(int $menuId): array
    {
        $breadCrumbs = [];

        $akt = $this->menuManager->getOne($menuId);

        if($akt->parent_id != null){
            $parent = $this->menuManager->getOne($akt->parent_id);

            if( $parent != null){
                $breadCrumbs[] = ['nazev' => $parent->nazev,'id' => $parent->id];

                while ($parent->parent_id != null){
                    $parent = $this->menuManager->getOne($parent->parent_id);
                    $breadCrumbs[] = ['nazev' => $parent->nazev,'id' => $parent->id];
                }
            }
        }

        return array_reverse($breadCrumbs, true);
    }
}