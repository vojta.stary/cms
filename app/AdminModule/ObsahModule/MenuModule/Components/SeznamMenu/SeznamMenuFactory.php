<?php

namespace App\AdminModule\ObsahModule\MenuModule\Components\SeznamMenu;

use App\AdminModule\ObsahModule\MenuModule\Models\MenuModel;
use App\Managers\MenuManager;
use App\Managers\StrankyManager;

class SeznamMenuFactory
{
    private $menuManager;
    private $menuModel;
    private $strankyManager;
    private $menuId = null;

    public function __construct(MenuManager     $menuManager,
                                MenuModel       $menuModel,
                                StrankyManager  $strankyManager)
    {
        $this->menuManager = $menuManager;
        $this->menuModel = $menuModel;
        $this->strankyManager = $strankyManager;
    }

    public function create()
    {
        return new SeznamMenuControl(   $this->menuManager,
                                        $this->strankyManager,
                                        $this->menuModel,
                                        $this->menuId);
    }

    public function setMenu(int $menuId)
    {
        $this->menuId = $menuId;
    }
}