<?php

namespace App\AdminModule\ObsahModule\MenuModule\Components\SeznamMenu;

use App\AdminModule\ObsahModule\MenuModule\Models\MenuModel;
use App\Managers\MenuManager;
use App\Managers\StrankyManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

class SeznamMenuControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $menuManager;
    private $menuModel;
    private $strankyManager;
    private $menuId;
    private $typ = null;

    public function __construct(MenuManager     $menuManager,
                                StrankyManager  $strankyManager,
                                MenuModel       $menuModel,
                                                $menuId)
    {
        $this->menuManager = $menuManager;
        $this->strankyManager = $strankyManager;
        $this->menuModel = $menuModel;
        $this->menuId = $menuId;
    }

    public function render()
    {
        $this->template->menuCount = $this->getMenuCount();
        $this->template->menu = $this->getMenu();
        $this->template->typ = $this->typ;
        $this->template->render(self::template);
    }

    /*=============================== formuláře ===============================*/
    protected function createComponentPridatPolozkaForm(): Form
    {
        $typ = ['page' => 'Stránka', 'dropdown' => 'Rozbalovací menu'];
        $form = new Form;
        $form->addSelect('typ','Typ položky',$typ)
            ->setPrompt('Vyberte možnost')
            ->setRequired('Musíte vybrat typ!');
        $form->addText('nazev','Název menu')
            ->setNullable();
        $form->addSelect('stranky_id','Stránka')
            ->setPrompt('Vyberte stránku');
        if ($this->menuId)
        {
            $form->addHidden('parent_id')
                ->setDefaultValue($this->menuId);
        }
        $form->addSubmit('send','Přidat');
        $form->onSuccess[] = [$this, 'sendPridatPolozkaForm'];
        return $form;
    }

    public function sendPridatPolozkaForm(Form $form, $values)
    {
        $data = $form->getHttpData();
        unset($data['_do']);
        unset($data['send']);
        $this->menuModel->pridatPolozka(ArrayHash::from($data));
        $this->presenter->flashMessage('Data byla uložena','success');
        $this->redirect('this');
    }

    /*=============================== handle metody ===============================*/

    /**
     * změna formuláře de typu
     * @param $typ
     * @return void
     */
    public function handleChangeForm($typ)
    {
        if ($this->presenter->isAjax()){
            $this->typ = $typ;
            if($typ){
                $this['pridatPolozkaForm']['typ']->setDefaultValue($typ);
            }
            if($typ == 'dropdown'){
                $this['pridatPolozkaForm']['nazev']->setRequired('Musíte vyplnit název!');
            }
            if($typ == 'page'){
                $this['pridatPolozkaForm']['stranky_id']->setItems($this->getSelectStranky())->setRequired('Musíte vybrat stránku!');
            }

            $this->redrawControl('formular');
        }
    }

    /**
     * odstranění položky ze seznamu
     * @param $id
     * @return void
     */
    public function handleRemPolozka($id)
    {
        if ($this->presenter->isAjax()){
            try {

                $this->menuManager->delete($id);
                $this->menuModel->resort($this->menuId);

                $this->presenter->flashMessage('Položka byla odebrána','success');
                $this->presenter->redrawControl('flash');
                $this->redrawControl('seznam');

            }catch (\PDOException $e){
                $this->presenter->flashMessage('Chyba zápisu do databáze','danger');
                $this->presenter->redrawControl('flash');
            }
        }
    }

    /**
     * změna pořadí směrem nahoru
     * @param int $id
     * @return void
     */
    public function handleSortUp(int $id)
    {
        if ($this->presenter->isAjax()){
            try {
                $this->menuModel->sortUp($id);
                $this->redrawControl('seznam');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('Chyba zápisu do databáze','danger');
                $this->presenter->redrawControl('flash');
            }
        }
    }

    /**
     * změna pořadí směrem dolů
     * @param int $id
     * @return void
     */
    public function handleSortDown(int $id)
    {
        if ($this->presenter->isAjax()){
            try {
                $this->menuModel->sortDown($id);
                $this->redrawControl('seznam');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('Chyba zápisu do databáze','danger');
                $this->presenter->redrawControl('flash');
            }
        }
    }

    /*=============================== pomocné funkce ===============================*/
    private function getMenu()
    {
        $database = $this->menuManager->select();

        if($this->menuId){
            $database->where(MenuManager::columnParentId, $this->menuId);
        }else{
            $database->where(MenuManager::columnParentId, null);
        }

        $data =  $database->order(MenuManager::columnSort.' ASC')->fetchAll();

        return $data;
    }

    private function getMenuCount()
    {
        $database = $this->menuManager->select();

        if($this->menuId){
            $database->where(MenuManager::columnParentId, $this->menuId);
        }else{
            $database->where(MenuManager::columnParentId, null);
        }

        return $database->count();
    }

    private function getSelectStranky()
    {
        $array = [];

        $database = $this->strankyManager->select();

        $stranky = $database->fetchAll();

        foreach ($stranky as $row){
            $array[$row->id] = $row->nazev;
        }

        return $array;
    }
}