<?php

namespace App\AdminModule\ObsahModule\SponzoriModule\Models;

use App\Managers\SponzoriManager;
use Nette\Utils\FileSystem;
use Nette\Utils\Image;
use Nette\Utils\Strings;

class SponzoriModel
{
    const dir = __DIR__.'/../../../../../www/src/images/sponzori/';

    private $sponzoriManager;

    /**
     * @param SponzoriManager $sponzoriManager
     */
    public function __construct(SponzoriManager $sponzoriManager)
    {
        $this->sponzoriManager = $sponzoriManager;
    }

    /*==================================== základní funkce ====================================*/
    /**
     * vytváří sponzora
     * @param $values
     * @return void
     * @throws \Nette\Utils\ImageException
     * @throws \Nette\Utils\UnknownImageFileException
     */
    public function create($values): void
    {
        $databaseData = [SponzoriManager::columnNazev => $values->nazev, SponzoriManager::columnUrl => $values->url];

        $newId = $this->sponzoriManager->create($databaseData);
        $this->saveLogo($values->logo, $newId);
    }

    /**
     * aktualizuje sponzora
     * @param $values
     * @return void
     * @throws \Nette\Utils\ImageException
     * @throws \Nette\Utils\UnknownImageFileException
     */
    public function update($values): void
    {
        $sponzor = $this->sponzoriManager->getOne($values->id);

        $logo = $values->logo;

        if ($logo->hasFile())
        {
            $this->vymazLogo($sponzor->logo, $sponzor->id);
            $this->saveLogo($logo, $sponzor->id);
        }

        $sponzor->update([SponzoriManager::columnNazev => $values->nazev, SponzoriManager::columnUrl => $values->url]);
    }

    /**
     * vymazaní sponzora
     * @param $values
     * @return void
     */
    public function delete($values): void
    {
        $sponzor = $this->sponzoriManager->getOne($values->id);

        if ($sponzor->logo){
            $this->vymazLogo($sponzor->logo, $sponzor->id);
        }

        $dir = self::dir .'/'.$sponzor->id.'/';

        if (is_dir($dir)){
            rmdir($dir);
        }

        $sponzor->delete();
    }

    /*==================================== práce se soubory ====================================*/
    /**
     * ukládá logo
     * @param $logo
     * @param $sponzorId
     * @return void
     * @throws \Nette\Utils\ImageException
     * @throws \Nette\Utils\UnknownImageFileException
     */
    private function saveLogo($logo, $sponzorId): void
    {
        if ($logo->hasFile()) {
            $newName = $logo->getName();

            $pripona = pathinfo($newName);

            //upravý názen na web friendly
            $newName = Strings::webalize($pripona['filename']) . '.' . $pripona['extension'];

            //uloží soubor
            $newFile = self::dir .'/'.$sponzorId.'/'. $newName;
            $logo->move($newFile);

            //uloží data do databáze
            $sponzor = $this->sponzoriManager->getOne($sponzorId);
            $sponzor->update([SponzoriManager::columnLogo => $newName]);

            //změní velikost
            $obrazek = Image::fromFile($newFile);
            if($obrazek->getHeight() > 450){
                $obrazek->resize(null,450);
                $obrazek->save($newFile,80);
            }
        }
    }

    /**
     * maže logo
     * @param $logo
     * @param $sponzorId
     * @return void
     */
    private function vymazLogo($logo, $sponzorId)
    {
        $file = self::dir.'/'.$sponzorId.'/'.$logo;

        if (is_file($file))
        {
            unlink($file);
        }
    }
}