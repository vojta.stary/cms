<?php

namespace App\AdminModule\ObsahModule\SponzoriModule\Components\EditSponzor;

use App\AdminModule\ObsahModule\SponzoriModule\Models\SponzoriModel;
use App\Managers\SponzoriManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\ImageException;

class EditSponzorControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $sponzoriModel;
    private $sponzoriManager;
    private $sponzorId;

    public function __construct(SponzoriModel   $sponzoriModel,
                                SponzoriManager $sponzoriManager,
                                                $sponzorId)
    {
        $this->sponzoriModel = $sponzoriModel;
        $this->sponzoriManager = $sponzoriManager;
        $this->sponzorId = $sponzorId;
    }

    public function render()
    {
        $sponzor = $this->sponzoriManager->getOne($this->sponzorId);

        $this['editSponzorForm']->setDefaults($sponzor);
        $this->template->data = $sponzor;
        $this->template->render(self::template);
    }

    /*============================ formulář ============================*/

    protected function createComponentEditSponzorForm(): Form
    {
        $form= new Form;
        $form->addHidden('id');
        $form->addText('nazev','Název')
            ->setRequired('Musíte vyplnit název!');
        $form->addText('url','Internetová adresa')
            ->setNullable();
        $form->addUpload('logo')
            ->addRule($form::IMAGE, 'Obrázek musí být JPEG, PNG, GIF nebo WebP.');
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendEditSponzorForm'];
        return $form;
    }

    public function sendEditSponzorForm(Form $form, $values)
    {
        try{
            $this->sponzoriModel->update($values);
            $this->presenter->flashMessage('Data byla uložena','success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
            $this->redirect('this');
        }catch (ImageException $e){
            $this->presenter->flashMessage('Logo se nepodařilo nahrát','warning');
            $this->redirect('this');
        }
    }
}