<?php

namespace App\AdminModule\ObsahModule\SponzoriModule\Components\NovySponzor;

use App\AdminModule\ObsahModule\SponzoriModule\Models\SponzoriModel;

class NovySponzorFactory
{
    private $sponzoriModel;

    public function __construct(SponzoriModel $sponzoriModel)
    {
        $this->sponzoriModel = $sponzoriModel;
    }

    public function create()
    {
        return new NovySponzorControl($this->sponzoriModel);
    }
}