<?php

namespace App\AdminModule\ObsahModule\SponzoriModule\Components\NovySponzor;

use App\AdminModule\ObsahModule\SponzoriModule\Models\SponzoriModel;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\ImageException;

class NovySponzorControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $sponzoriModel;

    public function __construct(SponzoriModel $sponzoriModel)
    {
        $this->sponzoriModel = $sponzoriModel;
    }

    public function render()
    {
        $this->template->render(self::template);
    }

    /*============================ formulář ============================*/

    protected function createComponentNovySponzorForm(): Form
    {
        $form= new Form;
        $form->addText('nazev','Název')
            ->setRequired('Musíte vyplnit název!');
        $form->addText('url','Internetová adresa')
            ->setNullable();
        $form->addUpload('logo')
            ->addRule($form::IMAGE, 'Obrázek musí být JPEG, PNG, GIF nebo WebP.');
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendNovySponzorForm'];
        return $form;
    }

    public function sendNovySponzorForm(Form $form, $values)
    {
        try{
            $this->sponzoriModel->create($values);
            $this->presenter->flashMessage('Data byla uložena','success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
            $this->redirect('this');
        }catch (ImageException $e){
            $this->presenter->flashMessage('Logo se nepodařilo nahrát','warning');
            $this->redirect('this');
        }
    }
}