<?php

namespace App\AdminModule\ObsahModule\SponzoriModule\Components\VymazSponzor;

use App\AdminModule\ObsahModule\SponzoriModule\Models\SponzoriModel;
use App\Managers\SponzoriManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class VymazSponzorControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $sponzoriModel;
    private $sponzoriManager;
    private $sponzorId;

    public function __construct(SponzoriModel   $sponzoriModel,
                                SponzoriManager $sponzoriManager,
                                                $sponzorId)
    {
        $this->sponzoriModel = $sponzoriModel;
        $this->sponzoriManager = $sponzoriManager;
        $this->sponzorId = $sponzorId;
    }

    public function render()
    {
        $sponzor = $this->sponzoriManager->getOne($this->sponzorId);

        $this['delSponzorForm']->setDefaults($sponzor);
        $this->template->data = $sponzor;
        $this->template->render(self::template);
    }

    /*============================ formulář ============================*/

    protected function createComponentDelSponzorForm(): Form
    {
        $form= new Form;
        $form->addHidden('id');
        $form->addSubmit('send','Vymazat');
        $form->onSuccess[] = [$this, 'sendDelSponzorForm'];
        return $form;
    }

    public function sendDelSponzorForm(Form $form, $values)
    {
        try{
            $this->sponzoriModel->delete($values);
            $this->presenter->flashMessage('Data byla vymazána','success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
            $this->redirect('this');
        }
    }
}