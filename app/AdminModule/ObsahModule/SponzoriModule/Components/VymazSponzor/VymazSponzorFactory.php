<?php

namespace App\AdminModule\ObsahModule\SponzoriModule\Components\VymazSponzor;

use App\AdminModule\ObsahModule\SponzoriModule\Models\SponzoriModel;
use App\Managers\SponzoriManager;

class VymazSponzorFactory
{
    private $sponzoriModel;
    private $sponzoriManager;
    private $sponzorId;

    public function __construct(SponzoriModel   $sponzoriModel,
                                SponzoriManager $sponzoriManager)
    {
        $this->sponzoriModel = $sponzoriModel;
        $this->sponzoriManager = $sponzoriManager;
    }

    public function create()
    {
        return new VymazSponzorControl( $this->sponzoriModel,
                                        $this->sponzoriManager,
                                        $this->sponzorId);
    }

    public function setSponzor(int $id)
    {
        $this->sponzorId = $id;
    }
}