<?php

namespace App\AdminModule\ObsahModule\SponzoriModule\Presenters;

use App\AdminModule\Components\Paginator\PaginatorFactory;
use App\AdminModule\ObsahModule\SponzoriModule\Components\EditSponzor\EditSponzorFactory;
use App\AdminModule\ObsahModule\SponzoriModule\Components\NovySponzor\NovySponzorFactory;
use App\AdminModule\ObsahModule\SponzoriModule\Components\VymazSponzor\VymazSponzorFactory;
use App\AdminModule\Presenters\BaseAdminPresenter;
use App\Managers\SponzoriManager;
use Nette\Application\UI\Multiplier;

final class SeznamPresenter extends BaseAdminPresenter
{
    const pageLimit = 12;

    private $sponzoriManager;
    private $novySponzorFactory;
    private $editSponzorFactory;
    private $vymazSponzorFactory;
    private $paginator;
    private $page = 1;
    private $itemsCount;
    private $filter = [];

    public function __construct(SponzoriManager     $sponzoriManager,
                                NovySponzorFactory  $novySponzorFactory,
                                EditSponzorFactory  $editSponzorFactory,
                                VymazSponzorFactory $vymazSponzorFactory,
                                PaginatorFactory    $paginatorFactory)
    {
        $this->sponzoriManager = $sponzoriManager;
        $this->novySponzorFactory = $novySponzorFactory;
        $this->editSponzorFactory = $editSponzorFactory;
        $this->vymazSponzorFactory = $vymazSponzorFactory;
        $this->paginator = $paginatorFactory;
    }

    /*=================== render metody ===================*/

    /**
     * metoda pro vykreslení a přípravu dat pro šablonu
     * @return void
     */
    public function renderDefault()
    {
        $offset = ($this->page - 1 )* self::pageLimit;

        $data = $this->getSponzori();

        $this->itemsCount = $data->count();

        $this->template->page = $this->page;
        $this->template->filter = $this->filter;
        $this->template->data = $data->limit(self::pageLimit,$offset)->fetchAll();
    }

    /*=================== handle metody ===================*/
    /**
     * nastavení ajax stránkování pro paginator
     * @param int $page
     * @param array $filter
     * @return void
     */
    public function handlePage(int $page, array $filter): void
    {
        if ($this->isAjax()){
            $this->filter = $filter;
            $this->page = $page;
            $this->redrawControl('list');
        }
    }

    /*=================== komponenty ===================*/

    /**
     * komponenta paginatoru seznamu
     * @return \App\AdminModule\Components\Paginator\PaginatorControl
     */
    protected function createComponentPaginator()
    {
        return $this->paginator->create($this->itemsCount,$this->page,'page!',$this->filter, self::pageLimit);
    }

    /**
     * nový sponzor
     * @return \App\AdminModule\ObsahModule\SponzoriModule\Components\NovySponzor\NovySponzorControl
     */
    protected function createComponentNovySponzor()
    {
        return $this->novySponzorFactory->create();
    }

    /**
     * editace sponzora
     * @return Multiplier
     */
    protected function createComponentEditSponzor()
    {
        return new Multiplier(function ($id){
            $this->editSponzorFactory->setSponzor($id);
            return $this->editSponzorFactory->create();
        });
    }

    /**
     * vymaz sponzora
     * @return Multiplier
     */
    protected function createComponentVymazSponzor()
    {
        return new Multiplier(function ($id){
            $this->vymazSponzorFactory->setSponzor($id);
            return $this->vymazSponzorFactory->create();
        });
    }

    /*=================== pomocné funkce ===================*/

    private function getSponzori()
    {
        $data = $this->sponzoriManager->select();

        return $data;
    }
}