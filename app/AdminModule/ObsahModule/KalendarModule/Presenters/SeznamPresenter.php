<?php

namespace App\AdminModule\ObsahModule\KalendarModule\Presenters;

use App\AdminModule\Components\Paginator\PaginatorFactory;
use App\AdminModule\ObsahModule\KalendarModule\Components\EditKalendar\EditKalendarFactory;
use App\AdminModule\ObsahModule\KalendarModule\Components\NovyKalendar\NovyKalendarFactory;
use App\AdminModule\ObsahModule\KalendarModule\Components\VymazKalendar\VymazKalendarFactory;
use App\AdminModule\Presenters\BaseAdminPresenter;
use App\Managers\TerminKalendarManager;
use Nette\Application\UI\Multiplier;
use Nette\Utils\DateTime;

final class SeznamPresenter extends BaseAdminPresenter
{
    const pageLimit = 12;

    private $terminKalendarManager;
    private $novyKalendarFactory;
    private $editKalendarFactory;
    private $vymazKalendarFactory;
    private $paginator;
    private $page = 1;
    private $itemsCount;
    private $filter = [];

    public function __construct(TerminKalendarManager   $terminKalendarManager,
                                NovyKalendarFactory     $novyKalendarFactory,
                                EditKalendarFactory     $editKalendarFactory,
                                VymazKalendarFactory    $vymazKalendarFactory,
                                PaginatorFactory        $paginatorFactory)
    {
        $this->terminKalendarManager = $terminKalendarManager;
        $this->novyKalendarFactory = $novyKalendarFactory;
        $this->editKalendarFactory = $editKalendarFactory;
        $this->vymazKalendarFactory = $vymazKalendarFactory;
        $this->paginator = $paginatorFactory;
    }

    /*=================== render metody ===================*/
    public function renderDefault()
    {
        $offset = ($this->page - 1 )* self::pageLimit;

        $data = $this->getTerminy();

        $this->itemsCount = $data->count();
        $this->template->data = $data->limit(self::pageLimit,$offset)->fetchAll();
    }

    /*=================== handle metody ===================*/
    /**
     * nastavení ajax stránkování pro paginator
     * @param int $page
     * @param array $filter
     * @return void
     */
    public function handlePage(int $page, array $filter): void
    {
        if ($this->isAjax()){
            $this->filter = $filter;
            $this->page = $page;
            $this->redrawControl('list');
        }
    }

    /**
     * nastavení filtrace seznamu
     * @param array $filter
     * @return void
     */
    public function handleFilter(array $filter): void
    {
        if ($this->isAjax())
        {
            $this->filter = $filter;
            $this->redrawControl('list');
        }
    }

    /*=================== komponenty ===================*/

    /**
     * komponenta paginatoru seznamu
     * @return \App\AdminModule\Components\Paginator\PaginatorControl
     */
    protected function createComponentPaginator()
    {
        return $this->paginator->create($this->itemsCount,$this->page,'page!',$this->filter, self::pageLimit);
    }

    /**
     * nový záznam termínu
     * @return \App\AdminModule\ObsahModule\KalendarModule\Components\NovyKalendar\NovyKalendarControl
     */
    protected function createComponentNovyKalendar()
    {
        return $this->novyKalendarFactory->create();
    }

    /**
     * editace záznamu termínu
     * @return Multiplier
     */
    protected function createComponentEditKalendar()
    {
        return new Multiplier(function ($id){
            $this->editKalendarFactory->setKalendar($id);
            return $this->editKalendarFactory->create();
        });
    }

    /**
     * vymazání záznamu termínu
     * @return Multiplier
     */
    protected function createComponentVymazKalendar()
    {
        return new Multiplier(function($id){
            $this->vymazKalendarFactory->setKalendar($id);
            return $this->vymazKalendarFactory->create();
        });
    }

    /*=================== pomocné funkce ===================*/
    /**
     * načítá data termínů z databáze
     * @return \Nette\Database\Table\Selection
     */
    private function getTerminy()
    {
        $database = $this->terminKalendarManager->select();

        if(isset($this->filter['datum'])){
            $datum = DateTime::from($this->filter['datum'])->format('Y-m-d');
            $database->where(TerminKalendarManager::columnDatum,$datum);
        }

        return $database->order(TerminKalendarManager::columnDatum.' DESC');
    }
}