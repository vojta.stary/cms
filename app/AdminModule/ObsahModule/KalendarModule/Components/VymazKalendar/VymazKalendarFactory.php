<?php

namespace App\AdminModule\ObsahModule\KalendarModule\Components\VymazKalendar;

use App\Managers\TerminKalendarManager;

class VymazKalendarFactory
{
    private $terminKalendarManager;
    private $terminId;

    public function __construct(TerminKalendarManager   $terminKalendarManager)
    {
        $this->terminKalendarManager = $terminKalendarManager;
    }

    public function create()
    {
        return new VymazKalendarControl($this->terminKalendarManager,
                                        $this->terminId);
    }

    public function setKalendar(int $id)
    {
        $this->terminId = $id;
    }
}