<?php

namespace App\AdminModule\ObsahModule\KalendarModule\Components\VymazKalendar;

use App\Managers\TerminKalendarManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\DateTime;

class VymazKalendarControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $terminKalendarManager;
    private $terminId;

    public function __construct(TerminKalendarManager   $terminKalendarManager,
                                                        $terminId)
    {
        $this->terminKalendarManager = $terminKalendarManager;
        $this->terminId = $terminId;
    }

    public function render()
    {
        $data = $this->terminKalendarManager->getOne($this->terminId);

        $this['vymazKalendarForm']->setDefaults($data);
        $this->template->data = $data;
        $this->template->render(self::template);
    }

    /*============================ formulář ============================*/

    /**
     * formulář vymazání terminu
     * @return Form
     */
    protected function createComponentVymazKalendarForm(): Form
    {
        $form = new Form;
        $form->addHidden('id');
        $form->addSubmit('send','Vymazat');
        $form->onSuccess[] = [$this, 'sendVymazKalendarForm'];
        return $form;
    }

    /**
     * zpracování formuláře vymazání terminu
     * @param Form $form
     * @param $values
     * @return void
     */
    public function sendVymazKalendarForm(Form $form, $values)
    {
        try {
            $this->terminKalendarManager->delete($values->id);
            $this->presenter->flashMessage('Data byla vymazána', 'success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Chyba zápisu dat do databáze!', 'danger');
            $this->redirect('this');
        }
    }
}