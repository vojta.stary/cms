<?php

namespace App\AdminModule\ObsahModule\KalendarModule\Components\NovyKalendar;

use App\Managers\TerminKalendarManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\DateTime;

class NovyKalendarControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $terminKalendarManager;

    public function __construct(TerminKalendarManager $terminKalendarManager)
    {
        $this->terminKalendarManager = $terminKalendarManager;
    }

    public function render()
    {
        $this->template->render(self::template);
    }

    /*============================ formulář ============================*/

    /**
     * formulář nového terminu
     * @return Form
     */
    protected function createComponentNovyKalendarForm(): Form
    {
        $form = new Form;
        $form->addText('nazev')
            ->setRequired('Musíte vyplnit text termínu!');
        $form->addText('datum')
            ->setRequired('Musíte vyplnit datum!');
        $form->addTextArea('obsah');
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendNovyKalendarForm'];
        return $form;
    }

    /**
     * zpracování formuláře nového terminu
     * @param Form $form
     * @param $values
     * @return void
     */
    public function sendNovyKalendarForm(Form $form, $values)
    {
        try {
            $values->datum = DateTime::from($values->datum)->format('Y-m-d');
            $this->terminKalendarManager->create($values);
            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Chyba zápisu dat do databáze!', 'danger');
            $this->redirect('this');
        }
    }
}