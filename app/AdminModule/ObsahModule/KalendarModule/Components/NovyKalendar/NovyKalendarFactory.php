<?php

namespace App\AdminModule\ObsahModule\KalendarModule\Components\NovyKalendar;

use App\Managers\TerminKalendarManager;

class NovyKalendarFactory
{
    private $terminKalendarManager;

    public function __construct(TerminKalendarManager $terminKalendarManager)
    {
        $this->terminKalendarManager = $terminKalendarManager;
    }

    public function create()
    {
        return new NovyKalendarControl($this->terminKalendarManager);
    }
}