<?php

namespace App\AdminModule\ObsahModule\KalendarModule\Components\EditKalendar;

use App\Managers\TerminKalendarManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;

class EditKalendarControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $terminKalendarManager;
    private $terminId;

    public function __construct(TerminKalendarManager   $terminKalendarManager,
                                                        $terminId)
    {
        $this->terminKalendarManager = $terminKalendarManager;
        $this->terminId = $terminId;
    }

    public function render()
    {
        $data = $this->terminKalendarManager->getOne($this->terminId);

        $this['editKalendarForm']->setDefaults($data);
        $this->template->data = $data;
        $this->template->render(self::template);
    }

    /*============================ formulář ============================*/

    /**
     * formulář editace terminu
     * @return Form
     */
    protected function createComponentEditKalendarForm(): Form
    {
        $form = new Form;
        $form->addHidden('id');
        $form->addText('nazev')
            ->setRequired('Musíte vyplnit text termínu!');
        $form->addText('datum')
            ->setRequired('Musíte vyplnit datum!');
        $form->addTextArea('obsah');
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendEditKalendarForm'];
        return $form;
    }

    /**
     * zpracování formuláře editace terminu
     * @param Form $form
     * @param $values
     * @return void
     */
    public function sendEditKalendarForm(Form $form, $values)
    {
        try {
            $values->datum = DateTime::from($values->datum)->format('Y-m-d');
            $this->terminKalendarManager->update($values->id, $values);
            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Chyba zápisu dat do databáze!', 'danger');
            $this->redirect('this');
        }
    }
}