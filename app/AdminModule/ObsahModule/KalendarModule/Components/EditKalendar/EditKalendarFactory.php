<?php

namespace App\AdminModule\ObsahModule\KalendarModule\Components\EditKalendar;

use App\Managers\TerminKalendarManager;

class EditKalendarFactory
{
    private $terminKalendarManager;
    private $terminId;

    public function __construct(TerminKalendarManager   $terminKalendarManager)
    {
        $this->terminKalendarManager = $terminKalendarManager;
    }

    public function create()
    {
        return new EditKalendarControl( $this->terminKalendarManager,
                                        $this->terminId);
    }

    public function setKalendar(int $id)
    {
        $this->terminId = $id;
    }
}