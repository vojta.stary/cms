<?php

namespace App\AdminModule\ObsahModule\DotazyModule\Models;

use App\Managers\DotazyManager;

final class DotazyModel
{
    private $dotazyManager;

    public function __construct(DotazyManager $dotazyManager)
    {
        $this->dotazyManager = $dotazyManager;
    }

    /**
     * přidává nový záznam
     * @param $values
     * @return void
     */
    public function pridatPolozka($values)
    {
        $database = $this->dotazyManager->select();

        $pocetMenu = $database->count();

        $sort = $pocetMenu + 1;

        $values->sort = $sort;

        $this->dotazyManager->create($values);
    }

    /**
     * upravuje pořadí směrem nahoru
     * @param int $id
     * @return void
     */
    public function sortUp(int $id)
    {
        $database = $this->dotazyManager->select();

        $zaznam = $this->dotazyManager->getOne($id);

        if ($zaznam->sort != 1){
            $aktualSort = $zaznam->sort;
            $newSort = $aktualSort - 1;

            $zaznamPod = $database->where(DotazyManager::columnSort,$newSort)->fetch();

            if ($zaznamPod){
                $zaznamPod->update([DotazyManager::columnSort => $aktualSort]);
            }
            $zaznam->update([DotazyManager::columnSort => $newSort]);
        }
    }

    /**
     * upravuje pořadí směrem dolu
     * @param int $id
     * @return void
     */
    public function sortDown(int $id)
    {
        $database = $this->dotazyManager->select();

        $zaznam = $this->dotazyManager->getOne($id);

        $pocetZaznamu = $database->count();

        if ($zaznam->sort != $pocetZaznamu){
            $aktualSort = $zaznam->sort;
            $newSort = $aktualSort + 1;

            $zaznamPod = $database->where(DotazyManager::columnSort,$newSort)->fetch();

            if ($zaznamPod){
                $zaznamPod->update([DotazyManager::columnSort => $aktualSort]);
            }
            $zaznam->update([DotazyManager::columnSort => $newSort]);
        }
    }

    /**
     * srovná pořadí
     */
    public function resort()
    {
        $zaznamy = $this->dotazyManager->select()
            ->order(DotazyManager::columnSort.' ASC')
            ->fetchAll();

        $i = 1;

        foreach ($zaznamy as $one){
            $one->update([DotazyManager::columnSort => $i]);
            $i++;
        }
    }
}