<?php

namespace App\AdminModule\ObsahModule\DotazyModule\Presenters;

use App\AdminModule\Components\Paginator\PaginatorFactory;
use App\AdminModule\ObsahModule\DotazyModule\Components\EditDotaz\EditDotazFactory;
use App\AdminModule\ObsahModule\DotazyModule\Components\NovyDotaz\NovyDotazFactory;
use App\AdminModule\ObsahModule\DotazyModule\Components\VymazDotaz\VymazDotazFactory;
use App\AdminModule\ObsahModule\DotazyModule\Models\DotazyModel;
use App\AdminModule\Presenters\BaseAdminPresenter;
use App\Managers\DotazyManager;
use Nette\Application\UI\Multiplier;

final class SeznamPresenter extends BaseAdminPresenter
{
    const pageLimit = 12;

    private $dotazyManager;
    private $dotazyModel;
    private $novyDotazFactory;
    private $editDotazFactory;
    private $vymazDotazFactory;
    private $paginator;
    private $page = 1;
    private $itemsCount;
    private $filter = [];


    public function __construct(DotazyManager       $dotazyManager,
                                DotazyModel         $dotazyModel,
                                NovyDotazFactory    $novyDotazFactory,
                                EditDotazFactory    $editDotazFactory,
                                VymazDotazFactory   $vymazDotazFactory,
                                PaginatorFactory    $paginatorFactory)
    {
        $this->dotazyManager = $dotazyManager;
        $this->dotazyModel = $dotazyModel;
        $this->novyDotazFactory = $novyDotazFactory;
        $this->editDotazFactory = $editDotazFactory;
        $this->vymazDotazFactory = $vymazDotazFactory;
        $this->paginator = $paginatorFactory;
    }

    /*=============================== render metody ===============================*/
    public function renderDefault()
    {
        $offset = ($this->page - 1)*self::pageLimit;

        $data = $this->getDotazy();

        $this->itemsCount = $data->count();

        $this->template->page = $this->page;
        $this->template->filter = $this->filter;
        $this->template->count = $this->itemsCount;
        $this->template->data = $data->limit(self::pageLimit,$offset)->order(DotazyManager::columnSort.' ASC')->fetchAll();
    }

    /*=============================== handle metody ===============================*/
    /**
     * nastavení ajax stránkování pro paginator
     * @param int $page
     * @param array $filter
     * @return void
     */
    public function handlePage(int $page, array $filter): void
    {
        if ($this->isAjax()){
            $this->filter = $filter;
            $this->page = $page;
            $this->redrawControl('list');
        }
    }

    /**
     * změna pořadí směrem nahoru
     * @param int $id
     * @return void
     */
    public function handleSortUp(int $id, int $page, array $filter)
    {
        if ($this->presenter->isAjax()){
            try {
                $this->dotazyModel->sortUp($id);

                $this->page = $page;
                $this->filter = $filter;
                $this->redrawControl('list');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('Chyba zápisu do databáze','danger');
                $this->presenter->redrawControl('flash');
            }
        }
    }

    /**
     * změna pořadí směrem dolů
     * @param int $id
     * @return void
     */
    public function handleSortDown(int $id, int $page, array $filter)
    {
        if ($this->presenter->isAjax()){
            try {
                $this->dotazyModel->sortDown($id);

                $this->page = $page;
                $this->filter = $filter;
                $this->redrawControl('list');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('Chyba zápisu do databáze','danger');
                $this->presenter->redrawControl('flash');
            }
        }
    }

    /*=============================== komponenty ===============================*/
    /**
     * komponenta paginatoru seznamu
     * @return \App\AdminModule\Components\Paginator\PaginatorControl
     */
    protected function createComponentPaginator()
    {
        return $this->paginator->create($this->itemsCount,$this->page,'page!',$this->filter, self::pageLimit);
    }

    /**
     * nový dotaz
     * @return \App\AdminModule\ObsahModule\DotazyModule\Components\NovyDotaz\NovyDotazControl
     */
    protected function createComponentNovyDotaz()
    {
        return $this->novyDotazFactory->create();
    }

    /**
     * editace dotazu
     * @return Multiplier
     */
    protected function createComponentEditDotaz()
    {
        return new Multiplier(function ($id){
            $this->editDotazFactory->setDotaz($id);
            return $this->editDotazFactory->create();
        });
    }

    /**
     * vymazání dotazu
     * @return Multiplier
     */
    protected function createComponentVymazDotaz()
    {
        return new Multiplier(function ($id){
            $this->vymazDotazFactory->setDotaz($id);
            return $this->vymazDotazFactory->create();
        });
    }

    /*=============================== pomocné funkce ===============================*/

    private function getDotazy()
    {
        $data = $this->dotazyManager->select();

        return $data;
    }
}