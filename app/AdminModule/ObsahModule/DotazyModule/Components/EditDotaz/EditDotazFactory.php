<?php

namespace App\AdminModule\ObsahModule\DotazyModule\Components\EditDotaz;

use App\Managers\DotazyManager;

class EditDotazFactory
{
    private $dotazyManager;
    private $dotazId;

    public function __construct(DotazyManager   $dotazyManager)
    {
        $this->dotazyManager = $dotazyManager;
    }

    public function create()
    {
        return new EditDotazControl($this->dotazyManager,
                                    $this->dotazId);
    }

    public function setDotaz(int $id)
    {
        $this->dotazId = $id;
    }
}