<?php

namespace App\AdminModule\ObsahModule\DotazyModule\Components\EditDotaz;

use App\Managers\DotazyManager;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

class EditDotazControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/templates/template.latte';

    private $dotazyManager;
    private $dotazId;

    public function __construct(DotazyManager   $dotazyManager,
                                                $dotazId)
    {
        $this->dotazyManager = $dotazyManager;
        $this->dotazId = $dotazId;
    }

    public function render()
    {
        $dotaz = $this->dotazyManager->getOne($this->dotazId);

        $this['editDotazForm']->setDefaults($dotaz);
        $this->template->dotaz = $dotaz;
        $this->template->render(self::template);
    }

    /*============================ formulář ============================*/

    /**
     * formulář editovaného dotazu
     * @return Form
     */
    protected function createComponentEditDotazForm(): Form
    {
        $form = new Form;
        $form->addHidden('id');
        $form->addText('nazev')
            ->setRequired('Musíte vyplnit text otázky!');
        $form->addTextArea('obsah');
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendEditDotazForm'];
        return $form;
    }

    /**
     * zpracování formuláře editovaného dotazu
     * @param Form $form
     * @param $values
     * @return void
     */
    public function sendEditDotazForm(Form $form, $values)
    {
        try {
            $data = $form->getHttpData();

            unset($data['_do']);
            unset($data['send']);

            $this->dotazyManager->update($data['id'], ArrayHash::from($data));
            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Chyba zápisu dat do databáze!', 'danger');
            $this->redirect('this');
        }
    }
}