<?php

namespace App\AdminModule\ObsahModule\DotazyModule\Components\VymazDotaz;

use App\AdminModule\ObsahModule\DotazyModule\Models\DotazyModel;
use App\Managers\DotazyManager;
use Nette\Application\UI\Form;

class VymazDotazControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/templates/template.latte';

    private $dotazyManager;
    private $dotazyModel;
    private $dotazId;

    public function __construct(DotazyManager   $dotazyManager,
                                DotazyModel     $dotazyModel,
                                                $dotazId)
    {
        $this->dotazyManager = $dotazyManager;
        $this->dotazyModel = $dotazyModel;
        $this->dotazId = $dotazId;
    }

    public function render()
    {
        $dotaz = $this->dotazyManager->getOne($this->dotazId);

        $this['delDotazForm']->setDefaults($dotaz);
        $this->template->dotaz = $dotaz;
        $this->template->render(self::template);
    }

    /*============================ formulář ============================*/

    /**
     * formulář vymazaného dotazu
     * @return Form
     */
    protected function createComponentDelDotazForm(): Form
    {
        $form = new Form;
        $form->addHidden('id');
        $form->addSubmit('send','Vymazat');
        $form->onSuccess[] = [$this, 'sendDelDotazForm'];
        return $form;
    }

    /**
     * zpracování formuláře vymazaného dotazu
     * @param Form $form
     * @param $values
     * @return void
     */
    public function sendDelDotazForm(Form $form, $values)
    {
        try {
            $this->dotazyManager->delete($values->id);
            $this->dotazyModel->resort();
            $this->presenter->flashMessage('Data byla vymazána', 'success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Chyba zápisu dat do databáze!', 'danger');
            $this->redirect('this');
        }
    }
}