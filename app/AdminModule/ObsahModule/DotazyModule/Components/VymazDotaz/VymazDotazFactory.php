<?php

namespace App\AdminModule\ObsahModule\DotazyModule\Components\VymazDotaz;

use App\AdminModule\ObsahModule\DotazyModule\Models\DotazyModel;
use App\Managers\DotazyManager;

class VymazDotazFactory
{
    private $dotazyManager;
    private $dotazyModel;
    private $dotazId;

    public function __construct(DotazyManager   $dotazyManager,
                                DotazyModel     $dotazyModel)
    {
        $this->dotazyManager = $dotazyManager;
        $this->dotazyModel = $dotazyModel;
    }

    public function create()
    {
        return new VymazDotazControl(   $this->dotazyManager,
                                        $this->dotazyModel,
                                        $this->dotazId);
    }

    public function setDotaz(int $id)
    {
        $this->dotazId = $id;
    }
}