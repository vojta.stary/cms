<?php

namespace App\AdminModule\ObsahModule\DotazyModule\Components\NovyDotaz;

use App\AdminModule\ObsahModule\DotazyModule\Models\DotazyModel;

class NovyDotazFactory
{
    private $dotazyModel;

    public function __construct(DotazyModel $dotazyModel)
    {
        $this->dotazyModel = $dotazyModel;
    }

    public function create()
    {
        return new NovyDotazControl($this->dotazyModel);
    }
}