<?php

namespace App\AdminModule\ObsahModule\DotazyModule\Components\NovyDotaz;

use App\AdminModule\ObsahModule\DotazyModule\Models\DotazyModel;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

class NovyDotazControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $dotazyModel;

    public function __construct(DotazyModel $dotazyModel)
    {
        $this->dotazyModel = $dotazyModel;
    }

    public function render()
    {
        $this->template->render(self::template);
    }

    /*============================ formulář ============================*/

    /**
     * formulář nového dotazu
     * @return Form
     */
    protected function createComponentNovyDotazForm(): Form
    {
        $form = new Form;
        $form->addText('nazev')
            ->setRequired('Musíte vyplnit text otázky!');
        $form->addTextArea('obsah');
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendNovyDotazForm'];
        return $form;
    }

    /**
     * zpracování formuláře nového dotazu
     * @param Form $form
     * @param $values
     * @return void
     */
    public function sendNovyDotazForm(Form $form, $values)
    {
        try {
            $data = $form->getHttpData();

            unset($data['_do']);
            unset($data['send']);

            $this->dotazyModel->pridatPolozka(ArrayHash::from($data));
            $this->presenter->flashMessage('Data byla uložena', 'success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Chyba zápisu dat do databáze!', 'danger');
            $this->redirect('this');
        }
    }
}