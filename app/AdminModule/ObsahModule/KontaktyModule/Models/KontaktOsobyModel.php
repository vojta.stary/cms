<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Models;

use App\Managers\KontaktOsobyManager;

class KontaktOsobyModel
{
    private $kontaktOsobyManager;

    public function __construct(KontaktOsobyManager $kontaktOsobyManager)
    {
        $this->kontaktOsobyManager = $kontaktOsobyManager;
    }

    /**
     * přidává nový záznam
     * @param $values
     * @return void
     */
    public function pridatPolozka($values)
    {
        $databaseMenu = $this->kontaktOsobyManager->select();

        $pocetMenu = $databaseMenu->count();

        $sort = $pocetMenu + 1;

        $values->sort = $sort;

        $this->kontaktOsobyManager->create($values);
    }

    /**
     * upravuje pořadí směrem nahoru
     * @param int $id
     * @return void
     */
    public function sortUp(int $id)
    {
        $database = $this->kontaktOsobyManager->select();

        $zaznam = $this->kontaktOsobyManager->getOne($id);

        if ($zaznam->sort != 1){
            $aktualSort = $zaznam->sort;
            $newSort = $aktualSort - 1;

            $zaznamNad = $database->where(KontaktOsobyManager::columnSort,$newSort)->fetch();

            if ($zaznamNad){
                $zaznamNad->update([KontaktOsobyManager::columnSort => $aktualSort]);
            }
            $zaznam->update([KontaktOsobyManager::columnSort => $newSort]);
        }
    }

    /**
     * upravuje pořadí směrem dolu
     * @param int $id
     * @return void
     */
    public function sortDown(int $id)
    {
        $database = $this->kontaktOsobyManager->select();

        $zaznam = $this->kontaktOsobyManager->getOne($id);

        $pocetZaznamu = $database->count();

        if ($zaznam->sort != $pocetZaznamu){
            $aktualSort = $zaznam->sort;
            $newSort = $aktualSort + 1;

            $zaznamPod = $database->where(KontaktOsobyManager::columnSort,$newSort)->fetch();

            if ($zaznamPod){
                $zaznamPod->update([KontaktOsobyManager::columnSort => $aktualSort]);
            }
            $zaznam->update([KontaktOsobyManager::columnSort => $newSort]);
        }
    }

    /**
     * srovná pořadí kategorií
     * @param $parentId
     */
    public function resort()
    {
        $zaznamy = $this->kontaktOsobyManager->select()
            ->order(KontaktOsobyManager::columnSort.' ASC')
            ->fetchAll();

        $i = 1;

        foreach ($zaznamy as $one){
            $one->update([KontaktOsobyManager::columnSort => $i]);
            $i++;
        }
    }
}