<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Models;

use App\Managers\ProvozovnyDobaManager;
use App\Managers\ProvozovnyManager;

final class ProvozovnyModel
{
    private $provozovnyManager;
    private $provozovnyDobaManager;

    public function __construct(ProvozovnyManager       $provozovnyManager,
                                ProvozovnyDobaManager   $provozovnyDobaManager)
    {
        $this->provozovnyManager = $provozovnyManager;
        $this->provozovnyDobaManager = $provozovnyDobaManager;
    }

    public function create($values)
    {
        if ($values->oteviraci_doba){
            $dataDoba = $values->oteviraci_doba;
        }

        $dataProvoz = $values;
        unset($dataProvoz->oteviraci_doba);

        $id = $this->provozovnyManager->create($dataProvoz);

        if ($dataDoba && !empty($dataDoba)){
            foreach ($dataDoba as $nazev => $doba){
                $dobaData = [ProvozovnyDobaManager::columnProvozovnyId => $id, ProvozovnyDobaManager::columnNazev=> $nazev, ProvozovnyDobaManager::columnDoba => $doba];

                $this->provozovnyDobaManager->create($dobaData);
            }
        }
    }
}