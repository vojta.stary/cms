<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Components\NovaKontaktOsoba;

use App\AdminModule\ObsahModule\KontaktyModule\Models\KontaktOsobyModel;
use Nette\Application\UI\Form;

class NovaKontaktOsobaControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/templates/template.latte';

    private $kontaktOsobyModel;

    public function __construct(KontaktOsobyModel $kontaktOsobyModel)
    {
        $this->kontaktOsobyModel = $kontaktOsobyModel;
    }

    public function render()
    {
        $this->template->render(self::template);
    }

    /*===================================== Formulář =====================================*/

    protected function createComponentNovaKontaktOsobaForm(): Form
    {
        $form = new Form;
        $form->addText('jmeno','Jméno')
            ->setRequired('Musíte vyplnit %label!');
        $form->addText('funkce','Funkce')
            ->setRequired('Musíte vyplnit %label!');
        $form->addEmail('email','Emailová adresa')
            ->setRequired('Musíte vyplnit %label!');
        $form->addText('tel','Tel. číslo')
            ->setHtmlType('tel')
            ->addRule($form::PATTERN,'Špatný formát tel. čísla!','[0-9]{3} [0-9]{3} [0-9]{3}')
            ->setRequired('Musíte vyplnit %label!');
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = [$this, 'sendNovaKontaktOsobaForm'];
        return $form;
    }

    public function sendNovaKontaktOsobaForm(Form $form, $values)
    {
        try {
            $this->kontaktOsobyModel->pridatPolozka($values);
            $this->presenter->flashMessage('Data byla uložena','success');
            $this->redirect('this');
        }catch (\PDOException $e)
        {
            $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
            $this->redirect('this');
        }
    }
}