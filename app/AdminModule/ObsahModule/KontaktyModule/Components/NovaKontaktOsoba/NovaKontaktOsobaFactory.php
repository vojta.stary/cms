<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Components\NovaKontaktOsoba;

use App\AdminModule\ObsahModule\KontaktyModule\Models\KontaktOsobyModel;

class NovaKontaktOsobaFactory
{
    private $kontaktOsobyModel;

    public function __construct(KontaktOsobyModel $kontaktOsobyModel)
    {
        $this->kontaktOsobyModel = $kontaktOsobyModel;
    }

    public function create()
    {
        return new NovaKontaktOsobaControl($this->kontaktOsobyModel);
    }
}