<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Components\VymazProvozovna;

use App\Managers\ProvozovnyManager;
use Nette\Application\UI\Form;

class VymazProvozovnaControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/templates/template.latte';

    private $provozovnyManager;
    private $provozovnaId;

    public function __construct(ProvozovnyManager   $provozovnyManager,
                                                    $provozovnaId)
    {
        $this->provozovnyManager = $provozovnyManager;
        $this->provozovnaId = $provozovnaId;
    }

    public function render()
    {
        $provoz = $this->provozovnyManager->getOne($this->provozovnaId);

        $this['delProvozovnaForm']->setDefaults($provoz);
        $this->template->provoz = $provoz;
        $this->template->render(self::template);
    }

    /*================================= formulář =================================*/
    /**
     * vymazání kontaktní osoby
     * @return Form
     */
    protected function createComponentDelProvozovnaForm(): Form
    {
        $form = new Form;
        $form->addHidden('id');
        $form->addSubmit('send','Vymazat');
        $form->onSuccess[] = [$this, 'sendDelProvozovnaForm'];
        return $form;
    }

    /**
     * @param Form $form
     * @param $values
     * @return void
     * @throws \Nette\Application\AbortException
     */
    public function sendDelProvozovnaForm(Form $form, $values): void
    {
        try {
            $this->provozovnyManager->delete($values->id);

            $this->presenter->flashMessage('Data byla vymazána','success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this['delUserForm']->setDefaults($values);
            $this->presenter->flashMessage('Chyba zápisu dat do databáze!','danger');
            $this->redirect('this');
        }
    }
}