<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Components\VymazProvozovna;

use App\Managers\ProvozovnyManager;

class VymazProvozovnaFactory
{
    private $provozovnyManager;
    private $provozovnaId;

    public function __construct(ProvozovnyManager   $provozovnyManager)
    {
        $this->provozovnyManager = $provozovnyManager;
    }

    public function create()
    {
        return new VymazProvozovnaControl(  $this->provozovnyManager,
                                            $this->provozovnaId);
    }

    public function setProvozovna(int $id)
    {
        $this->provozovnaId = $id;
    }
}