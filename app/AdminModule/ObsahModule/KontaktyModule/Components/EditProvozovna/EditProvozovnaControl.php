<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Components\EditProvozovna;

use App\Managers\ProvozovnyDobaManager;
use App\Managers\ProvozovnyManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

class EditProvozovnaControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $provozovnyManager;
    private $provozovnyDobaManager;
    private $provozovnaId;

    public function __construct(ProvozovnyManager       $provozovnyManager,
                                ProvozovnyDobaManager   $provozovnyDobaManager,
                                                        $provozovnaId)
    {
        $this->provozovnyManager = $provozovnyManager;
        $this->provozovnyDobaManager = $provozovnyDobaManager;
        $this->provozovnaId = $provozovnaId;
    }

    public function render()
    {
        $provozovna = $this->provozovnyManager->getOne($this->provozovnaId);

        $this['editProvozovnaForm']->setDefaults($provozovna);
        $this->template->provozovna = $provozovna;
        $this->template->render(self::template);
    }

    /*===================================== Formulář =====================================*/

    public function handleRemDoba(int $id)
    {
        if ($this->presenter->isAjax()) {
            try {
                $this->provozovnyDobaManager->delete($id);
                $this->redrawControl('provozDoba');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('$chyba zápisu dat do databáze','danger');
                $this->presenter->redrawControl('flash');
            }
        }
    }

    public function handleAddDoba(array $data)
    {
        if ($this->presenter->isAjax()) {
            try {
                $data = [ProvozovnyDobaManager::columnProvozovnyId => $data['id'], ProvozovnyDobaManager::columnNazev => $data['nazev'], ProvozovnyDobaManager::columnDoba => $data['doba']];
                $this->provozovnyDobaManager->create($data);
                $this->presenter->flashMessage('Data byla uložena','success');
                $this->presenter->redrawControl('flash');
                $this->redrawControl('provozDoba');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('$chyba zápisu dat do databáze','danger');
                $this->presenter->redrawControl('flash');
            }
        }
    }

    /*===================================== Formulář =====================================*/
    protected function createComponentEditProvozovnaForm(): Form
    {
        $form = new Form;
        $form->addHidden('id')
            ->setDefaultValue($this->provozovnaId);
        $form->addText('nazev')
            ->setRequired('Musíte vyplnit název!');
        $form->addText('ulice')
            ->setRequired('Musíte vyplnit ulice!');
        $form->addText('mesto')
            ->setRequired('Musíte vyplnit město!');
        $form->addText('psc')
            ->addRule($form::PATTERN, 'Neplatný formát psč!', '[0-9]{3} [0-9]{2}')
            ->setRequired('Musíte vyplnit psč!');
        $form->addText('tel')
            ->setNullable()
            ->addRule($form::PATTERN, 'Neplatný formát tel. čísla!', '[0-9]{3} [0-9]{3} [0-9]{3}');
        $form->addEmail('email')
            ->setNullable();
        $form->addText('mapa');
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendNovaProvozovnaForm'];
        return $form;
    }

    public function sendNovaProvozovnaForm(Form $form, $values)
    {
        try{
            $this->provozovnyManager->update($values->id,$values);

            $this->presenter->flashMessage('Data byla uložena','success');
            $this->redirect('this');
        }catch(\PDOException $e){
            $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
            $this->redirect('this');
        }
    }
}