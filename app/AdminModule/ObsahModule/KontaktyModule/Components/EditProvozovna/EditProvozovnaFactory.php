<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Components\EditProvozovna;

use App\Managers\ProvozovnyDobaManager;
use App\Managers\ProvozovnyManager;

class EditProvozovnaFactory
{
    private $provozovnyManager;
    private $provozovnyDobaManager;
    private $provozovnaId;

    public function __construct(ProvozovnyManager       $provozovnyManager,
                                ProvozovnyDobaManager   $provozovnyDobaManager)
    {
        $this->provozovnyManager = $provozovnyManager;
        $this->provozovnyDobaManager = $provozovnyDobaManager;
    }

    public function create()
    {
        return new EditProvozovnaControl(   $this->provozovnyManager,
                                            $this->provozovnyDobaManager,
                                            $this->provozovnaId);
    }

    public function setProvozovna(int $id)
    {
        $this->provozovnaId = $id;
    }
}