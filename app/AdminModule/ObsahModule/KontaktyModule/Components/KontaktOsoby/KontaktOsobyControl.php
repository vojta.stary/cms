<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Components\KontaktOsoby;

use App\AdminModule\ObsahModule\KontaktyModule\Components\EditKontaktOsoba\EditKontaktOsobaFactory;
use App\AdminModule\ObsahModule\KontaktyModule\Components\NovaKontaktOsoba\NovaKontaktOsobaFactory;
use App\AdminModule\ObsahModule\KontaktyModule\Components\VymazKontaktOsoba\VymazKontaktOsobaFactory;
use App\AdminModule\ObsahModule\KontaktyModule\Models\KontaktOsobyModel;
use App\Managers\KontaktOsobyManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;
use Nette\Utils\Paginator;

class KontaktOsobyControl extends Control
{
    const   template = __DIR__.'/templates/template.latte',
            pageLimit = 6;

    private $kontaktyOsosbyManager;
    private $kontaktOsobyModel;
    private $novaKontaktOsobaFactory;
    private $editKontaktOsobaFactory;
    private $vymazKontaktOsobaFactory;
    private $paginator;
    private $page = 1;
    private $itemsCount;
    private $radius = 5;

    public function __construct(KontaktOsobyManager         $kontaktOsobyManager,
                                KontaktOsobyModel           $kontaktOsobyModel,
                                NovaKontaktOsobaFactory     $novaKontaktOsobaFactory,
                                EditKontaktOsobaFactory     $editKontaktOsobaFactory,
                                VymazKontaktOsobaFactory    $vymazKontaktOsobaFactory)
    {
        $this->kontaktyOsosbyManager = $kontaktOsobyManager;
        $this->kontaktOsobyModel = $kontaktOsobyModel;
        $this->novaKontaktOsobaFactory = $novaKontaktOsobaFactory;
        $this->editKontaktOsobaFactory = $editKontaktOsobaFactory;
        $this->vymazKontaktOsobaFactory = $vymazKontaktOsobaFactory;
        $this->paginator = new Paginator();
    }

    public function render()
    {
        $offset = ($this->page - 1 )* self::pageLimit;

        $data = $this->getOsoby();

        $this->itemsCount = $data->count();

        //paginator
        $this->paginator->setItemCount($this->itemsCount);
        $this->paginator->setItemsPerPage(self::pageLimit);
        $this->paginator->setPage($this->page);

        $left = $this->page - $this->radius >= 1 ? $this->page - $this->radius : 1;
        $right = $this->page + $this->radius <= $this->paginator->pageCount ? $this->page + $this->radius : $this->paginator->pageCount;

        $this->template->paginator = $this->paginator;
        $this->template->left = $left;
        $this->template->right = $right;
        $this->template->page = $this->page;

        $this->template->count = $data->count();
        $this->template->osoby = $data->limit(self::pageLimit,$offset)->fetchAll();
        $this->template->render(self::template);
    }

    /*===================================== handle metody =====================================*/
    /**
     * nastavení ajax stránkování pro paginator
     * @param int $page
     * @return void
     */
    public function handlePage(int $page): void
    {
        if ($this->presenter->isAjax()){
            $this->page = $page;
            $this->redrawControl('list');
        }
    }

    /**
     * @param int $id
     * @return void
     */
    public function handleSortUp(int $id)
    {
        if ($this->presenter->isAjax()){
            try {
                $this->kontaktOsobyModel->sortUp($id);
                $this->redrawControl('list');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('Chyba zápisu do databáze','danger');
                $this->presenter->redrawControl('flash');
            }
        }
    }

    public function handleSortDown(int $id)
    {
        if ($this->presenter->isAjax()){
            try {
                $this->kontaktOsobyModel->sortDown($id);
                $this->redrawControl('list');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('Chyba zápisu do databáze','danger');
                $this->presenter->redrawControl('flash');
            }
        }
    }

    /*===================================== komponenty =====================================*/

    /**
     * nová kontaktní osoba
     * @return \App\AdminModule\ObsahModule\KontaktyModule\Components\NovaKontaktOsoba\NovaKontaktOsobaControl
     */
    protected function createComponentNovaOsoba()
    {
        return $this->novaKontaktOsobaFactory->create();
    }

    /**
     * editace kontaktní osoby
     * @return Multiplier
     */
    protected function createComponentEditOsoba()
    {
        return new Multiplier(function ($id){
            $this->editKontaktOsobaFactory->setOsoba($id);
            return $this->editKontaktOsobaFactory->create();
        });
    }

    /**
     * vymazání kontaktní osoby
     * @return Multiplier
     */
    protected function createComponentVymazOsoba()
    {
        return new Multiplier(function ($id){
            $this->vymazKontaktOsobaFactory->setOsoba($id);
            return $this->vymazKontaktOsobaFactory->create();
        });
    }

    /*===================================== pomocné funkce =====================================*/
    /**
     * načítání dat pro seznam
     * @return \Nette\Database\Table\Selection
     */
    public function getOsoby()
    {
        $database = $this->kontaktyOsosbyManager->select();

        return $database->order(KontaktOsobyManager::columnSort.' ASC');
    }
}