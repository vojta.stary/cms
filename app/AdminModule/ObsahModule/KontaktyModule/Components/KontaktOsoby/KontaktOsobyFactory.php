<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Components\KontaktOsoby;

use App\AdminModule\ObsahModule\KontaktyModule\Components\EditKontaktOsoba\EditKontaktOsobaFactory;
use App\AdminModule\ObsahModule\KontaktyModule\Components\NovaKontaktOsoba\NovaKontaktOsobaFactory;
use App\AdminModule\ObsahModule\KontaktyModule\Components\VymazKontaktOsoba\VymazKontaktOsobaFactory;
use App\AdminModule\ObsahModule\KontaktyModule\Models\KontaktOsobyModel;
use App\Managers\KontaktOsobyManager;

class KontaktOsobyFactory
{
    private $kontaktyOsosbyManager;
    private $kontaktOsobyModel;
    private $novaKontaktOsobaFactory;
    private $editKontaktOsobaFactory;
    private $vymazKontaktOsobaFactory;

    public function __construct(KontaktOsobyManager         $kontaktOsobyManager,
                                KontaktOsobyModel           $kontaktOsobyModel,
                                NovaKontaktOsobaFactory     $novaKontaktOsobaFactory,
                                EditKontaktOsobaFactory     $editKontaktOsobaFactory,
                                VymazKontaktOsobaFactory    $vymazKontaktOsobaFactory)
    {
        $this->kontaktyOsosbyManager = $kontaktOsobyManager;
        $this->kontaktOsobyModel = $kontaktOsobyModel;
        $this->novaKontaktOsobaFactory = $novaKontaktOsobaFactory;
        $this->editKontaktOsobaFactory = $editKontaktOsobaFactory;
        $this->vymazKontaktOsobaFactory = $vymazKontaktOsobaFactory;
    }

    public function create()
    {
        return new KontaktOsobyControl( $this->kontaktyOsosbyManager,
                                        $this->kontaktOsobyModel,
                                        $this->novaKontaktOsobaFactory,
                                        $this->editKontaktOsobaFactory,
                                        $this->vymazKontaktOsobaFactory);
    }
}