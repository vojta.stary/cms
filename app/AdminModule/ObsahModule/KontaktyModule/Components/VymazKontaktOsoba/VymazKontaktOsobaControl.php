<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Components\VymazKontaktOsoba;

use App\AdminModule\ObsahModule\KontaktyModule\Models\KontaktOsobyModel;
use App\Managers\KontaktOsobyManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class VymazKontaktOsobaControl extends Control
{
    const template = __DIR__ . '/templates/template.latte';

    private $kontaktOsobyManager;
    private $kontaktOsobyModel;
    private $id;

    public function __construct(KontaktOsobyManager $kontaktOsobyManager,
                                KontaktOsobyModel   $kontaktOsobyModel,
                                                    $id)
    {
        $this->kontaktOsobyManager = $kontaktOsobyManager;
        $this->kontaktOsobyModel = $kontaktOsobyModel;
        $this->id = $id;
    }

    public function render(){
        $data = $this->kontaktOsobyManager->getOne($this->id);

        $this['delKontaktOsobaForm']->setDefaults($data);
        $this->template->osoba = $data;
        $this->template->render(self::template);
    }

    /*================================= formulář =================================*/
    /**
     * vymazání kontaktní osoby
     * @return Form
     */
    protected function createComponentDelKontaktOsobaForm(): Form
    {
        $form = new Form;
        $form->addHidden('id');
        $form->addSubmit('send','Vymazat');
        $form->onSuccess[] = [$this, 'sendDelKontaktOsobaForm'];
        return $form;
    }

    /**
     * @param Form $form
     * @param $values
     * @return void
     * @throws \Nette\Application\AbortException
     */
    public function sendDelKontaktOsobaForm(Form $form, $values): void
    {
        try {
            $this->kontaktOsobyManager->delete($values->id);
            $this->kontaktOsobyModel->resort();

            $this->presenter->flashMessage('Data byla vymazána','success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this['delUserForm']->setDefaults($values);
            $this->presenter->flashMessage('Chyba zápisu dat do databáze!','danger');
            $this->redirect('this');
        }
    }
}