<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Components\VymazKontaktOsoba;

use App\AdminModule\ObsahModule\KontaktyModule\Models\KontaktOsobyModel;
use App\Managers\KontaktOsobyManager;

class VymazKontaktOsobaFactory
{
    private $kontaktOsobyManager;
    private $kontaktOsobyModel;
    private $id;

    public function __construct(KontaktOsobyManager $kontaktOsobyManager,
                                KontaktOsobyModel   $kontaktOsobyModel)
    {
        $this->kontaktOsobyManager = $kontaktOsobyManager;
        $this->kontaktOsobyModel = $kontaktOsobyModel;
    }

    public function create()
    {
        return new VymazKontaktOsobaControl($this->kontaktOsobyManager,
                                            $this->kontaktOsobyModel,
                                            $this->id);
    }

    public function setOsoba(int $id)
    {
        $this->id = $id;
    }
}