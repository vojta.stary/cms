<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Components\EditKontaktOsoba;

use App\Managers\KontaktOsobyManager;

class EditKontaktOsobaFactory
{
    private $kontaktOsobyManager;
    private $osobaId;

    public function __construct(KontaktOsobyManager $kontaktOsobyManager)
    {
        $this->kontaktOsobyManager = $kontaktOsobyManager;
    }

    public function create()
    {
        return new EditKontaktOsobaControl( $this->kontaktOsobyManager,
                                            $this->osobaId);
    }

    public function setOsoba(int $id)
    {
        $this->osobaId = $id;
    }
}