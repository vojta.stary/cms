<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Components\EditKontaktOsoba;

use App\Managers\KontaktOsobyManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class EditKontaktOsobaControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $kontaktOsobyManager;
    private $osobaId;

    public function __construct(KontaktOsobyManager $kontaktOsobyManager,
                                                    $osobaId)
    {
        $this->kontaktOsobyManager = $kontaktOsobyManager;
        $this->osobaId = $osobaId;
    }

    public function render()
    {
        $osoba = $this->kontaktOsobyManager->getOne($this->osobaId);

        $this['editKontaktOsobaForm']->setDefaults($osoba);
        $this->template->osoba = $osoba;
        $this->template->render(self::template);
    }

    /*===================================== Formulář =====================================*/
    /**
     * formulář editace kontaktní osoby
     * @return Form
     */
    protected function createComponentEditKontaktOsobaForm(): Form
    {
        $form = new Form;
        $form->addHidden('id');
        $form->addText('jmeno','Jméno')
            ->setRequired('Musíte vyplnit %label!');
        $form->addText('funkce','Funkce')
            ->setRequired('Musíte vyplnit %label!');
        $form->addEmail('email','Emailová adresa')
            ->setRequired('Musíte vyplnit %label!');
        $form->addText('tel','Tel. číslo')
            ->setHtmlType('tel')
            ->addRule($form::PATTERN,'Špatný formát tel. čísla!','[0-9]{3} [0-9]{3} [0-9]{3}')
            ->setRequired('Musíte vyplnit %label!');
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = [$this, 'sendEditKontaktOsobaForm'];
        return $form;
    }

    public function sendEditKontaktOsobaForm(Form $form, $values)
    {
        try {
            $this->kontaktOsobyManager->update($values->id, $values);
            $this->presenter->flashMessage('Data byla uložena','success');
            $this->redirect('this');
        }catch (\PDOException $e)
        {
            $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
            $this->redirect('this');
        }
    }
}