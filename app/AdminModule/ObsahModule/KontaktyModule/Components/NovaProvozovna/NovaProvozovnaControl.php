<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Components\NovaProvozovna;

use App\AdminModule\ObsahModule\KontaktyModule\Models\ProvozovnyModel;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

class NovaProvozovnaControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $provozovnyModel;

    public function __construct(ProvozovnyModel $provozovnyModel)
    {
        $this->provozovnyModel = $provozovnyModel;
    }

    public function render()
    {
        $this->template->render(self::template);
    }

    /*===================================== Formulář =====================================*/
    protected function createComponentNovaProvozovnaForm(): Form
    {
        $form = new Form;
        $form->addText('nazev')
            ->setRequired('Musíte vyplnit název!');
        $form->addText('ulice')
            ->setRequired('Musíte vyplnit ulice!');
        $form->addText('mesto')
            ->setRequired('Musíte vyplnit město!');
        $form->addText('psc')
            ->addRule($form::PATTERN, 'Neplatný formát psč!', '[0-9]{3} [0-9]{2}')
            ->setRequired('Musíte vyplnit psč!');
        $form->addText('tel')
            ->setNullable()
            ->addRule($form::PATTERN, 'Neplatný formát tel. čísla!', '[0-9]{3} [0-9]{3} [0-9]{3}');
        $form->addEmail('email')
            ->setNullable();
        $form->addText('mapa');
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendNovaProvozovnaForm'];
        return $form;
    }

    public function sendNovaProvozovnaForm(Form $form, $values)
    {
        try{
            $data = $form->getHttpData();
            unset($data['_do']);
            unset($data['send']);

            $this->provozovnyModel->create(ArrayHash::from($data));

            $this->presenter->flashMessage('Data byla uložena','success');
            $this->redirect('this');

        }catch (\PDOException $e){
            $this->presenter->flashMessage('Chyba zápisu dat do databáze','danger');
            $this->redirect('this');
        }
    }
}