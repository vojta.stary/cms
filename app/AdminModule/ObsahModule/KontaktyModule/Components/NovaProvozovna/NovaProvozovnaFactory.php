<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Components\NovaProvozovna;

use App\AdminModule\ObsahModule\KontaktyModule\Models\ProvozovnyModel;

class NovaProvozovnaFactory
{
    private $provozovnyModel;

    public function __construct(ProvozovnyModel $provozovnyModel)
    {
        $this->provozovnyModel = $provozovnyModel;
    }

    public function create()
    {
        return new NovaProvozovnaControl($this->provozovnyModel);
    }
}