<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Components\Provozovny;

use App\AdminModule\ObsahModule\KontaktyModule\Components\EditProvozovna\EditProvozovnaFactory;
use App\AdminModule\ObsahModule\KontaktyModule\Components\NovaProvozovna\NovaProvozovnaFactory;
use App\AdminModule\ObsahModule\KontaktyModule\Components\VymazProvozovna\VymazProvozovnaFactory;
use App\Managers\ProvozovnyManager;

class ProvozovnyFactory
{
    private $provozovnyManager;
    private $novaProvozovnaFactory;
    private $editProvozovnaFactory;
    private $vymazProvozovnaFactory;

    public function __construct(ProvozovnyManager       $provozovnyManager,
                                NovaProvozovnaFactory   $novaProvozovnaFactory,
                                EditProvozovnaFactory   $editProvozovnaFactory,
                                VymazProvozovnaFactory  $vymazProvozovnaFactory)
    {
        $this->provozovnyManager = $provozovnyManager;
        $this->novaProvozovnaFactory = $novaProvozovnaFactory;
        $this->editProvozovnaFactory = $editProvozovnaFactory;
        $this->vymazProvozovnaFactory = $vymazProvozovnaFactory;
    }

    public function create()
    {
        return new ProvozovnyControl(   $this->provozovnyManager,
                                        $this->novaProvozovnaFactory,
                                        $this->editProvozovnaFactory,
                                        $this->vymazProvozovnaFactory);
    }
}