<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Components\Provozovny;

use App\AdminModule\ObsahModule\KontaktyModule\Components\EditProvozovna\EditProvozovnaFactory;
use App\AdminModule\ObsahModule\KontaktyModule\Components\NovaProvozovna\NovaProvozovnaFactory;
use App\AdminModule\ObsahModule\KontaktyModule\Components\VymazProvozovna\VymazProvozovnaFactory;
use App\Managers\ProvozovnyManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;
use Nette\Utils\Paginator;

class ProvozovnyControl extends Control
{
    const   template = __DIR__.'/templates/template.latte',
            pageLimit = 6;

    private $provozovnyManager;
    private $novaProvozovnaFactory;
    private $editProvozovnaFactory;
    private $vymazProvozovnaFactory;
    private $paginator;
    private $page = 1;
    private $itemsCount;
    private $radius = 5;

    public function __construct(ProvozovnyManager       $provozovnyManager,
                                NovaProvozovnaFactory   $novaProvozovnaFactory,
                                EditProvozovnaFactory   $editProvozovnaFactory,
                                VymazProvozovnaFactory  $vymazProvozovnaFactory)
    {
        $this->provozovnyManager = $provozovnyManager;
        $this->novaProvozovnaFactory = $novaProvozovnaFactory;
        $this->editProvozovnaFactory = $editProvozovnaFactory;
        $this->vymazProvozovnaFactory = $vymazProvozovnaFactory;
        $this->paginator = new Paginator();
    }

    public function render()
    {
        $offset = ($this->page - 1 )* self::pageLimit;

        $data = $this->getProvozovny();

        $this->itemsCount = $data->count();

        //paginator
        $this->paginator->setItemCount($this->itemsCount);
        $this->paginator->setItemsPerPage(self::pageLimit);
        $this->paginator->setPage($this->page);

        $left = $this->page - $this->radius >= 1 ? $this->page - $this->radius : 1;
        $right = $this->page + $this->radius <= $this->paginator->pageCount ? $this->page + $this->radius : $this->paginator->pageCount;

        $this->template->paginator = $this->paginator;
        $this->template->left = $left;
        $this->template->right = $right;
        $this->template->page = $this->page;

        $this->template->count = $data->count();
        $this->template->provozovny = $data->limit(self::pageLimit,$offset)->fetchAll();
        $this->template->render(self::template);
    }

    /*===================================== handle metody =====================================*/
    /**
     * nastavení ajax stránkování pro paginator
     * @param int $page
     * @return void
     */
    public function handlePage(int $page): void
    {
        if ($this->presenter->isAjax()){
            $this->page = $page;
            $this->redrawControl('list');
        }
    }

    /*===================================== komponenty =====================================*/

    /**
     * nová provozovna
     * @return \App\AdminModule\ObsahModule\KontaktyModule\Components\NovaProvozovna\NovaProvozovnaControl
     */
    protected function createComponentNovaProvozovna()
    {
        return $this->novaProvozovnaFactory->create();
    }

    /**
     * editace provozovny
     * @return Multiplier
     */
    protected function createComponentEditProvozovna()
    {
        return new Multiplier(function ($id) {
            $this->editProvozovnaFactory->setProvozovna($id);
            return $this->editProvozovnaFactory->create();
        });
    }

    /**
     * vymazání provozovny
     * @return Multiplier
     */
    protected function createComponentVymazProvozovna()
    {
        return new Multiplier(function ($id){
            $this->vymazProvozovnaFactory->setProvozovna($id);
            return $this->vymazProvozovnaFactory->create();
        });
    }
    /*===================================== pomocné funkce =====================================*/
    /**
     * načítání dat pro seznam
     * @return \Nette\Database\Table\Selection
     */
    public function getProvozovny()
    {
        $database = $this->provozovnyManager->select();

        return $database;
    }
}