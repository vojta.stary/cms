<?php

namespace App\AdminModule\ObsahModule\KontaktyModule\Presenters;

use App\AdminModule\ObsahModule\KontaktyModule\Components\KontaktOsoby\KontaktOsobyFactory;
use App\AdminModule\ObsahModule\KontaktyModule\Components\Provozovny\ProvozovnyFactory;
use App\AdminModule\Presenters\BaseAdminPresenter;

final class EditPresenter extends BaseAdminPresenter
{
    private $kontaktOsosbyFactory;
    private $provozovnyFactory;

    public function __construct(KontaktOsobyFactory $kontaktOsobyFactory,
                                ProvozovnyFactory   $provozovnyFactory)
    {
        $this->kontaktOsosbyFactory = $kontaktOsobyFactory;
        $this->provozovnyFactory = $provozovnyFactory;
    }

    /*===================================== komponenty =====================================*/
    /**
     * kontaktní osoby
     * @return \App\AdminModule\ObsahModule\KontaktyModule\Components\KontaktOsoby\KontaktOsobyControl
     */
    protected function createComponentKontaktOsoby()
    {
        return $this->kontaktOsosbyFactory->create();
    }

    /**
     * provozovny
     * @return \App\AdminModule\ObsahModule\KontaktyModule\Components\Provozovny\ProvozovnyControl
     */
    protected function createComponentProvozovny()
    {
        return $this->provozovnyFactory->create();
    }
}