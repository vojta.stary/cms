<?php

namespace App\AdminModule\ObsahModule\StrankyModule\Models;

use App\AdminModule\ObsahModule\ClankyModule\Models\ClanekModel;
use App\Managers\StrankyManager;
use Nette\Utils\Strings;

class StrankyModel
{
    private $strankyManager;
    private $clanekModel;

    public function __construct(StrankyManager  $strankyManager,
                                ClanekModel     $clanekModel)
    {
        $this->strankyManager = $strankyManager;
        $this->clanekModel = $clanekModel;
    }

    /**
     * vytváří novou stránku
     * @param $values
     * @return int
     */
    public function create($values): int
    {
        $newId = $this->strankyManager->create($values);

        $url = Strings::webalize($newId.'-'.$values->nazev);

        $this->strankyManager->update($newId, [StrankyManager::columnUrl => $url]);

        return $newId;
    }

    public function updateStatic($values)
    {
        $url = Strings::webalize($values->id.'-'.$values->nazev);

        $values->url = $url;

        $this->strankyManager->update($values->id, $values);
    }

    /**
     * vymazání stránky
     * @param int $strankaId
     * @return void
     */
    public function delete(int $strankaId)
    {
        $stranka = $this->strankyManager->getOne($strankaId);

        if($stranka->typ == 'static'){
            $stranka->delete();
        }

        if($stranka->typ == 'article'){
            $clanky = $stranka->related('clanky','stranky_id')->fetchAll();

            if (!empty($clanky)){
                foreach ($clanky as $one){
                    $this->clanekModel->delete($one->id);
                }
            }
            $stranka->delete();
        }
    }
}