<?php

namespace App\AdminModule\ObsahModule\StrankyModule\Components\DashStranky;

use App\Managers\StrankyManager;

class DashStrankyControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/templates/template.latte';

    private $strankyManager;

    public function __construct(StrankyManager $strankyManager)
    {
        $this->strankyManager = $strankyManager;
    }

    public function render()
    {
        $data = $this->strankyManager->select()->order(StrankyManager::columnLastUpdate.' DESC')->limit('10','0')->fetchAll();

        $this->template->stranky = $data;
        $this->template->render(self::template);
    }
}