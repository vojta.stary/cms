<?php

namespace App\AdminModule\ObsahModule\StrankyModule\Components\DashStranky;

use App\Managers\StrankyManager;

class DashStrankyFactory
{
    private $strankyManager;

    public function __construct(StrankyManager $strankyManager)
    {
        $this->strankyManager = $strankyManager;
    }

    public function create()
    {
        return new DashStrankyControl($this->strankyManager);
    }
}