<?php

namespace App\AdminModule\ObsahModule\StrankyModule\Components\VymazStranka;

use App\AdminModule\ObsahModule\StrankyModule\Models\StrankyModel;
use App\Managers\StrankyManager;

class VymazStrankaFactory
{
    private $strankyModel;
    private $strankyManager;
    private $strankaId;


    public function __construct(StrankyModel    $strankyModel,
                                StrankyManager  $strankyManager)
    {
        $this->strankyModel = $strankyModel;
        $this->strankyManager = $strankyManager;
    }

    /**
     * vytvoření komponenty
     * @return VymazStrankaControl
     */
    public function create()
    {
        return new VymazStrankaControl( $this->strankyModel,
                                        $this->strankyManager,
                                        $this->strankaId);
    }

    /**
     * setter pro id stránky
     * @param int $id
     * @return void
     */
    public function setStranka(int $id)
    {
        $this->strankaId = $id;
    }
}