<?php

namespace App\AdminModule\ObsahModule\StrankyModule\Components\VymazStranka;

use App\AdminModule\ObsahModule\StrankyModule\Models\StrankyModel;
use App\Managers\StrankyManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class VymazStrankaControl extends Control
{
    const template = __DIR__.'/template/template.latte';

    private $strankyModel;
    private $strankyManager;
    private $strankaId;


    public function __construct(StrankyModel    $strankyModel,
                                StrankyManager  $strankyManager,
                                                $strankaId)
    {
        $this->strankyModel = $strankyModel;
        $this->strankyManager = $strankyManager;
        $this->strankaId = $strankaId;
    }

    /**
     * render metoda komponenty
     * @return void
     */
    public function render()
    {
        $stranka = $this->strankyManager->getOne($this->strankaId);

        $this->template->stranka = $stranka;
        $this->template->render(self::template);
    }

    /*================================= formulář =================================*/

    /**
     * formulář vymazání
     * @return Form
     */
    protected function createComponentVymazStrankaForm(): Form
    {
        $form = new Form;
        $form->addHidden('id')
            ->setDefaultValue($this->strankaId);
        $form->addSubmit('send', 'Vymazat');
        $form->onSuccess[] = [$this, 'sendVymazStrankaForm'];
        return $form;
    }

    /**
     * zpracování formuláře vymazání
     * @param Form $form
     * @param $values
     * @return void
     * @throws \Nette\Application\AbortException
     */
    public function sendVymazStrankaForm(Form $form, $values)
    {
        try{
            $this->strankyModel->delete($values->id);
            $this->presenter->flashMessage('Data byla vymazána', 'success');
            $this->presenter->redirect(':Admin:Obsah:Stranky:Seznam:default');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Interní chyba aplikace', 'danger');
            $this->redirect('this');
        }
    }
}