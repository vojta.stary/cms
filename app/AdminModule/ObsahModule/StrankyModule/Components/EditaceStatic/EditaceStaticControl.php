<?php

namespace App\AdminModule\ObsahModule\StrankyModule\Components\EditaceStatic;

use App\AdminModule\ObsahModule\StrankyModule\Components\VymazStranka\VymazStrankaFactory;
use App\AdminModule\ObsahModule\StrankyModule\Models\StrankyModel;
use App\Managers\StrankyManager;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

class EditaceStaticControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/template/template.latte';

    private $strankyManager;
    private $strankyModel;
    private $vymazStrankaFactory;
    private $baseInfo;
    private $strankaId;

    public function __construct(StrankyManager          $strankyManager,
                                StrankyModel            $strankyModel,
                                VymazStrankaFactory     $vymazStrankaFactory,
                                                        $strankaId,
                                                        $baseInfo)
    {
        $this->strankaId = $strankaId;
        $this->strankyManager = $strankyManager;
        $this->vymazStrankaFactory = $vymazStrankaFactory;
        $this->strankyModel = $strankyModel;
        $this->baseInfo = $baseInfo;
    }

    /*=================================== render ===================================*/

    /**
     * render metoda komponenty
     * @return void
     */
    public function render()
    {
        $stranka = $this->strankyManager->getOne($this->strankaId);

        $this['editStrankaForm']->setDefaults($stranka);

        $this->template->baseInfo = $this->baseInfo;
        $this->template->stranka = $stranka;
        $this->template->render(self::template);
    }

    /*=================================== handle metody ===================================*/

    /**
     * změna aktivity stránky
     * @param int $id
     * @return void
     */
    public function handleStrankaActive(int $id)
    {
        if( $this->presenter->isAjax()){
            $this->strankyManager->changeActive($id);
            $this->redrawControl('active');
        }
    }

    /*=================================== komponenty ===================================*/
    /**
     * komponenta vymazání stránky
     * @return \App\AdminModule\ObsahModule\StrankyModule\Components\VymazStranka\VymazStrankaControl
     */
    protected function createComponentVymazStranka()
    {
        $this->vymazStrankaFactory->setStranka($this->strankaId);
        return $this->vymazStrankaFactory->create();
    }

    /*=================================== formulář ===================================*/
    protected function createComponentEditStrankaForm(): Form
    {
        $form= new Form;
        $form->addText('nazev','Název stránky')
            ->setRequired('Musíte vyplnit název!');
        $form->addText('url','URL adresa')
            ->setRequired('Musíte vyplnit adresu!');
        $form->addTextArea('meta_desc','SEO popis');
        $form->addTextArea('content','Obsah stránky');
        $form->addHidden('id')
            ->setDefaultValue($this->strankaId);
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendEditStrankaForm'];
        return $form;
    }

    public function sendEditStrankaForm(Form $form, $values)
    {
        $data = $form->getHttpData();
        unset($data['send']);
        unset($data['_do']);

        $this->strankyModel->updateStatic(ArrayHash::from($data));
        $this->presenter->flashMessage('Data byla uložena','success');
        $this->redirect('this');
    }
}