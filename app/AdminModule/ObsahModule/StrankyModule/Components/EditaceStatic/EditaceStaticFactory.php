<?php

namespace App\AdminModule\ObsahModule\StrankyModule\Components\EditaceStatic;

use App\AdminModule\ObsahModule\StrankyModule\Components\VymazStranka\VymazStrankaFactory;
use App\AdminModule\ObsahModule\StrankyModule\Models\StrankyModel;
use App\Managers\StrankyManager;

class EditaceStaticFactory
{
    private $strankyManager;
    private $strankyModel;
    private $vymazStrankaFactory;
    private $strankaId;
    private $baseInfo;

    public function __construct(StrankyManager          $strankyManager,
                                StrankyModel            $strankyModel,
                                VymazStrankaFactory     $vymazStrankaFactory)
    {
        $this->strankyManager = $strankyManager;
        $this->strankyModel = $strankyModel;
        $this->vymazStrankaFactory = $vymazStrankaFactory;
    }

    public function create()
    {
        return new EditaceStaticControl($this->strankyManager,
                                        $this->strankyModel,
                                        $this->vymazStrankaFactory,
                                        $this->strankaId,
                                        $this->baseInfo);
    }

    public function setStranka(int $strankaId)
    {
        $this->strankaId = $strankaId;
    }

    public function setBaseInfo($baseInfo)
    {
        $this->baseInfo = $baseInfo;
    }
}