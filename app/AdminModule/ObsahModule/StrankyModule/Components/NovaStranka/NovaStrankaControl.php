<?php

namespace App\AdminModule\ObsahModule\StrankyModule\Components\NovaStranka;

use App\AdminModule\ObsahModule\StrankyModule\Models\StrankyModel;
use Nette\Application\UI\Form;

class NovaStrankaControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/template/template.latte';

    private $strankyModel;

    public function __construct(StrankyModel $strankyModel)
    {
        $this->strankyModel = $strankyModel;
    }

    public function render()
    {
        $this->template->render(self::template);
    }

    /*================================= formulář =================================*/
    protected function createComponentNovaStrankaForm(): Form
    {
        $typ = ['static' => 'Statická stránka','article' => 'Články'];

        $form = new Form;
        $form->addSelect('typ','Typ',$typ)
            ->setRequired('Musíte vybrat typ!')
            ->setPrompt('Vyberte typ');
        $form->addText('nazev','Název')
            ->setRequired('Musíte vyplnit název!');
        $form->addHidden('active')
            ->setDefaultValue(true);
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendNovaStrankaForm'];

        return $form;
    }

    public function sendNovaStrankaForm(Form $form, $values)
    {
        try{

            $id = $this->strankyModel->create($values);
            $this->presenter->flashMessage('Data byla uložena','success');
            $this->presenter->redirect(':Admin:Obsah:Stranky:Edit:default',$id);

        }catch (\PDOException $e){

            $this->presenter->flashMessage('Interní chyba aplikace', 'danger');
            $this->redirect('this');

        }
    }
}