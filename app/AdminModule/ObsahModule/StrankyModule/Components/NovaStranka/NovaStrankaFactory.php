<?php

namespace App\AdminModule\ObsahModule\StrankyModule\Components\NovaStranka;

use App\AdminModule\ObsahModule\StrankyModule\Models\StrankyModel;

class NovaStrankaFactory
{
    private $strankyModel;

    public function __construct(StrankyModel $strankyModel)
    {
        $this->strankyModel = $strankyModel;
    }

    public function create()
    {
        return new NovaStrankaControl($this->strankyModel);
    }
}