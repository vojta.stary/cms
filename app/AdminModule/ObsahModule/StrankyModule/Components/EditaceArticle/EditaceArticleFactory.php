<?php

namespace App\AdminModule\ObsahModule\StrankyModule\Components\EditaceArticle;

use App\AdminModule\ObsahModule\ClankyModule\Components\SeznamClanku\SeznamClankuFactory;
use App\AdminModule\ObsahModule\StrankyModule\Components\VymazStranka\VymazStrankaFactory;
use App\AdminModule\ObsahModule\StrankyModule\Models\StrankyModel;
use App\Managers\StrankyManager;

class EditaceArticleFactory
{
    private $strankyManager;
    private $strankyModel;
    private $vymazStrankaFactory;
    private $seznamClankuFactory;
    private $strankaId;
    private $baseInfo;

    public function __construct(StrankyManager          $strankyManager,
                                StrankyModel            $strankyModel,
                                VymazStrankaFactory     $vymazStrankaFactory,
                                SeznamClankuFactory     $seznamClankuFactory)
    {
        $this->strankyManager = $strankyManager;
        $this->strankyModel = $strankyModel;
        $this->vymazStrankaFactory = $vymazStrankaFactory;
        $this->seznamClankuFactory = $seznamClankuFactory;
    }

    public function create()
    {
        return new EditaceArticleControl($this->strankyManager,
            $this->strankyModel,
            $this->vymazStrankaFactory,
            $this->seznamClankuFactory,
            $this->strankaId,
            $this->baseInfo);
    }

    public function setStranka(int $strankaId)
    {
        $this->strankaId = $strankaId;
    }

    public function setBaseInfo($baseInfo)
    {
        $this->baseInfo = $baseInfo;
    }
}