<?php

namespace App\AdminModule\ObsahModule\StrankyModule\Presenters;

use App\AdminModule\ObsahModule\StrankyModule\Components\EditaceArticle\EditaceArticleFactory;
use App\AdminModule\ObsahModule\StrankyModule\Components\EditaceStatic\EditaceStaticFactory;
use App\AdminModule\Presenters\BaseAdminPresenter;
use App\Managers\StrankyManager;

final class EditPresenter extends BaseAdminPresenter
{
    private $strankyManager;
    private $editaceStaticFactory;
    private $editaceArticleFactory;
    private $strankaId;

    public function __construct(StrankyManager          $strankyManager,
                                EditaceStaticFactory    $editaceStaticFactory,
                                EditaceArticleFactory   $editaceArticleFactory)
    {
        $this->strankyManager = $strankyManager;
        $this->editaceStaticFactory = $editaceStaticFactory;
        $this->editaceArticleFactory = $editaceArticleFactory;
    }

    /*=================================== action metody ===================================*/

    public function actionDefault(int $id)
    {
        $stranka = $this->strankyManager->getOne($id);

        if($stranka){
            $this->strankaId = $id;
            $this->template->stranka = $stranka;
        }else{
            $this->flashMessage('Stránka nebyla nalezena','danger');
            $this->redirect(':Admin:Obsah:Stranky:Seznam:default');
        }
    }

    /*=================================== komponenty ===================================*/

    /**
     * komponenta editace statické stránky
     * @return \App\AdminModule\ObsahModule\StrankyModule\Components\EditaceStatic\EditaceStaticControl
     */
    protected function createComponentEditStatic()
    {
        $this->editaceStaticFactory->setBaseInfo($this->baseInfo);
        $this->editaceStaticFactory->setStranka($this->strankaId);
        return $this->editaceStaticFactory->create();
    }

    /**
     * komponenta editace stránky článků
     * @return \App\AdminModule\ObsahModule\StrankyModule\Components\EditaceArticle\EditaceArticleControl
     */
    protected function createComponentEditArticle()
    {
        $this->editaceArticleFactory->setBaseInfo($this->baseInfo);
        $this->editaceArticleFactory->setStranka($this->strankaId);
        return $this->editaceArticleFactory->create();
    }
}