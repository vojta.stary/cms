<?php

namespace App\AdminModule\ObsahModule\StrankyModule\Presenters;

use App\AdminModule\Components\Paginator\PaginatorFactory;
use App\AdminModule\ObsahModule\StrankyModule\Components\NovaStranka\NovaStrankaFactory;
use App\AdminModule\Presenters\BaseAdminPresenter;
use App\Managers\StrankyManager;

final class SeznamPresenter extends BaseAdminPresenter
{
    const pageLimit = 12;

    private $strankyManager;
    private $novaStrankaFactory;
    private $paginator;
    private $page = 1;
    private $itemsCount;
    private $filter = [];

    /**
     * @param StrankyManager $strankyManager
     * @param PaginatorFactory $paginatorFactory
     * @param NovaStrankaFactory $novaStrankaFactory
     */
    public function __construct(StrankyManager      $strankyManager,
                                PaginatorFactory    $paginatorFactory,
                                NovaStrankaFactory  $novaStrankaFactory)
    {
        $this->strankyManager = $strankyManager;
        $this->paginator = $paginatorFactory;
        $this->novaStrankaFactory = $novaStrankaFactory;
    }

    /*=================== render metody ===================*/

    /**
     * metoda pro vykreslení a přípravu dat pro šablonu
     * @return void
     */
    public function renderDefault()
    {
        $offset = ($this->page - 1 )* self::pageLimit;

        $data = $this->getStranky();

        $this->itemsCount = $data->count();

        $this->template->page = $this->page;
        $this->template->filter = $this->filter;
        $this->template->stranky = $data->limit(self::pageLimit,$offset)->fetchAll();
    }

    /*=================== handle metody ===================*/
    /**
     * nastavení ajax stránkování pro paginator
     * @param int $page
     * @param array $filter
     * @return void
     */
    public function handlePage(int $page, array $filter): void
    {
        if ($this->isAjax()){
            $this->filter = $filter;
            $this->page = $page;
            $this->redrawControl('list');
        }
    }

    /**
     * nastavení filtrace seznamu
     * @param array $filter
     * @return void
     */
    public function handleFilter(array $filter): void
    {
        if ($this->isAjax())
        {
            $this->filter = $filter;
            $this->redrawControl('list');
        }
    }

    /**
     * změna aktivity stránky
     * @param int $id
     * @param array $filter
     * @param int $page
     * @return void
     */
    public function handleStrankaActive(int $id,array $filter, int $page)
    {
        if( $this->isAjax()){
            $this->page = $page;
            $this->filter = $filter;
            $this->strankyManager->changeActive($id);
            $this->redrawControl('list');
        }
    }

    /*=================== komponenty ===================*/

    /**
     * komponenta paginatoru seznamu
     * @return \App\AdminModule\Components\Paginator\PaginatorControl
     */
    protected function createComponentPaginator()
    {
        return $this->paginator->create($this->itemsCount,$this->page,'page!',$this->filter, self::pageLimit);
    }

    /**
     * komponenta nové stránky
     * @return \App\AdminModule\ObsahModule\StrankyModule\Components\NovaStranka\NovaStrankaControl
     */
    protected function createComponentNovaStranka()
    {
        return $this->novaStrankaFactory->create();
    }

    /*=================== pomocné funkce ===================*/

    /**
     * načítání a filtrování dat pro seznam
     * @return \Nette\Database\Table\Selection
     */
    public function getStranky()
    {
        $database = $this->strankyManager->select();

        if (isset($this->filter['typ']))
        {
            $database->where(StrankyManager::columnTyp, $this->filter['typ']);
        }
        if (isset($this->filter['nazev']))
        {
            $database->where('nazev LIKE ?',$this->filter['nazev'].'%');
        }
        if (isset($this->filter['aktivni']))
        {
            $database->where(StrankyManager::columnActive,$this->filter['aktivni']);
        }

        return $database->order(StrankyManager::columnNazev.' ASC');
    }
}