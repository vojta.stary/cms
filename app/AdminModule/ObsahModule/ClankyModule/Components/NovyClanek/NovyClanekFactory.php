<?php

namespace App\AdminModule\ObsahModule\ClankyModule\Components\NovyClanek;

use App\AdminModule\ObsahModule\ClankyModule\Models\ClanekModel;
use App\Managers\StrankyManager;

class NovyClanekFactory
{
    private $clanekModel;
    private $strankyManager;
    private $strankaId = null;

    public function __construct(ClanekModel     $clanekModel,
                                StrankyManager  $strankyManager)
    {
        $this->clanekModel = $clanekModel;
        $this->strankyManager = $strankyManager;
    }

    public function create()
    {
        return new NovyClanekControl(   $this->clanekModel,
                                        $this->strankyManager,
                                        $this->strankaId);
    }

    public function setStranka($strankaId)
    {
        $this->strankaId = $strankaId;
    }
}