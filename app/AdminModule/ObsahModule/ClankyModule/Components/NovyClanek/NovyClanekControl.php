<?php

namespace App\AdminModule\ObsahModule\ClankyModule\Components\NovyClanek;

use App\AdminModule\ObsahModule\ClankyModule\Models\ClanekModel;
use App\Managers\StrankyManager;
use Nette\Application\UI\Form;

class NovyClanekControl extends \Nette\Application\UI\Control
{
    const template = __DIR__ . '/templates/template.latte';

    private $clanekModel;
    private $strankyManager;
    private $strankaId;

    public function __construct(ClanekModel     $clanekModel,
                                StrankyManager  $strankyManager,
                                                $strankaId)
    {
        $this->clanekModel = $clanekModel;
        $this->strankyManager = $strankyManager;
        $this->strankaId = $strankaId;
    }

    public function render()
    {
        $this->template->strankaId = $this->strankaId;
        $this->template->render(self::template);
    }

    /*==================================== fomulář ====================================*/

    protected function createComponentNovyClanekForm(): Form
    {
        $form = new Form;

        if(!$this->strankaId){
            $stranky = [];
            $data = $this->strankyManager->select()->where(StrankyManager::columnTyp,'article')->fetchAll();
            foreach ($data as $row){
                $stranky[$row->id] = $row->nazev;
            }
            $form->addSelect('stranky_id','Stránka',$stranky)
                ->setPrompt('Vyberte stránku')
                ->setRequired('Musite vybrat stránku!');
        }else{
            $form->addHidden('stranky_id')
                ->setDefaultValue($this->strankaId);
        }
        $form->addText('nazev', 'Název')
            ->setRequired('Musíte vyplnit název!');
        $form->addHidden('active')
            ->setDefaultValue('0');
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = [$this, 'sendNovyClanekForm'];
        return $form;
    }

    public function sendNovyClanekForm(Form $form, $values)
    {
        try{

            $id = $this->clanekModel->create($values);
            $this->presenter->flashMessage('Data byla uložena','success');
            $this->presenter->redirect(':Admin:Obsah:Clanky:Edit:default',$id);

        }catch (\PDOException $e){

            $this->presenter->flashMessage('Interní chyba aplikace', 'danger');
            $this->redirect('this');

        }
    }
}