<?php

namespace App\AdminModule\ObsahModule\ClankyModule\Components\DashClanky;

use App\Managers\ClankyManager;
use Nette\Application\UI\Control;

class DashClankyControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $clankyManager;

    public function __construct(ClankyManager $clankyManager)
    {
        $this->clankyManager = $clankyManager;
    }

    public function render()
    {
        $clanky = $this->clankyManager->select()->order(ClankyManager::columnLastUpdate.' DESC')->limit('10','0')->fetchAll();

        $this->template->clanky = $clanky;
        $this->template->render(self::template);
    }
}