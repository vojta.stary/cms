<?php

namespace App\AdminModule\ObsahModule\ClankyModule\Components\Fotogalerie;

use App\AdminModule\ObsahModule\ClankyModule\Models\FotogalerieModel;
use App\Managers\ClankyFotkyManager;

class FotogalerieFactory
{
    private $clankyFotkyManager;
    private $fotogalerieModel;
    private $clanekId;

    public function __construct(ClankyFotkyManager  $clankyFotkyManager,
                                FotogalerieModel    $fotogalerieModel)
    {
        $this->clankyFotkyManager = $clankyFotkyManager;
        $this->fotogalerieModel = $fotogalerieModel;
    }

    public function create()
    {
        return new FotogalerieControl(  $this->clankyFotkyManager,
                                        $this->fotogalerieModel,
                                        $this->clanekId);
    }

    public function setClanek(int $clanekId)
    {
        $this->clanekId = $clanekId;
    }
}