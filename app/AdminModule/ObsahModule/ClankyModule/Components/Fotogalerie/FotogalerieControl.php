<?php

namespace App\AdminModule\ObsahModule\ClankyModule\Components\Fotogalerie;

use App\AdminModule\ObsahModule\ClankyModule\Models\FotogalerieModel;
use App\Managers\ClankyFotkyManager;
use Nette\Application\UI\Form;

class FotogalerieControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/templates/template.latte';

    private $clankyFotkyManager;
    private $fotogalerieModel;
    private $clanekId;

    public function __construct(ClankyFotkyManager  $clankyFotkyManager,
                                FotogalerieModel    $fotogalerieModel,
                                                    $clanekId)
    {
        $this->clankyFotkyManager = $clankyFotkyManager;
        $this->fotogalerieModel = $fotogalerieModel;
        $this->clanekId = $clanekId;
    }

    /*================================== render metoda ==================================*/
    public function render()
    {
        $this->template->clanekId = $this->clanekId;
        $this->template->fotky = $this->getFotky();
        $this->template->render(self::template);
    }

    /*================================== formulář ==================================*/

    protected function createComponentUploadFotkyForm(): Form
    {
        $form = new Form;
        $form->addMultiUpload('fotky','Fotografie')
            ->addRule($form::IMAGE, 'Fotografie musí být JPEG, PNG, GIF nebo WebP.')
            ->addRule($form::MAX_LENGTH, 'Maximálně lze nahrát %d fotografií', 6)
            ->setRequired('Musíte vybrat fotografie!');
        $form->addHidden('clanky_id')
            ->setDefaultValue($this->clanekId);
        $form->addSubmit('send','Náhrat');
        $form->onSuccess[] = [$this, 'sendUploadFotkyForm'];
        return $form;
    }

    public function sendUploadFotkyForm(Form $form, $values)
    {
        if ($this->presenter->isAjax()){

            try{
                $this->fotogalerieModel->saveFotky($values);

                $this->presenter->flashMessage('Fotografie byly nahrány','success');
                $this->presenter->redrawControl('flash');
                $this->redrawControl('formular');
                $this->redrawControl('galerie');
            }catch(\Exception $e){
                $this->presenter->flashMessage('Nasatala chyba při nahrávání','danger');
                $this->presenter->redrawControl('flash');
            }

        }
    }

    /*================================== handle metody ==================================*/

    public function handleDeleteImage($id)
    {
        if ($this->presenter->isAjax()){
            try{
                $this->fotogalerieModel->deleteImage($id);
                $this->presenter->flashMessage('Fotografie byla vymazána','success');
                $this->presenter->redrawControl('flash');
                $this->redrawControl('galerie');
            }catch (\PDOException $e){
                $this->presenter->flashMessage('Fotografii nelze vymazat','danger');
                $this->presenter->flashMessage('Fotografie je použita jako náhled','warning');
                $this->presenter->redrawControl('flash');
            }

        }
    }

    /*================================== pomocné funkce ==================================*/
    /**
     * získává fotky článku
     * @return \Nette\Database\Table\ActiveRow[]
     */
    private function getFotky()
    {
        $database = $this->clankyFotkyManager->select();

        $data = $database->where(ClankyFotkyManager::columnClankyId, $this->clanekId)
            ->fetchAll();
        return $data;
    }
}