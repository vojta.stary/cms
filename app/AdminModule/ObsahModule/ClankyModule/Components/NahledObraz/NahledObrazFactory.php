<?php

namespace App\AdminModule\ObsahModule\ClankyModule\Components\NahledObraz;

use App\Managers\ClankyFotkyManager;
use App\Managers\ClankyManager;

class NahledObrazFactory
{
    private $clankyManager;
    private $clankyFotkyManager;
    private $clanekId;

    public function __construct(ClankyManager       $clankyManager,
                                ClankyFotkyManager  $clankyFotkyManager)
    {
        $this->clankyManager = $clankyManager;
        $this->clankyFotkyManager = $clankyFotkyManager;
    }

    public function create()
    {
        return new NahledObrazControl(  $this->clankyManager,
                                        $this->clankyFotkyManager,
                                        $this->clanekId);
    }

    public function setClanek(int $clanekId)
    {
        $this->clanekId = $clanekId;
    }
}