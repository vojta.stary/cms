<?php

namespace App\AdminModule\ObsahModule\ClankyModule\Components\NahledObraz;

use App\Managers\ClankyFotkyManager;
use App\Managers\ClankyManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class NahledObrazControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $clankyManager;
    private $clankyFotkyManager;
    private $clanekId;
    private $typ = null;

    public function __construct(ClankyManager       $clankyManager,
                                ClankyFotkyManager  $clankyFotkyManager,
                                                    $clanekId)
    {
        $this->clanekId = $clanekId;
        $this->clankyManager = $clankyManager;
        $this->clankyFotkyManager = $clankyFotkyManager;
    }

    public function render()
    {
        $this->template->clanekId = $this->clanekId;
        $this->template->typ = $this->typ;
        $this->template->render(self::template);
    }

    /*================================== formulář ==================================*/
    protected function createComponentNahledObrazForm(): Form
    {
        $typ = ['icon' => 'Ikona', 'img' => 'Fotografie'];

        $form = new Form;
        $form->addSelect('base_img_type','Typ obrázku', $typ)
            ->setPrompt('Vyberte možnost')
            ->setRequired('Musíte vybrat typ!');
        $form->addRadioList('base_img_icon', 'Ikona');
        $form->addRadioList('base_img_id', 'Fotografie');
        $form->addHidden('id')
            ->setDefaultValue($this->clanekId);
        $form->addSubmit('send','Náhrat');
        $form->onSuccess[] = [$this, 'sendNahledObrazForm'];
        return $form;
    }

    public function sendNahledObrazForm(Form $form,$values)
    {
        try {
            $data = $form->getHttpData();
            unset($data['send']);
            unset($data['_do']);

            if($data['base_img_type'] == 'img'){
                $databaseData = ['base_img_icon' => null,
                    'base_img_id' => $data['base_img_id'],
                    'base_img_type' => $data['base_img_type']];
            }

            if($data['base_img_type'] == 'icon'){
                $databaseData = ['base_img_icon' => $data['base_img_icon'],
                    'base_img_id' => null,
                    'base_img_type' => $data['base_img_type']];
            }

            $this->clankyManager->update($data['id'],$databaseData);
            $this->presenter->flashMessage('Data byla uložena','success');
            $this->redirect('this');

        }catch(\PDOException $e){
            $this->presenter->flashMessage('Nasatala chyba při nahrávání','danger');
            $this->redirect('this');
        }
    }

    /*================================== handle metody ==================================*/

    public function handleChangeForm($typ = null)
    {
        if ($this->presenter->isAjax()){
            $this->typ = $typ;

            if($typ == 'icon')
            {
                $icons = ['<i class="fa-solid fa-info text-info"></i>' => '<i class="fa-solid fa-info text-info"></i>',
                    '<i class="fa-solid fa-question text-secondary"></i>' => '<i class="fa-solid fa-question text-secondary"></i>',
                    '<i class="fa-solid fa-triangle-exclamation text-danger"></i>' => '<i class="fa-solid fa-triangle-exclamation text-danger"></i>',
                    '<i class="fa-solid fa-hand-point-up text-warning"></i>' => '<i class="fa-solid fa-hand-point-up text-warning"></i>'];

                $this['nahledObrazForm']['base_img_icon']->setItems($icons)->setRequired('Musíte vybrat ikonu!');
                $this['nahledObrazForm']['base_img_type']->setDefaultValue($typ);
            }

            if($typ == 'img')
            {
                $fotky = $this->getFotky();

                if(!empty($fotky)){
                    $this['nahledObrazForm']['base_img_id']->setItems($fotky)->setRequired('Musíte vybrat fotografii!');
                }
                $this['nahledObrazForm']['base_img_type']->setDefaultValue($typ);
            }

            $this->redrawControl('formular');
        }
    }

    /*================================== pomocné funkce ==================================*/
    private function getFotky()
    {
        $array = [];

        $database = $this->clankyFotkyManager->select();

        $data = $database->where(ClankyFotkyManager::columnClankyId, $this->clanekId)->fetchAll();

        if(!empty($data)){
            foreach ($data as $row){
                $array[$row->id] = $row->url;
            }
        }else{
            $array = null;
        }

        return $array;
    }
}