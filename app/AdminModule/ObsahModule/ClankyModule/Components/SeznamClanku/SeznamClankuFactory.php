<?php

namespace App\AdminModule\ObsahModule\ClankyModule\Components\SeznamClanku;

use App\AdminModule\Components\Paginator\PaginatorFactory;
use App\AdminModule\ObsahModule\ClankyModule\Components\NovyClanek\NovyClanekFactory;
use App\Managers\ClankyManager;
use App\Managers\StrankyManager;

class SeznamClankuFactory
{
    private $clankyManager;
    private $strankyManager;
    private $novyClanekFactory;
    private $strankaId = null;

    public function __construct(ClankyManager       $clankyManager,
                            StrankyManager          $strankyManager,
                                NovyClanekFactory   $novyClanekFactory)
    {
        $this->clankyManager = $clankyManager;
        $this->strankyManager = $strankyManager;
        $this->novyClanekFactory = $novyClanekFactory;
    }

    /**
     * vrací kompnentu
     * @return SeznamClankuControl
     */
    public function create()
    {
        return new SeznamClankuControl($this->clankyManager,
                                        $this->strankyManager,
                                        $this->novyClanekFactory,
                                        $this->strankaId);
    }

    /**
     * setter pro ID stránky
     * @param int $strankaId
     * @return void
     */
    public function setStranka($strankaId): void
    {
        $this->strankaId = $strankaId;
    }
}