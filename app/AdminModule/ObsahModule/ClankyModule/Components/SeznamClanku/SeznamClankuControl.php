<?php

namespace App\AdminModule\ObsahModule\ClankyModule\Components\SeznamClanku;


use App\AdminModule\ObsahModule\ClankyModule\Components\NovyClanek\NovyClanekFactory;
use App\Managers\ClankyManager;
use App\Managers\StrankyManager;
use Nette\Utils\Paginator;

class SeznamClankuControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/templates/template.latte',
            pageLimit = 12;

    private $clankyManager;
    private $strankyManager;
    private $novyClanekFactory;
    private $paginator;
    private $strankaId;
    private $page = 1;
    private $itemsCount;
    private $filter = [];
    private $radius = 5;

    public function __construct(ClankyManager       $clankyManager,
                                StrankyManager      $strankyManager,
                                NovyClanekFactory   $novyClanekFactory,
                                                    $strankaId)
    {
        $this->clankyManager = $clankyManager;
        $this->strankyManager = $strankyManager;
        $this->novyClanekFactory = $novyClanekFactory;
        $this->paginator = new Paginator();
        $this->strankaId = $strankaId;
    }

    public function render()
    {
        $offset = ($this->page - 1 )* self::pageLimit;

        $data = $this->getClanky();

        $this->itemsCount = $data->count();

        //paginator
        $this->paginator->setItemCount($this->itemsCount);
        $this->paginator->setItemsPerPage(self::pageLimit);
        $this->paginator->setPage($this->page);

        $left = $this->page - $this->radius >= 1 ? $this->page - $this->radius : 1;
        $right = $this->page + $this->radius <= $this->paginator->pageCount ? $this->page + $this->radius : $this->paginator->pageCount;

        if(!$this->strankaId){
            $stranky = $this->strankyManager->select()->where(StrankyManager::columnTyp,'article')->fetchAll();
            $this->template->stranky = $stranky;
        }

        $this->template->paginator = $this->paginator;
        $this->template->left = $left;
        $this->template->right = $right;
        $this->template->page = $this->page;
        $this->template->filter = $this->filter;
        $this->template->strankaId = $this->strankaId;
        $this->template->clanky = $data->limit(self::pageLimit,$offset)->fetchAll();

        $this->template->render(self::template);
    }

    /*===================================== handle metody =====================================*/
    /**
     * nastavení ajax stránkování pro paginator
     * @param int $page
     * @param array $filter
     * @return void
     */
    public function handlePage(int $page, array $filter): void
    {
        if ($this->presenter->isAjax()){
            $this->filter = $filter;
            $this->page = $page;
            $this->redrawControl('list');
        }
    }

    /**
     * nastavení filtrace seznamu
     * @param array $filter
     * @return void
     */
    public function handleFilter(array $filter): void
    {
        if ($this->presenter->isAjax())
        {
            $this->filter = $filter;
            $this->redrawControl('list');
        }
    }

    /**
     * změna aktivity článku
     * @param int $id
     * @param array $filter
     * @param int $page
     * @return void
     */
    public function handleClanekActive(int $id,array $filter, int $page)
    {
        if( $this->presenter->isAjax()){
            $this->page = $page;
            $this->filter = $filter;
            $this->clankyManager->changeActive($id);
            $this->redrawControl('list');
        }
    }

    /*===================================== komponenty =====================================*/
    /**
     * nový článek
     * @return \App\AdminModule\ObsahModule\ClankyModule\Components\NovyClanek\NovyClanekControl
     */
    protected function createComponentNovyClanek()
    {
        $this->novyClanekFactory->setStranka($this->strankaId);
        return $this->novyClanekFactory->create();
    }

    /*===================================== pomocné funkce =====================================*/
    /**
     * načítání a filtrování dat pro seznam
     * @return \Nette\Database\Table\Selection
     */
    public function getClanky()
    {
        $database = $this->clankyManager->select();

        if ($this->strankaId != null){
            $database->where(ClankyManager::columnStrankyId, $this->strankaId);
        }

        if (isset($this->filter['nazev']))
        {
            $database->where('nazev LIKE ?',$this->filter['nazev'].'%');
        }
        if (isset($this->filter['stranka']))
        {
            $database->where('stranky_id',$this->filter['stranka']);
        }
        if (isset($this->filter['aktivni']))
        {
            $database->where(ClankyManager::columnActive,$this->filter['aktivni']);
        }

        return $database->order(ClankyManager::columnNazev.' ASC');
    }
}