<?php

namespace App\AdminModule\ObsahModule\ClankyModule\Components\VymazClanek;

use App\AdminModule\ObsahModule\ClankyModule\Models\ClanekModel;
use App\Managers\ClankyManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class VymazClanekControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $clankyManager;
    private $clanekModel;
    private $clanekId;

    public function __construct(ClankyManager   $clankyManager,
                                ClanekModel     $clanekModel,
                                                $clanekId)
    {
        $this->clankyManager = $clankyManager;
        $this->clanekModel = $clanekModel;
        $this->clanekId = $clanekId;
    }

    public function render()
    {
        $clanek = $this->clankyManager->getOne($this->clanekId);

        $this['vymazClanekForm']->setDefaults($clanek);
        $this->template->clanek = $clanek;
        $this->template->render(self::template);
    }

    /*================================= formulář =================================*/

    /**
     * formulář vymazání
     * @return Form
     */
    protected function createComponentVymazClanekForm(): Form
    {
        $form = new Form;
        $form->addHidden('id');
        $form->addSubmit('send', 'Vymazat');
        $form->onSuccess[] = [$this, 'sendVymazClanekForm'];
        return $form;
    }

    /**
     * zpracování formuláře vymazání
     * @param Form $form
     * @param $values
     * @return void
     * @throws \Nette\Application\AbortException
     */
    public function sendVymazClanekForm(Form $form, $values)
    {
        try{
            $this->clanekModel->delete($values->id);
            $this->presenter->flashMessage('Data byla vymazána', 'success');
            $this->presenter->redirect(':Admin:Obsah:Clanky:Seznam:default');
        }catch (\PDOException $e){
            $this->presenter->flashMessage('Interní chyba aplikace', 'danger');
            $this->redirect('this');
        }
    }
}