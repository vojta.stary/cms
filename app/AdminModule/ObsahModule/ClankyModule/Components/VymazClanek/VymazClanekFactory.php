<?php

namespace App\AdminModule\ObsahModule\ClankyModule\Components\VymazClanek;

use App\AdminModule\ObsahModule\ClankyModule\Models\ClanekModel;
use App\Managers\ClankyManager;

class VymazClanekFactory
{
    private $clankyManager;
    private $clanekModel;
    private $clanekId;

    public function __construct(ClankyManager   $clankyManager,
                                ClanekModel     $clanekModel)
    {
        $this->clankyManager = $clankyManager;
        $this->clanekModel = $clanekModel;
    }

    public function create()
    {
        return new VymazClanekControl(  $this->clankyManager,
                                        $this->clanekModel,
                                        $this->clanekId);
    }

    public function setClanek(int $id)
    {
        $this->clanekId = $id;
    }
}