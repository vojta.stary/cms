<?php

namespace App\AdminModule\ObsahModule\ClankyModule\Presenters;

use App\AdminModule\ObsahModule\ClankyModule\Components\Fotogalerie\FotogalerieFactory;
use App\AdminModule\ObsahModule\ClankyModule\Components\NahledObraz\NahledObrazFactory;
use App\AdminModule\ObsahModule\ClankyModule\Components\VymazClanek\VymazClanekFactory;
use App\AdminModule\ObsahModule\ClankyModule\Models\ClanekModel;
use App\AdminModule\Presenters\BaseAdminPresenter;
use App\Managers\ClankyManager;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

final class EditPresenter extends BaseAdminPresenter
{
    private $clankyManager;
    private $clanekModel;
    private $fotogalerieFactory;
    private $nahledObrazFactory;
    private $vymazClanekFactory;
    private $clanekId;

    public function __construct(ClankyManager      $clankyManager,
                                ClanekModel        $clanekModel,
                                FotogalerieFactory $fotogalerieFactory,
                                NahledObrazFactory $nahledObrazFactory,
                                VymazClanekFactory $vymazClanekFactory)
    {
        $this->clankyManager = $clankyManager;
        $this->clanekModel = $clanekModel;
        $this->fotogalerieFactory = $fotogalerieFactory;
        $this->nahledObrazFactory = $nahledObrazFactory;
        $this->vymazClanekFactory = $vymazClanekFactory;
    }

    /*=================================== action metody ===================================*/

    public function actionDefault(int $id, $strankaId = null)
    {
        $clanek = $this->clankyManager->getOne($id);

        if (!$clanek) {
            $this->flashMessage('Článek nebyl nalezen', 'danger');

            if ($strankaId) {
                $this->redirect(':Admin:Obsah:Stranky:Edit:default', $strankaId);
            } else {
                $this->redirect(':Admin:Obsah:Clanky:Seznam:default');
            }
        }
        $this->clanekId = $id;
        $this->template->strankaId = $strankaId;
    }

    public function renderDefault()
    {
        $clanek = $this->clankyManager->getOne($this->clanekId);

        $this['editClanekForm']->setDefaults($clanek);
        $this->template->clanek = $clanek;
    }

    /*=================================== handle metody ===================================*/
    /**
     * změna aktivity článku
     * @param int $id
     * @return void
     */
    public function handleClanekActive(int $id)
    {
        if ($this->isAjax()) {
            $this->clankyManager->changeActive($id);
            $this->redrawControl('status');
        }
    }

    public function handleClanekPublic(int $id)
    {
        if ($this->isAjax()) {
            $this->clanekModel->publikovat($id);
            $this->presenter->flashMessage('Článek byl publikován','success');
            $this->presenter->redrawControl('flash');
            $this->redrawControl('status');
        }
    }

    /*=================================== komponenty ===================================*/

    /**
     * fotogalerie
     * @return \App\AdminModule\ObsahModule\ClankyModule\Components\Fotogalerie\FotogalerieControl
     */
    protected function createComponentFotogalerie()
    {
        $this->fotogalerieFactory->setClanek($this->clanekId);
        return $this->fotogalerieFactory->create();
    }

    /**
     * náhedový obrazek článku
     * @return \App\AdminModule\ObsahModule\ClankyModule\Components\NahledObraz\NahledObrazControl
     */
    protected function createComponentNahledObraz()
    {
        $this->nahledObrazFactory->setClanek($this->clanekId);
        return $this->nahledObrazFactory->create();
    }

    /**
     * vymazání článku
     * @return \App\AdminModule\ObsahModule\ClankyModule\Components\VymazClanek\VymazClanekControl
     */
    protected function createComponentVymazClanek()
    {
        $this->vymazClanekFactory->setClanek($this->clanekId);
        return $this->vymazClanekFactory->create();
    }

    /*=================================== formulář ===================================*/
    protected function createComponentEditClanekForm(): Form
    {
        $form= new Form;
        $form->addText('nazev','Název stránky')
            ->setRequired('Musíte vyplnit název!');
        $form->addText('url','URL adresa')
            ->setRequired('Musíte vyplnit adresu!');
        $form->addTextArea('meta_desc','SEO popis');
        $form->addTextArea('content','Obsah stránky');
        $form->addHidden('id')
            ->setDefaultValue($this->clanekId);
        $form->addSubmit('send','Uložit');
        $form->onSuccess[] = [$this, 'sendEditClanekForm'];
        return $form;
    }

    public function sendEditClanekForm(Form $form, $values)
    {
        try {
            $data = $form->getHttpData();
            unset($data['send']);
            unset($data['_do']);

            $this->clanekModel->update(ArrayHash::from($data));
            $this->flashMessage('Data byla uložena','success');
            $this->redirect('this');
        }catch (\PDOException $e){
            $this->flashMessage('Chyba zápisu dat','danger');
            $this->redirect('this');
        }
    }
}