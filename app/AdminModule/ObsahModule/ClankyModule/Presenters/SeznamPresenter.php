<?php

namespace App\AdminModule\ObsahModule\ClankyModule\Presenters;

use App\AdminModule\ObsahModule\ClankyModule\Components\SeznamClanku\SeznamClankuFactory;
use App\AdminModule\Presenters\BaseAdminPresenter;

final class SeznamPresenter extends BaseAdminPresenter
{
    private $seznamClankuFactory;

    public function __construct(SeznamClankuFactory $seznamClankuFactory)
    {
        $this->seznamClankuFactory = $seznamClankuFactory;
    }


    /*============================ komponenty ============================*/
    /**
     * seznam článků
     * @return \App\AdminModule\ObsahModule\ClankyModule\Components\SeznamClanku\SeznamClankuControl
     */
    protected function createComponentSeznamClanku()
    {
        return $this->seznamClankuFactory->create();
    }
}