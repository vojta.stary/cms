<?php

namespace App\AdminModule\ObsahModule\ClankyModule\Models;

use App\Managers\ClankyFotkyManager;
use Nette\Utils\FileSystem;
use Nette\Utils\Image;
use Nette\Utils\Strings;

class FotogalerieModel
{
    const dir = __DIR__.'/../../../../../www/src/fotogallery';

    private $clankyFotkyManager;

    public function __construct(ClankyFotkyManager $clankyFotkyManager)
    {
        $this->clankyFotkyManager = $clankyFotkyManager;
    }

    public function saveFotky($values)
    {
        foreach ($values->fotky as $fotka){
            // nahraje fotku na server
            if(!empty($fotka)) {

                $newImageName = $fotka->getName();

                $pripona = pathinfo($newImageName);

                $newImageName = Strings::webalize($pripona['filename']) . '.' . $pripona['extension'];

                if ($fotka->hasFile()) {
                    $newImageFile = self::dir .'/clanky/'. $values->clanky_id .'/'. $newImageName;
                    $fotka->move($newImageFile);
                }

                //uloží data do databáze
                $dataDatabase = [ClankyFotkyManager::columnClankyId => $values->clanky_id, ClankyFotkyManager::columnNazev => $pripona['filename'], ClankyFotkyManager::columnUrl => $newImageName];
                $this->clankyFotkyManager->create($dataDatabase);

                //změní velikost fotky
                $obrazek = Image::fromFile($newImageFile);
                if($obrazek->getWidth() > 1920){
                    $obrazek->resize(1920,null);
                    $obrazek->save($newImageFile,80);
                }


                //vytvori thumb obrázek
                $thumbSlozka = self::dir. '/clanky/' .$values->clanky_id. '/thumbs/' .$newImageName;
                FileSystem::createDir(self::dir. '/clanky/' .$values->clanky_id. '/thumbs');
                $thumb = Image::fromFile($newImageFile);
                $thumb->resize(null, 225);
                $thumb->save($thumbSlozka,80);
            }
        }
    }

    public function deleteImage(int $imageId)
    {
        $fotka = $this->clankyFotkyManager->getOne($imageId);

        $url = $fotka->url;

        $thumbSlozka = self::dir. '/clanky/' .$fotka->clanky_id. '/thumbs/';
        $slozka = self::dir .'/clanky/'. $fotka->clanky_id.'/';

        $this->clankyFotkyManager->delete($imageId);

        unlink($thumbSlozka.$url);
        unlink($slozka.$url);
    }

    public function deleteFolder(int $clanekId)
    {
        $slozka = self::dir.'/clanky/'.$clanekId;
        $thumbSlozka = self::dir.'/clanky/'.$clanekId.'/thumbs';

        if (is_dir($slozka)){
            if (is_dir($thumbSlozka)){
                rmdir($thumbSlozka);
            }
            rmdir($slozka);
        }
    }
}