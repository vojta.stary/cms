<?php

namespace App\AdminModule\ObsahModule\ClankyModule\Models;

use App\Managers\ClankyManager;
use Nette\Utils\DateTime;
use Nette\Utils\Strings;

class ClanekModel
{
    private $clankyManager;
    private $fotogalerieModel;

    public function __construct(ClankyManager       $clankyManager,
                                FotogalerieModel    $fotogalerieModel)
    {
        $this->clankyManager = $clankyManager;
        $this->fotogalerieModel = $fotogalerieModel;
    }

    /**
     * vytváří nový článek
     * @param $values
     * @return int
     */
    public function create($values): int
    {
        $newId = $this->clankyManager->create($values);

        $url = Strings::webalize($newId.'-'.$values->nazev);

        $this->clankyManager->update($newId, [ClankyManager::columnUrl => $url]);

        return $newId;
    }

    /**
     * uložení dat článku
     * @param $values
     * @return void
     */
    public function update($values)
    {
        $url = Strings::webalize($values->id.'-'.$values->nazev);

        $values->url = $url;

        $this->clankyManager->update($values->id, $values);
    }

    /**
     * vymazání článku
     * @param int $clanekId
     * @return void
     */
    public function delete(int $clanekId)
    {
        $clanek = $this->clankyManager->getOne($clanekId);

        $clanek->update([ClankyManager::columnBaseImgId => null]);

        $fotky = $clanek->related('clanky_fotky','clanky_id')->fetchAll();

        if (!empty($fotky)){
            foreach ($fotky as $one){
                $this->fotogalerieModel->deleteImage($one->id);
            }
            $this->fotogalerieModel->deleteFolder($clanek->id);
        }

        $clanek->delete();
    }

    /**
     * publikování článku
     * @param int $clanekId
     * @return void
     */
    public function publikovat(int $clanekId)
    {
        $datum = new DateTime();

        $clanek = $this->clankyManager->getOne($clanekId);
        $clanek->update([ClankyManager::columnPublicAt => $datum,
            ClankyManager::columnActive => true,
            ClankyManager::columnPublikovany => true]);
    }
}