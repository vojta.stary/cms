<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\ObsahModule\ClankyModule\Components\DashClanky\DashClankyFactory;
use App\AdminModule\ObsahModule\StrankyModule\Components\DashStranky\DashStrankyFactory;

final class DashboardPresenter extends BaseAdminPresenter
{
    private $dashClankyFactory;
    private $dashStrankyFactory;

    public function __construct(DashClankyFactory   $dashClankyFactory,
                                DashStrankyFactory  $dashStrankyFactory)
    {
        $this->dashClankyFactory = $dashClankyFactory;
        $this->dashStrankyFactory = $dashStrankyFactory;
    }

    /*================================== komponenty ==================================*/
    /**
     * seznam posledně aktualizovaných článků
     * @return \App\AdminModule\ObsahModule\ClankyModule\Components\DashClanky\DashClankyControl
     */
    protected function createComponentDashClanky()
    {
        return $this->dashClankyFactory->create();
    }

    /**
     * seznam posledně aktualizovaných stránek
     * @return \App\AdminModule\ObsahModule\StrankyModule\Components\DashStranky\DashStrankyControl
     */
    protected function createComponentDashStranky()
    {
        return $this->dashStrankyFactory->create();
    }
}