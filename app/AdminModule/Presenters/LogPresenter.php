<?php

namespace App\AdminModule\Presenters;

use Nette;
use Nette\Application\UI\Form;
use App\AdminModule\Models\AdminAuthenticator;
use App\Managers\BaseInfoManager;
use Nette\Application\UI\Presenter;

final class LogPresenter extends Presenter
{
    private $authenticator;
    private $baseInfoManager;
    /** @persistent */
    public $backlink = '';

    public function __construct(AdminAuthenticator $authenticator,
                                BaseInfoManager  $baseInfoManager)
    {
        $this->authenticator = $authenticator;
        $this->baseInfoManager = $baseInfoManager;
    }

    public function checkRequirements($element): void
    {
        $this->getUser()->getStorage()->setNamespace('cms');
        parent::checkRequirements($element);
    }

    public function startup()
    {
        parent::startup();
        $this->template->baseInfo = $this->baseInfoManager->getBaseInfo();
    }

    public function beforeRender()
    {
        parent::beforeRender();

        if($this->getUser()->isLoggedIn())
        {
            $this->redirect(':Admin:Dashboard:default');
        }
    }

    /**
     * logout user
     */
    public function actionOut(){
        $this->user->logout(true);
        $this->flashMessage('Odhlášení proběhlo úspěšně!','success');
        $this->redirect(':Admin:Log:in');
    }

    /**
     * sign in form
     * @return Form
     */
    protected function createComponentLogInForm(): Form
    {
        $form = new Form();
        $form->addProtection();
        $form->addEmail('email', 'Uživatelské jméno:')
            ->setRequired('Musíte vypnit uživatelské jméno!');
        $form->addPassword('password','Heslo:')
            ->setRequired('Musíte vyplnit heslo!');
        $form->addSubmit('send','Přihlásit');
        $form->onSuccess[] = [$this, 'signIn'];
        return $form;
    }

    public function signIn(Form $form, $values){
        try {
            $user = $this->getUser();
            $user->getStorage()->setNamespace('cms');
            $user->setAuthenticator($this->authenticator);
            $user->login($values->email, $values->password);
            $user->setExpiration('120 minutes');
            $this->flashMessage('Přihlášení proběhlo úspěšně.','success');

            $this->restoreRequest($this->backlink);
            $this->redirect(':Admin:Dashboard:default');

        } catch (Nette\Security\AuthenticationException $e) {
            $this->flashMessage('Nesprávné přihlašovací údaje!','danger');
        }
    }
}