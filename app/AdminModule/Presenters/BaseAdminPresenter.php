<?php

namespace App\AdminModule\Presenters;

use Nette;
use App\Managers\BaseInfoManager;
use Nette\Application\UI\Presenter;

class BaseAdminPresenter extends Presenter
{

    public $userData;
    public $allowRole = ['admin','editor'];
    public $baseInfo;
    private $baseInfoManager;


    public function injectBaseInfo(BaseInfoManager $baseInfoManager): void
    {
        $this->baseInfoManager = $baseInfoManager;
    }

    public function checkRequirements($element): void
    {
        $this->getUser()->getStorage()->setNamespace('cms');
        parent::checkRequirements($element);
    }

    public function startup(){
        parent::startup();

        $this->baseInfo = $this->baseInfoManager->getBaseInfo();

        $user = $this->getUser();

        if($user->isLoggedIn()){
            $userData = $user->getIdentity ();
            $this->userData = $userData;

            $this->template->userData = $userData;
        }

        if(!$user->isLoggedIn()){

            $this->redirect(':Admin:Log:in',['backlink' => $this->storeRequest()]);
        }

    }

    public function beforeRender()
    {
        parent::beforeRender();

        $this->template->baseInfo = $this->baseInfo;

        if(empty($this->userData->role))
        {
            $this->user->logout(true);
            $this->flashMessage('Uživatel není přiřazen.','warning');
            $this->flashMessage('Proběhlo automatické odhlášení.','warning');
            $this->redirect(':Admin:Log:in');
        }

        $checkUser = $this->checkUser($this->userData->role);

        if ($checkUser == false){
            $this->flashMessage('Nemáte oprávnění přístupu.','warning');
            $this->redirect(':Admin:Dashboard:default');
        }

    }

    private function checkUser($role)
    {
        $check = false;

        if (in_array($role, $this->allowRole)){
                $check = true;
            }

        return $check;
    }
}