<?php

namespace App\AdminModule\Presenters;

use Nette\Application\UI\Form;
use App\AdminModule\Models\ForgotPassModel;
use App\Managers\BaseInfoManager;
use Nette\Application\UI\Presenter;
use Nette\Mail\FallbackMailerException;

final class ForgotPassPresenter extends Presenter
{
    private $forgotModel;
    private $userId;
    private $baseInfoManager;

    public function __construct(ForgotPassModel $forgotModel,
                                BaseInfoManager  $baseInfoManager)
    {
        $this->forgotModel = $forgotModel;
        $this->baseInfoManager = $baseInfoManager;
    }

    public function startup()
    {
        parent::startup();
        $this->template->baseInfo = $this->baseInfoManager->getBaseInfo();
    }

    public function actionNewPassword(string $token = null)
    {
        $verify = $this->forgotModel->verifyToken($token);

        if ($verify == false) {
            $this->redirect('VerifyFalse:default');
        }else{
            $this->userId = $verify->id;
        }
    }

    protected function createComponentChangePassword(): Form
    {
        $form = new Form;
        $form->addProtection();
        $form->addPassword("password")
            ->setRequired('Musíte vyplnit nové heslo!')
            ->addRule($form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaků.', 8)
            ->addCondition($form::MIN_LENGTH, 8)
            ->addRule($form::PATTERN, 'Heslo musí obsahovat číslici', '.*[0-9].*');
        $form->addPassword("passVerify")
            ->setRequired('Musíte vyplnit nové heslo znovu pro kontrolu!')
            ->addRule($form::EQUAL, 'Hesla se neshodují!', $form['password'])
            ->setOmitted();
        $form->addHidden('id');
        $form->addSubmit('send', 'Odeslat');
        $form->onSuccess[] = [$this, 'sendNewPass'];
        return $form;
    }

    public function sendNewPass(Form $form, $values)
    {
        try {
            $this->forgotModel->saveNewPassword($this->userId,$values->password);
            $this->flashMessage('Heslo bylo úspěšně změněno. Můžete se přihlásit.','success');
            $this->redirect('Log:in');
        }catch (\PDOException $e){
            $this->flashMessage('Chyba zápisu dat do databáze','danger');
            $this->redirect('this');
        }
    }

    protected function createComponentForgotPassForm(): Form
    {
        $form = new Form;
        $form->addProtection();
        $form->addEmail('email')
            ->setRequired('Musíte vyplnit emailovou adresu!');
        $form->addSubmit('send','Odeslat');
        $form->onSuccess[] = [$this,'sendForgotPassForm'];
        return $form;
    }

    public function sendForgotPassForm(Form $form,$values)
    {
        try {
            $send = $this->forgotModel->generateForgotPass($values->email);
            if($send == true){
                $this->flashMessage('Na Vámi zadaný email byl odeslán odkaz pro vytvoření nového hesla.','success');
                $this->redirect('this');
            }else{
                $this->flashMessage('Nebyla zadána emailová adresa přiřazená k uživateli.','danger');
                $this->redirect('this');
            }
        }catch (FallbackMailerException $e){
            $this->flashMessage('Chyba při odesílání emailu','danger');
            $this->flashMessage('Email se nepodařilo odeslat','warning');
            $this->redirect('this');
        }
    }
}