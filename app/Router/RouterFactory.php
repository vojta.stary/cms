<?php

declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;


final class RouterFactory
{
	use Nette\StaticClass;

	public static function createRouter(): RouteList
	{
		$router = new RouteList();
        /*========================================= administrace =========================================*/
        $router[] = $back = new RouteList('Admin');

            /*========================================= nastavení =========================================*/
            $back[] = $nastaveni = new RouteList('Nastaveni');

                $nastaveni[] = $zakladInfo = new RouteList('ZakladniInformace');
                    $zakladInfo->addRoute('admin/nastaveni/zaklad-info/<presenter>/<action>[/<id>]');
                $nastaveni[] = $komunikace = new RouteList('Komunikace');
                    $komunikace->addRoute('admin/nastaveni/komunikace/<presenter>/<action>[/<id>]');
                $nastaveni[] = $reklama = new RouteList('Reklama');
                    $reklama->addRoute('admin/nastaveni/reklama/<presenter>/<action>[/<id>]');
                $nastaveni[] = $uzivatele = new RouteList('Uzivatele');
                    $uzivatele->addRoute('admin/nastaveni/uzivatele/<presenter>/<action>[/<id>]');
                $nastaveni->addRoute('admin/nastaveni/<presenter>/<action>[/<id>]');

            /*========================================= obsah =========================================*/
            $back[] = $obsah = new RouteList('Obsah');

                $obsah[] = $stranky = new RouteList('Stranky');
                    $stranky->addRoute('admin/obsah/stranky/<presenter>/<action>[/<id>]');
                $obsah[] = $clanky = new RouteList('Clanky');
                    $clanky->addRoute('admin/obsah/clanky/<presenter>/<action>[/<id>]');
                $obsah[] = $menu = new RouteList('Menu');
                    $menu->addRoute('admin/obsah/menu/<presenter>/<action>[/<id>]');
                $obsah[] = $kontakty = new RouteList('Kontakty');
                    $kontakty->addRoute('admin/obsah/kontakty/<presenter>/<action>[/<id>]');
                $obsah[] = $dotazy = new RouteList('Dotazy');
                    $dotazy->addRoute('admin/obsah/caste-dotazy/<presenter>/<action>[/<id>]');
                $obsah[] = $kalendar = new RouteList('Kalendar');
                    $kalendar->addRoute('admin/obsah/terminovy-kalendar/<presenter>/<action>[/<id>]');
                $obsah[] = $sponzori = new RouteList('Sponzori');
                    $sponzori->addRoute('admin/obsah/sponzori/<presenter>/<action>[/<id>]');

                $obsah->addRoute('admin/obsah/<presenter>/<action>[/<id>]');

            /*========================================= katalog =========================================*/
            $back[] = $katalog = new RouteList('Katalog');

                $katalog[] = $katKategorie = new RouteList('Kategorie');
                    $katKategorie->addRoute('admin/katalog/kategorie/<presenter>/<action>[/<id>]');
                $katalog[] = $produkty = new RouteList('Produkty');
                    $produkty->addRoute('admin/katalog/produkty/<presenter>/<action>[/<id>]');
                $katalog[] = $katNastaveni = new RouteList('Nastaveni');
                    $katNastaveni->addRoute('admin/katalog/nastaveni/<presenter>/<action>[/<id>]');

                $katalog->addRoute('admin/katalog/<presenter>/<action>[/<id>]');

            /*========================================= restaurace =========================================*/
            $back[] = $rest = new RouteList('Restaurace');
                $rest[] = $restMenu = new RouteList('Menu');
                    $restMenu->addRoute('admin/restaurace/menu/<presenter>/<action>[/<id>]');
                $rest->addRoute('admin/restaurace/<presenter>/<action>[/<id>]');

            $back->addRoute('admin/<presenter>/<action>[/<id>]', 'Dashboard:default');

        /*========================================= front =========================================*/
        $router[] = $front = new RouteList('Front');

            $front[] = $stranky = new RouteList('Stranky');
                $stranky->addRoute('stranka/<url>','Stranka:default');
            $front[] = $clanky = new RouteList('Clanky');
                $clanky->addRoute('clanek/<url>','Detail:default');

            $front[] = $frontKatalog = new RouteList('Katalog');
                $frontKatalog[] = $frontKategorie = new RouteList('Kategorie');
                    $frontKategorie->addRoute('katalog/kategorie/<url>', 'Detail:default');
                $frontKatalog[] = $frontProdukt = new RouteList('Produkty');
                    $frontProdukt->addRoute('katalog/produkt/<url>', 'Detail:default');
                $frontKatalog->addRoute('katalog/<presenter>/<action>[/<id>]', 'Uvod:default');

            $front->addRoute('sitemap.xml', 'Sitemap:default');
            $front->addRoute('<presenter>/<text>', 'Hledej:default');
            $front->addRoute('<presenter>/<action>[/<id>]', 'Homepage:default');

		return $router;
	}
}
