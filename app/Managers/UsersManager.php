<?php

namespace App\Managers;

use Nette\Database\Explorer;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Table\Selection;
use Nette\Security\Passwords;

final class UsersManager extends DatabaseManager
{
    const table = 'users',
            columnId = 'id',
            columnNemovitostId = 'nemovitost_id',
            columnProstorId = 'prostor_id',
            columnEmail = 'email',
            columnPassword = 'password',
            columnName = 'name',
            columnRole = 'role',
            columnActive = 'active',
            columnPassToken = 'passToken';

    private $passwords;


    public function __construct(Explorer    $database,
                                Passwords   $passwords)
    {
        parent::__construct($database);
        $this->passwords = $passwords;
    }

    /**
     * getter for login user
     * @param string $email
     * @return \Nette\Database\Table\ActiveRow|null
     */
    public function getLoginUser(string $email)
    {
        return $this->database
            ->table(self::table)
            ->where(self::columnEmail,$email)
            ->where(self::columnActive, true)
            ->fetch();
    }

    /**
     * @param int $id
     * @return ActiveRow|null
     */
    public function getOne(int $id)
    {
        return $this->database
            ->table(self::table)
            ->get($id);
    }

    /**
     * @return Selection
     */
    public function select(): \Nette\Database\Table\Selection
    {
        return $this->database
            ->table(self::table)
            ->where('NOT ('.self::columnRole.' ?)', 'spravce');
    }


    /**
     * create new user
     * @param $values
     * @return int
     */
    public function create($values): int
    {
        $values->password = $this->passwords->hash($values->password);

        $values->active = true;

        $row = $this->database
            ->table(self::table)
            ->insert($values);

        return $row->getPrimary();
    }

    /**
     * edit data of user
     * @param $values
     */
    public function update($id, $values): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->update($values);
    }

    /**
     * change activation of user
     * @param $userId
     */
    public function changeActive($userId): void
    {
        $user = $this->getOne($userId);

        if ($user->active == false){
            $user->update([self::columnActive => true]);
        }else{
            $user->update([self::columnActive => false]);
        }
    }

    /**
     * change user password
     * @param int $userId
     * @param string $password
     */
    public function changePassword(int $userId, string $password): void
    {
        $password = $this->passwords->hash($password);

        $user = $this->getOne($userId);
        $user->update([self::columnPassword => $password]);
    }

    /**
     * delete user data
     * @param int $userId
     */
    public function delete(int $userId): void
    {
        $user = $this->getOne($userId);
        $user->update([self::columnActive => false, self::columnEmail => null, self::columnPassword => '']);

    }

    /**
     * @param string $email
     * @return int
     */
    public function checkEmail(string $email): int
    {
        return $this->database
            ->table(self::table)
            ->where(self::columnEmail,$email)
            ->count();
    }
}
