<?php

namespace App\Managers;

use Nette\Database\Table\Selection;

final class StrankyGalerieManager extends DatabaseManager
{
    const table = 'stranky_galerie',
        columnId = 'id',
        columnStrankyId = 'stranky_id',
        columnNazev = 'nazev',
        columnUrl = 'url';

    public function create($values): void
    {
        $this->database
            ->table(self::table)
            ->insert($values);
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function getAll(): Selection
    {
        return $this->database
            ->table(self::table);
    }

    /**
     * @param int $id
     * @return \Nette\Database\Table\ActiveRow|null
     */
    public function getOne(int $id)
    {
        return $this->database
            ->table(self::table)
            ->where(self::columnId,$id)
            ->fetch();
    }

    /**
     * @param int $id
     * @param $values
     */
    public function update(int $id, $values): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->update($values);
    }

    /**
     * @param int $id
     */
    public function delete(int $id): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->delete();
    }
}