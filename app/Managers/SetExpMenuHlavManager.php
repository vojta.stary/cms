<?php

namespace App\Managers;

use Nette\Database\Table\Selection;

final class SetExpMenuHlavManager extends DatabaseManager
{
    const table = 'set_exp_menu_hlav',
        columnId = 'id',
        columnRestMenuId = 'rest_menu_id',
        columnMarginHeader = 'margin_header',
        columnColor = 'color',
        columnBgColor = 'bg_color',
        columnTitleSize = 'title_size',
        columnMarginTitleLeft = 'margin_title_left',
        columnMarginTitleRight = 'margin_title_right',
        columnMarginTitleTop = 'margin_title_top',
        columnMarginTitleBottom = 'margin_title_bottom',
        columnLogo = 'logo',
        columnLogoAlign = 'logo_align',
        columnMaxLogoSize = 'max_logo_size',
        columnMarginLogoLeft = 'margin_logo_left',
        columnMarginLogoRight = 'margin_logo_right',
        columnMarginLogoTop = 'margin_logo_top',
        columnMarginLogoBottom = 'margin_logo_bottom';

    /**
     * vytváří nový záznam
     * @param $values
     * @return int
     */
    public function create($values): int
    {
        $row = $this->database
            ->table(self::table)
            ->insert($values);

        return $row->getPrimary();
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function select(): Selection
    {
        return $this->database
            ->table(self::table);
    }

    /**
     * @param int $id
     * @return \Nette\Database\Table\ActiveRow|null
     */
    public function getOne(int $id)
    {
        return $this->database
            ->table(self::table)
            ->where(self::columnId,$id)
            ->fetch();
    }

    /**
     * @param int $id
     * @param $values
     */
    public function update(int $id, $values): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->update($values);
    }

    /**
     * @param int $id
     */
    public function delete(int $id): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->delete();
    }
}