<?php

namespace App\Managers;

use Nette\Database\Table\Selection;

final class ProduktyManager extends DatabaseManager
{
    const table = 'produkty',
        columnId = 'id',
        columnTyp = 'typ',
        columnNazev = 'nazev',
        columnKod = 'kod',
        columnEan = 'ean',
        columnUrl = 'url',
        columnVyrobciId = 'vyrobci_id',
        columnPopisShort = 'popis_short',
        columnPopisLong = 'popis_long',
        columnBaseImage = 'base_image',
        columnCena = 'cena',
        columnCenaDph = 'cena_dph',
        columnMetaDesc = 'meta_desc',
        columnActive = 'active',
        columnLastUpdate = 'last_update',
        columnCreatedAt = 'created_at';

    /**
     * @param $values
     * @return int
     */
    public function create($values): int
    {
        $row = $this->database
            ->table(self::table)
            ->insert($values);

        return $row->getPrimary();
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function select(): Selection
    {
        return $this->database
            ->table(self::table);
    }

    /**
     * @param int $id
     * @return \Nette\Database\Table\ActiveRow|null
     */
    public function getOne(int $id)
    {
        return $this->database
            ->table(self::table)
            ->where(self::columnId,$id)
            ->fetch();
    }

    /**
     * @param int $id
     * @param $values
     */
    public function update(int $id, $values): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->update($values);
    }

    /**
     * @param int $id
     */
    public function delete(int $id): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->delete();
    }

    /**
     * změna aktivace
     * @param $id
     */
    public function changeActive($id): void
    {
        $data = $this->getOne($id);

        if ($data->active == false){
            $data->update([self::columnActive => true]);
        }else{
            $data->update([self::columnActive => false]);
        }
    }
}