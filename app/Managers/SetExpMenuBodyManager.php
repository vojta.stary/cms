<?php

namespace App\Managers;

use Nette\Database\Table\Selection;

final class SetExpMenuBodyManager extends DatabaseManager
{
    const table = 'set_exp_menu_body',
        columnId = 'id',
        columnRestMenuId = 'rest_menu_id',
        columnMarginSkupLeft = 'margin_skup_left',
        columnMarginSkupRight = 'margin_skup_right',
        columnMarginSkupTop = 'margin_skup_top',
        columnMarginSkupBottom = 'margin_skup_bottom',
        columnHeaderFontSize = 'header_font_size',
        columnFontSize = 'font_size',
        columnMarginRowBottom = 'margin_row_bottom';

    /**
     * vytváří nový záznam
     * @param $values
     * @return int
     */
    public function create($values): int
    {
        $row = $this->database
            ->table(self::table)
            ->insert($values);

        return $row->getPrimary();
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function select(): Selection
    {
        return $this->database
            ->table(self::table);
    }

    /**
     * @param int $id
     * @return \Nette\Database\Table\ActiveRow|null
     */
    public function getOne(int $id)
    {
        return $this->database
            ->table(self::table)
            ->where(self::columnId,$id)
            ->fetch();
    }

    /**
     * @param int $id
     * @param $values
     */
    public function update(int $id, $values): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->update($values);
    }

    /**
     * @param int $id
     */
    public function delete(int $id): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->delete();
    }
}