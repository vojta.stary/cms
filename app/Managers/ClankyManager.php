<?php

namespace App\Managers;

use Nette\Database\Table\Selection;

final class ClankyManager extends DatabaseManager
{
    const table = 'clanky',
        columnId = 'id',
        columnStrankyId = 'stranky_id',
        columnNazev = 'nazev',
        columnUrl = 'url',
        columnContent = 'content',
        columnBaseImgType = 'base_img_type',
        columnBaseImgIcon = 'base_img_icon',
        columnBaseImgId = 'base_img_id',
        columnMetaDesc = 'meta_desc',
        columnActive = 'active',
        columnCreatedAt = 'created_at',
        columnLastUpdate = 'last_update',
        columnPublikovany = 'publikovany',
        columnPublicAt = 'public_at';


    /**
     * vytváří nový záznam
     * @param $values
     * @return int
     */
    public function create($values): int
    {
        $row = $this->database
            ->table(self::table)
            ->insert($values);

        return $row->getPrimary();
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function select(): Selection
    {
        return $this->database
            ->table(self::table);
    }

    /**
     * @param int $id
     * @return \Nette\Database\Table\ActiveRow|null
     */
    public function getOne(int $id)
    {
        return $this->database
            ->table(self::table)
            ->where(self::columnId,$id)
            ->fetch();
    }

    /**
     * @param int $id
     * @param $values
     */
    public function update(int $id, $values): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->update($values);
    }

    /**
     * @param int $id
     */
    public function delete(int $id): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->delete();
    }

    /**
     * change activation of user
     * @param $id
     */
    public function changeActive($id): void
    {
        $data = $this->getOne($id);

        if ($data->active == false){
            $data->update([self::columnActive => true]);
        }else{
            $data->update([self::columnActive => false]);
        }
    }
}