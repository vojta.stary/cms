<?php

namespace App\Managers;

use Nette\Database\Table\Selection;

final class KategorieManager extends DatabaseManager
{
    const table = 'kategorie',
        columnId = 'id',
        columnNazev = 'nazev',
        columnUrl = 'url',
        columnParentId = 'parent_id',
        columnContent = 'content',
        columnMetaDesc = 'meta_desc',
        columnSort = 'sort',
        columnActive = 'active',
        columnLastUpdate = 'last_update';

    /**
     * @param $values
     * @return int
     */
    public function create($values): int
    {
        $row = $this->database
            ->table(self::table)
            ->insert($values);

        return $row->getPrimary();
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function select(): Selection
    {
        return $this->database
            ->table(self::table);
    }

    /**
     * @param int $id
     * @return \Nette\Database\Table\ActiveRow|null
     */
    public function getOne(int $id)
    {
        return $this->database
            ->table(self::table)
            ->where(self::columnId,$id)
            ->fetch();
    }

    /**
     * @param int $id
     * @param $values
     */
    public function update(int $id, $values): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->update($values);
    }

    /**
     * @param int $id
     */
    public function delete(int $id): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->delete();
    }

    /**
     * změna aktivity
     * @param $id
     */
    public function changeActive($id): void
    {
        $data = $this->getOne($id);

        if ($data->active == false){
            $data->update([self::columnActive => true]);
        }else{
            $data->update([self::columnActive => false]);
        }
    }
}