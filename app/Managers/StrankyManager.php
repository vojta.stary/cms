<?php

namespace App\Managers;

use Nette\Database\Table\Selection;

final class StrankyManager extends DatabaseManager
{
    const table = 'stranky',
        columnId = 'id',
        columnTyp = 'typ',
        columnNazev = 'nazev',
        columnUrl = 'url',
        columnContent = 'content',
        columnMetaDesc = 'meta_desc',
        columnActive = 'active',
        columnCreatedAt = 'created_at',
        columnLastUpdate = 'last_update';


    /**
     * vytváří nový záznam
     * @param $values
     * @return int
     */
    public function create($values): int
    {
        $row = $this->database
            ->table(self::table)
            ->insert($values);

        return $row->getPrimary();
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function select(): Selection
    {
        return $this->database
            ->table(self::table);
    }

    /**
     * @param int $id
     * @return \Nette\Database\Table\ActiveRow|null
     */
    public function getOne(int $id)
    {
        return $this->database
            ->table(self::table)
            ->where(self::columnId,$id)
            ->fetch();
    }

    /**
     * @param int $id
     * @param $values
     */
    public function update(int $id, $values): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->update($values);
    }

    /**
     * @param int $id
     */
    public function delete(int $id): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->delete();
    }

    /**
     * change activation of user
     * @param $id
     */
    public function changeActive($id): void
    {
        $stranka = $this->getOne($id);

        if ($stranka->active == false){
            $stranka->update([self::columnActive => true]);
        }else{
            $stranka->update([self::columnActive => false]);
        }
    }
}