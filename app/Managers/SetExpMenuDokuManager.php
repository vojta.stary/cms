<?php

namespace App\Managers;

use Nette\Database\Table\Selection;

final class SetExpMenuDokuManager extends DatabaseManager
{
    const table = 'set_exp_menu_doku',
        columnId = 'id',
        columnRestMenuId = 'rest_menu_id',
        columnMarginLeft = 'margin_left',
        columnMarginRight = 'margin_right',
        columnMarginTop = 'margin_top',
        columnMarginBottom = 'margin_bottom',
        columnMarginHeader = 'margin_header',
        columnMarginFooter = 'margin_footer',
        columnOrientation = 'orientation',
        columnColor = 'color',
        columnBgColor = 'bg_color',
        columnBgImage = 'bg_image',
        columnBgImageSize = 'bg_image_size',
        columnColumnns = 'columns';

    /**
     * vytváří nový záznam
     * @param $values
     * @return int
     */
    public function create($values): int
    {
        $row = $this->database
            ->table(self::table)
            ->insert($values);

        return $row->getPrimary();
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function select(): Selection
    {
        return $this->database
            ->table(self::table);
    }

    /**
     * @param int $id
     * @return \Nette\Database\Table\ActiveRow|null
     */
    public function getOne(int $id)
    {
        return $this->database
            ->table(self::table)
            ->where(self::columnId,$id)
            ->fetch();
    }

    /**
     * @param int $id
     * @param $values
     */
    public function update(int $id, $values): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->update($values);
    }

    /**
     * @param int $id
     */
    public function delete(int $id): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->delete();
    }
}