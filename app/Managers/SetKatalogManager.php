<?php

namespace App\Managers;

final class SetKatalogManager extends DatabaseManager
{
    const table = 'set_katalog',
        columnName = 'name',
        columnValue = 'value',
        setNames = ['activation','zobrazCena','emptyImage'];

    /**
     * @param string $setName
     * @return mixed|\Nette\Database\Table\ActiveRow
     */
    public function getOneSet(string $setName)
    {
        $data = $this->database
            ->table(self::table)
            ->where(self::columnName, $setName)
            ->fetch();

        return $data->value;
    }

    public function getAllSet($setArray = null): array
    {
        if ($setArray){
            $setNames = $setArray;
        }else{
            $setNames = self::setNames;
        }

        $outData = [];

        foreach ($setNames as $name){
            $info = $this->database
                ->table(self::table)
                ->where(self::columnName, $name)
                ->fetch();
            $outData[$info->name] = $info->value;
        }

        return $outData;
    }

    public function updateInfo(string $setName, $value)
    {
        $this->database
            ->table(self::table)
            ->where(self::columnName, $setName)
            ->update([self::columnValue => $value]);
    }
}