<?php

namespace App\Managers;

use Nette\Database\Table\Selection;

final class ProduktyVlastnostiManager extends DatabaseManager
{
    const table = 'produkty_vlastnosti',
        columnId = 'id',
        columnProduktyId = 'produkty_id',
        columnVlastnostiId = 'vlastnosti_id',
        columnVlastnostiHodnotyId = 'vlastnosti_hodnoty_id';


    /**
     * @param $values
     * @return int
     */
    public function create($values): int
    {
        $row = $this->database
            ->table(self::table)
            ->insert($values);

        return $row->getPrimary();
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function select(): Selection
    {
        return $this->database
            ->table(self::table);
    }

    /**
     * @param int $id
     * @return \Nette\Database\Table\ActiveRow|null
     */
    public function getOne(int $id)
    {
        return $this->database
            ->table(self::table)
            ->where(self::columnId,$id)
            ->fetch();
    }

    /**
     * @param int $id
     * @param $values
     */
    public function update(int $id, $values): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->update($values);
    }

    /**
     * @param int $id
     */
    public function delete(int $id): void
    {
        $this->database
            ->table(self::table)
            ->where(self::columnId, $id)
            ->delete();
    }
}