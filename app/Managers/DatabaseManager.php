<?php

namespace App\Managers;

use Nette\Database\Explorer;
class DatabaseManager
{
    public $database;


    public function __construct(Explorer $database)
    {
        $this->database = $database;
    }

}