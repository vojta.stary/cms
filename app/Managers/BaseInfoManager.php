<?php

namespace App\Managers;

final class BaseInfoManager extends DatabaseManager
{
    const table = 'baseinfo',
        columnName = 'name',
        columnValue = 'value',
        infoNames = ['address','logo','company','compAddr','compCity','compPost','compIc','compDic','compBoss','phone','email','mapa','siteName','meta_desc','version'];


    /**
     * @param string $infoName
     * @return \Nette\Database\Table\ActiveRow|null
     */
    public function getOneBaseInfo(string $infoName)
    {
        return $this->database
            ->table(self::table)
            ->where(self::columnName, $infoName)
            ->fetch();
    }

    public function getBaseInfo($infoArray = null): array
    {
        if ($infoArray){
            $infoNames = $infoArray;
        }else{
            $infoNames = self::infoNames;
        }

        $outData = [];

        foreach ($infoNames as $name){
            $info = $this->database
                ->table(self::table)
                ->where(self::columnName, $name)
                ->fetch();
            $outData[$info->name] = $info->value;
        }

        return $outData;
    }

    public function updateInfo(string $infoName, $value)
    {
        $this->database
            ->table(self::table)
            ->where(self::columnName, $infoName)
            ->update([self::columnValue => $value]);
    }
}