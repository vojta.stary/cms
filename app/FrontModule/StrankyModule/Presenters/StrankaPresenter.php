<?php

namespace App\FrontModule\StrankyModule\Presenters;

use App\FrontModule\ClankyModule\Components\StrankaClanky\StrankaClankyFactory;
use App\FrontModule\Presenters\BasePresenter;
use App\Managers\StrankyManager;

final class StrankaPresenter extends BasePresenter
{
    private $strankyManager;
    private $strankaClankyFactory;
    private $url;
    private $strankaId;

    public function __construct(StrankyManager          $strankyManager,
                                StrankaClankyFactory    $strankaClankyFactory)
    {
        $this->strankyManager = $strankyManager;
        $this->strankaClankyFactory = $strankaClankyFactory;
    }

    public function actionDefault(string $url)
    {
        $stranka =  $this->strankyManager->select()->where(StrankyManager::columnUrl, $url)->fetch();

        if(!$stranka || $stranka->active == false){
            $this->error('','404');
        }else{
            $this->strankaId = $stranka->id;
            $this->template->stranka = $stranka;
        }
    }

    /*=================================== komponenty ===================================*/
    /**
     * seznam článků stránky
     * @return \App\FrontModule\ClankyModule\Components\StrankaClanky\StrankaClankyControl
     */
    protected function createComponentStrankaClanky()
    {
        $this->strankaClankyFactory->setStranka($this->strankaId);
        return $this->strankaClankyFactory->create();
    }
}