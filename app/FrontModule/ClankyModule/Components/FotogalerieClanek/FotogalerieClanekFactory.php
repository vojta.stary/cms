<?php

namespace App\FrontModule\ClankyModule\Components\FotogalerieClanek;

use App\Managers\ClankyFotkyManager;
use App\Managers\ClankyManager;

class FotogalerieClanekFactory
{
    private $clankyManager;
    private $clankyFotkyManager;
    private $clanekId;

    public function __construct(  ClankyManager       $clankyManager,
                                ClankyFotkyManager  $clankyFotkyManager)
    {
        $this->clankyManager = $clankyManager;
        $this->clankyFotkyManager = $clankyFotkyManager;
    }

    public function create()
    {
        return new FotogalerieClanekControl($this->clankyManager,
                                            $this->clankyFotkyManager,
                                            $this->clanekId);
    }

    public function setClanek(int $clanekId)
    {
        $this->clanekId = $clanekId;
    }
}