<?php

namespace App\FrontModule\ClankyModule\Components\FotogalerieClanek;

use App\Managers\ClankyFotkyManager;
use App\Managers\ClankyManager;
use Nette\Application\UI\Control;

class FotogalerieClanekControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $clankyManager;
    private $clankyFotkyManager;
    private $clanekId;

    public function __construct(  ClankyManager       $clankyManager,
                                ClankyFotkyManager  $clankyFotkyManager,
                                                    $clanekId)
    {
        $this->clankyManager = $clankyManager;
        $this->clankyFotkyManager = $clankyFotkyManager;
        $this->clanekId = $clanekId;
    }

    public function render()
    {
        $this->template->clanek = $this->clankyManager->getOne($this->clanekId);
        $this->template->fotky = $this->getFotky();
        $this->template->render(self::template);
    }

    /*=============================== pomovné funkce ===============================*/

    public function getFotky()
    {
        $database = $this->clankyFotkyManager->select();

        $fotky = $database->where(ClankyFotkyManager::columnClankyId, $this->clanekId)
            ->fetchAll();

        return $fotky;
    }
}