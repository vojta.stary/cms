<?php

namespace App\FrontModule\ClankyModule\Components\StrankaClanky;

use App\Managers\ClankyManager;
use Nette\Application\UI\Control;

class StrankaClankyControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $clankyManager;
    private $strankaId;
    private $limit = 5;

    public function __construct(ClankyManager   $clankyManager,
                                                $strankaId)
    {
        $this->clankyManager = $clankyManager;
        $this->strankaId = $strankaId;
    }

    public function render()
    {
        $this->template->pocetClanku = $this->getPocetClanku();
        $this->template->limit = $this->limit;
        $this->template->clanky = $this->getClanky();
        $this->template->render(self::template);
    }

    /*====================================== handle metody ======================================*/

    public function handleNacistClanky(int $limit = null)
    {
        if ($this->presenter->isAjax()) {
            $this->limit = $limit;
            $this->redrawControl('clanky');
            $this->redrawControl('clankyScript');
        }
    }

    /*====================================== pomocné funkce ======================================*/
    private function getClanky()
    {
        $database = $this->clankyManager->select();
        $database->where(ClankyManager::columnPublikovany, true)
            ->where(ClankyManager::columnActive, true)
            ->where(ClankyManager::columnStrankyId, $this->strankaId);

        $data = $database->order(ClankyManager::columnPublicAt . ' DESC')
            ->limit($this->limit, '0')
            ->fetchAll();

        return $data;
    }

    private function getPocetClanku(): int
    {
        $database = $this->clankyManager->select();
        $data = $database->where(ClankyManager::columnPublikovany, true)
            ->where(ClankyManager::columnActive, true)
            ->where(ClankyManager::columnStrankyId, $this->strankaId)
            ->count();

        return $data;
    }
}