<?php

namespace App\FrontModule\ClankyModule\Components\StrankaClanky;

use App\Managers\ClankyManager;

class StrankaClankyFactory
{
    public function __construct(ClankyManager   $clankyManager)
    {
        $this->clankyManager = $clankyManager;
    }

    public function create()
    {
        return new StrankaClankyControl($this->clankyManager,
                                        $this->strankaId);
    }

    public function setStranka(int $strankaId)
    {
        $this->strankaId = $strankaId;
    }
}