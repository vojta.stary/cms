<?php

namespace App\FrontModule\ClankyModule\Components\DashClanky;

use App\Managers\ClankyManager;

class DashClankyFactory
{
    private $clankyManager;

    public function __construct(ClankyManager   $clankyManager)
    {
        $this->clankyManager = $clankyManager;
    }

    public function create()
    {
        return new DashClankyControl($this->clankyManager);
    }
}