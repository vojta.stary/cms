<?php

namespace App\FrontModule\ClankyModule\Components\DashClanky;

use App\Managers\ClankyManager;
use Nette\Application\UI\Control;

class DashClankyControl extends Control
{
    const template = __DIR__ . '/templates/template.latte';

    private $clankyManager;
    private $limit = 5;

    public function __construct(ClankyManager $clankyManager)
    {
        $this->clankyManager = $clankyManager;
    }

    public function render()
    {
        $this->template->pocetClanku = $this->getPocetClanku();
        $this->template->limit = $this->limit;
        $this->template->clanky = $this->getClanky();
        $this->template->render(self::template);
    }

    /*====================================== handle metody ======================================*/

    public function handleNacistClanky(int $limit = null)
    {
        if ($this->presenter->isAjax()) {
            $this->limit = $limit;
            $this->redrawControl('clanky');
            $this->redrawControl('clankyScript');
        }
    }

    /*====================================== pomocné funkce ======================================*/
    private function getClanky()
    {
        $database = $this->clankyManager->select();
        $database->where(ClankyManager::columnPublikovany, true)
            ->where(ClankyManager::columnActive, true);

        $data = $database->order(ClankyManager::columnPublicAt . ' DESC')
            ->limit($this->limit, '0')
            ->fetchAll();

        return $data;
    }

    private function getPocetClanku(): int
    {
        $database = $this->clankyManager->select();
        $data = $database->where(ClankyManager::columnPublikovany, true)
            ->where(ClankyManager::columnActive, true)
            ->count();

        return $data;
    }
}