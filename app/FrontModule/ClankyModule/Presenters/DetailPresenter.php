<?php

namespace App\FrontModule\ClankyModule\Presenters;

use App\FrontModule\ClankyModule\Components\FotogalerieClanek\FotogalerieClanekFactory;
use App\FrontModule\Presenters\BasePresenter;
use App\Managers\ClankyManager;

final class DetailPresenter extends BasePresenter
{
    private $clankyManager;
    private $fotogalerieClanekFactory;
    private $clanekId;


    public function __construct(ClankyManager               $clankyManager,
                                FotogalerieClanekFactory    $fotogalerieClanekFactory)
    {
        $this->clankyManager = $clankyManager;
        $this->fotogalerieClanekFactory = $fotogalerieClanekFactory;
    }

    /*=============================== action metoda ===============================*/
    public function actionDefault(string $url)
    {
        $clanek =  $this->clankyManager->select()->where(ClankyManager::columnUrl, $url)->fetch();

        if(!$clanek || $clanek->active == false){
            $this->error('','404');
        }else{
            $this->clanekId = $clanek->id;
            $this->template->clanek = $clanek;
        }
    }

    /*=============================== komponenty ===============================*/
    /**
     * fotogalerie
     * @return \App\FrontModule\ClankyModule\Components\FotogalerieClanek\FotogalerieClanekControl
     */
    protected function createComponentFotogalerie()
    {
        $this->fotogalerieClanekFactory->setClanek($this->clanekId);
        return $this->fotogalerieClanekFactory->create();
    }
}