<?php

namespace App\FrontModule\Models;

use App\Managers\CookieManager;
use Nette\Http\Request;
use Nette\Http\Response;
use Nette\Utils\ArrayHash;
use Nette\Utils\Random;
use Nette\Utils\Strings;

final class CookieModel
{
    private $request;
    private $response;
    private $cookieManager;

    public function __construct(Request $request,
                                Response $response,
                                CookieManager $cookieManager)
    {
        $this->request = $request;
        $this->response = $response;
        $this->cookieManager = $cookieManager;
    }

    /**
     * ukládá nastavení cookie
     * @param string $type
     * @return void
     */
    public function saveSetting(string $type)
    {
        $key = $this->request->getCookie('_csid');

        if ($type == 'all') {
            $setting = '11';

            $databaseData = $this->cookieManager->select()->where(CookieManager::columnCookId, $key)->fetch();

            if ($databaseData != null) {
                $databaseData->update([CookieManager::columnSetting => $setting]);
                return;
            }
            $token = $this->generateRow($setting);
            $this->response->setCookie('_csid',$token, '730 days');

        }else{
            $setting = '10';

            $databaseData = $this->cookieManager->select()->where(CookieManager::columnCookId, $key)->fetch();

            if ($databaseData != null) {
                $databaseData->update([CookieManager::columnSetting => $setting]);
                return;
            }
            $token = $this->generateRow($setting);
            $this->response->setCookie('_csid',$token, '730 days');
        }
    }

    /**
     * @return mixed|\Nette\Database\Table\ActiveRow|null
     */
    public function getSetting()
    {
        $key = $this->request->getCookie('_csid');

        $setting = null;

        if($key) {
            $databaseData = $this->cookieManager->select()->where(CookieManager::columnCookId, $key)->fetch();
            $setting = ArrayHash::from(['basic' => Strings::substring($databaseData->setting,0,1),
                'static' => Strings::substring($databaseData->setting,1,1)]);
        }

        return $setting;
    }


    /**
     * generuje nové nastavení cookie
     * @param $setting
     * @return string
     */
    private function generateRow($setting): string
    {
        try {
            $token = Random::generate(15);
            $url = $this->request->getUrl()->getHost();
            $ipAddress = $this->request->getRemoteAddress();


            $databaseData = [CookieManager::columnCookId => $token,
                CookieManager::columnUrl => $url,
                CookieManager::columnIp => $ipAddress,
                CookieManager::columnSetting => $setting];

            $this->cookieManager->create($databaseData);
        }catch (\PDOException $e){
            if ((int)$e->errorInfo[1] === 1062) {
                $this->generateRow($setting);
            }
        }

        return $token;
    }
}