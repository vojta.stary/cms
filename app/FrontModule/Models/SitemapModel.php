<?php

namespace App\FrontModule\Models;

use App\Managers\ClankyManager;
use App\Managers\StrankyManager;

final class SitemapModel
{
    private $strankyManager;
    private $clankyManager;

    public function __construct(StrankyManager  $strankyManager,
                                ClankyManager   $clankyManager)
    {
        $this->strankyManager = $strankyManager;
        $this->clankyManager = $clankyManager;

    }

    public function getAllStranky()
    {
        $database = $this->strankyManager->select();

        $stranky = $database->where(StrankyManager::columnActive, true)
            ->fetchAll();

        return $stranky;
    }

    public function getAllClanky()
    {
        $database = $this->clankyManager->select();

        $clanky = $database->where(ClankyManager::columnPublikovany, true)
            ->where(ClankyManager::columnActive, true)
            ->fetchAll();

        return $clanky;
    }
}