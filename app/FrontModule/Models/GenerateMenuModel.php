<?php

namespace App\FrontModule\Models;

use App\Managers\MenuManager;

final class GenerateMenuModel
{
    private $menuManager;

    public function __construct(MenuManager $menuManager)
    {
        $this->menuManager = $menuManager;
    }

    public function getBase()
    {
        $database = $this->menuManager->select();

        $data = $database->where(MenuManager::columnParentId, null)
            ->order(MenuManager::columnSort.' ASC')
            ->fetchAll();

        return $data;
    }

    public function getSub(int $parentId)
    {
        $database = $this->menuManager->select();

        $data = $database->where(MenuManager::columnParentId, $parentId)
            ->order(MenuManager::columnSort.' ASC')
            ->fetchAll();

        return $data;
    }
}