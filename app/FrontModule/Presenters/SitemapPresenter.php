<?php

namespace App\FrontModule\Presenters;

use App\FrontModule\Models\SitemapModel;
use App\Managers\BaseInfoManager;
use Nette\Application\UI\Presenter;
use Nette\Http\IResponse;


final class SitemapPresenter extends Presenter
{
    /** @var IResponse @inject */
    public $response;
    private $sitemapModel;
    private $baseInfoManager;

    public function __construct(SitemapModel $sitemapModel, BaseInfoManager $baseInfoManager)
    {
        $this->sitemapModel = $sitemapModel;
        $this->baseInfoManager = $baseInfoManager;
    }

    public function renderDefault()
    {
        $this->response->setContentType('text/xml');

        $this->template->baseInfo = $this->baseInfoManager->getBaseInfo();
        $this->template->stranky = $this->sitemapModel->getAllStranky();
        $this->template->clanky = $this->sitemapModel->getAllClanky();
    }
}