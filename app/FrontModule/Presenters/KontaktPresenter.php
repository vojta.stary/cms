<?php

namespace App\FrontModule\Presenters;

use App\Managers\KontaktOsobyManager;
use App\Managers\ProvozovnyManager;
use App\Models\MailerModel;
use Nette\Application\UI\Form;
use Nette\Mail\FallbackMailerException;
use Nette\Utils\ArrayHash;

final class KontaktPresenter extends BasePresenter
{
    private $kontaktOsobyManager;
    private $provozovnyManager;
    private $mailerModel;

    public function __construct(KontaktOsobyManager $kontaktOsobyManager,
                                ProvozovnyManager   $provozovnyManager,
                                MailerModel         $mailerModel)
    {
        $this->kontaktOsobyManager = $kontaktOsobyManager;
        $this->provozovnyManager = $provozovnyManager;
        $this->mailerModel = $mailerModel;
    }

    public function renderDefault()
    {
        $osoby = $this->kontaktOsobyManager->select()->order(KontaktOsobyManager::columnSort.' ASC')->fetchAll();
        $mista = $this->provozovnyManager->select()->fetchAll();

        $this->template->mista = $mista;
        $this->template->osoby = $osoby;
    }

    /*============================== formulář ==============================*/
    protected function createComponentKontaktForm(): Form
    {
        $form = new Form;
        $form->addProtection();
        $form->addEmail('odesilatel','Vaše emailová adresa')
            ->setRequired('Musíte vyplnit %label');
        $form->addText('jmeno','Jméno')
            ->setRequired('Musíte vyplnit %label');
        $form->addText('predmet','Předmět zprávy')
            ->setRequired('Musíte vyplnit %label');
        $form->addTextArea('zprava','Obsah zprávy')
            ->setRequired('Musíte vyplnit %label');
        $form->addSubmit('send','Odeslat');
        $form->onSuccess[] = [$this, 'sendKontaktForm'];
        return $form;
    }

    public function sendKontaktForm(Form $form, $values)
    {
        try {
            $this->mailerModel->sendMail($values->odesilatel,'messageFront.latte',$values);
            $this->mailerModel->sendMail($this->baseInfo['email'],'messageBack.latte',$values);
            $this->flashMessage('Děkujeme Vám. Vaše zpráva byla odeslána.','success');
            $this->redirect('this');
        }catch(FallbackMailerException $e){
            $form->setDefaults($values);
            $form->addError('Chyba při odesílání. Formulář se nepodařilo odeslat.');
        }
    }
}