<?php

declare(strict_types=1);

namespace App\FrontModule\Presenters;

use App\FrontModule\Components\AsideMenu\AsideMenuFactory;
use App\FrontModule\Components\Cookie\CookieFactory;
use App\FrontModule\Components\SearchBar\SearchBarFactory;
use App\FrontModule\Components\Sponzori\SponzoriFactory;
use App\FrontModule\Components\TerminovyKalendar\TerminovyKalendarFactory;
use App\FrontModule\Models\CookieModel;
use App\Managers\BaseInfoManager;
use App\Managers\SetKatalogManager;
use Nette;
use Nette\Application\UI\Presenter;

class BasePresenter extends Presenter
{
    private $baseInfoManager;
    private $setKatalogManager;
    private $cookieModel;
    private $asideMenuFactory;
    private $cookieFactory;
    private $searchBarFactory;
    private $sponzoriFactory;
    private $terminovyKalendarFactory;
    public $baseInfo;
    public $setKatalog;
    public $setCookie;

    public function injectBaseInfo(BaseInfoManager $baseInfoManager): void
    {
        $this->baseInfoManager = $baseInfoManager;
    }

    public function injectSetKatalog(SetKatalogManager $setKatalogManager): void
    {
        $this->setKatalogManager = $setKatalogManager;
    }

    public function injectCookieModel(CookieModel $cookieModel): void
    {
        $this->cookieModel = $cookieModel;
    }

    public function injectAsideMenu(AsideMenuFactory $asideMenuFactory): void
    {
        $this->asideMenuFactory = $asideMenuFactory;
    }

    public function injectCookieFactory(CookieFactory $cookieFactory): void
    {
        $this->cookieFactory = $cookieFactory;
    }

    public function injectSearchBar(SearchBarFactory $searchBarFactory): void
    {
        $this->searchBarFactory = $searchBarFactory;
    }

    public function injectTerminovyKalendar(TerminovyKalendarFactory $terminovyKalendarFactory): void
    {
        $this->terminovyKalendarFactory = $terminovyKalendarFactory;
    }

    public function injectSponzori(SponzoriFactory $sponzoriFactory): void
    {
        $this->sponzoriFactory = $sponzoriFactory;
    }

    /*============================ nastavení aplikace ============================*/

    public function startup(){
        parent::startup();

        $this->baseInfo = $this->baseInfoManager->getBaseInfo();
        $this->setKatalog = $this->setKatalogManager->getAllSet();
        $this->setCookie = $this->cookieModel->getSetting();

    }

    public function beforeRender()
    {
        parent::beforeRender();

        $this->template->baseInfo = $this->baseInfo;
        $this->template->setKatalog = $this->setKatalog;
        $this->template->setCookie = $this->setCookie;
    }

    /*============================ komponenty ============================*/
    /**
     * cookie lista
     * @return mixed
     */
    protected function createComponentCookie()
    {
        return $this->cookieFactory->create();
    }

    /**
     * aside menu
     * @return mixed
     */
    protected function createComponentAsideMenu()
    {
        return $this->asideMenuFactory->create();
    }

    /**
     * search bar
     * @return mixed
     */
    protected function createComponentSearchBar()
    {
        $this->searchBarFactory->setBaseInfo($this->baseInfo);
        return $this->searchBarFactory->create();
    }

    /**
     * termínový kalendář
     * @return mixed
     */
    protected function createComponentTerminKalendar()
    {
        return $this->terminovyKalendarFactory->create();
    }

    /**
     * sponzoři
     * @return mixed
     */
    protected function createComponentSponzori()
    {
        return $this->sponzoriFactory->create();
    }
}