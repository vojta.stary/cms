<?php

namespace App\FrontModule\Presenters;

use App\Managers\ClankyManager;
use App\Managers\StrankyManager;

final class HledejPresenter extends BasePresenter
{
    private $strankyManager;
    private $clankyManager;
    private $text;

    public function __construct(StrankyManager  $strankyManager,
                                ClankyManager   $clankyManager)
    {
        $this->strankyManager = $strankyManager;
        $this->clankyManager = $clankyManager;
    }

    public function actionDefault(string $text = null)
    {
        $this->text = $text;
        $this->template->stranky = $this->getStranky();
        $this->template->fulltextStranky = $this->getStrankyFulltext();
        $this->template->fulltextClanky = $this->getClankyFulltext();
        $this->template->clanky = $this->getClanky();
        $this->template->vyraz = $text;
    }

    /*===================================== pomocné funkce =====================================*/
    public function getClanky()
    {
        $clanky = $this->clankyManager->select()
            ->where(ClankyManager::columnActive, true)
            ->where(ClankyManager::columnPublikovany, true)
            ->where('nazev LIKE ?','%'.$this->text.'%')
            ->fetchAll();

        return $clanky;
    }

    public function getStranky()
    {
        $stranky = $this->strankyManager->select()
            ->where(StrankyManager::columnActive, true)
            ->where('nazev LIKE ?','%'.$this->text.'%')
            ->fetchAll();

        return $stranky;
    }

    public function getStrankyFulltext()
    {
        $stranky = $this->strankyManager->select()
            ->where(StrankyManager::columnActive, true)
            ->where('MATCH (content) AGAINST (?)','%'.$this->text.'%')
            ->fetchAll();

        return $stranky;
    }

    public function getClankyFulltext()
    {
        $clanky = $this->clankyManager->select()
            ->where(ClankyManager::columnActive, true)
            ->where(ClankyManager::columnPublikovany, true)
            ->whereOr([
                'MATCH (content) AGAINST (?)' => '%'.$this->text.'%',
                'MATCH (uvod) AGAINST (?)' => '%'.$this->text.'%',
            ])
            ->fetchAll();

        return $clanky;
    }
}