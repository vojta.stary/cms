<?php

declare(strict_types=1);

namespace App\FrontModule\Presenters;

use App\FrontModule\ClankyModule\Components\DashClanky\DashClankyFactory;
use App\FrontModule\Components\CasteDotazy\CasteDotazyFactory;

final class HomepagePresenter extends BasePresenter
{
    private $dashClankyFactory;
    private $casteDotazyFactory;

    public function __construct(DashClankyFactory   $dashClankyFactory,
                                CasteDotazyFactory  $casteDotazyFactory)
    {
        $this->dashClankyFactory = $dashClankyFactory;
        $this->casteDotazyFactory = $casteDotazyFactory;
    }

    /*============================== komponenty ==============================*/
    /**
     * seznam článků
     * @return \App\FrontModule\ClankyModule\Components\DashClanky\DashClankyControl
     */
    protected function createComponentDashClanky()
    {
        return $this->dashClankyFactory->create();
    }

    /**
     * časté dotazy
     * @return \App\FrontModule\Components\CasteDotazy\CasteDotazyControl
     */
    protected function createComponentCasteDotazy()
    {
        return $this->casteDotazyFactory->create();
    }
}
