<?php

namespace App\FrontModule\Components\TerminovyKalendar;

use App\Managers\TerminKalendarManager;

class TerminovyKalendarFactory
{
    private $terminKalendarManager;

    public function __construct(TerminKalendarManager $terminKalendarManager)
    {
        $this->terminKalendarManager = $terminKalendarManager;
    }

    public function create()
    {
        return new TerminovyKalendarControl($this->terminKalendarManager);
    }
}