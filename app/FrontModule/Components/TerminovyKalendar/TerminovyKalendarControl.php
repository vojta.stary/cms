<?php

namespace App\FrontModule\Components\TerminovyKalendar;

use App\Managers\TerminKalendarManager;
use Nette\Application\UI\Control;
use Nette\Utils\DateTime;

class TerminovyKalendarControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $terminKalendarManager;

    public function __construct(TerminKalendarManager $terminKalendarManager)
    {
        $this->terminKalendarManager = $terminKalendarManager;
    }

    public function render()
    {
        $datum = new DateTime('now');

        $data = $this->terminKalendarManager->select()->order(TerminKalendarManager::columnDatum.' DESC')->fetchAll();

        $this->template->datum = $datum;
        $this->template->data = $data;
        $this->template->render(self::template);
    }
}