<?php

namespace App\FrontModule\Components\Cookie;

use App\FrontModule\Models\CookieModel;

class CookieFactory
{
    const template = __DIR__.'/templates/template.latte';

    private $cookieModel;

    public function __construct(CookieModel $cookieModel)
    {
        $this->cookieModel = $cookieModel;
    }

    public function create()
    {
        return new CookieControl(   $this->cookieModel);
    }
}