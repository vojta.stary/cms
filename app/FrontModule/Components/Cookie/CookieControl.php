<?php

namespace App\FrontModule\Components\Cookie;

use App\FrontModule\Models\CookieModel;

class CookieControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/templates/template.latte';

    private $cookieModel;
    private $setting;

    public function __construct(CookieModel $cookieModel)
    {
        $this->cookieModel = $cookieModel;
    }

    public function render()
    {
        $this->setting = $this->cookieModel->getSetting();

        $this->template->setting = $this->setting;
        $this->template->render(self::template);
    }

    /*===================================== handle metody =====================================*/

    public function handleCookieSet(string $type)
    {
        $this->cookieModel->saveSetting($type);
        $this->redirect('this');
    }
}