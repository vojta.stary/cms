<?php

namespace App\FrontModule\Components\Sponzori;

use App\Managers\SponzoriManager;

class SponzoriFactory
{
    private $sponzoriManager;

    public function __construct(SponzoriManager $sponzoriManager)
    {
        $this->sponzoriManager = $sponzoriManager;
    }

    public function create()
    {
        return new SponzoriControl($this->sponzoriManager);
    }
}