<?php

namespace App\FrontModule\Components\Sponzori;

use App\Managers\SponzoriManager;
use Nette\Application\UI\Control;

class SponzoriControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $sponzoriManager;

    public function __construct(SponzoriManager $sponzoriManager)
    {
        $this->sponzoriManager = $sponzoriManager;
    }

    public function render()
    {
        $data = $this->sponzoriManager->select()->fetchAll();

        $this->template->data = $data;
        $this->template->render(self::template);
    }
}