<?php

namespace App\FrontModule\Components\SearchBar;

use App\Managers\ClankyManager;
use App\Managers\StrankyManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class SearchBarControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $clankyManager;
    private $strankyManager;
    private $baseInfo;

    public function __construct(StrankyManager  $strankyManager,
                                ClankyManager   $clankyManager,
                                                $baseInfo)
    {
        $this->strankyManager = $strankyManager;
        $this->clankyManager = $clankyManager;
        $this->baseInfo = $baseInfo;
    }

    public function render()
    {
        $this->template->adresa = $this->baseInfo['address'];
        $this->template->render(self::template);
    }

    /*===================================== handle metody =====================================*/
    public function handleHledej(string $text = null)
    {
        if ($this->presenter->isAjax()){
            $this->presenter->payload->vysledek = $this->getVysledky($text);
            $this->presenter->sendPayload();
        }
    }

    /*===================================== pomocné funkce =====================================*/

    protected function createComponentSearchForm(): Form
    {
        $form = new Form;
        $form->addText('text','Vyhledávaný výraz')
            ->setRequired('Musíte zadat text!');
        $form->addSubmit('send','Hledat');
        $form->onSuccess[] = [$this, 'sendSearchForm'];
        return $form;
    }

    public function sendSearchForm(Form $form, $values)
    {
        $this->presenter->redirect(':Front:Hledej:default',$values->text);
    }

    /*===================================== pomocné funkce =====================================*/
    public function getVysledky(string $text)
    {
        $clanky = $this->clankyManager->select()
            ->where(ClankyManager::columnActive, true)
            ->where(ClankyManager::columnPublikovany, true)
            ->where('nazev LIKE ?','%'.$text.'%')
            ->fetchAll();

        $stranky = $this->strankyManager->select()
            ->where(StrankyManager::columnActive, true)
            ->where('nazev LIKE ?','%'.$text.'%')
            ->fetchAll();

        $data = [];

        if(!empty($clanky)){
            foreach ($clanky as $one)
            {
                $data['clanky'][] = ['id' => $one->id, 'url' => $one->url, 'nazev' => $one->nazev];
            }
        }else{
            $data['clanky'] = [];
        }

        if(!empty($stranky)){
            foreach ($stranky as $one)
            {
                $data['stranky'][] = ['id' => $one->id, 'url' => $one->url, 'nazev' => $one->nazev];
            }
        }else{
            $data['stranky'] = [];
        }

        return $data;
    }
}