<?php

namespace App\FrontModule\Components\SearchBar;

use App\Managers\ClankyManager;
use App\Managers\StrankyManager;

class SearchBarFactory
{
    private $clankyManager;
    private $strankyManager;
    private $baseInfo;

    public function __construct(StrankyManager  $strankyManager,
                                ClankyManager   $clankyManager)
    {
        $this->strankyManager = $strankyManager;
        $this->clankyManager = $clankyManager;
    }

    public function create()
    {
        return new SearchBarControl($this->strankyManager,
                                    $this->clankyManager,
                                    $this->baseInfo);
    }

    public function setBaseInfo($baseInfo)
    {
        $this->baseInfo = $baseInfo;
    }
}