<?php

namespace App\FrontModule\Components\CasteDotazy;

use App\Managers\DotazyManager;

class CasteDotazyFactory
{
    private $dotazyManager;

    public function __construct(DotazyManager $dotazyManager)
    {
        $this->dotazyManager = $dotazyManager;
    }

    public function create()
    {
        return new CasteDotazyControl($this->dotazyManager);
    }
}