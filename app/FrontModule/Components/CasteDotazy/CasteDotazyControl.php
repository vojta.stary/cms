<?php

namespace App\FrontModule\Components\CasteDotazy;

use App\Managers\DotazyManager;
use Nette\Application\UI\Control;

class CasteDotazyControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $dotazyManager;

    public function __construct(DotazyManager $dotazyManager)
    {
        $this->dotazyManager = $dotazyManager;
    }

    public function render()
    {
        $dotazy = $this->dotazyManager->select()
            ->order(DotazyManager::columnSort.' ASC')
            ->fetchAll();

        $this->template->dotazy = $dotazy;
        $this->template->render(self::template);
    }
}