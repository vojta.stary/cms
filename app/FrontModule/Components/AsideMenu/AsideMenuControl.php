<?php

namespace App\FrontModule\Components\AsideMenu;

use App\FrontModule\Models\GenerateMenuModel;
use Nette\Application\UI\Control;

class AsideMenuControl extends Control
{
    const template = __DIR__.'/templates/template.latte';

    private $generateMenuModel;

    public function __construct(GenerateMenuModel $generateMenuModel)
    {
        $this->generateMenuModel = $generateMenuModel;
    }

    public function render()
    {
        $this->template->generator = $this->generateMenuModel;
        $this->template->baseItems = $this->generateMenuModel->getBase();
        $this->template->render(self::template);
    }
}