<?php

namespace App\FrontModule\Components\AsideMenu;

use App\FrontModule\Models\GenerateMenuModel;

class AsideMenuFactory
{
    private $generateMenuModel;

    public function __construct(GenerateMenuModel $generateMenuModel)
    {
        $this->generateMenuModel = $generateMenuModel;
    }

    public function create()
    {
        return new AsideMenuControl($this->generateMenuModel);
    }
}