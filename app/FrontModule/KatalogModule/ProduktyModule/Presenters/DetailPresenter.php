<?php

namespace App\FrontModule\KatalogModule\ProduktyModule\Presenters;

use App\FrontModule\KatalogModule\KategorieModule\Models\KatBreadcrumbsModel;
use App\FrontModule\KatalogModule\Presenters\BaseKatalogPresenter;
use App\Managers\ProduktyManager;

final class DetailPresenter extends BaseKatalogPresenter
{
    private $produktyManager;
    private $katBreadcrumbsModel;
    private $produktId;
    private $fromKat = null;

    public function __construct(ProduktyManager         $produktyManager,
                                KatBreadcrumbsModel     $katBreadcrumbsModel)
    {
        $this->produktyManager = $produktyManager;
        $this->katBreadcrumbsModel = $katBreadcrumbsModel;
    }

    public function actionDefault(string $url, int $katId = null)
    {
        $produkt = $this->produktyManager->select()
            ->where(ProduktyManager::columnUrl, $url)
            ->fetch();

        if (!$produkt){
            $this->error('','404');
        }

        $this->produktId = $produkt->id;
        $this->fromKat = $katId;
    }

    public function renderDefault()
    {
        $produkt = $this->produktyManager->getOne($this->produktId);

        $this->template->breadCrumbs = $this->katBreadcrumbsModel->generujProduktBreadcrumbs($this->fromKat, $produkt->id);
        $this->template->produkt = $produkt;
    }
}