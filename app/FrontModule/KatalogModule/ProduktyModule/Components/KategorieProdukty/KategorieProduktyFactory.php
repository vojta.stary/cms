<?php

namespace App\FrontModule\KatalogModule\ProduktyModule\Components\KategorieProdukty;

use App\FrontModule\KatalogModule\KategorieModule\Models\KatVlastnostiModel;
use App\Managers\ProduktyManager;

class KategorieProduktyFactory
{
    private $produktyManager;
    private $katVlastnostiModel;
    private $kategorieId = false;

    public function __construct(ProduktyManager     $produktyManager,
                                KatVlastnostiModel  $katVlastnostiModel)
    {
        $this->produktyManager = $produktyManager;
        $this->katVlastnostiModel = $katVlastnostiModel;
    }

    public function create()
    {
        return new KategorieProduktyControl($this->produktyManager,
                                            $this->katVlastnostiModel,
                                            $this->kategorieId);
    }

    public function setKategorie(int $id)
    {
        $this->kategorieId = $id;
    }
}