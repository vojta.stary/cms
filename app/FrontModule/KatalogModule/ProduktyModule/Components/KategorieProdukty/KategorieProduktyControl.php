<?php

namespace App\FrontModule\KatalogModule\ProduktyModule\Components\KategorieProdukty;

use App\FrontModule\KatalogModule\KategorieModule\Models\KatVlastnostiModel;
use App\Managers\ProduktyKategorieManager;
use App\Managers\ProduktyManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\Paginator;

class KategorieProduktyControl extends Control
{
    const template = __DIR__.'/templates/template.latte';
    const pageLimit = 12;

    private $produktyManager;
    private $katVlastnostiModel;
    private $kategorieId;
    private $paginator;
    private $radius = 5;
    private $pageSetting = ['kategorieId' => null,
                            'page' => 1,
                            'filter' => [],
                            'sort' => 'ASC',
                            'sortBy' => 'cena'];


    public function __construct(ProduktyManager     $produktyManager,
                                KatVlastnostiModel  $katVlastnostiModel,
                                                    $kategorieId)
    {
        $this->produktyManager = $produktyManager;
        $this->katVlastnostiModel = $katVlastnostiModel;
        $this->kategorieId = $kategorieId;
        $this->paginator = new Paginator();
    }

    public function render()
    {
        //nastavení session
        $session = $this->presenter->getSession('katalog_nastaveni');

        if ($session->get('pageSetting') == null){
            $session->set('pageSetting',$this->pageSetting,'60 minutes');
        }
        if ($session->get('pageSetting')['kategorieId'] != $this->kategorieId) {
            $pageSetting = $session->get('pageSetting');
            $pageSetting['kategorieId'] = $this->kategorieId;
            $pageSetting['filter'] = [];
            $pageSetting['page'] = 1;
            $session->set('pageSetting',$pageSetting,'60 minutes');
        }

        //načtení nastavení stránky ze session
        $pageSetting = $session->get('pageSetting');

        //načtení produktů
        $offset = ( $pageSetting['page'] - 1 ) * self::pageLimit;
        $produkty = $this->getProdukty($this->kategorieId , $pageSetting);
        $itemsCount = $produkty->count();

        // paginator
        $this->paginator->setItemsPerPage(self::pageLimit);
        $this->paginator->setItemCount($itemsCount);
        $this->paginator->setPage($pageSetting['page']);

        $left = $pageSetting['page'] - $this->radius >= 1 ? $pageSetting['page'] - $this->radius : 1;
        $right = $pageSetting['page'] + $this->radius <= $this->paginator->pageCount ? $pageSetting['page'] + $this->radius : $this->paginator->pageCount;

        $this->template->paginator = $this->paginator;
        $this->template->left = $left;
        $this->template->right = $right;

        // promené pro šablonu
        $this->template->katVlastnosti = $this->katVlastnostiModel->kategorieVlastnosti($this->kategorieId);
        $this->template->kategorieId = $this->kategorieId;
        $this->template->pageSetting = $pageSetting;
        $this->template->produkty = $produkty->limit( self::pageLimit, $offset )->fetchAll();
        $this->template->render(self::template);
    }

    /*===================================== filtrační formulář =====================================*/
    protected function createComponentFilterForm(): Form
    {
        $form = new Form;
        $form->addSubmit('send','Filtrovat');
        $form->onSuccess[] = [$this, 'sendFilterForm'];
        return $form;
    }

    public function sendFilterForm(Form $form)
    {
        //$data = $form->getHttpData();
        //dump($data);

        if ($this->presenter->isAjax()) {
            $data = $form->getHttpData();

            $session = $this->presenter->getSession('katalog_nastaveni');
            $pageSetting = $session->get('pageSetting');
            $pageSetting['page'] = 1;
            if (isset($data['vlastnosti_hodnoty'])) {
                $pageSetting['filter']['vlastnosti_hodnoty'] = $data['vlastnosti_hodnoty'];
            }else{
                unset($pageSetting['filter']['vlastnosti_hodnoty']);
            }
            $session->set('pageSetting', $pageSetting, '60 minutes');
            $this->redrawControl('seznam');
        }
    }

    /*===================================== handle metody =====================================*/
    /**
     * nastavení ajax stránkování pro paginator
     * @param int $page
     * @return void
     */
    public function handlePage(int $page): void
    {
        if ($this->presenter->isAjax()) {
            $session = $this->presenter->getSession('katalog_nastaveni');
            $pageSetting = $session->get('pageSetting');
            $pageSetting['page'] = $page;
            $session->set('pageSetting', $pageSetting, '60 minutes');
            $this->redrawControl('seznam');
        }
    }

    /**
     * nastavení řazení produktů podle
     * @param string $sortBy
     * @return void
     */
    public function handleSortBy(string $sortBy)
    {
        if ($this->presenter->isAjax()) {
            $session = $this->presenter->getSession('katalog_nastaveni');
            $pageSetting = $session->get('pageSetting');
            $pageSetting['sortBy'] = $sortBy;
            $pageSetting['sort'] = 'ASC';
            $pageSetting['page'] = 1;
            $session->set('pageSetting', $pageSetting, '60 minutes');
            $this->redrawControl('seznam');
            $this->redrawControl('filter');
        }
    }

    /**
     * nastavení řazení produktů sestup/vzestup
     * @param string $sort
     * @param array $pageSetting
     * @return void
     */
    public function handleSort(string $sort)
    {
        if ($this->presenter->isAjax()) {
            $session = $this->presenter->getSession('katalog_nastaveni');
            $pageSetting = $session->get('pageSetting');
            $pageSetting['sort'] = $sort;
            $pageSetting['page'] = 1;
            $session->set('pageSetting', $pageSetting, '60 minutes');
            $this->redrawControl('seznam');
            $this->redrawControl('filter');
        }
    }
    /*===================================== pomocné funkce =====================================*/
    private function getProdukty($kategorieId, $pageSetting)
    {
        $database = $this->produktyManager->select();

        if ($kategorieId) {
            $database->where(':'.ProduktyKategorieManager::table.'.'.ProduktyKategorieManager::columnKategorieId, $kategorieId);
        }

        if ($pageSetting['sortBy'] == 'cena') {
            $database->order(ProduktyManager::columnCenaDph.' '.$pageSetting['sort']);
        }
        if ($pageSetting['sortBy'] == 'nazev') {
            $database->order(ProduktyManager::columnNazev.' '.$pageSetting['sort']);
        }

        if (isset($pageSetting['filter']['vlastnosti_hodnoty'])) {



            foreach ($pageSetting['filter']['vlastnosti_hodnoty'] as $vlastnostiId => $values) {
                $hodnoty = array();
                foreach ($values as $row => $value) {
                    array_push($hodnoty, $value);
                }
                $database->where(':produkty_vlastnosti.vlastnosti_hodnoty_id IN (?)', $hodnoty);
            }

        }

        $database->where(ProduktyManager::columnActive, true);

        return $database;
    }
}