<?php

namespace App\FrontModule\KatalogModule\KategorieModule\Presenters;

use App\FrontModule\KatalogModule\KategorieModule\Components\KatNavigace\KatNavigaceFactory;
use App\FrontModule\KatalogModule\KategorieModule\Models\KatBreadcrumbsModel;
use App\FrontModule\KatalogModule\Presenters\BaseKatalogPresenter;
use App\FrontModule\KatalogModule\ProduktyModule\Components\KategorieProdukty\KategorieProduktyFactory;
use App\Managers\KategorieManager;

final class DetailPresenter extends BaseKatalogPresenter
{
    private $kategorieManager;
    private $katBreadcrumbsModel;
    private $katNavigaceFactory;
    private $kategorieProduktyFactory;
    private $kategorieId;

    public function __construct(KategorieManager            $kategorieManager,
                                KatBreadcrumbsModel         $katBreadcrumbsModel,
                                KatNavigaceFactory          $katNavigaceFactory,
                                KategorieProduktyFactory    $kategorieProduktyFactory)
    {
        $this->kategorieManager = $kategorieManager;
        $this->katBreadcrumbsModel = $katBreadcrumbsModel;
        $this->katNavigaceFactory = $katNavigaceFactory;
        $this->kategorieProduktyFactory = $kategorieProduktyFactory;
    }

    public function actionDefault(string $url)
    {
        $kategorie = $this->kategorieManager->select()->where(KategorieManager::columnUrl, $url)->fetch();
        if (!$kategorie || !$kategorie->active) {
            $this->error('Kategorie nebyla nalezena.','404');
        }
        $this->kategorieId = $kategorie->id;
    }

    public function renderDefault()
    {
        $kategorie = $this->kategorieManager->getOne($this->kategorieId);

        $this->template->breadCrumbs = $this->katBreadcrumbsModel->generujBreadcrumbs($kategorie->id);
        $this->template->kategorie = $kategorie;
    }

    /*===================================== komponenty =====================================*/
    protected function createComponentKatNavigace()
    {
        $this->katNavigaceFactory->setParent($this->kategorieId);
        return $this->katNavigaceFactory->create();
    }

    protected function createComponentKategorieProdukty()
    {
        $this->kategorieProduktyFactory->setKategorie($this->kategorieId);
        return $this->kategorieProduktyFactory->create();
    }
}