<?php

namespace App\FrontModule\KatalogModule\KategorieModule\Components\KatNavigace;

use App\FrontModule\KatalogModule\KategorieModule\Models\KatNavigaceModel;

class KatNavigaceFactory
{
    private $katNavigaceModel;
    private $parentId = null;

    public function __construct(KatNavigaceModel $katNavigaceModel)
    {
        $this->katNavigaceModel = $katNavigaceModel;
    }

    public function create()
    {
        return new KatNavigaceControl(  $this->katNavigaceModel,
                                        $this->parentId);
    }

    public function setParent(int $parentId)
    {
        $this->parentId = $parentId;
    }
}