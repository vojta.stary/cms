<?php

namespace App\FrontModule\KatalogModule\KategorieModule\Components\KatNavigace;

use App\FrontModule\KatalogModule\KategorieModule\Models\KatNavigaceModel;

class KatNavigaceControl extends \Nette\Application\UI\Control
{
    const template = __DIR__.'/templates/template.latte';

    private $katNavigaceModel;
    private $parentId = null;

    public function __construct(KatNavigaceModel    $katNavigaceModel,
                                                    $parentId)
    {
        $this->katNavigaceModel = $katNavigaceModel;
        $this->parentId = $parentId;
    }

    public function render()
    {
        $this->template->kategorie = $this->katNavigaceModel->getKategorie($this->parentId);
        $this->template->parent = $this->parentId;
        $this->template->render(self::template);
    }
}