<?php

namespace App\FrontModule\KatalogModule\KategorieModule\Models;

use App\Managers\ProduktyManager;
use App\Managers\ProduktyVlastnostiManager;
use App\Managers\VlastnostiManager;
use Nette\Utils\ArrayHash;

final class KatVlastnostiModel
{
    private $produktyManager;
    private $vlastnostiManager;
    private $produktyVlastnostiManager;

    public function __construct(VlastnostiManager           $vlastnostiManager,
                                ProduktyManager             $produktyManager,
                                ProduktyVlastnostiManager   $produktyVlastnostiManager)
    {
        $this->vlastnostiManager = $vlastnostiManager;
        $this->produktyManager = $produktyManager;
        $this->produktyVlastnostiManager = $produktyVlastnostiManager;
    }

    /**
     * získá vlastnosti a hodnoty produktu pro dannou kategorii
     * @param int|null $kategorieId
     * @return ArrayHash
     */
    public function kategorieVlastnosti(int $kategorieId = null)
    {
        $vystup = [];

        if ($kategorieId) {
            $produkty = $this->produktyManager->select()
                ->where(':produkty_kategorie.kategorie_id',$kategorieId)
                ->where(ProduktyManager::columnActive, true)
                ->fetchAll();
        }else {
            $produkty = $this->produktyManager->select()
                ->where(ProduktyManager::columnActive, true)
                ->fetchAll();
        }

        foreach ($produkty as $produkt) {

            $vlastnosti = $this->vlastnostiManager->select()
                ->where(':produkty_vlastnosti.produkty_id',$produkt->id)
                ->where(VlastnostiManager::columnFiltr, true)
                ->order(VlastnostiManager::columnNazev.' ASC')
                ->fetchAll();

            foreach ($vlastnosti as $vlastnost) {
                if ( !in_array($vlastnost->nazev, $vystup)) {
                    $hodnota = $this->vyberHodnotu($vlastnost->id, $produkt->id);

                    if (array_key_exists($vlastnost->nazev, $vystup)) {
                        if ( !in_array($hodnota, $vystup[$vlastnost->nazev])) {
                            $vystup[$vlastnost->nazev][] = $hodnota;
                        }
                    }else{
                        $vystup[$vlastnost->nazev][] = $hodnota;
                    }
                    $nazevSort = array_column($vystup[$vlastnost->nazev], 'nazev');

                    array_multisort($nazevSort, SORT_ASC, $vystup[$vlastnost->nazev]);
                }
            }
        }

        return ArrayHash::from($vystup);
    }

    /**
     * přiřazuje hodnoty k vlastnosti
     * @param int $vlastnostId
     * @param int $produktId
     */
    private function vyberHodnotu(int $vlastnostId, int $produktId)
    {
        $database = $this->produktyVlastnostiManager->select()
            ->where(ProduktyVlastnostiManager::columnVlastnostiId,$vlastnostId)
            ->where(ProduktyVlastnostiManager::columnProduktyId,$produktId)
            ->fetch();

        $data =  ['id' => $database->vlastnosti_hodnoty_id, 'nazev' => $database->vlastnosti_hodnoty->hodnota, 'vlastnosti_id' => $database->vlastnosti_id];

        return ArrayHash::from($data);
    }
}