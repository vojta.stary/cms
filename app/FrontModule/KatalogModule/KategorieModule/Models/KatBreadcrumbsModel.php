<?php

namespace App\FrontModule\KatalogModule\KategorieModule\Models;

use App\Managers\KategorieManager;
use App\Managers\ProduktyKategorieManager;
use App\Managers\ProduktyManager;

final class KatBreadcrumbsModel
{
    private $kategorieManager;
    private $produktyManager;
    private $produktyKategorieManager;

    public function __construct(KategorieManager            $kategorieManager,
                                ProduktyManager             $produktyManager,
                                ProduktyKategorieManager    $produktyKategorieManager)
    {
        $this->kategorieManager = $kategorieManager;
        $this->produktyManager = $produktyManager;
        $this->produktyKategorieManager = $produktyKategorieManager;
    }

    /**
     * generuje breadcrumbs navigaci pro kategorii
     * @param int $kategorieId
     * @return array
     */
    public function generujBreadcrumbs(int $kategorieId): array
    {
        $breadCrumbs = [];

        $aktKategorie = $this->kategorieManager->getOne($kategorieId);

        if($aktKategorie->parent_id != null){
            $parentKat = $this->kategorieManager->getOne($aktKategorie->parent_id);

            if( $parentKat != null){
                $breadCrumbs[] = ['nazev' => $parentKat->nazev,'id' => $parentKat->id, 'url' => $parentKat->url];

                while ($parentKat->parent_id != null){
                    $parentKat = $this->kategorieManager->getOne($parentKat->parent_id);
                    $breadCrumbs[] = ['nazev' => $parentKat->nazev,'id' => $parentKat->id, 'url' => $parentKat->url];
                }
            }
        }

        return array_reverse($breadCrumbs, true);
    }

    /**
     * generuje breadcrumbs navigaci pro produkt
     * @param int $kategorieId
     * @param int $produktId
     * @return array
     */
    public function generujProduktBreadcrumbs( $kategorieId,int $produktId): array
    {
        $breadCrumbs = [];

        if($kategorieId){
            $aktKategorie = $this->kategorieManager->getOne($kategorieId);

            $breadCrumbs[] = ['nazev' => $aktKategorie->nazev,'url' => $aktKategorie->url];

            if( $aktKategorie->parent_id != null){
                $parentKat = $this->kategorieManager->getOne($aktKategorie->parent_id);

                if( $parentKat != null){
                    $breadCrumbs[] = ['nazev' => $parentKat->nazev,'url' => $parentKat->url];

                    while ($parentKat->parent_id != null){
                        $parentKat = $this->kategorieManager->getOne($parentKat->parent_id);
                        $breadCrumbs[] = ['nazev' => $parentKat->nazev,'url' => $parentKat->url];
                    }
                }
            }
        }else{
            $produkt = $this->produktyManager->getOne($produktId);

            $kategorie = $this->produktyKategorieManager->select()
                ->where(ProduktyKategorieManager::columnProduktyId, $produkt->id)
                ->where(ProduktyKategorieManager::columnBaseKategorie, true)
                ->fetch();

            $aktKategorie = $this->kategorieManager->getOne($kategorie->kategorie_id);

            $breadCrumbs[] = ['nazev' => $aktKategorie->nazev,'url' => $aktKategorie->url];

            if( $aktKategorie->parent_id != null){
                $parentKat = $this->kategorieManager->getOne($aktKategorie->parent_id);

                if( $parentKat != null){
                    $breadCrumbs[] = ['nazev' => $parentKat->nazev,'url' => $parentKat->url];

                    while ($parentKat->parent_id != null){
                        $parentKat = $this->kategorieManager->getOne($parentKat->parent_id);
                        $breadCrumbs[] = ['nazev' => $parentKat->nazev,'url' => $parentKat->url];
                    }
                }
            }
        }

        return array_reverse($breadCrumbs, true);
    }
}