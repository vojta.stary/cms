<?php

namespace App\FrontModule\KatalogModule\KategorieModule\Models;

use App\Managers\KategorieManager;

final class KatNavigaceModel
{
    private $kategorieManager;

    public function __construct(KategorieManager $kategorieManager)
    {
        $this->kategorieManager = $kategorieManager;
    }

    /**
     * získává seznam základních kategorií
     * @return \Nette\Database\Table\ActiveRow[]
     */
    public function getKategorie($parentId = null)
    {
        $database = $this->kategorieManager->select();

        $kategorie = $database->where(KategorieManager::columnParentId, $parentId)
            ->where(KategorieManager::columnActive, true)
            ->order(KategorieManager::columnSort.' ASC')
            ->fetchAll();

        return $kategorie;
    }
}