<?php

namespace App\FrontModule\KatalogModule\Presenters;

use App\FrontModule\Presenters\BasePresenter;

class BaseKatalogPresenter extends BasePresenter
{
    public function startup()
    {
        parent::startup();

        if (!$this->setKatalog['activation']){
            $this->error('','404');
        }
    }
}