<?php

namespace App\FrontModule\KatalogModule\Presenters;

use App\FrontModule\KatalogModule\KategorieModule\Components\KatNavigace\KatNavigaceFactory;
use App\FrontModule\KatalogModule\ProduktyModule\Components\KategorieProdukty\KategorieProduktyFactory;

final class UvodPresenter extends BaseKatalogPresenter
{
    private $katNavigaceFactory;
    private $kategorieProduktyFactory;

    public function __construct(KatNavigaceFactory          $katNavigaceFactory,
                                KategorieProduktyFactory    $kategorieProduktyFactory)
    {
        $this->katNavigaceFactory = $katNavigaceFactory;
        $this->kategorieProduktyFactory = $kategorieProduktyFactory;
    }

    /*=============================== komponenty ===============================*/
    /**
     * senzma kategorií
     * @return \App\FrontModule\KatalogModule\KategorieModule\Components\KatNavigace\KatNavigaceControl
     */
    protected function createComponentKategorie()
    {
        return $this->katNavigaceFactory->create();
    }

    protected function createComponentKategorieProdukty()
    {
        return $this->kategorieProduktyFactory->create();
    }
}