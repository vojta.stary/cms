<?php

namespace App\Models;

use Latte\Engine;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use Nette\Mail\SmtpMailer;
use Nette\Mail\FallbackMailer;
use App\Managers\BaseInfoManager;

final class MailerModel
{
    const dirTemplate = __DIR__.'/../AdminModule/templates/EmailTemplates/';
    const dirAttach = __DIR__.'/../../www/src/images/logo/';

    private $baseInfoManager;

    public function __construct(BaseInfoManager $baseInfoManager)
    {
        $this->baseInfoManager = $baseInfoManager;
    }

    /**
     * send email
     * @param $sendTo
     * @param $template
     * @param $values
     */
    public function sendMail($sendTo, $template, $values = array(),$attachment = null):bool
    {
        //nastaveni emailu
        $mailInfo = ['mailAdresa','mailSmtp','mailZabez','mailPort','mailUser','mailPass'];

        $dataNast = $this->baseInfoManager->getBaseInfo($mailInfo);

        //odesílání emailu
        $baseInfo = $this->getBaseInfo();

        $mailData = ['baseInfo' => $baseInfo];

        $mailData['data'] = $values;

        $latte = new Engine;

        $mail = new Message;
        $mail->setFrom($dataNast['mailAdresa'])
            ->addTo($sendTo)
            ->setHtmlBody($latte->renderToString(dirname(__FILE__).self::dirTemplate.$template, $mailData),self::dirAttach);


        $sendMailer = new SendmailMailer();

        if ($dataNast != ''){
            $nastaveniSmtp = ['host' => $dataNast['mailSmtp'],
                'username' => $dataNast['mailUser'],
                'password' => $dataNast['mailPass'],
                'secure' => $dataNast['mailZabez'],
                'port' => $dataNast['mailPort']
            ];
        }else{
            $nastaveniSmtp = ['host' => $dataNast['mailSmtp'],
                'username' => $dataNast['mailUser'],
                'password' => $dataNast['mailPass'],
                'port' => $dataNast['mailPort']
            ];
        }

        $smtpMailer = new SmtpMailer($nastaveniSmtp);

        $mailer = new FallbackMailer([$sendMailer,$smtpMailer]);
        $mailer->send($mail);
        return true;
    }

    private function getBaseInfo()
    {
        return $this->baseInfoManager->getBaseInfo();
    }
}