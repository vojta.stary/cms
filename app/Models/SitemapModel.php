<?php

namespace App\Models;

use App\Managers\KategorieManager;
use App\Managers\ProduktyManager;
use App\Managers\StrankyManager;
use Nette\Utils\ArrayHash;

class SitemapModel
{
    private $strankyManager;

    public function __construct(StrankyManager $strankyManager)
    {
        $this->strankyManager = $strankyManager;

    }

    public function getAllStranky()
    {
        $database = $this->strankyManager->getAll();

        $stranky = $database->where(StrankyManager::columnActive, true)
            ->where('NOT ('.StrankyManager::columnActive.' ?)', false)
            ->fetchAll();

        return $stranky;
    }
}