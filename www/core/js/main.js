$(document).ready(function () {
    //====================== naja =====================
    naja.initialize();

    naja.addEventListener('start', (event) => {
        $('.spinner').addClass('show');
    });

    naja.addEventListener('complete', (event) => {
        $('.spinner').removeClass('show');
    });

    naja.addEventListener('error', (event) => {
        $('.spinner').removeClass('show');
        alert('Chyba načítání!')
    });
});
