function loadTinyObsah(id)
{
    var obsah = $('#tiny-obsah-'+id).val();

    tinymce.init({
        selector: '#editTiny-'+id,
        language: 'cs',
        statusbar: false,
        plugins: 'code lists link autoresize image imagetools wordcount table',
        max_height: 600,
        menubar: false,
        entity_encoding : "raw",
        file_picker_types: 'file image media',
        toolbar1: 'code | undo redo | styleselect | bold italic | numlist bullist | alignleft aligncenter alignright | link | image',
        toolbar2: 'table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
        image_title: true,
        style_formats: [
            { title: 'Headings', items: [
                    { title: 'Heading 3', format: 'h3' },
                    { title: 'Heading 4', format: 'h4' },
                    { title: 'Heading 5', format: 'h5' },
                    { title: 'Heading 6', format: 'h6' }
                ]},
            { title: 'Inline', items: [
                    { title: 'Bold', format: 'bold' },
                    { title: 'Italic', format: 'italic' },
                    { title: 'Underline', format: 'underline' },
                    { title: 'Strikethrough', format: 'strikethrough' },
                    { title: 'Superscript', format: 'superscript' },
                    { title: 'Subscript', format: 'subscript' },
                    { title: 'Code', format: 'code' }
                ]},
            { title: 'Blocks', items: [
                    { title: 'Paragraph', format: 'p' }
                ]},
            { title: 'Align', items: [
                    { title: 'Left', format: 'alignleft' },
                    { title: 'Center', format: 'aligncenter' },
                    { title: 'Right', format: 'alignright' },
                    { title: 'Justify', format: 'alignjustify' }
                ]}
        ],
        automatic_uploads: true,
        file_picker_types: 'image',
        /* and here's our custom image picker*/
        file_picker_callback: function (cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
            input.onchange = function () {
                var file = this.files[0];

                var reader = new FileReader();
                reader.onload = function () {
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);

                    cb(blobInfo.blobUri(), { title: file.name });
                };
                reader.readAsDataURL(file);
            };

            input.click();
        },
        setup:  function (editor) {
            editor.on('init', function (e) {
                editor.setContent(obsah);
            })
        }
    });
}