
$(document).ready(function (){
    //====================== naja =====================
    naja.initialize();

    naja.addEventListener('start', (event) => {
        $('#spinner').addClass('show');
    });

    naja.addEventListener('complete', (event) => {
        $('#spinner').removeClass('show');
        initTolltip();
    });

    naja.addEventListener('error', (event) => {
        $('#spinner').removeClass('show');
        initTolltip();
        alert('Chyba načítání!')
    });

    naja.addEventListener('abort', (event) => {
        $('#spinner').removeClass('show');
        initTolltip();
        alert('Načítání bylo přerušeno!')
    });

    //====================== datepicker =====================
    $('.datepicker').datepicker({
        todayBtn: "linked",
        clearBtn: true,
        language: "cs",
        daysOfWeekHighlighted: "0,6",
        todayHighlight: true,
        format: 'dd.mm.yyyy'
    });

    //====================== colorpicker =====================
    initColorPicker();

    //====================== nicescroll =====================
    $("aside").niceScroll({
        cursorcolor:"#087990"
    });

    $('.accordion-button').click(function(){
        setTimeout(
            function()
            {
                $('#aside').getNiceScroll().resize();
            }, 500);
    });

    //====================== aside  =====================
    $('#asideButton').click(function(){
        var visible = $('#aside').attr('data-visible');

        if(visible == 'close'){
            $('#aside').addClass('show');
            $('#aside').attr('data-visible','open');
        }
        if( visible == 'open'){
            $('#aside').removeClass('show');
            $('#aside').attr('data-visible','close');
        }
        //console.log(visible);
    });

    //====================== tooltip =====================
    initTolltip();
});

//====================== tooltip init =====================
function initTolltip()
{
    $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
}

//====================== aside close =====================
document.addEventListener("click", (evt) => {
    const asideElement = document.getElementById("aside");
    const buttonElement = document.getElementById("asideButton");
    let targetElement = evt.target; // clicked element

    do {
        if (targetElement == asideElement) {
            return;
        }
        if (targetElement == buttonElement) {

            return;
        }
        // Go up the DOM
        targetElement = targetElement.parentNode;
    } while (targetElement);

    // This is a click outside.
    $('#aside').removeClass('show');
    $('#aside').attr('data-visible','close');
});

function initColorPicker() {
    $('.color-picker').minicolors({
        format: 'rgb',
        opacity: true,
        theme: 'bootstrap'
    });
}