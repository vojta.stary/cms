var dropdown_toogle = document.querySelectorAll('.dropdown-toggle');
var dropdown = document.querySelectorAll('dropdown');
var modal_button = document.querySelectorAll('.modal-tigger');
var dismiss = document.querySelectorAll('[data-dismiss]');
var accordion_button = document.querySelectorAll('.accordion-button');
var tabs_button = document.querySelectorAll('.tabs-tigger');
var show_button = document.querySelectorAll('.show-tigger');
var loader_tigger = document.querySelectorAll('.loader-tigger');
//============================= dropdown ========================================
dropdown_toogle.forEach(function(item){
    item.addEventListener("click", function(){
        dropdown_toogle.forEach(function(item){
            item.classList.remove('show');
        });
        item.classList.add('show');
    });
});
//============================= show ========================================
loader_tigger.forEach(function (toogle){
    toogle.addEventListener('click', function (){
        var loader = document.querySelector('.spinner');
        loader.classList.add('show');
    });
});
//============================= show ========================================
show_button.forEach(function (toogle){
    toogle.addEventListener('click', function (){
        var target = this.getAttribute('data-target');
        var open = this.getAttribute('data-open');
        var icon = this.querySelector('.icon');
        var element = document.getElementById(target);

        if (open === 'close') {
            element.classList.add('show');
            this.setAttribute('data-open','open');
            icon.classList.remove('fa-chevron-down');
            icon.classList.add('fa-chevron-up');
            return;
        }
        element.classList.remove('show');
        this.setAttribute('data-open','close');
        icon.classList.add('fa-chevron-down');
        icon.classList.remove('fa-chevron-up');
    });
});
//============================= tabs ========================================
tabs_button.forEach(function(toogle){
    toogle.addEventListener("click", function (){
        var target = this.getAttribute('data-target');

        var tab = document.getElementById(target);
        var parent = tab.getAttribute('data-parent');

        var tiggers = document.getElementById(parent).querySelectorAll('.tabs-tigger');
        tiggers.forEach(function (tigger){
            tigger.classList.remove('active');
        });

        var tabs = document.getElementById(parent).querySelectorAll('.tabs');
        tabs.forEach(function (box){
            box.classList.remove('active');
        });

        var tiggersActive = document.querySelectorAll('[data-target="'+target+'"]');
        tiggersActive.forEach(function(activeBtn){
            activeBtn.classList.add('active');
        });

        //console.log(tiggersActive);

        //this.classList.add('active');
        tab.classList.add('active');

    });
});

//============================= accordion ========================================
accordion_button.forEach(function(item){
    item.addEventListener("click", function(){
        var target = this.getAttribute('data-target');
        var accordion = document.querySelector(target);
        var parentName = accordion.getAttribute('data-parent');
        var parent = document.querySelector(parentName);
        var other_accordions = parent.querySelectorAll('.accordion-collapse');
        var other_buttons = parent.querySelectorAll('.accordion-button');
        var classList = this.classList;

        if (Array.from(classList).includes('collapsed')){
            other_buttons.forEach(function (button){
                button.classList.add('collapsed');
            });
            this.classList.remove('collapsed');

            other_accordions.forEach(function (other){
                other.classList.add('collapse');
            });
            accordion.classList.remove('collapse');
        }else{
            this.classList.add('collapsed');
            other_accordions.forEach(function (other){
                other.classList.add('collapse');
            });
        }
    });
});


//============================= modal ========================================
modal_button.forEach(function(item){
    item.addEventListener("click", function (){
        var target = item.getAttribute('data-target');
        var modal = document.querySelector(target);

        //addBackdrop();
        modal.classList.add('show');
    });
});
//========== dismis attribute ==========
dismiss.forEach(function (item){
    item.addEventListener("click",function(){
        var modals = document.querySelectorAll('.modal');
        modals.forEach(function (item){
            item.classList.remove('show');
            //remBackdrop();
        });
    });
});

//============================= document click listener ========================================
document.addEventListener("click", (event) => {
    let targetElement = event.target; // clicked element


    //==================== dropdown close =============================
    var trida = $(targetElement).attr('class');
    var target = $(targetElement).attr('data-target');

    if(target !== 'dropdown'){
        if(trida !== 'dropdown'){
            dropdown_toogle.forEach(function(item){
                item.classList.remove('show');
            });
        }
    }
});


//============================= backdrop ========================================
function addBackdrop(){
    const div = document.createElement('div');
    div.classList.add('backdrop');
    document.body.insertAdjacentElement('afterbegin', div);
}

function remBackdrop(){
    const backdrop = document.querySelector('.backdrop');

    if (backdrop){
        backdrop.remove();
    }
}